<?php defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('user_image')) {
  
    function user_image($id = null) {
        $CI = get_instance();
        $CI->load->model('Model_account','account');
        $profile = $CI->account->Profile($CI->session->userdata('ID_USER'));
        if($id){
           $profile = $CI->account->Profile($id);
        }
        if($profile->photo && file_exists($profile->photo)){
        	return base_url().''.$profile->photo;
        }else{
        	return base_url().'assets/dist/img/user-blank.png';
        }
    }
}

if (!function_exists('account_name')) {

    function account_name($id = false) {
        $CI = get_instance();
        $CI->load->model('Model_account','account');
        $profile = $CI->account->Profile($CI->session->userdata('ID_USER'));
        if($id){
          return $CI->session->userdata('ID_USER');
        }else{
            if($profile->fullname){
              return $profile->fullname;
          }else{
              return $profile->email;
          }
        }
    }
}

if (!function_exists('sidebar_menu')) {

    function sidebar_menu($all = false,$selected = null) {
        $CI = get_instance();
        $CI->load->model('Model_helper','helper');
        $abc = $CI->helper->get_menu($all,$selected);
    }
}


if (!function_exists('option_setting')) {

    function option_setting($name,$selected = -1) {
       $html = '<option></option>'; 
       $object = setting($name);
       $data = json_decode($object);
       foreach($data as $row){
           if($selected !=null && $selected==$row->id){
                $html .=  "<option value='".$row->id."' selected>".$row->name."</option>";
            }else{
                $html .=  "<option value='".$row->id."'>".$row->name."</option>";
            }
       }
       return $html;
    }
}

if (!function_exists('get_option')) {

    function get_option($data,$name,$selected = -1) {
       $html = '<option></option>'; 
       foreach($data as $row){
           if($selected !=null && $selected==$row->id){
                $html .=  "<option value='".$row->id_main."' selected>".$row->$name."</option>";
            }else{
                $html .=  "<option value='".$row->id_main."'>".$row->$name."</option>";
            }
       }
       return $html;
    }
}

if (!function_exists('get_roles')) {

    function get_roles($data,$name,$selected = -1) {
       $html = '<option></option>'; 
       foreach($data as $row){
           if($selected !=null && $selected==$row->id){
                $html .=  "<option value='".$row->id."' selected>".$row->$name."</option>";
            }else{
                $html .=  "<option value='".$row->id."'>".$row->$name."</option>";
            }
       }
       return $html;
    }
}

if (!function_exists('get_lokasi')) {

    function get_lokasi($data,$name,$selected = -1) {
       $html = '<option></option>'; 
       foreach($data as $row){
           if($selected !=null && $selected==$row->idLokasi){
                $html .=  "<option value='".$row->idLokasi."' selected>".$row->$name."</option>";
            }else{
                $html .=  "<option value='".$row->idLokasi."'>".$row->$name."</option>";
            }
       }
       return $html;
    }
}

if (!function_exists('user_role')) {

    function user_role($id) {
        $CI = get_instance();
        $CI->load->model('Model_account','model');
        return $CI->model->Roles($id);
    }
}


if (!function_exists('user_status')) {

    function user_status($index) {
       if($index=='1'){
          return '<span class="label label-primary">Active</span>';
       }else{
          return '<span class="label label-danger">No Active</span>';
       }
    }
}

if (!function_exists('crud_action')) {

    function crud_action($url,$id,$edit = true,$remove = true) {
       $html = '<a href="'.base_url($url.'detail/'.$id).'" class="btn btn-xs btn-success btn-details" data-toggle="tooltip" 
     data-placement="top" title="View data"><i class="fa fa-search"></i></a>';
       if($edit){
         $html .= ' <a href="'.base_url($url.'edit/'.$id).'" class="btn btn-xs btn-info btn-edit" data-toggle="tooltip" 
     data-placement="top" title="Edit data"><i class="fa fa-edit"></i></a>';
         }
         if($remove){
           $html .= ' <a href="'.base_url($url.'delete/'.$id).'" class="delete btn btn-xs btn-danger btn-remove" data-toggle="tooltip" 
     data-placement="top" title="Delete data"><i class="fa fa-trash"></i></a>';
       }
       return $html;
    }
}

if (!function_exists('my_date_indo')) {

    function my_date_indo($val) {
        if ($val != NULL) {
            date_default_timezone_set('Asia/Jakarta');
            $bulan = date_format(date_create($val),'m');
            Switch ($bulan){
              case 1 : $bulan="Januari";
                  Break;
              case 2 : $bulan="Februari";
                  Break;
              case 3 : $bulan="Maret";
                  Break;
              case 4 : $bulan="April";
                  Break;
              case 5 : $bulan="Mei";
                  Break;
              case 6 : $bulan="Juni";
                  Break;
              case 7 : $bulan="Juli";
                  Break;
              case 8 : $bulan="Agustus";
                  Break;
              case 9 : $bulan="September";
                  Break;
              case 10 : $bulan="Oktober";
                  Break;
              case 11 : $bulan="November";
                  Break;
              case 12 : $bulan="Desember";
                  Break;
            }

            $tgl = date_format(date_create($val),'d');
            $thn = date_format(date_create($val),'Y');
            return $tgl." ".$bulan." ".$thn;
        } else {
            return NULL;
        }
    }
}

if (!function_exists('my_date_indo_bulan')) {

    function my_date_indo_bulan($val) {
        if ($val != NULL) {
            date_default_timezone_set('Asia/Jakarta');
            $bulan = $val;
            Switch ($bulan){
              case 1 : $bulan="Januari";
                  Break;
              case 2 : $bulan="Februari";
                  Break;
              case 3 : $bulan="Maret";
                  Break;
              case 4 : $bulan="April";
                  Break;
              case 5 : $bulan="Mei";
                  Break;
              case 6 : $bulan="Juni";
                  Break;
              case 7 : $bulan="Juli";
                  Break;
              case 8 : $bulan="Agustus";
                  Break;
              case 9 : $bulan="September";
                  Break;
              case 10 : $bulan="Oktober";
                  Break;
              case 11 : $bulan="November";
                  Break;
              case 12 : $bulan="Desember";
                  Break;
            }

            return $bulan;
        } else {
            return NULL;
        }
    }
}

if (!function_exists('my_date')) {

    function my_date($val) {
        if ($val != NULL) {
            date_default_timezone_set('Asia/Jakarta');
            $date = date_create($val);
            return date_format($date, 'd-m-Y');
        } else {
            return NULL;
        }
    }
}

if (!function_exists('my_date_month')) {

    function my_date_month($val) {
        if ($val != NULL) {
            date_default_timezone_set('Asia/Jakarta');
            $bulan = $val;
            Switch ($bulan){
              case 1 : $bulan="Jan";
                  Break;
              case 2 : $bulan="Feb";
                  Break;
              case 3 : $bulan="Mar";
                  Break;
              case 4 : $bulan="Apr";
                  Break;
              case 5 : $bulan="Mei";
                  Break;
              case 6 : $bulan="Jun";
                  Break;
              case 7 : $bulan="Jul";
                  Break;
              case 8 : $bulan="Aug";
                  Break;
              case 9 : $bulan="Sep";
                  Break;
              case 10 : $bulan="Oct";
                  Break;
              case 11 : $bulan="Nov";
                  Break;
              case 12 : $bulan="Dec";
                  Break;
            }

            return $bulan;
        } else {
            return NULL;
        }
    }
}

if (!function_exists('date_db')) {

    function date_db($val) {
        if ($val != NULL) {
            return date('Y-m-d', strtotime(str_replace('/', '-', $val)));
        } else {
            return NULL;
        }
    }

}

if (!function_exists('json_select2')) {

    function json_select2($response,$useId = false) {
       $data = array();
         if($response){
          foreach($response as $row){
             if($useId){
               $data[] = array(
                  'id'=>$row->id_main,
                  'text'=>$row->name
                );
             }else{
                $data[] = array(
                  'id'=>$row->name,
                  'text'=>$row->name
                );
             }
          }
         }
        return $data;
    }

}

if (!function_exists('price')) {

    function price($val) {
      return number_format($val,0,"",".");
    }

}

if (!function_exists('price_desimal')) {

    function price_desimal($val) {
      return number_format($val,2,",",".");
    }

}

if (!function_exists('price2')) {

    function price2($val) {
      return number_format($val,0,"",",");
    }

}

if (!function_exists('tofloat')) {

    function tofloat($val) {
      return floatval(str_replace(',', '.', str_replace('.', '', $val)));
    }

}

if (!function_exists('random_bg')) {

    function random_bg() {
       $data = array('bg-green','bg-red','bg-yellow','bg-blue','bg-aqua');
       return $data[rand(0,(count($data)-1))];
    }

}

if (!function_exists('index_number')) {

    function index_number($val,$digit) {
        $i_number = strlen($val);
        $digit = $digit;
        for($i=$digit;$i>$i_number;$i--){
          $val = "0".$val;
        }
        return $val;
    }

}

if (!function_exists('json_datatables')) {

    function json_datatables($sEcho,$aaData,$total) {
        $sOutput = array(
            "sEcho" => $sEcho,
            "iTotalRecords" => $total,
            "iTotalDisplayRecords" => $total,
            "aaData" => $aaData
        );
        header('Content-Type: application/json');
        return json_encode($sOutput);
    }

}

if (!function_exists('upload')) {

    function upload($name,$path) {
        $result = array();

        if(isset($_FILES[$name])){

          $count = count($_FILES[$name]['name']);
            for($i=0; $i<$count; $i++) {    
              $temp = explode(".", $_FILES[$name]["name"][$i]);
              $new_name = md5(date('Y-m-d H:i:s').''.$i);
              $newfilename = $new_name. '.' . end($temp);
              $new_path = $path.''.$newfilename;
              $moved = move_uploaded_file($_FILES[$name]["tmp_name"][$i], $new_path);
              if($moved){
                  $result[] = array('file_name'=>$_FILES[$name]["name"][$i],'path'=>$new_path);
              }

            }
        }

        return $result;
    }

}

if (!function_exists('gen_uuid')) {

    function gen_uuid(){
      return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
        mt_rand( 0, 0xffff ),
        mt_rand( 0, 0x0fff ) | 0x4000,
        mt_rand( 0, 0x3fff ) | 0x8000,
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
      );
    }

}

if (!function_exists('gen_token')) {

    function gen_token(){
        return base64_encode(gen_uuid().''.date("Y-m-d H:i:s"));
    }

}

if (!function_exists('get_messages')) {

    function get_messages() {
        $CI = get_instance();
        $CI->load->model('Model_messages','message');
        $id_user = $CI->session->userdata('ID_USER');
        $data = $CI->message->UnReadByUser($id_user);
        return $data;
    }

}

if (!function_exists('get_width_size')) {

    function get_width_size() {

        $width = setting('app_bill_width');
        $size = setting('app_bill_size');
        $convert = 0;

        if($size=='0'){
            $convert = (float)96.000000000001*$width;
        }else if($size=='1'){
            $convert = (float)37.795276*$width;
        }else{
            $convert = (float)3.7795275590551*$width;
        }

        return $convert;
    }

}

if (!function_exists('pass')) {

    function pass($password) {
      $convert = sha1($password).":".sha1("askred");
      return $convert;
    }

}

if (!function_exists('updateLogin')) {

    function updateLogin($id, $login) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->updateLogin($id,$login);
    }

}

if (!function_exists('updateLogout')) {

    function updateLogout($id, $logout) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->updateLogout($id,$logout);
    }
}

if (!function_exists('logdata')) {

    function logdata($id, $akses, $action) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->logdata($id, $akses, $action);
    }
}

if (!function_exists('umur')) {

    function umur($tgllahir,$tglakad) {
      $tgllahir = date_format(date_create($tgllahir),"Y/m/d");
      $tglakad = date_format(date_create($tglakad),"Y/m/d");
      $result=  _date_diff(strtotime($tgllahir), strtotime($tglakad));
      $tahun=$result["y"];
      $bulan=$result["m"]/100;
      return ($tahun+$bulan);
    }
}

if (!function_exists('tenorbulan')) {

    function tenorbulan($tglakad,$tglakhir) {
      $timeStart = strtotime(date_format(date_create($tglakad),"Y-m-d"));
      $timeEnd = strtotime(date_format(date_create($tglakhir),"Y-m-d"));
      // Menambah bulan ini + semua bulan pada tahun sebelumnya
      $numBulan = (date("Y",$timeEnd)-date("Y",$timeStart))*12;
      // menghitung selisih bulan
      $numBulan += date("m",$timeEnd)-date("m",$timeStart);

      return $numBulan;
    }
}

if (!function_exists('_date_range_limit')) {
  function _date_range_limit($start, $end, $adj, $a, $b, &$result){
      if ($result[$a] < $start) {
          $result[$b] -= intval(($start - $result[$a] - 1) / $adj) + 1;
          $result[$a] += $adj * intval(($start - $result[$a] - 1) / $adj + 1);
      }

      if ($result[$a] >= $end) {
          $result[$b] += intval($result[$a] / $adj);
          $result[$a] -= $adj * intval($result[$a] / $adj);
      }

      return $result;
  }
}

if (!function_exists('_date_range_limit_days')) {
  function _date_range_limit_days(&$base, &$result)
  {
      $days_in_month_leap = array(31, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
      $days_in_month = array(31, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

      _date_range_limit(1, 13, 12, "m", "y", $base);

      $year = $base["y"];
      $month = $base["m"];

      if (!$result["invert"]) {
          while ($result["d"] < 0) {
              $month--;
              if ($month < 1) {
                  $month += 12;
                  $year--;
              }

              $leapyear = $year % 400 == 0 || ($year % 100 != 0 && $year % 4 == 0);
              $days = $leapyear ? $days_in_month_leap[$month] : $days_in_month[$month];

              $result["d"] += $days;
              $result["m"]--;
          }
      } else {
          while ($result["d"] < 0) {
              $leapyear = $year % 400 == 0 || ($year % 100 != 0 && $year % 4 == 0);
              $days = $leapyear ? $days_in_month_leap[$month] : $days_in_month[$month];

              $result["d"] += $days;
              $result["m"]--;

              $month++;
              if ($month > 12) {
                  $month -= 12;
                  $year++;
              }
          }
      }

      return $result;
  }
}


if (!function_exists('_date_normalize')) {
  function _date_normalize(&$base, &$result)
  {
      $result = _date_range_limit(0, 60, 60, "s", "i", $result);
      $result = _date_range_limit(0, 60, 60, "i", "h", $result);
      $result = _date_range_limit(0, 24, 24, "h", "d", $result);
      $result = _date_range_limit(0, 12, 12, "m", "y", $result);

      $result = _date_range_limit_days($base, $result);

      $result = _date_range_limit(0, 12, 12, "m", "y", $result);

      return $result;
  }
}

/**
 * Accepts two unix timestamps.
 */

if (!function_exists('_date_diff')) {
  function _date_diff($one, $two)
  {
      $invert = false;

      $key = array("y", "m", "d", "h", "i", "s");
      $a = array_combine($key, array_map("intval", explode(" ", date("Y m d H i s", $one))));
      $b = array_combine($key, array_map("intval", explode(" ", date("Y m d H i s", $two))));

      $result = array();
      $result["y"] = $b["y"] - $a["y"];
      $result["m"] = $b["m"] - $a["m"];
      $result["d"] = $b["d"] - $a["d"];
      $result["h"] = $b["h"] - $a["h"];
      $result["i"] = $b["i"] - $a["i"];
      $result["s"] = $b["s"] - $a["s"];
      $result["invert"] = $invert ? 1 : 0;
      //$result["days"] = intval(abs(($one - $two)/86400));
      $result["days"] = intval((($two-$one)/86400));

      if ($invert) {
          _date_normalize($a, $result);
      } else {
          _date_normalize($b, $result);
      }

      return $result;
  }
}

if (!function_exists('cekNamaBank')) {

    function cekNamaBank($kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekNamaBank($kode);
      return $abc;
    }

}

if (!function_exists('cekNamaBankNyaCapem')) {

    function cekNamaBankNyaCapem($kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekNamaBankNyaCapem($kode);
      return $abc;
    }

}

if (!function_exists('cekNamaAsdur')) {

    function cekNamaAsdur($kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekNamaAsdur($kode);
      return $abc;
    }

}

if (!function_exists('cekPekerjaan')) {

    function cekPekerjaan($kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekPekerjaan($kode);
      return $abc;
    }

}

if (!function_exists('cekProduk')) {

    function cekProduk($kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekProduk($kode);
      return $abc;
    }

}

if (!function_exists('hariini')) {

    function hariini() {
      return date("Y-m-d H:i:s");
    }
}

if (!function_exists('create_user')) {

    function create_user() {
      $CI = get_instance();
      return $CI->session->userdata("idUser");
    }
}

if (!function_exists('cekDokumenUmum')) {

    function cekDokumenUmum($kode,$dok) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekDokumenUmum($kode,$dok);
      return $abc;
    }

}

if (!function_exists('cekDokumenKhusus')) {

    function cekDokumenKhusus($kode,$dok) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekDokumenKhusus($kode,$dok);
      return $abc;
    }

}

if (!function_exists('MenungguKlaim')) {

    function MenungguKlaim() {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->MenungguKlaim();
      return $abc;
    }

}

if (!function_exists('DitangguhkanKlaim')) {

    function DitangguhkanKlaim() {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->DitangguhkanKlaim();
      return $abc;
    }

}

if (!function_exists('DitolakKlaim')) {

    function DitolakKlaim() {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->DitolakKlaim();
      return $abc;
    }

}

if (!function_exists('DisetujuiKlaim')) {

    function DisetujuiKlaim() {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->DisetujuiKlaim();
      return $abc;
    }

}

if (!function_exists('MenungguRestitusi')) {

    function MenungguRestitusi() {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->MenungguRestitusi();
      return $abc;
    }

}

if (!function_exists('DitangguhkanRestitusi')) {

    function DitangguhkanRestitusi() {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->DitangguhkanRestitusi();
      return $abc;
    }

}

if (!function_exists('DitolakRestitusi')) {

    function DitolakRestitusi() {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->DitolakRestitusi();
      return $abc;
    }

}

if (!function_exists('DisetujuiRestitusi')) {

    function DisetujuiRestitusi() {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->DisetujuiRestitusi();
      return $abc;
    }

}

if (!function_exists('lead_time')) {

    function lead_time($dt1,$dt2) {
      $dt1 = date_format(date_create($dt1),'Y-m-d');
      $dt2 = date_format(date_create($dt2),'Y-m-d');
      // $today = new DateTime();
      $dt3 = new DateTime($dt1);
      $dt4 = new DateTime($dt2);
      $interval = $dt3->diff($dt4);
      $hari = $interval->days." hari";
      return $hari;
    }

}

if (!function_exists('lead_time_stop')) {

    function lead_time_stop($dt1,$dt2) {
      $dt1 = date_format(date_create($dt1),'Y-m-d');
      $dt2 = date_format(date_create($dt2),'Y-m-d');
      $today = new DateTime();
      $dt3 = new DateTime($dt1);
      $dt4 = new DateTime($dt2);
      $interval = $dt3->diff($dt4);
      $hari = $interval->days." hari";
      return $hari;
    }

}

if (!function_exists('tgl_polis')) {

    function tgl_polis($noreg) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->tgl_polis($noreg);
      return $abc;
    }

}

if (!function_exists('cekPolis')) {

    function cekPolis($noreg) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekPolis($noreg);
      return $abc;
    }

}

if (!function_exists('net_premi')) {

    function net_premi($premi, $feebank,$feebroker) {
      $net_premi = $premi-($feebank+$feebroker);
      return price($net_premi);
    }

}

if (!function_exists('cekDokKurang')) {

    function cekDokKurang($noreg) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekDokKurang($noreg);
      return $abc;
    }

}

if (!function_exists('cekDokLengkap')) {

    function cekDokLengkap($noreg) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekDokLengkap($noreg);
      return $abc;
    }

}

if (!function_exists('bangpem')) {

    function bangpem($noreg) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->bangpem($noreg);
      return $abc;
    }

}

if (!function_exists('cekDokKlaim')) {
    function cekDokKlaim($noreg) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekDokKlaim($noreg);
      return $abc;
    }

}

if (!function_exists('getDebitur4')) {
    function getDebitur4($bulan) {
      $CI = get_instance();
      $CI->load->model('Model_laporan','laporan');
      $abc = $CI->laporan->getProduksi4($bulan);
      return $abc;
    }

}

if (!function_exists('cekDebiturAsdur')) {
    function cekDebiturAsdur($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekDebiturAsdur($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekDebiturAsdurPolis')) {
    function cekDebiturAsdurPolis($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekDebiturAsdurPolis($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekDebiturResAsdur')) {
    function cekDebiturResAsdur($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekDebiturResAsdur($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekDebiturResAsdurBayar')) {
    function cekDebiturResAsdurBayar($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekDebiturResAsdurBayar($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekUpAsdurPolis')) {
    function cekUpAsdurPolis($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekUpAsdurPolis($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekNomAsdurBayar')) {
    function cekNomAsdurBayar($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekNomAsdurBayar($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekUpAsdur')) {
    function cekUpAsdur($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekUpAsdur($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekNomAsdur')) {
    function cekNomAsdur($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekNomAsdur($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekPremiAsdur')) {
    function cekPremiAsdur($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekPremiAsdur($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekDebiturAsdurTahun')) {
    function cekDebiturAsdurTahun($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekDebiturAsdurTahun($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekDebiturAsdurPolisTahun')) {
    function cekDebiturAsdurPolisTahun($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekDebiturAsdurPolisTahun($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekDebiturResAsdurTahun')) {
    function cekDebiturResAsdurTahun($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekDebiturResAsdurTahun($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekDebiturResAsdurBayarTahun')) {
    function cekDebiturResAsdurBayarTahun($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekDebiturResAsdurBayarTahun($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekUpAsdurTahun')) {
    function cekUpAsdurTahun($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekUpAsdurTahun($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekUpAsdurPolisTahun')) {
    function cekUpAsdurPolisTahun($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekUpAsdurPolisTahun($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekResAsdurTahun')) {
    function cekResAsdurTahun($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekResAsdurTahun($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekResAsdurBayarTahun')) {
    function cekResAsdurBayarTahun($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekResAsdurBayarTahun($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekPremiAsdurTahun')) {
    function cekPremiAsdurTahun($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekPremiAsdurTahun($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekSlaDebitur')) {
    function cekSlaDebitur($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekSlaDebitur($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekNonSlaDebitur')) {
    function cekNonSlaDebitur($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekNonSlaDebitur($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekSlaResDebitur')) {
    function cekSlaResDebitur($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekSlaResDebitur($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekNonSlaResDebitur')) {
    function cekNonSlaResDebitur($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekNonSlaResDebitur($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekSlaPlafon')) {
    function cekSlaPlafon($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekSlaPlafon($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekNonSlaPlafon')) {
    function cekNonSlaPlafon($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekNonSlaPlafon($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekSlaResPlafon')) {
    function cekSlaResPlafon($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekSlaResPlafon($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekNonSlaResPlafon')) {
    function cekNonSlaResPlafon($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekNonSlaResPlafon($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekSlaDebitur2')) {
    function cekSlaDebitur2($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekSlaDebitur2($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekNonSlaDebitur2')) {
    function cekNonSlaDebitur2($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekNonSlaDebitur2($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekSlaPlafon2')) {
    function cekSlaPlafon2($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekSlaPlafon2($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekNonSlaPlafon2')) {
    function cekNonSlaPlafon2($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekNonSlaPlafon2($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekSlaResDebitur2')) {
    function cekSlaResDebitur2($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekSlaResDebitur2($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekNonSlaResDebitur2')) {
    function cekNonSlaResDebitur2($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekNonSlaResDebitur2($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekSlaResPlafon2')) {
    function cekSlaResPlafon2($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekSlaResPlafon2($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekNonSlaResPlafon2')) {
    function cekNonSlaResPlafon2($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekNonSlaResPlafon2($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekResiko')) {
    function cekResiko($idResiko) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekResiko($idResiko);
      return $abc;
    }

}

if (!function_exists('cekSlaKlaimDebitur')) {
    function cekSlaKlaimDebitur($tahun,$kodeAsuransi) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekSlaKlaimDebitur($tahun,$kodeAsuransi);
      return $abc;
    }

}

if (!function_exists('cekNonSlaKlaimDebitur')) {
    function cekNonSlaKlaimDebitur($tahun,$kodeAsuransi) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekNonSlaKlaimDebitur($tahun,$kodeAsuransi);
      return $abc;
    }

}

if (!function_exists('cekSlaKlaimPlafon')) {
    function cekSlaKlaimPlafon($tahun,$kodeAsuransi) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekSlaKlaimPlafon($tahun,$kodeAsuransi);
      return $abc;
    }

}

if (!function_exists('cekNonSlaKlaimPlafon')) {
    function cekNonSlaKlaimPlafon($tahun,$kodeAsuransi) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekNonSlaKlaimPlafon($tahun,$kodeAsuransi);
      return $abc;
    }

}


if (!function_exists('cekSlaKlaimDebitur2')) {
    function cekSlaKlaimDebitur2($tahun,$kodeAsuransi) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekSlaKlaimDebitur2($tahun,$kodeAsuransi);
      return $abc;
    }

}

if (!function_exists('cekNonSlaKlaimDebitur2')) {
    function cekNonSlaKlaimDebitur2($tahun,$kodeAsuransi) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekNonSlaKlaimDebitur2($tahun,$kodeAsuransi);
      return $abc;
    }

}

if (!function_exists('cekSlaKlaimPlafon2')) {
    function cekSlaKlaimPlafon2($tahun,$kodeAsuransi) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekSlaKlaimPlafon2($tahun,$kodeAsuransi);
      return $abc;
    }

}

if (!function_exists('cekNonSlaKlaimPlafon2')) {
    function cekNonSlaKlaimPlafon2($tahun,$kodeAsuransi) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekNonSlaKlaimPlafon2($tahun,$kodeAsuransi);
      return $abc;
    }

}

//
if (!function_exists('cekDebiturKlaimAsdurTahun')) {
    function cekDebiturKlaimAsdurTahun($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekDebiturKlaimAsdurTahun($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekKlaimAsdurTahun')) {
    function cekKlaimAsdurTahun($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekKlaimAsdurTahun($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekDebiturKlaimAsdurBayarTahun')) {
    function cekDebiturKlaimAsdurBayarTahun($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekDebiturKlaimAsdurBayarTahun($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekKlaimAsdurBayarTahun')) {
    function cekKlaimAsdurBayarTahun($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekKlaimAsdurBayarTahun($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekDebiturKlaimAsdur')) {
    function cekDebiturKlaimAsdur($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekDebiturKlaimAsdur($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekNomKlaimAsdur')) {
    function cekNomKlaimAsdur($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekNomKlaimAsdur($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekDebiturKlaimAsdurBayar')) {
    function cekDebiturKlaimAsdurBayar($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekDebiturKlaimAsdurBayar($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekNomKlaimAsdurBayar')) {
    function cekNomKlaimAsdurBayar($tahun,$kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekNomKlaimAsdurBayar($tahun,$kode);
      return $abc;
    }

}

if (!function_exists('cekOpBank')) {
    function cekOpBank($kode) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->cekOpBank($kode);
      return $abc;
    }

}

if (!function_exists('is_online')) {
    function is_online() {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->is_online();
      return $abc;
    }

}

if (!function_exists('not_online')) {
    function not_online() {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->not_online();
      return $abc;
    }

}

if (!function_exists('restitusiBj')) {
    function restitusiBj($noreg) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->restitusiBj($noreg);
      return $abc;
    }

}

if (!function_exists('count_chat_all')) {
    function count_chat_all() {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->count_chat_all();
      return $abc;
    }

}

if (!function_exists('count_chat')) {
    function count_chat($idUser) {
      $CI = get_instance();
      $CI->load->model('Model_helper','help');
      $abc = $CI->help->count_chat($idUser);
      return $abc;
    }

}

