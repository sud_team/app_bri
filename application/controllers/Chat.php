<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends CI_Controller {

	public function __Construct(){
		parent::__Construct();
		$this->app->auth();
		$this->load->model("Model_dashboard","dash");
	}

	public function index(){
		$idUser = create_user();
		$roles = $this->session->userdata("roles");
		$and = "";
		if($roles=="2"){
			$and .= " and (roles = '2' or roles = '3')";
		}else if($roles=="4"){
			$and .= " and (roles = '4' or roles = '3')";
		}else {
			$and .= "";
		}


        $data["teman"] = $this->db->query("select * from t_user where idUser != '$idUser' and is_online = '1' ".$and);
        $this->load->view('public/chat_dashboard', $data);
    }

    public function getChats()
    {
        header('Content-Type: application/json');
        if ($this->input->is_ajax_request()) {
            // Find friend
            $id = $this->input->post('chatWith');
            $friend = $this->db->query("select idUser,username from t_user where idUser = '$id'")->row();
            // $friend = $this->db->get_where('users', array('id' => $this->input->post('chatWith')), 1)->row();

            // Get Chats
            $idUser = $this->session->userdata("idUser");
            $chats = $this->db->query("select top 100 * from t_chat a join t_user users on a.send_by = users.idUser where (send_by = '$idUser' AND send_to = '$friend->idUser') or (send_to = '$idUser' AND send_by = '$friend->idUser') order by a.time desc")->result();
            $result = array(
                'name' => $friend->username,
                'chats' => $chats
            );
            echo json_encode($result);
        }
    }

    public function sendMessage()
    {	
    	$pesan = $this->input->post('message');
    	$send_to = $this->input->post('chatWith');
    	$send_by = $this->session->userdata("idUser");
    	$time = hariini();
        $update = $this->db->query("update t_chat set count_read = 1 where send_to = '$send_to' and send_by = '$send_by'"); 
    	$id = $this->db->query("select max(chat_id) id from t_chat")->row()->id+1;
        $query = $this->db->query("insert into t_chat (chat_id,message,send_to,send_by,time,count_read) values ('$id','$pesan','$send_to','$send_by','$time','0')");
    }

}
