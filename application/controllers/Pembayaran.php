<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends CI_Controller {

	public function __Construct(){
		parent::__Construct();
		$this->app->auth();
    $this->load->model("Model_helper","helper");
    $this->load->model("Model_pembayaran","bayar");
	}

	public function internalmemo(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		// $data["debitur"] = $this->debitur->debitur();
		$this->app->render('Pembayaran','pembayaran/internal_memo',$data);
	}

  public function datapembayaran(){
    $data["produk"] = $this->helper->produk();
    $data["cabang"] = $this->helper->cabang();
    // $data["debitur"] = $this->debitur->debitur();
    $this->app->render('Pembayaran','pembayaran/data_pembayaran',$data);
  }

  public function filter_internalmemo(){
    $draw = intval($this->input->post("draw"));
    $start = intval($this->input->post("start"));
    $length = intval($this->input->post("length"));

    $jenis_im = $this->input->post("jenis_im");
    if($jenis_im=="Klaim"){
      $total = $this->bayar->filter_internalmemo_klaim();
      $peserta = $this->bayar->filter_internalmemo_klaim()->result();
      $recordsTotal = $total->num_rows();
    }else if($jenis_im=="Restitusi"){
      $total = $this->bayar->filter_internalmemo_restitusi();
      $peserta = $this->bayar->filter_internalmemo_restitusi()->result();
      $recordsTotal = $total->num_rows();
    }else{
      $klaim1 = $this->bayar->filter_internalmemo_klaim();
      $klaim = $this->bayar->filter_internalmemo_klaim()->result();
      $restitusi1 = $this->bayar->filter_internalmemo_restitusi();
      $restitusi = $this->bayar->filter_internalmemo_restitusi()->result();
      $recordsTotal = $klaim1->num_rows()+$restitusi1->num_rows();
      $peserta = array_merge($klaim,$restitusi);
    }

    $data = array();
    $i = 1;

    foreach($peserta as $r) {
        
      $data[] = array(
          $i,
            $r->NomorPinjaman,
            $r->NamaDebitur,
            my_date_indo($r->create_selesai),
            cekNamaBank($r->kodeBank),
            cekNamaAsdur($r->kodeAsuransi),
            $r->jenis,
            price($r->nominal),
            '<a id="#exampleModal" onclick="cekPembayaran(\''.$r->NomorPinjaman.'-'.$r->jenis.'\')" class="btn btn-xs btn-info" style="color:#ffffff">Bayar</a>'
       );

     $i++;
    }

    $output = array(
          "draw" => $draw,
          "recordsTotal" => $recordsTotal,
          "recordsFiltered" => $recordsTotal,
          "data" => $data
      );

    echo json_encode($output);
    exit();
  }

  public function capem($kode){
    $capem = $this->helper->capem($kode);
    echo "<option value=''>Semua</option>";
    foreach ($capem as $key) {
      echo "<option value='".$key->kodeBank."'>".$key->capem."</option>";
    }
  }

  public function cekPembayaran($nomor){
    $pisah = explode("-", $nomor);
    if($pisah[1]=="Klaim"){
      $kode = "KLM";
    }else{
      $kode = "RST";
    }

    $kode = "PB".$kode.date("ymdhis");

    $text = '<div class="row m-b-20">
                  <div class="text-left text-info w-70 m-l-20">
                      <lable class="col-sm-12 control-label text-info font-weight-bold">Nomor Pinjaman :<strong> '.$pisah[0].'</strong></lable>
                      <input type="hidden" name="NomorPinjaman" value="'.$pisah[0].'">
                      <input type="hidden" name="jenis" value="'.$pisah[1].'">
                  </div>
              </div>
              <div class="row m-b-20">
                  <div class="text-left text-info w-50 m-l-20">
                      <small class="form-control-feedback f-s-10">Kode Pembayaran</small>
                      <input type="text" placeholder="Kode Pembayaran" name="kode" id="kode" class="col-xs-3 custom-select f-s-12 " readonly value="'.$kode.'">
                  </div>
              </div>';
    echo $text;
  }

  public function savePembayaran(){
    $save = $this->bayar->savePembayaran();
    if($save){
      $this->session->set_flashdata("success","Pembayaran Berhasil Diajukan");
    }else{
      $this->session->set_flashdata("danger","Pembayaran Gagal Diajukan");
    }

    redirect("pembayaran/internalmemo");
  }


  public function filter_pembayaran(){
    $draw = intval($this->input->post("draw"));
    $start = intval($this->input->post("start"));
    $length = intval($this->input->post("length"));

    $jenis_im = $this->input->post("jenis_im");
    if($jenis_im=="Klaim"){
      $total = $this->bayar->filter_pembayaran_klaim();
      $peserta = $this->bayar->filter_pembayaran_klaim()->result();
      $recordsTotal = $total->num_rows();
    }else if($jenis_im=="Restitusi"){
      $total = $this->bayar->filter_pembayaran_restitusi();
      $peserta = $this->bayar->filter_pembayaran_restitusi()->result();
      $recordsTotal = $total->num_rows();
    }else{
      $klaim1 = $this->bayar->filter_pembayaran_klaim();
      $klaim = $this->bayar->filter_pembayaran_klaim()->result();
      $restitusi1 = $this->bayar->filter_pembayaran_restitusi();
      $restitusi = $this->bayar->filter_pembayaran_restitusi()->result();
      $recordsTotal = $klaim1->num_rows()+$restitusi1->num_rows();
      $peserta = array_merge($klaim,$restitusi);
    }

    $data = array();
    $i = 1;

    foreach($peserta as $r) {
        
      $data[] = array(
          $i,
            $r->KodePembayaran,
            $r->NomorPinjaman,
            $r->NamaDebitur,
            my_date_indo($r->TanggalBayar),
            cekNamaBank($r->kodeBank),
            cekNamaAsdur($r->kodeAsuransi),
            $r->jenis,
            price($r->nominal),
            '<input type="checkbox" name="checkbox[]" value="\''.$r->NomorPinjaman.'\'" class="chkDel">',
       );

     $i++;
    }

    $output = array(
          "draw" => $draw,
          "recordsTotal" => $recordsTotal,
          "recordsFiltered" => $recordsTotal,
          "data" => $data
      );

    echo json_encode($output);
    exit();
  }
	
}
