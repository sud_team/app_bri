<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

	public function __Construct(){
		parent::__Construct();
		$this->app->auth();
		$this->load->model('Model_helper','helper');
		$this->load->model('Model_setting','setting');
	}

  public function setPrapen(){
    $this->app->render('List Debitur','master/prapen',null);
  }

  public function filter_setPrapen(){
    $draw = intval($this->input->post("draw"));
    $start = intval($this->input->post("start"));
    $length = intval($this->input->post("length"));

    $peserta = $this->setting->list_debitur_prapen();

    $data = array();
    $i = 1;

    foreach($peserta->result() as $key) {
      if ($key->StatusPrapen==0){
        $btn = '<span id="link"><a onclick="updatePrapen(\''.$key->NomorPinjaman.'\')" class="btn btn-primary btn-xs" style="color: #ffffff;">Set Prapen</a></span>';
      }else{
        $btn = '-';
      }

      $data[] = array(
        $key->NomorPinjaman,
        $key->NamaDebitur,
        my_date($key->TglAkadKredit),
        my_date($key->TglAkhirKredit),
        $key->TenorBulan,
        price($key->plafon),
        price($key->JumlahPremiTenor),
        $btn
      );

      $i++;
    }

    $output = array(
      "draw" => $draw,
      "recordsTotal" => $peserta->num_rows(),
      "recordsFiltered" => $peserta->num_rows(),
      "data" => $data
    );

    echo json_encode($output);
    exit();
  }

  public function updatePrapen(){
    $update = $this->setting->updatePrapen();
    echo $update;
  }

  public function setProrate(){
    $this->app->render('List Debitur','master/prorate',null);
  }

  public function filter_setProrate(){
    $draw = intval($this->input->post("draw"));
    $start = intval($this->input->post("start"));
    $length = intval($this->input->post("length"));

    $peserta = $this->setting->list_debitur_prorate();

    $data = array();
    $i = 1;

    foreach($peserta->result() as $key) {
      $btn = '<a onclick="updateProrate(\''.$key->NomorPinjaman.'\')" class="btn btn-primary btn-xs" style="color: #ffffff;">Update Premi</a>';

      $text = '<span id="link"><input type="text" id="premi" name="JumlahPremiTenor" class="col-xs-3 custom-select f-s-12 price" value="'.price($key->JumlahPremiTenor).'"></span>';


      $data[] = array(
        $key->NomorPinjaman,
        $key->NamaDebitur,
        my_date($key->TglAkadKredit),
        my_date($key->TglAkhirKredit),
        $key->TenorBulan,
        price($key->plafon),
        $text,
        $btn
      );

      $i++;
    }

    $output = array(
      "draw" => $draw,
      "recordsTotal" => $peserta->num_rows(),
      "recordsFiltered" => $peserta->num_rows(),
      "data" => $data
    );

    echo json_encode($output);
    exit();    
  }

  public function updateProrate(){
    $update = $this->setting->updateProrate();
    echo $update;
  }

  public function setPindahan(){
    $this->app->render('Debitur','master/pindahan',null);
  }

  public function filter_setPindahan(){
    $draw = intval($this->input->post("draw"));
    $start = intval($this->input->post("start"));
    $length = intval($this->input->post("length"));

    $peserta = $this->setting->list_debitur_pindahan();
    $t_asuransi = $this->setting->t_asuransi();

    $data = array();
    $i = 1;

    foreach($peserta->result() as $key) {
      $btn = '<span id="link"><a onclick="updatePindahan(\''.$key->NomorPinjaman.'\')" class="btn btn-primary btn-xs" style="color: #ffffff;">Pindahkan</a></span>';

      if($key->StatusBayar==1){
        $asuransi = $key->asuransi;
      }else{

      $asuransi = '<input type="hidden" id="tahun'.$key->NomorPinjaman.'" name="tahun'.$key->NomorPinjaman.'" value="'.$key->TahunKe.'"><select id="kodeAsuransi'.$key->NomorPinjaman.'" name="kodeAsuransi'.$key->NomorPinjaman.'">';
                    foreach ($t_asuransi as $value) {
                      $selected = '';
                      if($value->kodeAngka==$key->kodeAsuransi){
                        $selected = 'selected';
                      }
                      $asuransi .= '<option value="'.$value->kodeAngka.'" '.$selected.'>'.$value->asuransi.'</option>';
                    }
                  $asuransi .= '</select>';

      }

      $data[] = array(
        $key->NomorPinjaman,
        $key->NamaDebitur,
        my_date($key->TglAkadKredit),
        my_date($key->TglAkhirKredit),
        $key->TenorBulan,
        $key->TahunKe,
        $asuransi,
        $btn
      );

      $i++;
    }

    $output = array(
      "draw" => $draw,
      "recordsTotal" => $peserta->num_rows(),
      "recordsFiltered" => $peserta->num_rows(),
      "data" => $data
    );

    echo json_encode($output);
    exit();
  }

  public function updatePindahan(){
    $update = $this->setting->updatePindahan();
    echo $update;
  }


}
