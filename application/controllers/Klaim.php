<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Klaim extends CI_Controller {

	public function __Construct(){
		parent::__Construct();
		$this->app->auth();
		$this->load->model('Model_helper','helper');
		$this->load->model('Model_klaim','klaim');
	}

	public function formKlaim($noreg){
		$data["NomorRegistrasi"] = $noreg;
		$data["NomorPinjaman"] = $this->klaim->NomorPinjaman($noreg);
		$data["debitur"] = $this->klaim->debitur($noreg);
		$data["resiko"] = $this->helper->Resiko();
		$data["umum"] = $this->klaim->DokUmum();
		$data["pinjaman"] = $this->klaim->pinjaman($noreg);
		// $data["dokumenumum"] = $this->klaim->pinjamandokumen("KLM".$noreg);
		$data["khusus"] = $this->klaim->DokKhusus2('KLM'.$noreg);
		$this->app->render('Data Debitur Klaim','klaim/formKlaim',$data);
	}

	public function DokKhusus($idResiko){
		$data["khusus"] = $this->klaim->DokKhusus($idResiko);
		echo '<tbody>';
		$ab = 1;
		foreach ($data["khusus"] as $key) {
            echo '<tr>
                <td class="w-3">&nbsp;</td>
                <td class="w-50">'.$key->dokumen.'</td>
                <td align="right">
                    <div class="form-actions p-2">
                        <label  class="btn btn-outline-warning btn-sm">
                            <input type="hidden" name="ab[]" value="'.$ab.'">
                            <input type="hidden" name="khusus'.$ab.'" value="'.$key->dokumen.'">
                            <input type="file" name="filekhusus'.$ab.'">
                        </label>
                    </div>
                </td>
            </tr>';
		$ab++;
		}
        echo '</tbody>';
	}

	public function saveKlaim(){
		$KodeKlaim = $this->input->post("KodeKlaim");
		$noreg = $this->input->post("NomorRegistrasi");
		// print_r($noreg);
		// die();
		$idResiko = $this->input->post("idResiko");
		$save = $this->klaim->saveKlaim();
		$config['upload_path'] = './upload/klaim/';
		$config['allowed_types'] = 'xls|xlsx|doc|docx|pdf|jpg|jpeg|txt';
	    $config['max_size'] = '30000';
	    $config['max_width']  = '8000'; 
	    $config['max_height']  = '7768';

	    $this->load->library('upload', $config);
	    $this->upload->initialize($config);

	    foreach ($this->input->post("no") as $i) {
		    $dokumen = $this->input->post("dokumen".$i);
	        if(empty($this->upload->do_upload("fileumum".$i))){		        	
		        $this->klaim->saveKlaimDokumen($KodeKlaim,$dokumen,"",0,$idResiko,'umum');
	        }else{
	        	$upload_dataumum = $this->upload->data(); 
	        	$this->klaim->saveKlaimDokumen($KodeKlaim,$dokumen,$upload_dataumum["file_name"],1,$idResiko,'umum');
	        }
	    }

	    foreach ($this->input->post("ab") as $i) {
		    $dokumen1 = $this->input->post("khusus".$i);
	        if(empty($this->upload->do_upload("filekhusus".$i))){
	        	$this->klaim->saveKlaimDokumen($KodeKlaim,$dokumen1,"",0,$idResiko,'khusus');
	        }else{
	        	$upload_datakhusus = $this->upload->data(); 
	        	$this->klaim->saveKlaimDokumen($KodeKlaim,$dokumen1,$upload_datakhusus["file_name"],1,$idResiko,'khusus');
	        }
	    }

	    if($save){
			$this->session->set_flashdata("success","Klaim Berhasil Diajukan");
		}else{
			$this->session->set_flashdata("danger","Klaim Gagal Diajukan");
		}

		redirect("klaim/formKeputusan/".$noreg);
	}
	
	public function menunggu(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		// $data["debiturklaim"] = $this->klaim->debiturklaim();
		$this->app->render('List Klaim','klaim/menungguKlaim',$data);
	}

	public function filter_menunggu(){
		  $draw = intval($this->input->post("draw"));
	      $start = intval($this->input->post("start"));
	      $length = intval($this->input->post("length"));

	      $peserta = $this->klaim->debiturklaim();
			
	      $data = array();
	      $i = 1;

	      foreach($peserta->result() as $key) {
	      		if($this->session->userdata("roles")==2 && $this->session->userdata("hak_akses")==5){
	      			if($key->spv==1){
	            		$pengajuan = '<a href="'.base_url().'klaim/formKeputusan/M'.$key->NomorRegistrasi.'" class="btn btn-xs btn-info" style="color: #ffffff;">View</a>';
	      			}else{
	            		$pengajuan = '<a href="'.base_url().'klaim/formKeputusan/M'.$key->NomorRegistrasi.'" class="btn btn-xs btn-danger" style="color: #ffffff;">Proses</a>';
	      			}
	      		}else{
	            		$pengajuan = '<a href="'.base_url().'klaim/formKeputusan/M'.$key->NomorRegistrasi.'" class="btn btn-xs btn-danger" style="color: #ffffff;">Proses</a>';
	      		}

	      		if($key->keputusan==0){
	      			$hari = lead_time($key->TglKlaim,date("Y-m-d H:i:s"));
	      		}

	      		if($key->keputusan==1){
	      			$hari = lead_time($key->TglKlaim,$key->create_selesai);
	      		}


	              $data[] = array(
	              	'',
	                  my_date($key->create_date),
	                  $key->NomorPinjaman,
	                  $key->NamaDebitur,
	                  my_date($key->TglLahir),
	                  str_replace(".", " Tahun ", $key->UsiaDebitur)." Bulan",
	                  cekPekerjaan($key->KodePekerjaan),
	                  my_date($key->TglAkadKredit),
	                  $key->TenorTahun,
	                  price($key->plafon),
	                  cekProduk($key->idProduk),
	                  cekNamaBank($key->kodeBank),
	                  cekNamaAsdur($key->kodeAsuransi),
	                  $hari,
	                  $pengajuan
	             );

	           $i++;
	      }

	      $output = array(
	            "draw" => $draw,
	            "recordsTotal" => $peserta->num_rows(),
	            "recordsFiltered" => $peserta->num_rows(),
	            "data" => $data
	        );

	      echo json_encode($output);
	      exit();
	}

	public function ditangguhkan(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		// $data["debiturklaim"] = $this->klaim->debiturklaimditangguhkan();
		$this->app->render('List Klaim','klaim/ditangguhkan',$data);
	}

	public function filter_ditangguhkan(){
		  $draw = intval($this->input->post("draw"));
	      $start = intval($this->input->post("start"));
	      $length = intval($this->input->post("length"));

	      $peserta = $this->klaim->debiturklaimditangguhkan();
			
	      $data = array();
	      $i = 1;

	      foreach($peserta->result() as $key) {
	      		if($this->session->userdata("roles")==2 && $this->session->userdata("hak_akses")==5){
	      			if($key->spv==1){
	            		$pengajuan = '<a href="'.base_url().'klaim/formKeputusan/M'.$key->NomorRegistrasi.'" class="btn btn-xs btn-info" style="color: #ffffff;">View</a>';
	      			}else{
	            		$pengajuan = '<a href="'.base_url().'klaim/formKeputusan/M'.$key->NomorRegistrasi.'" class="btn btn-xs btn-danger" style="color: #ffffff;">Proses</a>';
	      			}
	      		}else{
	            		$pengajuan = '<a href="'.base_url().'klaim/formKeputusan/M'.$key->NomorRegistrasi.'" class="btn btn-xs btn-danger" style="color: #ffffff;">View</a>';
	      		}
                

	              $data[] = array(
	              	'',
	                  $key->NomorPinjaman,
	                  $key->NamaDebitur,
	                  my_date($key->TglLahir),
	                  str_replace(".", " Tahun ", $key->UsiaDebitur)." Bulan",
	                  cekPekerjaan($key->KodePekerjaan),
	                  my_date($key->TglAkadKredit),
	                  $key->TenorTahun,
	                  cekProduk($key->idProduk),
	                  price($key->plafon),
	                  cekNamaBank($key->kodeBank),
	                  cekNamaAsdur($key->kodeAsuransi),
	                  $pengajuan
	             );

	           $i++;
	      }

	      $output = array(
	            "draw" => $draw,
	            "recordsTotal" => $peserta->num_rows(),
	            "recordsFiltered" => $peserta->num_rows(),
	            "data" => $data
	        );

	      echo json_encode($output);
	      exit();
	}

	public function ditolak(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		// $data["debiturklaim"] = $this->klaim->debiturklaimditolak();
		$this->app->render('List Klaim','klaim/ditolak',$data);
	}

	public function filter_ditolak(){
		  $draw = intval($this->input->post("draw"));
	      $start = intval($this->input->post("start"));
	      $length = intval($this->input->post("length"));

	      $peserta = $this->klaim->debiturklaimditolak();
			
	      $data = array();
	      $i = 1;

	      foreach($peserta->result() as $key) {
	      		if($this->session->userdata("roles")==2 && $this->session->userdata("hak_akses")==5){
	      			if($key->spv==1){
	            		$pengajuan = '<a href="'.base_url().'klaim/formKeputusan/M'.$key->NomorRegistrasi.'" class="btn btn-xs btn-info" style="color: #ffffff;">View</a>';
	      			}else{
	            		$pengajuan = '<a href="'.base_url().'klaim/formKeputusan/M'.$key->NomorRegistrasi.'" class="btn btn-xs btn-danger" style="color: #ffffff;">Proses</a>';
	      			}
	      		}else{
	            		$pengajuan = '<a href="'.base_url().'klaim/formKeputusan/M'.$key->NomorRegistrasi.'" class="btn btn-xs btn-danger" style="color: #ffffff;">View</a>';
	      		}
                

	              $data[] = array(
	              	'',
	                  $key->NomorPinjaman,
	                  $key->NamaDebitur,
	                  my_date($key->TglLahir),
	                  str_replace(".", " Tahun ", $key->UsiaDebitur)." Bulan",
	                  cekPekerjaan($key->KodePekerjaan),
	                  my_date($key->TglAkadKredit),
	                  $key->TenorTahun,
	                  price($key->plafon),
	                  cekProduk($key->idProduk),
	                  cekNamaBank($key->kodeBank),
	                  cekNamaAsdur($key->kodeAsuransi),
	                  $pengajuan
	             );

	           $i++;
	      }

	      $output = array(
	            "draw" => $draw,
	            "recordsTotal" => $peserta->num_rows(),
	            "recordsFiltered" => $peserta->num_rows(),
	            "data" => $data
	        );

	      echo json_encode($output);
	      exit();
	}

	public function disetujui(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		// $data["debiturklaim"] = $this->klaim->debiturklaimdisetujui();
		$this->app->render('Klaim Disetujui','klaim/disetujui',$data);
	}

	public function filter_disetujui(){
		  $draw = intval($this->input->post("draw"));
	      $start = intval($this->input->post("start"));
	      $length = intval($this->input->post("length"));

	      $peserta = $this->klaim->debiturklaimdisetujui();
			
	      $data = array();
	      $i = 1;

	      foreach($peserta->result() as $key) {
	      		
	            $pengajuan = '<a href="'.base_url().'klaim/formKeputusan/S'.$key->NomorRegistrasi.'" class="btn btn-xs btn-info" style="color: #ffffff;">View</a>';
	      		
	      		if($key->keputusan==1){
	      			$hari = lead_time($key->TglKlaim,$key->create_selesai);
	      		}
                
	              $data[] = array(
	              	// '',
	                  my_date($key->create_selesai),
	                  $key->NomorPinjaman,
	                  $key->NamaDebitur,
	                  price($key->NominalDisetujui),
	                  cekProduk($key->idProduk),
	                  cekNamaBank($key->kodeBank),
	                  cekNamaAsdur($key->kodeAsuransi),
	                  $hari,
	                  $pengajuan
	             );

	           $i++;
	      }

	      $output = array(
	            "draw" => $draw,
	            "recordsTotal" => $peserta->num_rows(),
	            "recordsFiltered" => $peserta->num_rows(),
	            "data" => $data
	        );

	      echo json_encode($output);
	      exit();
	}

	public function formKeputusan($noreg){
		$this->app->setActive('klaim/formKeputusan');
		$data["status"] = substr($noreg, 0,1);
		if ($data["status"]=="S") {
			$data["status"] = "disetujui";
			$noreg = str_replace("S","",$noreg);
		}else{
			$data["status"] = "menunggu";
			$noreg = str_replace("M","",$noreg);
		}
		$data["NomorRegistrasi"] = $noreg;
		$data["NomorPinjaman"] = $this->klaim->NomorPinjaman($noreg);
		$data["debitur"] = $this->klaim->debitur($noreg);
		$data["resiko"] = $this->helper->Resiko();
		$data["umum"] = $this->klaim->DokUmum();
		$data["pinjaman"] = $this->klaim->pinjaman($noreg);
		$data["khusus"] = $this->klaim->DokKhusus2('KLM'.$noreg);
		if($this->session->userdata("hak_akses")==5 && $this->session->userdata("roles")==2){
			if($data["status"]=="disetujui"){
				$this->app->render('Data Debitur Klaim','klaim/formKeputusan',$data);
			}else{
				$this->app->render('Data Debitur Klaim','klaim/formKlaimUpdate',$data);
			}
		}else{
			$this->app->render('Data Debitur Klaim','klaim/formKeputusan',$data);
		}
	}

	public function formKlaimUpdate($noreg){
		$this->app->setActive('klaim/formKeputusan');
		$data["status"] = substr($noreg, 0,1);
		if ($data["status"]=="S") {
			$data["status"] = "disetujui";
			$noreg = str_replace("S","",$noreg);
		}else{
			$data["status"] = "menunggu";
			$noreg = str_replace("M","",$noreg);
		}
		$data["NomorRegistrasi"] = $noreg;
		$data["NomorPinjaman"] = $this->klaim->NomorPinjaman($noreg);
		$data["debitur"] = $this->klaim->debitur($noreg);
		$data["resiko"] = $this->helper->Resiko();
		$data["umum"] = $this->klaim->DokUmum();
		$data["pinjaman"] = $this->klaim->pinjaman($noreg);
		$data["khusus"] = $this->klaim->DokKhusus2('KLM'.$noreg);
		$this->app->render('Data Debitur Klaim','klaim/formKlaimUpdate',$data);
	}

	public function approveSpv($noreg){
		$kode = str_replace("KLM", "", $noreg);
		$tgl = hariini();
		$user = create_user();
		$update = $this->db->query("update t_pinjaman_klaim set spv = 1, create_spv = '$tgl', user_spv = '$user' where KodeKlaim = '$noreg'");
		// if($this->db->affected_rows>0){
			$this->session->set_flashdata("success","APPROVE berhasil disimpan");
		// }else{
		// 	$this->session->set_flashdata("danger","APPROVE gagal disimpan");
		// }
		redirect('klaim/formKeputusan/'.$kode);
	}

	public function tolakSpv($noreg){
		$kode = str_replace("KLM", "", $noreg);
		$tgl = hariini();
		$user = create_user();
		// $insert = $this->db->query("insert into t_pinjaman_klaim_ditolak select KodeKlaim,NomorRegistrasi,TglKlaim,TglResiko,NominalDiajukan,idResiko,kronologi,keputusan,NominalDisetujui,keterangan,diajukan,spv,pialang,asuransi,selesai,create_user,create_date,update_user,update_date,create_spv,create_pialang,create_asuransi,create_selesai,user_spv,user_pialang,user_asuransi,'Ditolak SPV' from t_pinjaman_klaim where KodeKlaim = '$kode'");
		$update = $this->db->query("update t_pinjaman set StatusKlaim = 0 where NomorRegistrasi = '$kode'");
		$update2 = $this->db->query("delete from t_pinjaman_klaim_dokumen where KodeKlaim = '$noreg'");
		$update2 = $this->db->query("delete from t_pinjaman_klaim where KodeKlaim = '$noreg'");
		// if($this->db->affected_rows>0){
			$this->session->set_flashdata("success","Reject berhasil disimpan");
		// }else{
			// $this->session->set_flashdata("danger","Reject gagal disimpan");
		// }		
		redirect('klaim/formKlaim/'.$kode);
	}

	public function approvePialang($noreg){
		$kode = str_replace("KLM", "", $noreg);
		$tgl = hariini();
		$user = create_user();
		$update = $this->db->query("update t_pinjaman_klaim set pialang = 1, create_pialang = '$tgl', user_pialang = '$user' where KodeKlaim = '$noreg'");
		// if($this->db->affected_rows>0){
			$this->session->set_flashdata("success","APPROVE berhasil disimpan");
		// }else{
			// $this->session->set_flashdata("danger","APPROVE gagal disimpan");
		// }
		redirect('klaim/formKeputusan/'.$kode);
	}

	public function saveKeputusan(){
		$kode = str_replace("KLM", "", $this->input->post("KodeKlaim"));
		$saveKeputusan = $this->klaim->saveKeputusan();
		// if($this->db->affected_rows()>0){
		// if($this->db->affected_rows>0){
			$this->session->set_flashdata("success","APPROVE berhasil disimpan");
		// }else{
			// $this->session->set_flashdata("danger","APPROVE gagal disimpan");
		// }
		redirect('klaim/formKeputusan/S'.$kode);
		// }
	}

	public function batal($noreg){
		$kode = str_replace("KLM", "", $noreg);
		$tgl = hariini();
		$user = create_user();
		// $insert = $this->db->query("insert into t_pinjaman_klaim_ditolak select KodeKlaim,NomorRegistrasi,TglKlaim,TglResiko,NominalDiajukan,idResiko,kronologi,keputusan,NominalDisetujui,keterangan,diajukan,spv,pialang,asuransi,selesai,create_user,create_date,update_user,update_date,create_spv,create_pialang,create_asuransi,create_selesai,user_spv,user_pialang,user_asuransi,'Ditolak SPV' from t_pinjaman_klaim where KodeKlaim = '$kode'");
		$update = $this->db->query("update t_pinjaman set StatusKlaim = 0 where NomorRegistrasi = '$kode'");
		$update2 = $this->db->query("delete from t_pinjaman_klaim_dokumen where KodeKlaim = '$noreg'");
		$update2 = $this->db->query("delete from t_pinjaman_klaim where KodeKlaim = '$noreg'");
		// if($this->db->affected_rows>0){
			$this->session->set_flashdata("success","Pembatalan berhasil");
		// }else{
			// $this->session->set_flashdata("danger","Reject gagal disimpan");
		// }		
		redirect('klaim/formKlaim/'.$kode);
	}
}
