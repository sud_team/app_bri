<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restitusi extends CI_Controller {

	public function __Construct(){
		parent::__Construct();
		$this->app->auth();
		$this->load->model('Model_helper','helper');
		$this->load->model('Model_restitusi','restitusi');
	}

	public function formRestitusi($noreg){
		$data["NomorPinjaman"] = $this->restitusi->NomorPinjaman($noreg);
		$data["debitur"] = $this->restitusi->debitur($noreg);
		$data["resiko"] = $this->helper->Resiko();
		$data["umum"] = $this->restitusi->DokUmum();
		$data["pinjaman"] = $this->restitusi->pinjaman($noreg);
		// $data["dokumenumum"] = $this->restitusi->pinjamandokumen("KLM".$noreg);
		$data["resdok"] = $this->restitusi->resdok('RS'.$noreg);
		$this->app->render('Data Debitur Restitusi','restitusi/formRestitusi',$data);
	}

	public function DokKhusus($idResiko){
		$data["khusus"] = $this->restitusi->DokKhusus($idResiko);
		echo '<tbody>';
		$ab = 1;
		foreach ($data["khusus"] as $key) {
            echo '<tr>
                <td class="w-3">&nbsp;</td>
                <td class="w-50">'.$key->dokumen.'</td>
                <td align="right">
                    <div class="form-actions p-2">
                        <label  class="btn btn-outline-warning btn-sm">
                            <input type="hidden" name="ab[]" value="'.$ab.'">
                            <input type="hidden" name="khusus'.$ab.'" value="'.$key->dokumen.'">
                            <input type="file" name="filekhusus'.$ab.'">
                        </label>
                    </div>
                </td>
            </tr>';
		$ab++;
		}
        echo '</tbody>';
	}

	public function saveRestitusi(){
		$KodeRestitusi = $this->input->post("KodeRestitusi");
		$noreg = $this->input->post("NomorRegistrasi");
		$save = $this->restitusi->saveRestitusi();
		if($this->db->affected_rows()>0){
			$config['upload_path'] = './upload/restitusi/';
			$config['allowed_types'] = 'xls|xlsx|doc|docx|pdf|jpg|jpeg|txt';
		    $config['max_size'] = '30000';
		    $config['max_width']  = '8000'; 
		    $config['max_height']  = '7768';

		    for ($i=1; $i <= 3 ; $i++) { 
		    	$dokumen = $this->input->post("namaDokumen".$i);
			    
			    $config['file_name']  = $KodeRestitusi.$dokumen;
		
			    $this->load->library('upload', $config);
			    $this->upload->initialize($config);

		        if(!empty($this->upload->do_upload("fileumum".$i))){
		        	$upload_dataumum = $this->upload->data(); 
			        $this->restitusi->saveRestitusiDokumen($KodeRestitusi,$dokumen,$upload_dataumum["file_name"],$i);
		        }
		    }

		}

		if($save){
			$this->session->set_flashdata("success","Restitusi Berhasil Diajukan");
		}else{
			$this->session->set_flashdata("danger","Restitusi Gagal Diajukan");
		}

		redirect("restitusi/formKeputusan/".$noreg);
	}
	
	public function menunggu(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		// $data["debiturrestitusi"] = $this->restitusi->debiturrestitusi();
		$this->app->render('List Restitusi','restitusi/menungguRestitusi',$data);
	}

	public function filter_menunggu(){
		  $draw = intval($this->input->post("draw"));
	      $start = intval($this->input->post("start"));
	      $length = intval($this->input->post("length"));

	      $peserta = $this->restitusi->debiturrestitusi();
			
	      $data = array();
	      $i = 1;

	      foreach($peserta->result() as $key) {
	      		if($this->session->userdata("roles")==2 && $this->session->userdata("hak_akses")==5){
	      			if($key->spv==1){
	            		$pengajuan = '<a href="'.base_url().'restitusi/formKeputusan/M'.$key->NomorRegistrasi.'" class="btn btn-xs btn-info" style="color: #ffffff;">View</a>';
	      			}else{
	            		$pengajuan = '<a href="'.base_url().'restitusi/formKeputusan/M'.$key->NomorRegistrasi.'" class="btn btn-xs btn-danger" style="color: #ffffff;">Proses</a>';
	      			}
	      		}else{
	            		$pengajuan = '<a href="'.base_url().'restitusi/formKeputusan/M'.$key->NomorRegistrasi.'" class="btn btn-xs btn-danger" style="color: #ffffff;">view</a>';
	      		}

	      		if($key->keputusan==0){
	      			$hari = lead_time($key->TglPengajuanRestitusi,date("Y-m-d H:i:s"));
	      		}

	      		if($key->keputusan==1){
	      			$hari = lead_time($key->TglPengajuanRestitusi,$key->TglPelunasanRestitusi);
	      		}


	              $data[] = array(
	              	'',
	                  my_date($key->TglPengajuanRestitusi),
	                  $key->NomorPinjaman,
	                  $key->NamaDebitur,
	                  my_date($key->TglLahir),
	                  str_replace(".", " Tahun ", $key->UsiaDebitur)." Bulan",
	                  cekPekerjaan($key->KodePekerjaan),
	                  my_date($key->TglAkadKredit),
	                  $key->TenorTahun,
	                  price($key->plafon),
	                  cekProduk($key->idProduk),
	                  cekNamaBank($key->kodeBank),
	                  cekNamaAsdur($key->kodeAsuransi),
	                  $hari,
	                  $pengajuan
	             );

	           $i++;
	      }

	      $output = array(
	            "draw" => $draw,
	            "recordsTotal" => $peserta->num_rows(),
	            "recordsFiltered" => $peserta->num_rows(),
	            "data" => $data
	        );

	      echo json_encode($output);
	      exit();
	}

	public function ditangguhkan(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		$data["debiturrestitusi"] = $this->restitusi->debiturrestitusiditangguhkan();
		$this->app->render('List Restitusi','restitusi/ditangguhkan',$data);
	}

	public function ditolak(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		$data["debiturrestitusi"] = $this->restitusi->debiturrestitusiditolak();
		$this->app->render('List Restitusi','restitusi/ditolak',$data);
	}

	public function disetujui(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		// $data["debiturrestitusi"] = $this->restitusi->debiturrestitusidisetujui();
		$this->app->render('Restitusi Disetujui','restitusi/disetujui',$data);
	}

	public function filter_disetujui(){
		  $draw = intval($this->input->post("draw"));
	      $start = intval($this->input->post("start"));
	      $length = intval($this->input->post("length"));

	      $peserta = $this->restitusi->debiturrestitusidisetujui();
			
	      $data = array();
	      $i = 1;

	      foreach($peserta->result() as $key) {
	      		
	            $pengajuan = '<a href="'.base_url().'restitusi/formKeputusan/S'.$key->NomorRegistrasi.'" class="btn btn-xs btn-info" style="color: #ffffff;">View</a>';
	      		
	      		if($key->keputusan==1){
	      			$hari = lead_time($key->TglPengajuanRestitusi,$key->TglPelunasanRestitusi);
	      		}
                
	              $data[] = array(
	              	// '',
	                  my_date($key->TglPelunasanRestitusi),
	                  $key->NomorPinjaman,
	                  $key->NamaDebitur,
	                  price($key->PengembalianPremi),
	                  cekProduk($key->idProduk),
	                  cekNamaBank($key->kodeBank),
	                  cekNamaAsdur($key->kodeAsuransi),
	                  $hari,
	                  $pengajuan
	             );

	           $i++;
	      }

	      $output = array(
	            "draw" => $draw,
	            "recordsTotal" => $peserta->num_rows(),
	            "recordsFiltered" => $peserta->num_rows(),
	            "data" => $data
	        );

	      echo json_encode($output);
	      exit();
	}

	public function formKeputusan($noreg){
		$this->app->setActive('restitusi/formKeputusan');
		$data["status"] = substr($noreg, 0,1);
		if ($data["status"]=="S") {
			$data["status"] = "disetujui";
			$noreg = str_replace("S","",$noreg);
		}else{
			$data["status"] = "menunggu";
			$noreg = str_replace("M","",$noreg);
		}
		$data["NomorPinjaman"] = $this->restitusi->NomorPinjaman($noreg);
		$data["debitur"] = $this->restitusi->debitur($noreg);
		$data["pinjaman"] = $this->restitusi->pinjaman($noreg);
		$data["resdok"] = $this->restitusi->resdok('RS'.$noreg);
		if($this->session->userdata("hak_akses")==5 && $this->session->userdata("roles")==2){
			if($data["status"]=="disetujui"){
				$this->app->render('Data Debitur Restitusi','restitusi/formKeputusan',$data);
			}else{
				$this->app->render('Data Debitur Restitusi','restitusi/formRestitusiUpdate',$data);
			}
		}else{
			$this->app->render('Data Debitur Restitusi','restitusi/formKeputusan',$data);
		}
	}

	public function formRestitusiUpdate($noreg){
		$this->app->setActive('restitusi/formKeputusan');
		$data["status"] = substr($noreg, 0,1);
		if ($data["status"]=="S") {
			$data["status"] = "disetujui";
			$noreg = str_replace("S","",$noreg);
		}else{
			$data["status"] = "menunggu";
			$noreg = str_replace("M","",$noreg);
		}
		$data["NomorPinjaman"] = $this->restitusi->NomorPinjaman($noreg);
		$data["debitur"] = $this->restitusi->debitur($noreg);
		$data["pinjaman"] = $this->restitusi->pinjaman($noreg);
		$data["resdok"] = $this->restitusi->resdok('RS'.$noreg);
		$this->app->render('Data Debitur Restitusi','restitusi/formRestitusiUpdate',$data);
	}

	public function approveSpv($noreg){
		$kode = str_replace("RS", "", $noreg);
		$tgl = hariini();
		$user = create_user();
		$update = $this->db->query("update t_pinjaman_restitusi set spv = 1, create_spv = '$tgl', user_spv = '$user' where KodeRestitusi = '$noreg'");
		$this->session->set_flashdata("success","APPROVE Berhasil Diajukan");
		redirect('restitusi/formKeputusan/'.$kode);
	}

	public function approvePialang($noreg){
		$kode = str_replace("RS", "", $noreg);
		$tgl = hariini();
		$user = create_user();
		$update = $this->db->query("update t_pinjaman_restitusi set pialang = 1, create_pialang = '$tgl', user_pialang = '$user' where KodeRestitusi = '$noreg'");
		$this->session->set_flashdata("success","APPROVE Berhasil Diajukan");
		redirect('restitusi/formKeputusan/'.$kode);
	}

	public function saveKeputusan(){
		$kode = str_replace("RS", "", $this->input->post("KodeRestitusi"));
		$saveKeputusan = $this->restitusi->saveKeputusan();
		// if($this->db->affected_rows()>0){
		$this->session->set_flashdata("success","APPROVE Berhasil Diajukan");
		redirect('restitusi/formKeputusan/S'.$kode);
		// }
	}

	public function tolakSpv($noreg){
		$kode = str_replace("RS", "", $noreg);
		$nor = $this->input->post("NomorRegistrasi");
		$tgl = hariini();
		$user = create_user();
		$update = $this->db->query("update t_pinjaman set StatusRestitusi = 0 where NomorRegistrasi = '$kode'");
		$update2 = $this->db->query("delete from t_pinjaman_restitusi_dokumen where KodeRestitusi = '$noreg'");
		$update2 = $this->db->query("delete from t_pinjaman_restitusi where KodeRestitusi = '$noreg'");
		// if($this->db->affected_rows>0){
			$this->session->set_flashdata("success","Reject berhasil");
		// }else{
			// $this->session->set_flashdata("danger","Reject gagal disimpan");
		// }		
		redirect('restitusi/formRestitusi/'.$kode);
	}

	public function batal($noreg){
		$kode = str_replace("RS", "", $noreg);
		$tgl = hariini();
		$user = create_user();
		$update = $this->db->query("update t_pinjaman set StatusRestitusi = 0 where NomorRegistrasi = '$kode'");
		$update2 = $this->db->query("delete from t_pinjaman_restitusi_dokumen where KodeRestitusi = '$noreg'");
		$update2 = $this->db->query("delete from t_pinjaman_restitusi where KodeRestitusi = '$noreg'");
		// if($this->db->affected_rows>0){
			$this->session->set_flashdata("success","Pembatalan berhasil");
		// }else{
			// $this->session->set_flashdata("danger","Reject gagal disimpan");
		// }		
		redirect('restitusi/formRestitusi/'.$kode);
	}

	public function hitungrestitusi(){
		$tglpelunasan = $this->input->post("tglpelunasan");
		$noreg = $this->input->post("noreg");
		$query = $this->db->query("select TenorBulan, TglAkadKredit, JumlahPremiTenor, KodePekerjaan from t_pinjaman where NomorRegistrasi = '$noreg'");
		$Tenor = $query->row()->TenorBulan;
		$Akad = $query->row()->TglAkadKredit;
		$Premi = $query->row()->JumlahPremiTenor;
		$KodePekerjaan = $query->row()->KodePekerjaan;
		if($KodePekerjaan=="BJ"){
			$querybj = $this->db->query("select ospremi, min(TahunKe) tahun from t_pinjaman_bj where NomorRegistrasi = '$noreg' and StatusBayar = 1 group by ospremi");
			$Premi = $querybj->row()->ospremi;
			$Tenor = 12;
		}

		$datetime1 = new DateTime($Akad);
		$datetime2 = new DateTime($tglpelunasan);
		$difference = $datetime1->diff($datetime2);

		$selisih = $difference->m;
		$sisa = $Tenor-$selisih;
		$sisapremi = ($sisa/$Tenor) * (35*$Premi / 100);

		$data['result']= array(
			'selisih' => $selisih,
			'sisa' => $sisa,
			'sisapremi' => price($sisapremi)
		);

		echo json_encode($data);
		exit;
	}
}
