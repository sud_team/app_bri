<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ftp extends CI_Controller {

    public function __Construct(){
        parent::__Construct();
        $this->app->auth();
        $this->load->library('ftp');
        $this->load->model("Model_dashboard","dash");
        $this->load->helper("download");
    }

    public function index(){
        $this->app->render('List File Ftp','ftp',null);
    }

    public function ftp_list(){

        $config['hostname'] = '172.22.0.2';
        $config['username'] = 'JTMBROKER04';
        $config['password'] = 'admin!123';
        $config['debug']    = TRUE;

        $conn = $this->ftp->connect($config);

        $list = $this->ftp->list_files('/PENUTUPAN/');

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));

            
        $data = array();
        $i = 1;

        for ($i=0; $i < count($list) ; $i++) {
            if (preg_match("/AGRA/i", $list[$i])) {
                $data[] = array(
                    my_date_indo(str_replace("-01.csv","",str_replace("/PENUTUPAN/AGRA","",$list[$i]))),
                    str_replace("/PENUTUPAN/","",$list[$i]),
                    '<a href="'.base_url().'ftp/ftp_download/'.str_replace("/PENUTUPAN/","",$list[$i]).'" target="_blank" class="btn btn-info btn-xs" style="color:#ffffff">Download</a>',
                );
            }
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data
        );

        echo json_encode($output);
        exit();


        $this->ftp->close();
    }

    public function ftp_download($file){
        $config['hostname'] = '172.22.0.2';
        $config['username'] = 'JTMBROKER04';
        $config['password'] = 'admin!123';
        $config['debug']    = TRUE;

        $conn = $this->ftp->connect($config);

        $path = "/PENUTUPAN/".$file;
        $local = "/data/www/html/via/upload/ftp/01/".$file;

        $get = $this->ftp->download($path, $local, 'auto');
        if($get){
            force_download($local, NULL);
        }else{
            echo "<script><alert>Get Gagal</alert></script>";
        }

        $this->ftp->close();
    }

    public function ftp_history(){

        $config['hostname'] = '172.22.0.2';
        $config['username'] = 'JTMBROKER04';
        $config['password'] = 'admin!123';
        $config['debug']    = TRUE;

        $conn = $this->ftp->connect($config);

        $list = $this->ftp->list_files('/PENUTUPAN/');

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));

            
        $data = array();
        $i = 1;

        for ($i=0; $i < count($list) ; $i++) {
            if (!preg_match("/AGRA/i", $list[$i])) {
                $data[] = array(
                    str_replace("/PENUTUPAN/","",$list[$i]),
                    '<a href="'.base_url().'ftp/ftp_download_history/'.str_replace("/PENUTUPAN/","",$list[$i]).'" target="_blank" class="btn btn-info btn-xs" style="color:#ffffff">Download</a>',
                );
            }
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data
        );

        echo json_encode($output);
        exit();


        $this->ftp->close();
    }

    public function ftp_download_history($file){
        $config['hostname'] = '172.22.0.2';
        $config['username'] = 'JTMBROKER04';
        $config['password'] = 'admin!123';
        $config['debug']    = TRUE;

        $conn = $this->ftp->connect($config);

        $path = "/PENUTUPAN/".$file;
        $local = "/data/www/html/via/upload/ftp/01/".$file;

        $get = $this->ftp->download($path, $local, 'auto');
        if($get){
            force_download($local, NULL);
        }else{
            echo "<script><alert>Get Gagal</alert></script>";
        }

        $this->ftp->close();
    }

    public function ftp_upload_history(){
        if($this->input->post("Upload")){
            $config['upload_path'] = './upload/ftp/01/';
            $config['allowed_types'] = 'csv';
            $config['max_size'] = '30000';
            $config['max_width']  = '8000'; 
            $config['max_height']  = '7768';

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            $this->upload->do_upload("upload_file");
            $upload_data = $this->upload->data(); 

            if($upload_data["file_ext"]!=".csv"){
                echo "File Harus format csv";
            }else{
                $config['hostname'] = '172.22.0.2';
                $config['username'] = 'JTMBROKER04';
                $config['password'] = 'admin!123';
                $config['debug']    = TRUE;

                $conn = $this->ftp->connect($config);

                $path = "/DATA_GAGAL/".$upload_data["file_name"];
                $local = "/data/www/html/via/upload/ftp/01/".$upload_data["file_name"];

                $this->ftp->upload($local, $path, 'auto');
                
            }
        }

        $this->ftp->close();

        redirect("ftp");
    }

    public function ftp_upload02(){
        if($this->input->post("Upload")){
            $config['upload_path'] = './upload/ftp/02/';
            $config['allowed_types'] = 'csv';
            $config['max_size'] = '30000';
            $config['max_width']  = '8000'; 
            $config['max_height']  = '7768';

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            $this->upload->do_upload("upload_file");
            $upload_data = $this->upload->data(); 

            if($upload_data["file_ext"]!=".csv"){
                echo "File Harus format csv";
            }else{
                $config['hostname'] = '172.22.0.2';
                $config['username'] = 'JTMBROKER04';
                $config['password'] = 'admin!123';
                $config['debug']    = TRUE;

                $conn = $this->ftp->connect($config);

                $path = "/DATA_GAGAL/".$upload_data["file_name"];
                $local = "/data/www/html/via/upload/ftp/02/".$upload_data["file_name"];

                $this->ftp->upload($local, $path, 'auto');
                
            }
        }

        $this->ftp->close();

        redirect("ftp");
    }

    public function ftp_list02(){

        $config['hostname'] = '172.22.0.2';
        $config['username'] = 'JTMBROKER04';
        $config['password'] = 'admin!123';
        $config['debug']    = TRUE;

        $conn = $this->ftp->connect($config);

        $list = $this->ftp->list_files('/DATA_GAGAL/');

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));

            
        $data = array();
        $i = 1;

        for ($i=0; $i < count($list) ; $i++) {
            $data[] = array(
                my_date_indo(str_replace("-02.csv","",str_replace("/DATA_GAGAL/AGRA","",$list[$i]))),
                str_replace("/DATA_GAGAL/","",$list[$i]),
                '<a href="'.base_url().'upload/ftp/02'.str_replace("/DATA_GAGAL","",$list[$i]).'" target="_blank" class="btn btn-info btn-xs" style="color:#ffffff">Download</a>',
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data
        );

        echo json_encode($output);
        exit();


        $this->ftp->close();
    }

    public function ftp_upload03(){
        if($this->input->post("Upload")){
            $config['upload_path'] = './upload/ftp/03/';
            $config['allowed_types'] = 'csv';
            $config['max_size'] = '30000';
            $config['max_width']  = '8000'; 
            $config['max_height']  = '7768';

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            $this->upload->do_upload("upload_file");
            $upload_data = $this->upload->data(); 

            if($upload_data["file_ext"]!=".csv"){
                echo "File Harus format csv";
            }else{
                $config['hostname'] = '172.22.0.2';
                $config['username'] = 'JTMBROKER04';
                $config['password'] = 'admin!123';
                $config['debug']    = TRUE;

                $conn = $this->ftp->connect($config);

                $path = "/DATA_BERHASIL/".$upload_data["file_name"];
                $local = "/data/www/html/via/upload/ftp/03/".$upload_data["file_name"];

                $this->ftp->upload($local, $path, 'auto');
                
            }
        }

        $this->ftp->close();

        redirect("ftp");
    }

    public function ftp_list03(){

        $config['hostname'] = '172.22.0.2';
        $config['username'] = 'JTMBROKER04';
        $config['password'] = 'admin!123';
        $config['debug']    = TRUE;

        $conn = $this->ftp->connect($config);

        $list = $this->ftp->list_files('/DATA_BERHASIL/');

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));

            
        $data = array();
        $i = 1;

        for ($i=0; $i < count($list) ; $i++) {
            $data[] = array(
                my_date_indo(str_replace("-03.csv","",str_replace("/DATA_BERHASIL/AGRA","",$list[$i]))),
                str_replace("/DATA_BERHASIL/","",$list[$i]),
                '<a href="'.base_url().'upload/ftp/03'.str_replace("/DATA_BERHASIL","",$list[$i]).'" target="_blank" class="btn btn-info btn-xs" style="color:#ffffff">Download</a>',
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data
        );

        echo json_encode($output);
        exit();


        $this->ftp->close();
    }

    public function ftp_upload04(){
        if($this->input->post("Upload")){
            $config['upload_path'] = './upload/ftp/04/';
            $config['allowed_types'] = 'csv';
            $config['max_size'] = '30000';
            $config['max_width']  = '8000'; 
            $config['max_height']  = '7768';

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            $this->upload->do_upload("upload_file");
            $upload_data = $this->upload->data(); 

            if($upload_data["file_ext"]!=".csv"){
                echo "File Harus format csv";
            }else{
                $config['hostname'] = '172.22.0.2';
                $config['username'] = 'JTMBROKER04';
                $config['password'] = 'admin!123';
                $config['debug']    = TRUE;

                $conn = $this->ftp->connect($config);

                $path = "/DATA_RESTITUSI/".$upload_data["file_name"];
                $local = "/data/www/html/via/upload/ftp/04/".$upload_data["file_name"];

                $this->ftp->upload($local, $path, 'auto');
                
            }
        }

        $this->ftp->close();

        redirect("ftp");
    }

    public function ftp_list04(){

        $config['hostname'] = '172.22.0.2';
        $config['username'] = 'JTMBROKER04';
        $config['password'] = 'admin!123';
        $config['debug']    = TRUE;

        $conn = $this->ftp->connect($config);

        $list = $this->ftp->list_files('/DATA_RESTITUSI/');

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));

            
        $data = array();
        $i = 1;

        for ($i=0; $i < count($list) ; $i++) {
            $data[] = array(
                my_date_indo(str_replace("-04.csv","",str_replace("/DATA_RESTITUSI/AGRA","",$list[$i]))),
                str_replace("/DATA_RESTITUSI/","",$list[$i]),
                '<a href="'.base_url().'upload/ftp/04'.str_replace("/DATA_RESTITUSI","",$list[$i]).'" target="_blank" class="btn btn-info btn-xs" style="color:#ffffff">Download</a>',
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data
        );

        echo json_encode($output);
        exit();


        $this->ftp->close();
    }

    public function ftp_upload05($file){
        if($this->input->post("Upload")){
            $config['upload_path'] = './upload/ftp/05/';
            $config['allowed_types'] = 'csv';
            $config['max_size'] = '30000';
            $config['max_width']  = '8000'; 
            $config['max_height']  = '7768';

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            $this->upload->do_upload("upload_file");
            $upload_data = $this->upload->data(); 

            if($upload_data["file_ext"]!=".csv"){
                echo "File Harus format csv";
            }else{
                $config['hostname'] = '172.22.0.2';
                $config['username'] = 'JTMBROKER04';
                $config['password'] = 'admin!123';
                $config['debug']    = TRUE;

                $conn = $this->ftp->connect($config);

                $path = "/DATA_KLAIM/".$upload_data["file_name"];
                $local = "/data/www/html/via/upload/ftp/05/".$upload_data["file_name"];

                $this->ftp->upload($local, $path, 'auto');
                
            }
        }

        $this->ftp->close();

        redirect("ftp");
    }

     public function ftp_list05(){

        $config['hostname'] = '172.22.0.2';
        $config['username'] = 'JTMBROKER04';
        $config['password'] = 'admin!123';
        $config['debug']    = TRUE;

        $conn = $this->ftp->connect($config);

        $list = $this->ftp->list_files('/DATA_KLAIM/');

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));

            
        $data = array();
        $i = 1;

        for ($i=0; $i < count($list) ; $i++) {
            $data[] = array(
                my_date_indo(str_replace("-05.csv","",str_replace("/DATA_KLAIM/AGRA","",$list[$i]))),
                str_replace("/DATA_KLAIM/","",$list[$i]),
                '<a href="'.base_url().'upload/ftp/05'.str_replace("/DATA_KLAIM","",$list[$i]).'" target="_blank" class="btn btn-info btn-xs" style="color:#ffffff">Download</a>',
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data
        );

        echo json_encode($output);
        exit();


        $this->ftp->close();
    }

}
