<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produksi extends CI_Controller {

	public function __Construct(){
		parent::__Construct();
		$this->app->auth();
		$this->load->model('Model_helper','helper');
		$this->load->model('Model_produksi','produksi');
	}

	public function upload(){
		error_reporting(0);
		$datax["debitur"] = "";
		if($this->input->post("Upload")){
			$datax["array"] = "";
			$berhasil = 0;
	
			$config['upload_path'] = './upload/produksi/';
			$config['allowed_types'] = 'csv';
		    $config['max_size'] = '30000';
		    $config['max_width']  = '8000'; 
		    $config['max_height']  = '7768';

		    $this->load->library('upload', $config);
		    $this->upload->initialize($config);

		    $this->upload->do_upload("upload_file");
	        $upload_data = $this->upload->data(); 

	        if($upload_data["file_ext"]!=".csv"){
	        	echo "File Harus format csv";
	        }else{
	        	$data = array(); 		      
		        include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		        
		        $csvreader = PHPExcel_IOFactory::createReader('CSV')->setDelimiter("|");;
		        $loadcsv = $csvreader->load($upload_data["full_path"]);
		        $this->session->set_userdata("file",$upload_data["full_path"]);
		        $sheet = $loadcsv->getActiveSheet()->getRowIterator(0);
		        $namafile = $upload_data["raw_name"];
		        $datax["file02"] = $upload_data["raw_name"]."-02";
		        $datax["file03"] = $upload_data["raw_name"]."-03";

			    $numrow = 1;
			    foreach($sheet as $row){

			      	if($numrow > 1){
			        	$cellIterator = $row->getCellIterator();
			        	$cellIterator->setIterateOnlyExistingCells(false);


				        $get = array(); 
				        foreach ($cellIterator as $cell) {
				          array_push($get, $cell->getValue()); 
				        }	  

						$cif = $get[0]; 
				        $nopin = $get[1]; 
				        $kode = $get[2]; 
				        $nama = $get[3]; 
				        $loan = $get[4]; 
				        $jk = $get[5]; 
				        $ktp = $get[6]; 
				        $tmpt = $get[7]; 
				        $tgl = $get[8]; 
				        $alamat = $get[9]; 
				        $pekerjaan = $get[10]; 
				        $pk = $get[11]; 
				        $tglakad = $get[12]; 
				        $tglakhir = $get[13]; 
				        $plafon = $get[14]; 
				        $premi = $get[15]; 
				        $ref = $get[16]; 
				        $top = $get[17]; 
				        $asdur = $get[18]; 
				        
				        array_push($data, [
				          	'cif' => $get[0], 
					        'nopin' => $get[1], 
					        'kode' => $get[2], 
					        'nama' => $get[3], 
					        'loan' => $get[4], 
					        'jk' => $get[5], 
					        'ktp' => $get[6], 
					        'tmpt' => $get[7], 
					        'tgl' => $get[8], 
					        'alamat' => $get[9], 
					        'pekerjaan' => $get[10], 
					        'pk' => $get[11], 
					        'tglakad' => $get[12], 
					        'tglakhir' => $get[13], 
					        'plafon' => $get[14], 
					        'premi' => $get[15], 
					        'ref' => $get[16], 
					        'top' => $get[17], 
					        'asdur' => $get[18]
				        ]);		       
			      	}
			      

			      $numrow++;
			    }


			    $datax["array"] = $data;

			    $datax["debitur"] .= "<tbody>"; 
			    $no = 1;

			    $saveUrut = $this->produksi->t_urut_upload(count($data));

			    $urut = $this->produksi->cekNomorUrut();

			    $datax["detail"] = '<table id="uat03" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">';
		        $datax["detail"] .= '<thead>';
		        $datax["detail"] .= '<tr>';
		        $datax["detail"] .= '<td style="text-align: center;">Kode</td>';
		        $datax["detail"] .= '<td style="text-align: center;">&nbsp;</td>';
		        $datax["detail"] .= '</tr>';
		        $datax["detail"] .= '</thead>';
		        $datax["detail"] .= '<tbody>';

		        $datax["gagal"] = '<table id="uat02" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">';
		        $datax["gagal"] .= '<thead>';
		        $datax["gagal"] .= '<tr>';
		        $datax["gagal"] .= '<td style="text-align: center;">Kode</td>';
		        $datax["gagal"] .= '<td style="text-align: center;">&nbsp;</td>';
		        $datax["gagal"] .= '</tr>';
		        $datax["gagal"] .= '</thead>';
		        $datax["gagal"] .= '<tbody>';
			    $xyz = "";

			    $NomorInvoice = "INV".$kode.date("ymdHis");
			    foreach ($data as $row) {
			    	$cekBank = $this->helper->cekBank($row["kode"]);
			    	if($cekBank){
			    		$cekAsdur = $this->helper->cekAsdur($row["asdur"]);
			    		if($cekAsdur){
			    			$umur = round(umur($row["tgl"],$row["tglakad"]),0);
			    			$umur_asli = umur($row["tgl"],$row["tglakad"]);
					    	$batas = $umur+round(umur($row["tglakad"],$row["tglakhir"]),0);
					    	$batas_asli = $umur+umur($row["tglakad"],$row["tglakhir"]);
					    	$tenor = ceil(umur($row["tglakad"],$row["tglakhir"]));
					    	$tenorbulan = round(umur($row["tglakad"],$row["tglakhir"]),0)*12;

					    	$StatusPinjaman = $this->helper->StatusPinjaman($umur,$row["plafon"],$batas,$row["pekerjaan"]);
					    	$nopin = $this->helper->cekNoPinjaman($row["nopin"]);
					    	$top = $this->helper->cekStatusPinjaman($row["cif"]);
					    	$cekKodePekerjaan = $this->helper->cekKodePekerjaan($row["pekerjaan"]);
					    	$cekRatePremiProduk = $this->helper->cekRatePremiProduk($row["asdur"]);

					    	if(!$StatusPinjaman){
					    		$status = "Umur Tidak Sesuai";
					    		$sesuai = -1;
					    	}else{
					    		$status = $StatusPinjaman->keterangan;
					    		$sesuai = 1;
					    	}

					    	$sesuai_nopin = 1;
					    	if(!$nopin){
					    		$status = "Nomor Pinjaman Sudah Ada";
					    		$sesuai_nopin = -1;
					    	}

					    	if($top){
					    		//1 new
					    		$topup = "1";
					    	}else{
					    		//2 topup
					    		$topup = "2";
					    	}

					    	$cekRatePremi = "";
					    	if($cekKodePekerjaan!=FALSE){
					    		$KodePekerjaan = $cekKodePekerjaan;
					    		$cekRatePremi = $this->helper->cekRatePremi($KodePekerjaan);
					    	}else{
					    		$KodePekerjaan = -1;
					    		$status = "Kode Pekerjaan Tidak Sesuai";
					    	}
				    	

					    	if($sesuai==1 && $sesuai_nopin==1 && $KodePekerjaan!=-1){
		    				$NomorRegistrasi = date("ymdHis")+$no;
	    					$this->produksi->saveProduksi($NomorRegistrasi,$row["cif"],$row["nopin"],$row["kode"],$row["nama"],$row["loan"],$row["jk"],$row["ktp"],$row["tmpt"],date_format(date_create($row["tgl"]),'Y-m-d'),$row["alamat"],$KodePekerjaan,$row["pk"],date_format(date_create($row["tglakad"]),'Y-m-d'),date_format(date_create($row["tglakhir"]),'Y-m-d'),$row["plafon"],$row["premi"],$row["ref"],$topup,$row["asdur"],$StatusPinjaman->ac,$umur_asli,$tenor,$tenorbulan,$batas_asli,"0".$cekRatePremi,"0".$cekRatePremiProduk->FeeBroker,"0".$cekRatePremiProduk->FeeBank,$StatusPinjaman->idTc,$StatusPinjaman->keterangan,$NomorInvoice);
	    						$berhasil = $berhasil+1;
					    	}

					    	$premi = $row["plafon"]*$cekRatePremi;
							$JumlahPremiTenor = $tenor*$premi; 

							if($KodePekerjaan=="BJ"){
								$JumlahPremiTenor = $row["plafon"]*$cekRatePremi; 								
							}

					    	$datax["debitur"] .= "<tr>";	
					    	$datax["debitur"] .= "<td>".$no."</td>";	
					    	$datax["debitur"] .= "<td>".$NomorRegistrasi."</td>";	
					    	$datax["debitur"] .= "<td>".$row["cif"]."</td>";	
					    	$datax["debitur"] .= "<td>".$row["nopin"]."</td>";	
					    	$datax["debitur"] .= "<td>".$row["kode"]."</td>";	
					    	$datax["debitur"] .= "<td>".$row["nama"]."</td>";	
					    	$datax["debitur"] .= "<td>".$row["loan"]."</td>";	
					    	$datax["debitur"] .= "<td>".$row["jk"]."</td>";	
					    	$datax["debitur"] .= "<td>".$row["ktp"]."</td>";	
					    	$datax["debitur"] .= "<td>".$row["tmpt"]."</td>";	
					    	$datax["debitur"] .= "<td>".date_format(date_create($row["tgl"]),'Y-m-d')."</td>";	
					    	$datax["debitur"] .= "<td>".$row["alamat"]."</td>";
					    	$datax["debitur"] .= "<td>".$row["pekerjaan"]."</td>";
					    	$datax["debitur"] .= "<td>".$row["pk"]."</td>";
					    	$datax["debitur"] .= "<td>".date_format(date_create($row["tglakad"]),'Y-m-d')."</td>";	
					    	$datax["debitur"] .= "<td>".date_format(date_create($row["tglakhir"]),'Y-m-d')."</td>";	
					    	$datax["debitur"] .= "<td>".price($row["plafon"])."</td>";	
					    	$datax["debitur"] .= "<td>".price($JumlahPremiTenor)."</td>";	
					    	$datax["debitur"] .= "<td>".$row["ref"]."</td>";	
					    	$datax["debitur"] .= "<td>".$row["top"]."</td>";	
					    	$datax["debitur"] .= "<td>".$row["asdur"]."</td>";	
					    	$datax["debitur"] .= "<td>".$status."</td>";	
					    	$datax["debitur"] .= "</tr>";

					    	if($sesuai==-1 || $sesuai_nopin==-1 || $KodePekerjaan==-1){
					    		$datax["gagal"] .= '<tr>';
						        $datax["gagal"] .= '<td style="text-align: center;">02 DATA GAGAL '.$row["nopin"].'</td>';
						        $datax["gagal"] .= '<td style="text-align: center;">'.$status.'</td>';
						        $datax["gagal"] .= '</tr>';
						        $namafile = str_replace("01","02",$namafile);
						        $this->produksi->file02($namafile,"02 DATA GAGAL",$row["nopin"],$status);
					    	}else{
					    		$datax["detail"] .= '<tr>';
						        $datax["detail"] .= '<td style="text-align: center;">03 DATA BERHASIL '.$row["nopin"].'</td>';
						        $datax["detail"] .= '<td style="text-align: center;">'.$NomorRegistrasi.'</td>';
						        $datax["detail"] .= '</tr>';
						        $namafile = str_replace("01","03",$namafile);
						        $this->produksi->file03($namafile,"03 DATA BERHASIL",$row["nopin"],$NomorRegistrasi);
					    	}
					    	
					        					        
					        // $xyz .= $xyz.'|'.$status;

			    		}else{
			    			$datax["debitur"] .= $row["asdur"]." Kode Asuransi Tidak Terdaftar<br>";
			    		}
			    	}else{
			    		$datax["debitur"] .= $row["kode"]." Kode Bank Tidak Terdaftar<br>";
			    	}
			    	$no++;	
			    }
			    $datax["debitur"] .= "</tbody>"; 
	        }

	        $datax["detail"] .= '</tbody>';
	        $datax["detail"] .= '</table>';

	        $datax["gagal"] .= '</tbody>';
	        $datax["gagal"] .= '</table>';

	        // $gagal = count($datax["array"])-$berhasil;
	        // if ($gagal==0) {
	        // 	$xxx = "-";
	        // }else{
	        // 	$xxx = $xyz;
	        // }
	        // $datax["summary"] = '<table id="uat02" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">';
	        // $datax["summary"] .= '<thead>';
	        // $datax["summary"] .= '<tr>';
	        // $datax["summary"] .= '<th>Summary Total</th>';
	        // $datax["summary"] .= '<th>Summary Berhasil</th>';
	        // $datax["summary"] .= '<th>Summary Gagal</th>';
	        // $datax["summary"] .= '<th>Detail Gagal</th>';
	        // $datax["summary"] .= '</tr>';
	        // $datax["summary"] .= '</thead>';
	        // $datax["summary"] .= '<tbody>';
	        // $datax["summary"] .= '<tr>';
	        // $datax["summary"] .= '<td style="text-align: center;">'.count($datax["array"]).'</td>';
	        // $datax["summary"] .= '<td style="text-align: center;">'.$berhasil.'</td>';
	        // $datax["summary"] .= '<td style="text-align: center;">'.$gagal.'</td>';
	        // $datax["summary"] .= '<td>'.$xxx.'</td>';
	        // $datax["summary"] .= '</tr>';
	        // $datax["summary"] .= '</tbody>';
	        // $datax["summary"] .= '</table>';
		}
	
		$this->app->render('Upload','produksi/upload',$datax);
		
	}

	public function nonac(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		// $data["nonac"] = $this->produksi->NonAc();
		$this->app->render('List Non AC','produksi/nonac',$data);
	}

	public function uploadDok(){
		error_reporting(0);
		$config['upload_path'] = './upload/dokumen_produksi/';
		$config['allowed_types'] = 'jpg|jpeg|pdf|doc|docx|xlsx|xls|txt';
	    $config['max_size'] = '30000';
	    $config['max_width']  = '8000'; 
	    $config['max_height']  = '7768';


		foreach ($this->input->post("urut") as $i) {
		$nama = $this->input->post("namaDokumen".$i);
		$noreg = $this->input->post("noreg");
		$config['file_name'] = $noreg.$nama;

	    $this->load->library('upload', $config);
	    $this->upload->initialize($config);
		    if(!empty($this->upload->do_upload("dok".$i))){
		        $upload_data = $this->upload->data();
				$uploadDok = $this->produksi->uploadDok($upload_data["file_name"],$noreg,$nama);
			}
	    }
	    
		redirect('produksi/nonac');
	}

	public function capem($kode){
		$capem = $this->helper->capem($kode);
		echo "<option value=''>Semua</option>";
		foreach ($capem as $key) {
			echo "<option value='".$key->kodeBank."'>".$key->capem."</option>";
		}
	}

	public function filter_nonac(){
		$draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));

          $peserta = $this->produksi->FilterNonAc();
          $data = array();
          $i = 1;
        
          foreach($peserta->result() as $r) {
               
          		$btn = '<input type="hidden" name="NomorRegistrasi" id="NomorRegistrasi" value="'.$r->NomorRegistrasi.'"><a onclick="cekModalDokKurang(\''.$r->NomorRegistrasi.'\')" class="btn btn-primary btn-xs" style="color: #ffffff;">';
		        	foreach (cekDokKurang($r->NomorRegistrasi)->result() as $de) { 
			       		$btn .= $de->dokumen.","; 
			       	}
               	$btn .= '</a>';

                  $data[] = array(
                  	$i,
                      $r->NomorPinjaman,
                      $r->cif,
                      $r->NamaDebitur,
                      date_format(date_create($r->TglAkadKredit),'d-M-Y'),
                      date_format(date_create($r->TglAkhirKredit),'d-M-Y'),
                      $r->TenorTahun,
                      price($r->plafon),
                      $btn
                 );

               $i++;
          }

          $output = array(
                "draw" => $draw,
                "recordsTotal" => $peserta->num_rows(),
                "recordsFiltered" => $peserta->num_rows(),
                "data" => $data
            );

          echo json_encode($output);
          exit();
	}

	public function alokasi(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		// $data["nonac"] = $this->produksi->NonAc();
		$this->app->render('List Alokasi','produksi/alokasi',$data);
	}

	public function filter_alokasi(){
			$draw = intval($this->input->post("draw"));
          	$start = intval($this->input->post("start"));
          	$length = intval($this->input->post("length"));

          	$peserta = $this->produksi->FilterAlokasi();
          	$asuransi = $this->helper->asuradurNot99();
          	$data = array();
          	$i = 1;
        
          foreach($peserta->result() as $r) {
               
          		$btn = '<a onclick="simpanAsdur(\''.$r->NomorRegistrasi.'\')" class="btn btn-primary btn-xs" style="color: #ffffff;">Simpan</a>';

               	$input = 	'<select class="js-example-basic-cabang col-xs-3 custom-select f-s-12" name="asdur" id="asdur">
                    	    <option value="">Semua</option>';
                            foreach ($asuransi as $pd){
                            $input .= '<option value="'.$pd->kodeAngka.'">'.$pd->asuransi.'</option>';
                            }
                    		$input .= '</select>';


                  $data[] = array(
                      $r->NomorPinjaman,
                      $r->NamaDebitur,
                      date_format(date_create($r->TglAkadKredit),'d-m-Y'),
                      date_format(date_create($r->TglAkhirKredit),'d-m-Y'),
                      $r->TenorTahun,
                      price($r->plafon),
                      cekNamaBank($r->kodeBank),
                      $input,
                      $btn
                 );

               $i++;
          }

          $output = array(
                "draw" => $draw,
                "recordsTotal" => $peserta->num_rows(),
                "recordsFiltered" => $peserta->num_rows(),
                "data" => $data
            );

          echo json_encode($output);
          exit();
	}

	public function simpanAsdur(){
		$terbit = $this->produksi->simpanAsdur();
		if($terbit==0){
			echo "Gagal Simpan Alokasi";
		}else{
			echo "Alokasi Berhasil Disimpan";
		}
	}

	public function rekonsel(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		// $data["rekonsel"] = $this->produksi->rekonsel();
		$data["asuransi"] = $this->helper->asuradur();
		$this->app->render('List Rekonsel','produksi/rekonsel',$data);
	}

	public function validasiRekonsel(){
		$validasi = $this->produksi->validasiRekonsel();
		if($validasi){
			echo "Data Berhasil Divalidasi";
		}else{
			echo "Data Gagal Divalidasi ";
		}
	}

	public function filter_rekonsel(){
		$draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));

          $peserta = $this->produksi->filter_rekonsel();
          $data = array();
          $i = 1;
        
          foreach($peserta->result() as $r) {
                  $data[] = array(
                  	'<input type="checkbox" name="checkbox[]" value="\''.$r->NomorPinjaman.'\'" class="chkDel">',
                      $r->NomorPinjaman,
                      $r->cif,
                      $r->NamaDebitur,
                      date_format(date_create($r->TglAkadKredit),'d-M-Y'),
                      date_format(date_create($r->TglAkhirKredit),'d-M-Y'),
                      $r->TenorTahun,
                      price($r->plafon),
                      price($r->JumlahPremiTenor),
                      cekNamaBank($r->kodeBank),
                      cekNamaAsdur($r->kodeAsuransi)
                 );

               $i++;
          }

          $output = array(
                "draw" => $draw,
                "recordsTotal" => $peserta->num_rows(),
                "recordsFiltered" => $peserta->num_rows(),
                "data" => $data
            );

          echo json_encode($output);
          exit();
	}

	public function rekonsel_bj(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		// $data["rekonsel"] = $this->produksi->rekonsel();
		$data["asuransi"] = $this->helper->asuradur();
		$this->app->render('List Rekonsel BJ','produksi/rekonsel_bj',$data);
	}

	public function validasiRekonselBj(){
		$validasi = $this->produksi->validasiRekonselBj();
		if($validasi){
			echo "Data Berhasil Divalidasi";
		}else{
			echo "Data Gagal Divalidasi ";
		}
	}

	public function filter_rekonsel_bj(){
		$draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));

          $peserta = $this->produksi->filter_rekonsel_bj();
          $data = array();
          $i = 1;
        
          foreach($peserta->result() as $r) {
          		$bj = $this->produksi->filter_rekonsel_bj_detail($r->NomorRegistrasi,$r->tahun);
                  $data[] = array(
                  	'<input type="checkbox" name="checkbox[]" value="\''.$bj->idTbj.'\'" class="chkDel">',
                      $r->NomorPinjaman,
                      $r->NamaDebitur,
                      date_format(date_create($r->TglAkadKredit),'d-M-Y'),
                      $r->TenorTahun,
                      price($bj->plafon),
                      price($bj->SisaPinjaman),
                      price($r->tahun),
                      price($bj->ospremi),
                      cekNamaBank($r->kodeBank),
                      cekNamaAsdur($r->kodeAsuransi)
                 );

               $i++;
          }

          $output = array(
                "draw" => $draw,
                "recordsTotal" => $peserta->num_rows(),
                "recordsFiltered" => $peserta->num_rows(),
                "data" => $data
            );

          echo json_encode($output);
          exit();
	}

	public function polis(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		// $data["polis"] = $this->produksi->polis();
		$this->app->render('List Polis','produksi/polis',$data);
	}

	public function filter_polis(){
		$draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));

          $peserta = $this->produksi->filter_polis();
          $data = array();
          $i = 1;

          foreach($peserta->result() as $r) {
        		$text = '<input type="text" id="nopolis" name="nopolis" class="col-xs-3 custom-select f-s-12">
                                <br>-
                                <input type="date" class="col-12 custom-select f-s-12 datepicker" placeholder="dd/mm/yyyy" name="tglpolis" id="tglpolis">';
                $btn = '<a class="btn btn-xs btn-warning" onclick="terbitkan(\''.$r->NomorRegistrasi.'\')" style="color: #ffffff;">Terbitkan</a>';
                  $data[] = array(
                      '',
                      $r->NomorPinjaman,
                      $r->cif,
                      $r->NamaDebitur,
                      date_format(date_create($r->TglAkadKredit),'d-M-Y'),
                      date_format(date_create($r->TglAkhirKredit),'d-M-Y'),
                      $r->TenorTahun,
                      price($r->plafon),
                      cekNamaBank($r->kodeBank),
                      cekNamaAsdur($r->kodeAsuransi),
                      $text,
                      $btn
                 );

               $i++;
          }

          $output = array(
                "draw" => $draw,
                "recordsTotal" => $peserta->num_rows(),
                "recordsFiltered" => $peserta->num_rows(),
                "data" => $data
            );

          echo json_encode($output);
          exit();
	}

	public function terbitkanPolis(){
		$terbit = $this->produksi->terbitkanPolis();
		if($terbit==0){
			echo "Gagal Proses Polis";
		}else if($terbit==-1){
			echo "Gagal Input Polis";
		}else{
			echo "Polis Berhasil Terbit";
		}
	}

	public function melengkapiDokumen(){
		$noreg = $this->input->post("noreg");
		$debitur = $this->db->query("select NamaDebitur, cif from t_debitur where idDebitur = (select idDebitur from t_pinjaman where NomorRegistrasi = '$noreg') ");
		$i=0; 
		$baris = "";
		$baris .= '<div class="row text-left m-b-10">';
        $baris .= '<lable class="col-sm-3 control-label">No. CIF</lable>';
        $baris .= '<lable class="col-sm-5 control-label" id="cif">'.$debitur->row()->cif.'</lable>';
		$baris .= '</div>';
		$baris .= '<div class="row text-left m-b-10">';
        $baris .= '<lable class="col-sm-3 control-label">No. Pinjaman</lable>';
        $baris .= '<lable class="col-sm-5 control-label" id="pinjaman">'.$debitur->row()->NomorPinjaman.'</lable>';
        $baris .= '</div>';
        $baris .= '<div class="row text-left m-b-10">';
        $baris .= '<lable class="col-sm-3 control-label">Nama</lable>';
        $baris .= '<lable class="col-sm-5 control-label" id="nama">'.$debitur->row()->NamaDebitur.'</lable>';
        $baris .= '</div>';
        $baris .= '<div class="col-xs-12">';
        $baris .= '<hr>';
        $baris .= '</div>';
		foreach (cekDokKurang($noreg)->result() as $abc) {
            $baris .= '<div class="row p-10 text-left">';
            $baris .= '<input type="hidden" name="urut[]" value="'.$i.'">';
            $baris .= '<input type="hidden" name="noreg" id="noreg" value="'.$noreg.'">';
            $baris .= '<input type="hidden" name="namaDokumen'.$i.'" value="'.$abc->dokumen.'">';
            $baris .= '<input type="file" name="dok'.$i.'" class="btn btn-outline-info btn-sm w-50"><lable class="col-sm-4 control-label">'.$abc->dokumen.'</lable>';
            $baris .= '</div>';
        $i++; 
    	}
    	echo $baris;
	}
}
