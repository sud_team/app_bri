<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __Construct(){
		parent::__Construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Model_account','account');
		$this->load->model('Model_helper','helper');
	}

	public function index(){
		$this->load->view('login');
	}

	public function newPass(){
		$this->load->view('new_pass');
	}

	public function auth(){
		if($_POST){
			if(empty($this->input->post('username')) || empty($this->input->post('password'))){
				$this->session->set_flashdata('warning', 'Invalid login, The email or password is incorrect.'); 
				redirect('login');
			}else{
				$user = $this->account->Auth($this->input->post('username'),pass($this->input->post('password')));
				if($user){
					$data = array(
						'IS_LOGIN'=>true,
						'CONTROLLER'=>$this->helper->Controller($user->idUser),
						'username'=>$user->username,
						'nama'=>$user->nama,
						'email'=>$user->email,
						'hak_akses'=>$user->hak_akses,
						'online'=>$user->is_online,
						'idUser'=>$user->idUser,
						'roles'=>$user->roles,
						'kodeBank'=>$user->kodeBank
					);
					if($user->status=='1'){
						if ($user->ganti_password=='1'){
							if($user->is_online=='0'){
								$this->session->set_userdata($data);
								updateLogin($user->idUser,date("Y-m-d H:i:s"));
								logdata($this->session->userdata('idUser'),date("Y-m-d H:i:s"),'Login');
								// echo "masuk";
								redirect("dashboard");
							}else{
								if($user->roles==1 || $user->roles==2){
									$this->session->set_userdata($data);
									updateLogin($user->idUser,date("Y-m-d H:i:s"));
									logdata($this->session->userdata('idUser'),date("Y-m-d H:i:s"),'Login');
									// echo "masuk";
									redirect("dashboard");
								}else{
									$this->session->set_flashdata('warning', 'Username Online in Another PC'); 
									redirect('login');
								}
							}
						}else{
							$username = $user->username;
							$idUser = $user->idUser;
							$this->session->set_userdata("idUserPass",$idUser);
							redirect('login/newPass');
						}
					}else{
						$this->session->set_flashdata('warning', 'Username Not Actived'); 
						redirect('login');
					}
				}else{
					$this->session->set_flashdata('warning', 'Invalid login, The username or password is incorrect.'); 
					redirect('login');
				}	
			}
		}else{
			$this->load->view('login');
		}
	}

	public function updatePassword(){
		$idUser = $this->input->post("idUser");
		$pass = pass($this->input->post("password"));
		$query = $this->db->query("update t_user set pass = '$pass', ganti_password = '1' where idUser = '$idUser'");
		$this->session->set_flashdata('success', 'Password Berhasil Di Update.'); 
		redirect('login');
	}

	public function logout(){
		if($this->session->userdata('idUser')){
			updateLogout($this->session->userdata('idUser'),date("Y-m-d H:i:s"));
		}
		// logdata($this->session->userdata('idUser'),date("Y-m-d H:i:s"),'Logout');
		$this->session->sess_destroy();
		$data = array(
			'IS_LOGIN'=>false,
			'CONTROLLER'=>null,
			'username'=>null,
			'nama'=>null,
			'email'=>null,
			'hak_akses'=>null,
			'online'=>null,
			'idUser'=>null,
			'roles'=>null
		);

		$this->session->set_userdata($data);
		redirect('login');
	}
}
