<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {

	public function __Construct(){
		parent::__Construct();
		$this->app->auth();
		$this->load->model('Model_master','master');
		$this->load->model('Model_helper','helper');
	}

	public function wilayah(){
		// $this->load->view("public/master/tes");
		$this->app->render('List Wilayah','master/wilayah',null);
	}

	public function tc(){
		$data["keterangan"] = $this->master->KeteranganTc();
		$data["tc"] = $this->master->tc();
		$this->app->render('List TC','master/tc',$data);
	}

	public function saveTc(){
		$save = $this->master->saveTc();
		if($save){
			$this->session->set_flashdata("success","TC berhasil disimpan");
		}else{
			$this->session->set_flashdata("danger","TC gagal disimpan");
		}
		redirect("master/tc");

	}

	public function asuransi(){
		$data["asuransi"] = $this->master->asuransi();
		$data["rate"] = $this->master->rate();
		$data["produk"] = $this->master->produk();
		$this->app->render('List Asuransi','master/asuransi',$data);
	}

	public function saveAsuransi(){
		$save = $this->master->saveAsuransi();
		if($save){
			$this->session->set_flashdata("success","Asuransi berhasil disimpan");
		}else{
			$this->session->set_flashdata("danger","Asuransi gagal disimpan");
		}
		redirect("master/asuransi");

	}

	public function saveRatePremi(){
		$save = $this->master->saveRatePremi();
		if($save){
			$this->session->set_flashdata("success","Rate Premi berhasil disimpan");
		}else{
			$this->session->set_flashdata("danger","Rate Premi gagal disimpan");
		}
		redirect("master/asuransi");

	}

	public function produk(){
		$data["produk"] = $this->master->produk();
		$this->app->render('List Produk','master/produk',$data);
	}

	public function saveProduk(){
		$save = $this->master->saveProduk();
		if($save){
			$this->session->set_flashdata("success","Produk berhasil disimpan");
		}else{
			$this->session->set_flashdata("danger","Produk gagal disimpan");
		}
		redirect("master/produk");

	}

	public function kcp(){
		$data["cabang"] = $this->helper->cabang();
		$this->app->render('List Cabang Dan Capem','master/kcp',$data);
	}

	public function getBank($idCabang){
		$draw = intval($this->input->post("draw"));
	    $start = intval($this->input->post("start"));
	    $length = intval($this->input->post("length"));
	    
	    $bank = $this->master->getBank($idCabang);

	    $data = array();
	    $i = 1;
	    
	    foreach($bank->result() as $r) {

          	$data[] = array(
          		$i,
          		$r->cabang,
          		$r->capem,
          		"",
         	);
       	$i++;
      	}

	      $output = array(
	            "draw" => $draw,
	            "recordsTotal" => $bank->num_rows(),
	            "recordsFiltered" => $bank->num_rows(),
	            "data" => $data
	        );

	      echo json_encode($output);
	      exit();
	}

	public function capem($kode){
		$capem = $this->helper->capem($kode);
		echo "<option value=''>Semua</option>";
		foreach ($capem as $key) {
			echo "<option value='".$key->kodeBank."'>".$key->capem."</option>";
		}
	}

	public function user(){
		$data["roles"] = $this->helper->roles();
		$data["akses"] = $this->helper->hak_akses();
		$data["cabang"] = $this->helper->cabang();
		$this->app->render('List User','master/user',$data);
	}

	public function getUser(){
		$draw = intval($this->input->post("draw"));
	    $start = intval($this->input->post("start"));
	    $length = intval($this->input->post("length"));

	    $user = $this->master->getUser();

	    $data = array();
	    $i = 1;
	    
	    foreach($user->result() as $r) {

          	$data[] = array(
          		$i,
          		$r->username,
          		$r->cabang,
          		$r->capem,
          		$r->roles,
          		$r->hak_akses,
          		"",
         	);
       	$i++;
      	}

	      $output = array(
	            "draw" => $draw,
	            "recordsTotal" => $user->num_rows(),
	            "recordsFiltered" => $user->num_rows(),
	            "data" => $data
	        );

	      echo json_encode($output);
	      exit();
	}

	public function saveUser(){
		$save = $this->master->saveUser();
		if($save){
			$this->session->set_flashdata("success","User berhasil disimpan");
		}else{
			$this->session->set_flashdata("danger","User gagal disimpan");
		}
		redirect("master/user");

	}

	public function userUpload(){
		// insert 
		$query = $this->db->query("select idUser from t_user where idUser  = 804 ");
		$menu = $this->db->query("select idMenu from t_menu where idMenu in (11,12,14,15,16,17,19)");
		$count = 0;
		foreach ($query->result() as $key) {
			$idUser = $key->idUser;
			foreach ($menu->result() as $value) {
				$idMenu = $value->idMenu;
				$insert = $this->db->query("insert into t_menu_akses values ('$idUser','$idMenu')");
				if($insert){
					$count++;
				}
			}
		}
		echo $count;
	}
	
	public function reset(){
		$data["user"] = $this->master->bank();
		$this->app->render('List Data User','master/reset',$data);
	}


	public function updatePass(){
		$idUser = $this->input->post("idUser");
		$i=0; 
		$baris = "";
		$baris .= '<div class="row text-left m-b-10">';
        $baris .= '<lable class="col-sm-4 control-label">USERNAME</lable>';
        $baris .= '<lable class="col-sm-4 control-label">: '.$this->session->userdata("username").'</lable>';

        $baris .= '</div>';
        $baris .= '<div class="row text-left m-b-10">';
        $baris .= '<lable class="col-sm-4 control-label">Nama</lable>';
        $baris .= '<input type="password" class="form-control" name="pass">';
        $baris .= '<input type="hidden" class="form-control" name="idUser" value="'.$idUser.'">';

        $baris .= '</div>';
        $baris .= '<div class="col-xs-12">';
        $baris .= '<hr>';
        $baris .= '</div>';
    	echo $baris;
	}

	public function saveUpdate(){
		$idUser = $this->input->post("idUser");
		$pass = sha1($this->input->post("pass")).":".sha1("askred");
		$query = $this->db->query("update t_user set pass = '$pass' where idUser = '$idUser'");
		$this->session->set_flashdata("success","User Update berhasil disimpan");
		redirect('master/reset');
	}

	public function unlockPass(){
		$idUser = $this->input->post("idUser");
		$query = $this->db->query("update t_user set is_online = '0' where idUser = '$idUser'");
		// $this->session->set_flashdata("success","User berhasil unclock");
		// redirect('master/reset');
		echo "User berhasil unclock";
	}

	public function editAsuransi(){
		$kode = $this->input->post("kodeAngka"); 
		$query = $this->db->query("select * from t_asuransi where kodeAngka = '$kode'")->row();

		echo '<div class="col-xs-12" id="dok">
                <div class="form-group row">
                    <label class="col-form-label col-md-4 p-10">Kode Angka
                    </label>
                    <div class="col-md-5">
                        <b>'.$query->kodeAngka.'</b>
                    </div>
                </div> 
                <div class="form-group row">
                    <label class="col-form-label col-md-4 p-10">Singkatan Asuransi
                    </label>
                    <div class="col-md-7">
                        <input name="kodeAngka" type="hidden" value="'.$query->kodeAngka.'">
                        <input class="form-control f-s-12" placeholder="Masukan Kode Asuransi" name="kode" type="text" value="'.$query->kodeAsuransi.'">
                    </div>
                </div> 
                <div class="form-group row">
                    <label class="col-form-label col-md-4 p-10">Asuransi
                    </label>
                    <div class="col-md-7">
                        <input class="form-control f-s-12" placeholder="Masukan Nama Asuransi" name="asuransi" type="text" value="'.$query->asuransi.'">
                    </div>
                </div> 
                <div class="form-group row">
                    <label class="col-form-label col-md-4 p-10">Fee Broker
                    </label>
                    <div class="col-md-7">
                        <input class="form-control f-s-12" placeholder="Masukan Fee Broker (0.x)" name="feebroker" type="text" value="'.$query->FeeBroker.'">
                    </div>
                </div> 
                <div class="form-group row">
                    <label class="col-form-label col-md-4 p-10">Fee Bank
                    </label>
                    <div class="col-md-7">
                        <input class="form-control f-s-12" placeholder="Masukan Fee Bank (0.x)" name="feebank" type="text" value="'.$query->FeeBank.'">
                    </div>
                </div> 
              </div>';
	}

	public function editAsuransiSave(){
		$kodeAngka = $this->input->post("kodeAngka");
		$kode = $this->input->post("kode");
		$asuransi = $this->input->post("asuransi");
		$feebroker = $this->input->post("feebroker");
		$feebank = $this->input->post("feebank");
		$query = $this->db->query("update t_asuransi set asuransi = '$asuransi', FeeBroker = '$feebroker',FeeBank = '$feebank',kodeAsuransi = '$kode' where kodeAngka = '$kodeAngka'");
		if($query){
			$this->session->set_flashdata("success","Asuransi Berhasil Di Update");
		}else{
			$this->session->set_flashdata("danger","Asuransi Gagal Di Update");
		}

		redirect('master/asuransi');
	}

	public function editRate(){
		$kode = $this->input->post("kodePremi"); 
		$query = $this->db->query("select * from t_asuransi where kodeAngka = '$kode'")->row();

		echo '<div class="col-xs-12" id="dok">
                <div class="form-group row">
                    <label class="col-form-label col-md-4 p-10">Kode Angka
                    </label>
                    <div class="col-md-5">
                        <b>'.$query->kodeAngka.'</b>
                    </div>
                </div> 
                <div class="form-group row">
                    <label class="col-form-label col-md-4 p-10">Singkatan Asuransi
                    </label>
                    <div class="col-md-7">
                        <input name="kodeAngka" type="hidden" value="'.$query->kodeAngka.'">
                        <input class="form-control f-s-12" placeholder="Masukan Kode Asuransi" name="kode" type="text" value="'.$query->kodeAsuransi.'">
                    </div>
                </div> 
                <div class="form-group row">
                    <label class="col-form-label col-md-4 p-10">Asuransi
                    </label>
                    <div class="col-md-7">
                        <input class="form-control f-s-12" placeholder="Masukan Nama Asuransi" name="asuransi" type="text" value="'.$query->asuransi.'">
                    </div>
                </div> 
              </div>';
	}
}
