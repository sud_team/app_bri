<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Debitur extends CI_Controller {

	public function __Construct(){
		parent::__Construct();
		$this->app->auth();
		$this->load->model('Model_helper','helper');
		$this->load->model('Model_debitur','debitur');
    // $this->output->cache(3600);
	}

	public function cari(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		// $data["debitur"] = $this->debitur->debitur();
		$this->app->render('Cari Data Debitur','debitur',$data);
	}

	public function filter_debitur(){
		$draw = intval($this->input->post("draw"));
          $start = intval($this->input->post("start"));
          $length = intval($this->input->post("length"));

          $peserta = $this->debitur->debitur();
			
          $data = array();
          $i = 1;

          foreach($peserta->result() as $key) {
                if ($key->StatusKlaim==1 || $key->StatusRestitusi==1){
                	if ($key->StatusKlaim==1){
                        $status = "Klaim";
                	}
                    if ($key->StatusRestitusi==1){

                        $status = "Restitusi";
                    }
                } else if($key->StatusRekonsel==0) {$status = "Proses Rekonsel";}  
                else{ 
                  $status = '<a href="'.base_url().'klaim/formKlaim/'.$key->NomorRegistrasi.'" class="btn btn-xs btn-danger" style="color: #ffffff;">Klaim</a> - <a href="'.base_url().'restitusi/formRestitusi/'.$key->NomorRegistrasi.'" class="btn btn-xs btn-warning" style="color: #ffffff;">Restitusi</a>';
                }

                if ($key->TcKet=="AC"){
                    $keterangan = $key->TcKet;
                } 
                else {
                    foreach (cekDokLengkap($key->NomorRegistrasi)->result() as $value){
                    $keterangan = '<a class="btn btn-xs btn-info" href="'.base_url().'upload/dokumen_produksi/'.$value->namafile.'" target="_blank">'.$value->dokumen.'</a>';
                    }
                }

                $pengajuan = "-";
                if(($this->session->userdata("roles")==2 && $this->session->userdata("hak_akses")==5) || $this->session->userdata("roles")==1){
                  if($this->session->userdata("kodeBank")=="000"){
                    $pengajuan = $status;
                  }else{
                    $pengajuan = $status;
                  }
                }else{
                    $pengajuan = "-";
                    // $pengajuan = $status;
                }

                $btn = '<input type="hidden" name="NomorRegistrasi" id="NomorRegistrasi" value="'.$key->NomorRegistrasi.'"><a onclick="cekTracking(\''.$key->NomorRegistrasi.'\')" class="btn btn-primary btn-xs" style="color: #ffffff;">Tracking</a>';

                  $data[] = array(
                  	'',
                      $key->NomorPinjaman,
                      $key->NamaDebitur,
                      my_date($key->TglLahir),
                      str_replace(".", " Tahun ", $key->UsiaDebitur)." Bulan",
                      cekPekerjaan($key->KodePekerjaan),
                      my_date($key->TglAkadKredit),
                      ($key->TenorBulan)." Bulan",
                      price($key->plafon),
                      cekProduk($key->idProduk),
                      $pengajuan,
                      $keterangan,
                      cekNamaBank($key->kodeBank),
                      cekNamaAsdur($key->kodeAsuransi),
                      $btn
                 );

               $i++;
          }

          $output = array(
                "draw" => $draw,
                "recordsTotal" => $peserta->num_rows(),
                "recordsFiltered" => $peserta->num_rows(),
                "data" => $data
            );

          echo json_encode($output);
          exit();
	}

	public function capem($kode){
		$capem = $this->helper->capem($kode);
		echo "<option value=''>Semua</option>";
		foreach ($capem as $key) {
			echo "<option value='".$key->kodeBank."'>".$key->capem."</option>";
		}
	}

  public function cekTracking(){
    $NomorRegistrasi = $this->input->post("noreg");
    $pinjaman = $this->db->query("select create_date, TglRekonsel from t_pinjaman where NomorRegistrasi = '$NomorRegistrasi'")->row();
    if(empty($pinjaman)){
      $TglRekonsel = "";
    }else{
      if ($pinjaman->TglRekonsel=="1900-01-01 00:00:00") {
        $TglRekonsel = "";
      } else {
        $TglRekonsel = $pinjaman->TglRekonsel;
      }
      
    }
    $TglPolis = $this->db->query("select TglPolis from t_pinjaman_polis where NomorRegistrasi = '$NomorRegistrasi'");
    if(empty($TglPolis->row())){
      $tglpolis = "";
    }else{
      if ($TglPolis->row()->TglPolis=="1900-01-01 00:00:00") {
        $tglpolis = "";
      } else {
        $tglpolis = $TglPolis->row()->TglPolis;
      }
      
    }
    $tglspvklaim = $this->db->query("select create_spv, TanggalBayar from t_pinjaman_klaim where NomorRegistrasi = '$NomorRegistrasi'");
    $tglspvrestitusi = $this->db->query("select create_spv, TanggalBayar from t_pinjaman_restitusi where NomorRegistrasi = '$NomorRegistrasi'");
    
    $tglspvk = "";
    if(empty($tglspvklaim->row())){
      $tglspvk = "";
    }else{
      if($tglspvklaim->row()->create_spv=="1900-01-01 00:00:00"){
       $tglspvk = "";
      }else{
       $tglspvk = $tglspvklaim->row()->create_spv;
      }
    }

    $tglspvr = "";
    if(empty($tglspvrestitusi->row())){
      $tglspvr = "";
    }else{
      if ($tglspvrestitusi->row()->create_spv=="1900-01-01 00:00:00") {
      $tglspvr = "";
      } else {
      $tglspvr = $tglspvrestitusi->row()->create_spv;
      }
      
    }

    $tglbyrk = "";
    if(empty($tglspvklaim->row())){
      $tglbyrk = "";
    }else{
      if ($tglspvklaim->row()->TanggalBayar=="1900-01-01 00:00:00") {
      $tglbyrk = "";
      } else {
      $tglbyrk = $tglspvklaim->row()->TanggalBayar;
      }
    }

    $tglbyrR = "";
    if(empty($tglspvrestitusi->row())){
      $tglbyrR = "";
    }else{
      if ($tglspvrestitusi->row()->TanggalBayar=="1900-01-01 00:00:00") {
      $tglbyrR = "";
      } else {
      $tglbyrR = $tglspvrestitusi->row()->TanggalBayar;
      }
    }

    echo '<div class="row bs-wizard  " style="border-bottom:0;">
                <div class=" bs-wizard-step complete w-20">
                    <div class="text-center bs-wizard-stepnum">Produksi</div>';
                    if (!empty($pinjaman->create_date)){
                    echo '<div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center col-m-2">'.my_date_indo($pinjaman->create_date).'</div>';
                    }
                echo '</div>

                <div class=" bs-wizard-step complete  w-20">
                    <!-- complete -->
                    <div class="text-center bs-wizard-stepnum">Rekonsel</div>';
                    if (!empty($TglRekonsel)){ 
                    echo '<div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center col-xs-2">'.my_date_indo($TglRekonsel).'</div>';
                    }
                echo '</div>

                <div class=" bs-wizard-step complete w-20">
                    <!-- complete -->
                    <div class="text-center bs-wizard-stepnum">Polis</div>';
                    if (!empty($tglpolis)){
                    echo '<div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center col-xs-2">'.my_date_indo($tglpolis).'</div>';
                    }
                echo '</div>

                <div class=" bs-wizard-step complete w-20">
                    <!-- active -->
                    <div class="text-center bs-wizard-stepnum">Klaim / Restitusi</div>';
                    if (!empty($tglspvk)){
                    echo '<div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center ">',my_date_indo($tglspvk).'</div>';
                    }

                    if (!empty($tglspvr)){
                    echo '<div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center ">',my_date_indo($tglspvr).'</div>';
                    }                    
                echo '</div>

                <div class=" bs-wizard-step complete  w-20">
                    <!-- active -->
                    <div class="text-center bs-wizard-stepnum">Penyelesaian</div>';
                    if (!empty($tglbyrk)){
                    echo '<div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center">'.my_date_indo($tglbyrk).'</div>';
                    }

                    if (!empty($tglbyrR)){
                    echo '<div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center">'.my_date_indo($tglbyrR).'</div>';
                    }
                echo '</div>
            </div>
            ';

  }
}
