<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
	public function __Construct(){
		parent::__Construct();
		$this->app->auth();
		$this->load->model('Model_helper','helper');
		$this->load->model('Model_laporan','laporan');
		// $this->output->cache(3600);
		// $this->output->delete_cache();
	}

	public function detail(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		$data["asuradur"] = $this->helper->asuradur();
		$this->app->render('Laporan Detail','laporan/laporan',$data);
	}

	public function produksi(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		$data["asuradur"] = $this->helper->asuradur();
		$this->app->render('Laporan Detail Produksi','laporan/produksi',$data);
	}

	public function polis(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		$data["asuradur"] = $this->helper->asuradur();
		$this->app->render('Laporan Detail Polis','laporan/polis',$data);
	}

	public function klaim(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		$data["asuradur"] = $this->helper->asuradur();
		$this->app->render('Laporan Detail Klaim','laporan/klaim',$data);
	}

	public function restitusi(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		$data["asuradur"] = $this->helper->asuradur();
		$this->app->render('Laporan Detail Restitusi','laporan/restitusi',$data);
	}

	public function summary(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		$data["asuradur"] = $this->helper->asuradur();
		$this->app->render('Laporan Summary','laporan/summary',$data);
	}

	public function asuransi(){
		$this->app->render('Laporan Premi Asuransi','laporan/laporan_premi',null);
	}

	public function lap_asuransi(){
		$draw = intval($this->input->post("draw"));
      	$start = intval($this->input->post("start"));
      	$length = intval($this->input->post("length"));

      	$peserta = $this->laporan->lap_asuransi();
		
      	$data = array();
      	$i = 1;

      	foreach($peserta->result() as $key) {
            $data[] = array(
              	$key->asuransi,
              	price($key->plafon),
              	price($key->JumlahPremiTenor),
              	price($key->TotalOsPremi),
              	price($key->FeePremiBank),
              	price($key->FeePremiBroker),
              	price($key->NetPremi)
            );
       		$i++;
      	}

      	$output = array(
            "draw" => $draw,
            "recordsTotal" => $peserta->num_rows(),
            "recordsFiltered" => $peserta->num_rows(),
            "data" => $data
        );

      	echo json_encode($output);
      	exit();
	}

	public function invoice(){
		$this->app->render('Invoice Pembayaran','laporan/invoice/form_invoice',null);
	}

	public function cariProduksi(){
	    if($this->input->post("tipe")==1){
	    	$tgl_mulai = $this->input->post("tgl_awal");
			$tgl_akhir = $this->input->post("tgl_akhir");
			$data["periode"] = my_date_indo($this->input->post("tgl_awal"))." s/d ".my_date_indo($this->input->post("tgl_akhir"));
			$data["asuransi"] = cekNamaAsdur($this->input->post("asuransi"));
			$data["produk"] = cekProduk($this->input->post("produk"));
			$cabang = $this->input->post("cabang");
			$capem = $this->input->post("capem");
			if(empty($cabang)){
				$data["bank"] = "Semua";	
			}else{
				if(empty($capem)){
					$kodeBank = $cabang;
				}else{
					$kodeBank = $capem;
				}
				$bangpem = bangpem($kodeBank);
		    	if($bangpem->capem==NULL){
		    		$data["bank"] = $bangpem->cabang;
		    	}else{
		    		$data["bank"] = $bangpem->capem." - ".$bangpem->cabang; 
		    	}
			}

		    $data["debitur"] = $this->laporan->getProduksi();
		    $this->app->render2('Laporan Produksi','laporan/produksi/rincian_data_produksi_asuransi',$data);
	    }
	    else if($this->input->post("tipe")==2){
	    	if(empty($this->input->post("bulan"))){
	    		$tgl_mulai = "01-".date("m-Y");
	    	}else{
	    		$tgl_mulai = "01-".($this->input->post("bulan"));
	    	}
			$data["periode"] = date_format(date_create($tgl_mulai),'M Y');
			$data["asuransi"] = cekNamaAsdur($this->input->post("asuransi"));
			$data["produk"] = cekProduk($this->input->post("produk"));
			$cabang = $this->input->post("cabang");
			$capem = $this->input->post("capem");
			if(empty($cabang)){
				$data["bank"] = "Semua";	
			}else{
				if(empty($capem)){
					$kodeBank = $cabang;
				}else{
					$kodeBank = $capem;
				}
				$bangpem = bangpem($kodeBank);
		    	if($bangpem->capem==NULL){
		    		$data["bank"] = $bangpem->cabang;
		    	}else{
		    		$data["bank"] = $bangpem->capem." - ".$bangpem->cabang; 
		    	}
			}

	    	$data["debitur"] = $this->laporan->getProduksi2();
	    	$data["premiterima"] = 0;
	    	$data["premiterimaaktif"] = 0;
	    	foreach ($data["debitur"]->result() as $key) {
	    		$data["premiterima"] = $data["premiterima"]+$key->premiterima;
		    	$data["premiterimaaktif"] = $data["premiterimaaktif"]+$key->premiterimaaktif;
	    	}
	    	$data["totalpremi"] = $data["premiterima"]+$data["premiterimaaktif"];
	    	$this->app->render2('Laporan Produksi','laporan/produksi/rekap_pertanggungan_bulanan_per_asuransi',$data);
	    }else if($this->input->post("tipe")==3){
	    	$tgl_mulai = $this->input->post("tgl_awal2");
			$tgl_akhir = $this->input->post("tgl_akhir2");
			$data["periode"] = my_date_indo($this->input->post("tgl_awal2"))." s/d ".my_date_indo($this->input->post("tgl_akhir2"));
			$data["asuransi"] = cekNamaAsdur($this->input->post("asuransi"));
			$data["produk"] = cekProduk($this->input->post("produk"));
			$cabang = $this->input->post("cabang");
			$capem = $this->input->post("capem");
			if(empty($cabang)){
				$data["bank"] = "Semua";	
			}else{
				if(empty($capem)){
					$kodeBank = $cabang;
				}else{
					$kodeBank = $capem;
				}
				$bangpem = bangpem($kodeBank);
		    	if($bangpem->capem==NULL){
		    		$data["bank"] = $bangpem->cabang;
		    	}else{
		    		$data["bank"] = $bangpem->capem." - ".$bangpem->cabang; 
		    	}
			}


	    	$data["debitur"] = $this->laporan->getProduksi3();
	    	$this->app->render2('Laporan Produksi','laporan/produksi/produksi_asuransi_per_bank',$data);
	    }else{
	    	$data["tahun"] = $this->input->post("tahun");
			$data["periode"] = $data["tahun"];
			$data["asuransi2"] = cekNamaAsdur($this->input->post("asuransi"));
			$data["produk"] = cekProduk($this->input->post("produk"));
			$cabang = $this->input->post("cabang");
			$capem = $this->input->post("capem");
			if(empty($cabang)){
				$data["bank"] = "Semua";	
			}else{
				if(empty($capem)){
					$kodeBank = $cabang;
				}else{
					$kodeBank = $capem;
				}
				$bangpem = bangpem($kodeBank);
		    	if($bangpem->capem==NULL){
		    		$data["bank"] = $bangpem->cabang;
		    	}else{
		    		$data["bank"] = $bangpem->capem." - ".$bangpem->cabang; 
		    	}
			}


	    	$data["debitur"] = array();
	    	for($a=1; $a<=12; $a++){ 
	    		if($a<10){
	    			$a = "0".$a;
	    		}
	    		$cde = $this->laporan->getProduksi4($a)->result();
				if(empty($cde)){
					// $tgl = date_format(date_create($this->input->post("tahun")),'Y-'.$a);
					$tgl = $this->input->post("tahun").'-'.$a;
					$cde =  array (
							(object) array(
							"bulan" => $a,
							"tahun" => $tgl,
				            "jmldeb" => 0,
				            "plafon" => 0,
				            "premi" => 0,
				            "brokerage" => 0,
				            "net" => 0
						)
					);
				}
				
	    		array_push($data["debitur"], $cde);
	    	}
			// echo "<pre>";
	    	
	    	$data["asuransi"] = $this->helper->asuradur();
	    	// die();
	    	$this->app->render2('Laporan Produksi','laporan/produksi/rekap_produksi_asuransi_tahunan',$data);
	    }
	}
	
	public function cariSummary(){
	    if($this->input->post("tipe")==1){
	    	$tgl_mulai = $this->input->post("tgl_awal");
			$tgl_akhir = $this->input->post("tgl_akhir");
			$data["periode"] = my_date_indo($this->input->post("tgl_awal"))." s/d ".my_date_indo($this->input->post("tgl_akhir"));
			$data["asuransi"] = cekNamaAsdur($this->input->post("asuransi"));
			$data["produk"] = cekProduk($this->input->post("produk"));
			$cabang = $this->input->post("cabang");
			$capem = $this->input->post("capem");
			if(empty($cabang)){
				$data["bank"] = "Semua";	
			}else{
				if(empty($capem)){
					$kodeBank = $cabang;
				}else{
					$kodeBank = $capem;
				}
				$bangpem = bangpem($kodeBank);
		    	if($bangpem->capem==NULL){
		    		$data["bank"] = $bangpem->cabang;
		    	}else{
		    		$data["bank"] = $bangpem->capem." - ".$bangpem->cabang; 
		    	}
			}

		    $data["debitur"] = $this->laporan->getSumProduksi();
		    $this->app->render2('Laporan Produksi','laporan/summary/sum_produksi',$data);
	    }
	    else if($this->input->post("tipe")==2){
	    	$tgl_mulai = $this->input->post("tgl_awal");
			$tgl_akhir = $this->input->post("tgl_akhir");
			$data["periode"] = my_date_indo($this->input->post("tgl_awal"))." s/d ".my_date_indo($this->input->post("tgl_akhir"));
			$data["asuransi"] = cekNamaAsdur($this->input->post("asuransi"));
			$data["produk"] = cekProduk($this->input->post("produk"));
			$cabang = $this->input->post("cabang");
			$capem = $this->input->post("capem");
			if(empty($cabang)){
				$data["bank"] = "Semua";	
			}else{
				if(empty($capem)){
					$kodeBank = $cabang;
				}else{
					$kodeBank = $capem;
				}
				$bangpem = bangpem($kodeBank);
		    	if($bangpem->capem==NULL){
		    		$data["bank"] = $bangpem->cabang;
		    	}else{
		    		$data["bank"] = $bangpem->capem." - ".$bangpem->cabang; 
		    	}
			}

	    	$data["debitur"] = $this->laporan->getSumRestitusi();
	    	$this->app->render2('Laporan Produksi','laporan/summary/sum_restitusi',$data);
	    }else{
	    	$tgl_mulai = $this->input->post("tgl_awal");
			$tgl_akhir = $this->input->post("tgl_akhir");
			$data["periode"] = my_date_indo($this->input->post("tgl_awal"))." s/d ".my_date_indo($this->input->post("tgl_akhir"));
			$data["asuransi"] = cekNamaAsdur($this->input->post("asuransi"));
			$data["produk"] = cekProduk($this->input->post("produk"));
			$cabang = $this->input->post("cabang");
			$capem = $this->input->post("capem");
			if(empty($cabang)){
				$data["bank"] = "Semua";	
			}else{
				if(empty($capem)){
					$kodeBank = $cabang;
				}else{
					$kodeBank = $capem;
				}
				$bangpem = bangpem($kodeBank);
		    	if($bangpem->capem==NULL){
		    		$data["bank"] = $bangpem->cabang;
		    	}else{
		    		$data["bank"] = $bangpem->capem." - ".$bangpem->cabang; 
		    	}
			}
	    	$data["debitur"] = $this->laporan->getSumKlaim();
	    	$this->app->render2('Laporan Produksi','laporan/summary/sum_klaim',$data);
	    }
	}

	public function cariPolis(){
	    if($this->input->post("tipe")==1){
	    	$tgl_mulai = $this->input->post("tgl_awal");
			$tgl_akhir = $this->input->post("tgl_akhir");
			$data["periode"] = my_date_indo($this->input->post("tgl_awal"))." s/d ".my_date_indo($this->input->post("tgl_akhir"));
			$data["asuransi"] = cekNamaAsdur($this->input->post("asuransi"));
			$data["produk"] = cekProduk($this->input->post("produk"));
			$cabang = $this->input->post("cabang");
			$capem = $this->input->post("capem");
			if(empty($cabang)){
				$data["bank"] = "Semua";	
			}else{
				if(empty($capem)){
					$kodeBank = $cabang;
				}else{
					$kodeBank = $capem;
				}
				$bangpem = bangpem($kodeBank);
		    	if($bangpem->capem==NULL){
		    		$data["bank"] = $bangpem->cabang;
		    	}else{
		    		$data["bank"] = $bangpem->capem." - ".$bangpem->cabang; 
		    	}
			}

		    $data["debitur"] = $this->laporan->getPolis1();
		    $data["terbit"] = array();
		    $data["belum"] = array();
		    $this->app->render2('Laporan Polis','laporan/polis/rincian_polis_belum_terbit',$data);
	    }else if($this->input->post("tipe")==2){
	    	if(empty($this->input->post("bulan"))){
	    		$tgl_mulai = "01-".date("m-Y");
	    	}else{
	    		$tgl_mulai = "01-".($this->input->post("bulan"));
	    	}
	    	$data["tahun"] = date_format(date_create($tgl_mulai),'Y-m');
	    	$data["periode"] = date_format(date_create($tgl_mulai),'M Y');
			$data["asuransi"] = cekNamaAsdur($this->input->post("asuransi"));
			$data["produk"] = cekProduk($this->input->post("produk"));
			$cabang = $this->input->post("cabang");
			$capem = $this->input->post("capem");
			if(empty($cabang)){
				$data["bank"] = "Semua";	
			}else{
				if(empty($capem)){
					$kodeBank = $cabang;
				}else{
					$kodeBank = $capem;
				}
				$bangpem = bangpem($kodeBank);
		    	if($bangpem->capem==NULL){
		    		$data["bank"] = $bangpem->cabang;
		    	}else{
		    		$data["bank"] = $bangpem->capem." - ".$bangpem->cabang; 
		    	}
			}
			
	    	$data["debitur"] = $this->laporan->getPolis2();
	    	$data["pengajuan"] = 0;
	    	$data["terbit"] = 0;
	    	$data["debitursla"] = 0;
	    	$data["debiturnonsla"] = 0;
	    	foreach ($data["debitur"]->result() as $key) {
	    		$data["pengajuan"] = $data["pengajuan"]+$key->jmlpengajuanplafon;
		    	$data["terbit"] = $data["terbit"]+$key->jmlpengajuanjalan;
		    	$data["debitursla"] = $data["debitursla"]+cekSlaDebitur($data["tahun"],$key->kodeAsuransi);
	    		$data["debiturnonsla"] = $data["debiturnonsla"]+cekNonSlaDebitur($data["tahun"],$key->kodeAsuransi);
	    	}
	    	$this->app->render2('Laporan Polis','laporan/polis/rekap_polis_bulanan',$data);
	    }else if($this->input->post("tipe")==3){
	    	$data["tahun"] = date_format(date_create($this->input->post("tgl_akhir2")),'Y-m-d');
	    	$data["periode"] = my_date_indo($data["tahun"]);
			$data["asuransi"] = cekNamaAsdur($this->input->post("asuransi"));
			$data["produk"] = cekProduk($this->input->post("produk"));
			$cabang = $this->input->post("cabang");
			$capem = $this->input->post("capem");
			if(empty($cabang)){
				$data["bank"] = "Semua";	
			}else{
				if(empty($capem)){
					$kodeBank = $cabang;
				}else{
					$kodeBank = $capem;
				}
				$bangpem = bangpem($kodeBank);
		    	if($bangpem->capem==NULL){
		    		$data["bank"] = $bangpem->cabang;
		    	}else{
		    		$data["bank"] = $bangpem->capem." - ".$bangpem->cabang; 
		    	}
			}
			
	    	$data["debitur"] = $this->laporan->getPolis3();
	    	$data["pengajuan"] = 0;
	    	$data["terbit"] = 0;
	    	$data["debitursla"] = 0;
	    	$data["debiturnonsla"] = 0;
	    	foreach ($data["debitur"]->result() as $key) {
	    		$data["pengajuan"] = $data["pengajuan"]+$key->jmlpengajuanplafon;
		    	$data["terbit"] = $data["terbit"]+$key->jmlpengajuanjalan;
		    	$data["debitursla"] = $data["debitursla"]+cekSlaDebitur2($data["tahun"],$key->kodeAsuransi);
	    		$data["debiturnonsla"] = $data["debiturnonsla"]+cekNonSlaDebitur2($data["tahun"],$key->kodeAsuransi);
	    	}
	    	$this->app->render2('Laporan Polis','laporan/polis/rekap_polis_keseluruhan',$data);
	    }else if($this->input->post("tipe")==4){
	    	$data["tahun"] = $this->input->post("tahun");
			$data["periode"] = my_date_indo($data["tahun"]);
			$data["asuransi2"] = cekNamaAsdur($this->input->post("asuransi"));
			$data["produk"] = cekProduk($this->input->post("produk"));
			$cabang = $this->input->post("cabang");
			$capem = $this->input->post("capem");
			if(empty($cabang)){
				$data["bank"] = "Semua";	
			}else{
				if(empty($capem)){
					$kodeBank = $cabang;
				}else{
					$kodeBank = $capem;
				}
				$bangpem = bangpem($kodeBank);
		    	if($bangpem->capem==NULL){
		    		$data["bank"] = $bangpem->cabang;
		    	}else{
		    		$data["bank"] = $bangpem->capem." - ".$bangpem->cabang; 
		    	}
			}
			
	    	$data["asuransi"] = $this->helper->asuradur();
	    	$data["debitur"] = array();
	    	for($a=1; $a<=12; $a++){ 
	    		if($a<10){
	    			$a = "0".$a;
	    		}
	    		$cde = $this->laporan->getPolis4($a)->result();
				if(empty($cde)){
					// $tgl = date_format(date_create($this->input->post("tahun")),'Y-'.$a);
					$tgl = $this->input->post("tahun").'-'.$a;
					$cde =  array ( 
							(object) array(
							"bulan" => $a,
							"tahun" => $tgl,
				            "jmldeb" => 0,
				            "plafon" => 0,
				            "jmldebx" => 0,
				            "plafonx" => 0
						)
					);
				}
				
	    		array_push($data["debitur"], $cde);
	    	}

	    	$this->app->render2('Laporan Polis','laporan/polis/rekap_polis_tahunan',$data);
	    }else{
	    	$tgl_mulai = $this->input->post("tgl_awal3");
			$tgl_akhir = $this->input->post("tgl_akhir3");
			$data["periode"] = my_date_indo($this->input->post("tgl_awal3"))." s/d ".my_date_indo($this->input->post("tgl_akhir3"));
			$data["asuransi"] = cekNamaAsdur($this->input->post("asuransi"));
			$data["produk"] = cekProduk($this->input->post("produk"));
			$cabang = $this->input->post("cabang");
			$capem = $this->input->post("capem");
			if(empty($cabang)){
				$data["bank"] = "Semua";	
			}else{
				if(empty($capem)){
					$kodeBank = $cabang;
				}else{
					$kodeBank = $capem;
				}
				$bangpem = bangpem($kodeBank);
		    	if($bangpem->capem==NULL){
		    		$data["bank"] = $bangpem->cabang;
		    	}else{
		    		$data["bank"] = $bangpem->capem." - ".$bangpem->cabang; 
		    	}
			}
	    	$data["debitur"] = $this->laporan->getPolis5();
	    	$this->app->render2('Laporan Polis','laporan/polis/rekap_polis_perbank',$data);
	    }
	}

	public function cariRestitusi(){
	    if($this->input->post("tipe")==1){
	    	$tgl_mulai = $this->input->post("tgl_awal");
			$tgl_akhir = $this->input->post("tgl_akhir");
			$data["periode"] = my_date_indo($this->input->post("tgl_awal"))." s/d ".my_date_indo($this->input->post("tgl_akhir"));
			$data["asuransi"] = cekNamaAsdur($this->input->post("asuransi"));
			$data["produk"] = cekProduk($this->input->post("produk"));
			$cabang = $this->input->post("cabang");
			$capem = $this->input->post("capem");
			if(empty($cabang)){
				$data["bank"] = "Semua";	
			}else{
				if(empty($capem)){
					$kodeBank = $cabang;
				}else{
					$kodeBank = $capem;
				}
				$bangpem = bangpem($kodeBank);
		    	if($bangpem->capem==NULL){
		    		$data["bank"] = $bangpem->cabang;
		    	}else{
		    		$data["bank"] = $bangpem->capem." - ".$bangpem->cabang; 
		    	}
			}
		    $data["debitur"] = $this->laporan->getRestitusi1();
		    $this->app->render2('Laporan Restitusi','laporan/restitusi/rincian_pengajuan_restitusi',$data);
	    }else if($this->input->post("tipe")==2){
	    	if(empty($this->input->post("bulan"))){
	    		$tgl_mulai = "01-".date("m-Y");
	    	}else{
	    		$tgl_mulai = "01-".($this->input->post("bulan"));
	    	}
	    	$data["tahun"] = date_format(date_create($tgl_mulai),'Y-m');

	    	$data["periode"] = date_format(date_create($tgl_mulai),'M Y');
			$data["asuransi"] = cekNamaAsdur($this->input->post("asuransi"));
			$data["produk"] = cekProduk($this->input->post("produk"));
			$cabang = $this->input->post("cabang");
			$capem = $this->input->post("capem");
			if(empty($cabang)){
				$data["bank"] = "Semua";	
			}else{
				if(empty($capem)){
					$kodeBank = $cabang;
				}else{
					$kodeBank = $capem;
				}
				$bangpem = bangpem($kodeBank);
		    	if($bangpem->capem==NULL){
		    		$data["bank"] = $bangpem->cabang;
		    	}else{
		    		$data["bank"] = $bangpem->capem." - ".$bangpem->cabang; 
		    	}
			}

	    	$data["debitur"] = $this->laporan->getRestitusi2();
	    	$data["pengajuan"] = 0;
	    	$data["terbit"] = 0;
	    	$data["debitursla"] = 0;
	    	$data["debiturnonsla"] = 0;
	    	foreach ($data["debitur"]->result() as $key) {
	    		$data["pengajuan"] = $data["pengajuan"]+$key->nominalajukan;
		    	$data["terbit"] = $data["terbit"]+$key->nominaljalan;
		    	$data["debitursla"] = $data["debitursla"]+cekSlaResDebitur($data["tahun"],$key->kodeAsuransi);
	    		$data["debiturnonsla"] = $data["debiturnonsla"]+cekNonSlaResDebitur($data["tahun"],$key->kodeAsuransi);
	    	}
	    	$this->app->render2('Laporan Restitusi','laporan/restitusi/rekap_restitusi_bulanan',$data);
	    }else if($this->input->post("tipe")==3){
	    	$data["tahun"] = date_format(date_create($this->input->post("tgl_akhir2")),'Y-m-d');
	    	$data["periode"] = my_date_indo($data["tahun"]);
			$data["asuransi"] = cekNamaAsdur($this->input->post("asuransi"));
			$data["produk"] = cekProduk($this->input->post("produk"));
			$cabang = $this->input->post("cabang");
			$capem = $this->input->post("capem");
			if(empty($cabang)){
				$data["bank"] = "Semua";	
			}else{
				if(empty($capem)){
					$kodeBank = $cabang;
				}else{
					$kodeBank = $capem;
				}
				$bangpem = bangpem($kodeBank);
		    	if($bangpem->capem==NULL){
		    		$data["bank"] = $bangpem->cabang;
		    	}else{
		    		$data["bank"] = $bangpem->capem." - ".$bangpem->cabang; 
		    	}
			}

	    	
	    	$data["debitur"] = $this->laporan->getRestitusi3();
	    	$data["pengajuan"] = 0;
	    	$data["terbit"] = 0;
	    	$data["debitursla"] = 0;
	    	$data["debiturnonsla"] = 0;
	    	foreach ($data["debitur"]->result() as $key) {
	    		$data["pengajuan"] = $data["pengajuan"]+$key->nominalajukan;
		    	$data["terbit"] = $data["terbit"]+$key->nominaljalan;
		    	$data["debitursla"] = $data["debitursla"]+cekSlaResDebitur2($data["tahun"],$key->kodeAsuransi);
	    		$data["debiturnonsla"] = $data["debiturnonsla"]+cekNonSlaResDebitur2($data["tahun"],$key->kodeAsuransi);
	    	}
	    	$this->app->render2('Laporan Restitusi','laporan/restitusi/rekap_restitusi_keseluruhan',$data);
	    }else if($this->input->post("tipe")==4){
	    	$data["tahun"] = $this->input->post("tahun2");
			$data["periode"] = $data["tahun"];
			$data["asuransi2"] = cekNamaAsdur($this->input->post("asuransi"));
			$data["produk"] = cekProduk($this->input->post("produk"));
			$cabang = $this->input->post("cabang");
			$capem = $this->input->post("capem");
			if(empty($cabang)){
				$data["bank"] = "Semua";	
			}else{
				if(empty($capem)){
					$kodeBank = $cabang;
				}else{
					$kodeBank = $capem;
				}
				$bangpem = bangpem($kodeBank);
		    	if($bangpem->capem==NULL){
		    		$data["bank"] = $bangpem->cabang;
		    	}else{
		    		$data["bank"] = $bangpem->capem." - ".$bangpem->cabang; 
		    	}
			}
			
	    	$data["asuransi"] = $this->helper->asuradur();
	    	$data["debitur"] = array();
	    	for($a=1; $a<=12; $a++){ 
	    		if($a<10){
	    			$a = "0".$a;
	    		}
	    		$cde = $this->laporan->getRestitusi4($a)->result();
				if(empty($cde)){
					// $tgl = date_format(date_create($this->input->post("tahun2")),'Y-'.$a);
					$tgl = $this->input->post("tahun2").'-'.$a;
					$cde =  array ( 
							(object) array(
							"bulan" => $a,
							"tahun" => $tgl,
				            "jmldeb" => 0,
				            "plafon" => 0,
				            "jmldebx" => 0,
				            "plafonx" => 0
						)
					);
				}
				
	    		array_push($data["debitur"], $cde);
	    	}

	    	$this->app->render2('Laporan Restitusi','laporan/restitusi/rekap_restitusi_tahunan',$data);
	    }else{
	    	$tgl_mulai = $this->input->post("tgl_awal2");
			$tgl_akhir = $this->input->post("tgl_akhir2");
			$data["periode"] = my_date_indo($this->input->post("tgl_awal2"))." s/d ".my_date_indo($this->input->post("tgl_akhir2"));
			$data["asuransi"] = cekNamaAsdur($this->input->post("asuransi"));
			$data["produk"] = cekProduk($this->input->post("produk"));
			$cabang = $this->input->post("cabang");
			$capem = $this->input->post("capem");
			if(empty($cabang)){
				$data["bank"] = "Semua";	
			}else{
				if(empty($capem)){
					$kodeBank = $cabang;
				}else{
					$kodeBank = $capem;
				}
				$bangpem = bangpem($kodeBank);
		    	if($bangpem->capem==NULL){
		    		$data["bank"] = $bangpem->cabang;
		    	}else{
		    		$data["bank"] = $bangpem->capem." - ".$bangpem->cabang; 
		    	}
			}
	    	$data["debitur"] = $this->laporan->getRestitusi5();
	    	$this->app->render2('Laporan Restitusi','laporan/restitusi/penyelesaian_restitusi_perbank',$data);
	    }
	}

	public function cariKlaim(){
	    if($this->input->post("tipe")==1){
	    	$tgl_mulai = $this->input->post("tgl_awal");
			$tgl_akhir = $this->input->post("tgl_akhir");
			$data["periode"] = my_date_indo($this->input->post("tgl_awal"))." s/d ".my_date_indo($this->input->post("tgl_akhir"));
			$data["asuransi"] = cekNamaAsdur($this->input->post("asuransi"));
			$data["produk"] = cekProduk($this->input->post("produk"));
			$cabang = $this->input->post("cabang");
			$capem = $this->input->post("capem");
			if(empty($cabang)){
				$data["bank"] = "Semua";	
			}else{
				if(empty($capem)){
					$kodeBank = $cabang;
				}else{
					$kodeBank = $capem;
				}
				$bangpem = bangpem($kodeBank);
		    	if($bangpem->capem==NULL){
		    		$data["bank"] = $bangpem->cabang;
		    	}else{
		    		$data["bank"] = $bangpem->capem." - ".$bangpem->cabang; 
		    	}
			}


		    $data["debitur"] = $this->laporan->getKlaim1();
		    
		    $this->app->render2('Laporan Klaim','laporan/klaim/rincian_pengajuan_klaim',$data);
	    }else if($this->input->post("tipe")==2){
	    	if(empty($this->input->post("bulan"))){
	    		$tgl_mulai = "01-".date("m-Y");
	    	}else{
	    		$tgl_mulai = "01-".($this->input->post("bulan"));
	    	}
			$data["periode"] = date_format(date_create($tgl_mulai),'M Y');
			$data["asuransi"] = cekNamaAsdur($this->input->post("asuransi"));
			$data["produk"] = cekProduk($this->input->post("produk"));
			$cabang = $this->input->post("cabang");
			$capem = $this->input->post("capem");
			if(empty($cabang)){
				$data["bank"] = "Semua";	
			}else{
				if(empty($capem)){
					$kodeBank = $cabang;
				}else{
					$kodeBank = $capem;
				}
				$bangpem = bangpem($kodeBank);
		    	if($bangpem->capem==NULL){
		    		$data["bank"] = $bangpem->cabang;
		    	}else{
		    		$data["bank"] = $bangpem->capem." - ".$bangpem->cabang; 
		    	}
			}
			
	    	$data["tahun"] = date_format(date_create($tgl_mulai),'Y-m');
	    	$data["debitur"] = $this->laporan->getKlaim2();
	    	$data["pengajuan"] = 0;
	    	$data["terbit"] = 0;
	    	$data["debitursla"] = 0;
	    	$data["debiturnonsla"] = 0;
	    	foreach ($data["debitur"]->result() as $key) {
	    		$data["pengajuan"] = $data["pengajuan"]+$key->nominalajukan;
		    	$data["terbit"] = $data["terbit"]+$key->nominaljalan;
		    	$data["debitursla"] = $data["debitursla"]+cekSlaKlaimDebitur($data["tahun"],$key->kodeAsuransi);
	    		$data["debiturnonsla"] = $data["debiturnonsla"]+cekNonSlaKlaimDebitur($data["tahun"],$key->kodeAsuransi);
	    	}
	    	$this->app->render2('Laporan Klaim','laporan/klaim/rekap_klaim_bulanan',$data);
	    }else if($this->input->post("tipe")==3){
	    	$data["tahun"] = date_format(date_create($this->input->post("tgl_akhir2")),'Y-m-d');
			$data["periode"] = my_date_indo($data["tahun"]);
			$data["asuransi"] = cekNamaAsdur($this->input->post("asuransi"));
			$data["produk"] = cekProduk($this->input->post("produk"));
			$cabang = $this->input->post("cabang");
			$capem = $this->input->post("capem");
			if(empty($cabang)){
				$data["bank"] = "Semua";	
			}else{
				if(empty($capem)){
					$kodeBank = $cabang;
				}else{
					$kodeBank = $capem;
				}
				$bangpem = bangpem($kodeBank);
		    	if($bangpem->capem==NULL){
		    		$data["bank"] = $bangpem->cabang;
		    	}else{
		    		$data["bank"] = $bangpem->capem." - ".$bangpem->cabang; 
		    	}
			}
			
	    	$data["debitur"] = $this->laporan->getKlaim3();
	    	$data["pengajuan"] = 0;
	    	$data["terbit"] = 0;
	    	$data["debitursla"] = 0;
	    	$data["debiturnonsla"] = 0;
	    	foreach ($data["debitur"]->result() as $key) {
	    		$data["pengajuan"] = $data["pengajuan"]+$key->nominalajukan;
		    	$data["terbit"] = $data["terbit"]+$key->nominaljalan;
		    	$data["debitursla"] = $data["debitursla"]+cekSlaKlaimDebitur2($data["tahun"],$key->kodeAsuransi);
	    		$data["debiturnonsla"] = $data["debiturnonsla"]+cekNonSlaKlaimDebitur2($data["tahun"],$key->kodeAsuransi);
	    	}
	    	$this->app->render2('Laporan Klaim','laporan/klaim/rekap_klaim_keseluruhan',$data);
	    }else if($this->input->post("tipe")==4){
	    	$data["tahun"] = $this->input->post("tahun");
	    	$data["periode"] = $this->input->post("tahun");
			$data["asuransi2"] = cekNamaAsdur($this->input->post("asuransi"));
			$data["produk"] = cekProduk($this->input->post("produk"));
			$cabang = $this->input->post("cabang");
			$capem = $this->input->post("capem");
			if(empty($cabang)){
				$data["bank"] = "Semua";	
			}else{
				if(empty($capem)){
					$kodeBank = $cabang;
				}else{
					$kodeBank = $capem;
				}
				$bangpem = bangpem($kodeBank);
		    	if($bangpem->capem==NULL){
		    		$data["bank"] = $bangpem->cabang;
		    	}else{
		    		$data["bank"] = $bangpem->capem." - ".$bangpem->cabang; 
		    	}
			}
			
	    	$data["asuransi"] = $this->helper->asuradur();
	    	$data["debitur"] = array();
	    	for($a=1; $a<=12; $a++){ 
	    		if($a<10){
	    			$a = "0".$a;
	    		}
	    		$cde = $this->laporan->getKlaim4($a)->result();
				if(empty($cde)){
					// $tgl = date_format(date_create($this->input->post("tahun")),'Y-'.$a);
					$tgl = $this->input->post("tahun").'-'.$a;
					$cde =  array ( 
							(object) array(
							"bulan" => $a,
							"tahun" => $tgl,
				            "jmldeb" => 0,
				            "plafon" => 0,
				            "jmldebx" => 0,
				            "plafonx" => 0
						)
					);
				}
				
	    		array_push($data["debitur"], $cde);
	    	}

	    	$this->app->render2('Laporan Klaim','laporan/klaim/rekap_klaim_tahunan',$data);
	    }else{
	    	$data["tahun"] = $this->input->post("tahun");
	    	$data["periode"] = $this->input->post("tahun");
			$data["asuransi"] = cekNamaAsdur($this->input->post("asuransi"));
			$data["produk"] = cekProduk($this->input->post("produk"));
			$cabang = $this->input->post("cabang");
			$capem = $this->input->post("capem");
			if(empty($cabang)){
				$data["bank"] = "Semua";	
			}else{
				if(empty($capem)){
					$kodeBank = $cabang;
				}else{
					$kodeBank = $capem;
				}
				$bangpem = bangpem($kodeBank);
		    	if($bangpem->capem==NULL){
		    		$data["bank"] = $bangpem->cabang;
		    	}else{
		    		$data["bank"] = $bangpem->capem." - ".$bangpem->cabang; 
		    	}
			}
	    	$data["debitur"] = $this->laporan->getKlaim5();
	    	$this->app->render2('Laporan Klaim','laporan/klaim/penyelesaian_klaim_perbank',$data);
	    }
	}

	public function dataInvoice(){
		$draw = intval($this->input->post("draw"));
	      $start = intval($this->input->post("start"));
	      $length = intval($this->input->post("length"));

	      $peserta = $this->laporan->dataInvoice();
			
	      $data = array();
	      $i = 1;

	      foreach($peserta->result() as $key) {
	          $data[] = array(
	          	  $i,
	          	  my_date_indo($key->tgl),
	          	  $key->deb,
	          	  '<span style="text-align: right;">'.price($key->premi).'</span>',
	          	  '<a href="'.base_url().'laporan/print_invoice2" class="btn btn-info btn-xs text-white" target="_blank" ">Print <i class="fa fa-print" aria-hidden="true"></i></a>',
	          	  // '<a href="'.base_url().'laporan/print_invoice/'.$key->tgl.'" class="btn btn-info btn-xs text-white" target="_blank" ">Print <i class="fa fa-print" aria-hidden="true"></i></a>',
	         );
	           $i++;
	      }

	      $output = array(
	            "draw" => $draw,
	            "recordsTotal" => $peserta->num_rows(),
	            "recordsFiltered" => $peserta->num_rows(),
	            "data" => $data
	        );

	      echo json_encode($output);
	      exit();
	}

	public function print_invoice2(){
		$this->load->view('public/test',null);
	}

	public function print_invoice($tgl){
		$data["debitur"] = $this->laporan->invoice($tgl);
		$data["NomorInvoice"] = $this->laporan->NomorInvoice($tgl);
		$data["totaldebitur"] = $this->laporan->invoiceTotal($tgl);

		// echo "<pre>";
		// print_r($data["totaldebitur"]);
		// echo "</pre>";

		// die();

	    $html = '<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>KWITANSI PEMBAYARAN PREMI ASURANSI KREDIT</title>
  <link href="'.base_url().'assets/ela/css/lib/invoice/invoice.css" rel="stylesheet">
  <link href="'.base_url().'application/third_party/mpdf/mpdf.css" rel="stylesheet">
  <link href="'.base_url().'application/third_party/mpdf/lang2fonts.css" rel="stylesheet">

</head>

<body class="body-invoice">

  <header class="clearfix">
    <img  style="width:10%;" src="'.base_url().'assets/ela/images/logoinv.jpg"><div id="alamat">Graha Tirtadi Lantai. 4 Suite 401
		<br> JL. Raden Saleh, No. 20, Kenari, 
		<br> Kota Jakarta Pusat, DKI Jakarta 10330
        <br> Telp. : +62 (21) 39837478</div>
    </header>
      <h1>KWITANSI PEMBAYARAN PREMI ASURANSI KREDIT</h1>
    <table style="width: 100%;" border="0.1" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <th>#</th>
            <th>ASURANSI</th>
            <th>DEBITUR</th>
            <th>PREMI</th>
        </tr>
      </thead>
        <tbody>';
        $totaldebitur = 0;
        $totalpremi = 0;
        $no=1 ; foreach ($data["totaldebitur"]  as $value) {
          $html .= '<tr>
              <td class="no">'.$no.'</td>
              <td class="pinjaman">'.cekNamaAsdur($value->kodeAsuransi).'</td>
              <td class="cif" style="text-align: center;">'.$value->deb.'</td>
              <td class="nama"  style="text-align: right;">'.price($value->premi).'</td>
            </tr>';
        $no++;
        $totaldebitur = $totaldebitur+$value->deb;
        $totalpremi = $totalpremi+$value->premi;
        }
        $html .= '</tbody>
      </table>
      <div id="notices">
        <div><strong>NOTICE</strong>:</div>
        <div class="notice">Bukti pembayaran sah pendaftaran peserta Asuransi Kredit Bank</div>
      </div>
      <div id="total">
        <h1>Rincian Pembayaran</h1>
        <table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
      		<tr>
      			<td>Tanggal Pembayaran</td>
      			<td style="text-align:right;">'.my_date_indo($tgl).'</td>
      		</tr>
      		<tr>
      			<td>Jumlah Debitur</td>
      			<td style="text-align:right;">'.$totaldebitur.'</td>
      		</tr>
      		<tr>
      			<td>Total Premi</td>
      			<td style="text-align:right;">'.price($totalpremi).'</td>
      		</tr>
      	</table>
      </div>
      <br>
      <div id="paraf" class="" >
        <img style="width:150px;" src="'.base_url().'assets/ela/images/mtstagi.png">
        <div><strong>Kristinan Benny Hapsoro</strong></div>
        <div>Direktur Utama</div>
      </div>
  ';

  $html2 = '<div class="page_break">
    <header class="clearfix">
    <div id="logo" ><img src="assets/ela/images/logorep.png" width="70" /><img src="'.base_url().'assets/ela/images/logorep.png"></div><div id="alamat" >Graha Tirtadi Lantai. 4 Suite 401
	<br> JL. Raden Saleh, No. 20, RT.10/RW.1, Kenari, 
	<br> Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10330
	<br> Telp. : +62 (21) 39837478</div>
    </header>
      <h1>INVOICE PEMBAYARAN PREMI ASURANSI KREDIT</h1>
    <table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <th>#</th>
          <th>NO. PINJAMAN</th>
          <th>DEBITUR</th>
          <th>PEKERJAAN</th>
          <th>TGL AKAD</th>
          <th>TENOR</th>
          <th>TOTAL PREMI</th>
          <th>ASURANSI</th>
        </tr>
      </thead>
      <tbody>';
        foreach ($data["debitur"] as $key){
        if($key->KodePekerjaan=="BJ"){
        	$JumlahPremiTenor = $this->laporan->OsPremi($key->NomorPinjaman);
        }else{
        	$JumlahPremiTenor = $key->JumlahPremiTenor;
        }
          $html2 .='
          <tr>
            <td class="no">'.$key->rank.'</td>
            <td class="pinjaman">'.$key->NomorPinjaman.'</td>
            <td class="cif">'.$key->NamaDebitur.'</td>
            <td class="nama">'.cekPekerjaan($key->KodePekerjaan).'</td>
            <td class="tglk">'.my_date_indo($key->TglAkadKredit).'</td>
            <td class="tenor">'.$key->TenorTahun.'</td>
            <td class="plafon">'.price($JumlahPremiTenor).'</td>
            <td class="premi">'.cekNamaAsdur($key->kodeAsuransi).'</td>
          </tr>';            
       } 
      $html2 .= '</tbody>
    </table>
  </div>
  </body>
</html>';


		$this->load->library('M_pdf');

        $this->m_pdf->pdf->SetHTMLFooter('<div style="text-align:center; color:#6d6d6d">Invoice ini dicetak secara elektronik dan valid dengan tanda tangan dan stempel.</div>');


       //generate the PDF from the given html
		$this->m_pdf->pdf->WriteHTML($html);
		$this->m_pdf->pdf->AddPage();
		$this->m_pdf->pdf->WriteHTML($html2);

		$this->m_pdf->pdf->Output('Kwitansi_'.$tgl.'.pdf','D');	
		// print_r($html);	
		// print_r($html2);	
	}
}
