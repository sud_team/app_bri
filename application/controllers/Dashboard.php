<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __Construct(){
		parent::__Construct();
		$this->app->auth();
		$this->load->model("Model_dashboard","dash");
		$this->load->model("Model_helper","helper");
	}

	public function pie(){
		$this->load->view("public/pie");
	}

	public function index(){
		$data["produk"] = $this->helper->produk();
		$data["cabang"] = $this->helper->cabang();
		
		$this->app->render('Dashboard','dashboard',$data);
	}

	public function kalkulator(){
		$plafon = str_replace(".","",$this->input->post("plafon")); 
		$period = $this->input->post("period"); 
		$ratePremi = $this->input->post("ratePremi"); 
		$sukuBunga = $this->input->post("sukuBunga"); 
		echo price(($plafon+($plafon*$sukuBunga))*$ratePremi*$period);
	}

	public function prapen(){
		$plafon = str_replace(".","",$this->input->post("plafon")); 
		$usiadebitur = $this->input->post("usiadebitur"); 
		$usiabatas = $this->input->post("usiabatas"); 
		$tenor = $this->input->post("tenor"); 
		$a = ($usiabatas-$usiadebitur)*0.004;
		$b = (($usiadebitur+$tenor)-$usiabatas)*0.005;
		$c = ($a+$b)*$plafon;
		echo price($c);
	}

	public function restitusi(){
		$plafon = str_replace(".","",$this->input->post("plafon")); 
		$period = $this->input->post("period"); 
		$ratePremi = $this->input->post("interest"); 
		$bulanBayar = $this->input->post("bulanBayar"); 
		$n = $period*12;
		$totalpremi = $plafon*$ratePremi*$period;
		$hitung = (($n-$bulanBayar)/$n)*($totalpremi/2);
		echo price($hitung);
	}

	public function grafik1($tahun){
		$hasil = array();
		$bln = array();
		for ($i=1; $i <= 12 ; $i++) {
			if($i<10){
    			$a = "0".$i;
    		} 
			$bulan = my_date_month($i);
			$plafon = $this->dash->grafik1($tahun."-".$a);
			if(empty($plafon)){
				$plafon = 0;
			}else{
				$plafon = round($plafon,0);
			}
			array_push($bln, $bulan);
			array_push($hasil, $plafon);
		}

		$data["bln"] = json_encode($bln);
		$data["hasil"] = json_encode($hasil);
	}

	public function JatuhTempo(){
			$draw = intval($this->input->post("draw"));
          	$start = intval($this->input->post("start"));
          	$length = intval($this->input->post("length"));

          	$peserta = $this->dash->JatuhTempo();
          	$asuransi = $this->helper->asuradurNot99();
          	$data = array();
          	$i = 1;
        
          foreach($peserta->result() as $r) {

          		$dt = $this->dash->JatuhTempoDt($r->NomorPinjaman,$r->TahunKe);
                
                $data[] = array(
                      $r->NomorPinjaman,
                      $r->NamaDebitur,
                      $r->asuransi,
                      cekNamaBank($r->kodeBank),
                      date_format(date_create($dt->TglJatuhTempo),'Y-m-d'),
                      $r->TahunKe,
                      price($dt->ospremi)
                 );

               $i++;
          }

          $output = array(
                "draw" => $draw,
                "recordsTotal" => $peserta->num_rows(),
                "recordsFiltered" => $peserta->num_rows(),
                "data" => $data
            );

          echo json_encode($output);
          exit();
	}

	public function grafik($id){
		$pisah = explode("-", $id);
		$cabang = $pisah[0];
		$capem = $pisah[1];
		$tahun = $pisah[2];
		$tahun2 = date("Y");
		$data["tahun"] = $tahun;
		$hasil1 = array();
		$ospremi1 = array();
		$bln1 = array();

		$arr 	= array();
		$arr1 	= array();
		$result = array();
		for ($i=1; $i <= 12 ; $i++) {
			if($i<10){
    			$a = "0".$i;
    		} 
			$bulan = my_date_month($i);
			$plafon = $this->dash->grafik1($cabang, $capem, $tahun."-".$a);
			$ospremi = $this->dash->grafikospremi($cabang, $capem, $tahun."-".$a);     
			
			if(empty($plafon)){
				$plafon = 0;
			}else{
				$plafon = round($plafon,0);
			}

			if(empty($ospremi)){
				$ospremi = 0;
			}else{
				$ospremi =  round($ospremi,0);
			}

			array_push($hasil1,$plafon);
			array_push($ospremi1,$ospremi);

		}	
			$arr["name"] = "Premi Dibayar";
			$arr["data"] = $hasil1;

			$arr1["name"] = "OS Premi";
			$arr1["data"] = $ospremi1;

			array_push($result,$arr);
			array_push($result,$arr1);

			print json_encode($result, JSON_NUMERIC_CHECK);
	}

	public function piex($id){
		$pisah = explode("-", $id);
		$cabang = $pisah[0];
		$capem = $pisah[1];
		$tahun = $pisah[2];

		$premit = $this->dash->grafikpremitahun($cabang, $capem, $tahun);
		$klaimt = $this->dash->grafikklaimtahun($cabang, $capem, $tahun);
		$restitusit = $this->dash->grafikrestitusitahun($cabang, $capem, $tahun);
		$plafont = $this->dash->grafik1tahun($cabang, $capem, $tahun);

		$result = array();
		if(empty($plafont)){
			$plafont = 0;
		}else{
			$plafont = round($plafont,0);
		}

		if(empty($premit)){
			$premit = 0;
		}else{
			$premit = round($premit,0);
		}

		if(empty($klaimt)){
			$klaimt = 0;
		}else{
			$klaimt = round($klaimt,0);
		}

		if(empty($restitusit)){
			$restitusit = 0;
		}else{
			$restitusit = round($restitusit,0);
		}

		$abc = array(
				'name'=>'Uang Pertanggungan',
                'y'=>$plafont
			);
		$bce = array(
				'name'=>'Premi',
                'y'=>$premit
			);
		$cde = array(
				'name'=>'Klaim',
                'y'=>$klaimt
			);
		$def = array(
				'name'=>'Restitusi',
                'y'=>$restitusit
			);
		array_push($result,$abc);
		array_push($result,$bce);
		array_push($result,$cde);
		array_push($result,$def);

		print json_encode($result, JSON_NUMERIC_CHECK);
	}

	public function changepassword(){
		$id = create_user();
		$username = $this->session->userdata("username");
		$baris = "";
		$baris .= '<div class="row text-left p-10">';
        $baris .= '<div class="col-sm-4 col-xs-2 control-label text-megna">Username :</div>';
		$baris .= '<div class="col-sm-6 col-xs-2 control-label" id="cif">'.$username.'</div>';


        $baris .= '</div>';
        $baris .= '<div class="row p-10">';
        $baris .= '<div class="col-sm-4 col-xs-2 control-label text-megna">Password Baru :</div>';
        $baris .= '<input class="col-sm-6 col-xs-2 control-label custom-select f-s-12 " id="pass" name="password" type="password" value="">';
        // $baris .= '<lable class="col-sm-4 control-label">Re-Password</lable>';
        // $baris .= '<input class="control-label custom-select f-s-12 " id="repass" name="repassword" type="password" value="">';
        $baris .= '</div>';
        $baris .= '<div class="col-xs-12 text-danger">Ganti password anda secara berkala untuk kenyamanan dan keamanan';
        $baris .= '</div>';
    	echo $baris;
	}

	public function updatePass(){
		$idUser = create_user();
		$pass = sha1($this->input->post("password")).":".sha1("askred");
		$query = $this->db->query("update t_user set pass = '$pass' where idUser = '$idUser'");
		$this->session->set_flashdata("success","Update berhasil disimpan");
		redirect("dashboard");
	}

	public function invoice(){
	    $this->pdf->set_paper(array(0, 0, 595, 841), 'portrait');
	    $this->pdf->load_view("public/laporan/invoice");
		$this->pdf->render();

	    $canvas = $this->pdf->get_canvas();

	    //For Footer
	    $footer = $canvas->open_object();
	    $font = Font_Metrics::get_font("Arial");
      	$canvas->line(15,790,580,790,array(0,0,0),1);
		$canvas->page_text(175, 800, "Invoice ini dicetak secara elektronik dan valid tanpa tanda tangan dan stempel.", $font, 9, array(0, 0, 0));
	    $canvas->page_text(290, 810, "{PAGE_NUM} dari {PAGE_COUNT}", $font, 8, array(0, 0, 0));
	    $canvas->close_object();
	    $canvas->add_object($footer, "all");

	    $pdf_file_name = $file_name . '.pdf';
	    $this->pdf->stream($pdf_file_name, array("Attachment" => 0));
	}

	public function ftp(){
		$this->load->library('ftp');

		$config['hostname'] = '172.22.0.2';
		$config['username'] = 'JTMBROKER04';
		$config['password'] = 'admin!123';
		$config['debug']        = TRUE;

		$conn = $this->ftp->connect($config);

		if($conn){
			echo "Login";
		}else{
			echo "Gagal";
		}
		
		$this->ftp->close();
		// $ftp_server = "172.22.0.2";
		// $ftp_username = "JTMBROKER04";
		// $ftp_password = "admin!123";

		// // Create an FTP connection
		// $conn = ftp_connect($ftp_server);

		// // Login to FTP account using username and password
		// $login = ftp_login($conn, $ftp_username, $ftp_password);

		// if($login){
		// 	echo "Login";
		// }else{
		// 	echo "Gagal";
		// }

		// // // Get the contents of the current directory
		// // // Change the second parameter if wanting a subdirectories contents
		// // $files = ftp_nlist($conn, ".");

		// // // Loop through $files
		// // foreach ($files as $file) {

		// // 	// Do something with the file here. 

		// // 	// For now we'll echo the filenames in a list
		// // 	echo $file."<br />";

		// // }
	}
}
