<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_klaim extends CI_Model{

	public function __Construct(){
		parent::__Construct();
		$this->load->database();
	}

	public function nomorpinjaman($noreg){
		$query = $this->db->query("select NomorPinjaman from t_pinjaman where NomorRegistrasi = '$noreg'");
		return $query->row()->NomorPinjaman;
	}

	public function debitur($noreg){
		$query = $this->db->query("select * from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) left join t_pinjaman_polis c on(a.NomorRegistrasi=c.NomorRegistrasi) where a.NomorRegistrasi = '$noreg'");
		return $query->row();
	}

	public function DokUmum(){
		$query = $this->db->query("select * from t_resiko_dokumen where idResiko = 0");
		return $query->result();
	}

	public function DokKhusus($idResiko){
		$query = $this->db->query("select * from t_resiko_dokumen where idResiko = $idResiko");
		return $query->result();
	}

	public function DokKhusus2($noreg){
		$query = $this->db->query("select * from t_pinjaman_klaim_dokumen where KodeKlaim = '$noreg' and jenis = 'khusus'");
		return $query->result();
	}

	public function saveKlaim(){
		$KodeKlaim = $this->input->post("KodeKlaim");
		$noreg = $this->input->post("NomorRegistrasi");
		$TglKlaim = date_format(date_create($this->input->post("tglklaim")),'Y-m-d');
		$TglResiko = date_format(date_create($this->input->post("tglresiko")),'Y-m-d');
		$NominalDiajukan = str_replace(".", "", $this->input->post("nominaldiajukan"));
		$idResiko = $this->input->post("idResiko");
		$kronologi = $this->input->post("kronologi");
		$user = create_user();
		$hariini = hariini();

		$cek = $this->db->query("select KodeKlaim from t_pinjaman_klaim where KodeKlaim = '$KodeKlaim'")->row()->KodeKlaim;
		if(empty($cek)){
			$update = $this->db->query("update t_pinjaman set StatusKlaim = 1 where NomorRegistrasi = '$noreg'");
			if($this->db->affected_rows()>0){
				$query = $this->db->query("insert into t_pinjaman_klaim values ('$KodeKlaim','$noreg','$TglKlaim','$TglResiko','$NominalDiajukan','$idResiko','$kronologi','','','','1','0','0','0','0','$user','$hariini','','','','','','','','','','','','','0','','')");
				if($query){
					return TRUE;
				}else{
					return FALSE;
				}
			}
		}else{
			if(empty($idResiko)){
			$query = $this->db->query("update t_pinjaman_klaim set update_user = '$user',update_date = '$hariini' where KodeKlaim = '$KodeKlaim'");
			}else{
			$update = $this->db->query("update t_pinjaman set StatusKlaim = 1 where NomorRegistrasi = '$noreg'");
			$query = $this->db->query("update t_pinjaman_klaim set TglKlaim = '$TglKlaim',TglResiko = '$TglResiko',NominalDiajukan = '$NominalDiajukan',idResiko = '$idResiko',kronologi = '$kronologi', update_user = '$user', update_date = '$hariini', keputusan = '0', diajukan = '1' where KodeKlaim = '$KodeKlaim'");
			}
			if($query){
				return TRUE;
			}else{
				return FALSE;
			}
		}


	}

	public function saveKlaimDokumen($KodeKlaim,$dokumen,$nama,$upload,$idResiko,$jenis){
		$cek = $this->db->query("select top 1 KodeKlaim from t_pinjaman_klaim_dokumen where KodeKlaim = '$KodeKlaim'");
		if(empty($cek->row()->KodeKlaim)){
			$query = $this->db->query("insert into t_pinjaman_klaim_dokumen values ('$KodeKlaim','$dokumen','$nama','$upload','$idResiko','$jenis')");
		}else{
			$cek_dok = $this->db->query("select top 1 dokumen from t_pinjaman_klaim_dokumen where KodeKlaim = '$KodeKlaim' and dokumen = '$dokumen' ");
			if(empty($cek_dok->row()->dokumen)){
				$query = $this->db->query("insert into t_pinjaman_klaim_dokumen values ('$KodeKlaim','$dokumen','$nama','$upload','$idResiko','$jenis')");
			}else{
				if(!empty($nama)){
					$query = $this->db->query("update t_pinjaman_klaim_dokumen set NamaFile = '$nama', upload = '1' where KodeKlaim = '$KodeKlaim' and dokumen = '$dokumen' and jenis = '$jenis'");
				}
			}
		}
	}

	public function pinjaman($noreg){
		$query = $this->db->query("select * from t_pinjaman_klaim where NomorRegistrasi = '$noreg'");
		if(!empty($query->row())){
			return $query->row();
		}
	}

	public function debiturklaim(){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		if ($this->session->userdata("roles")==3) {
		      $and .= " and spv=1 ";
	    } else if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and pialang=1 and kodeAsuransi = '$kodeAngka'";
	    } else{
		    $and = "";
	    }

		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		$produk = $this->input->post("produk");
		$status = $this->input->post("status");
		$nopin = $this->input->post("nopin");
		$nama = $this->input->post("nama");
		if(empty($this->input->post("tgl_mulai")) && empty($this->input->post("tgl_akhir"))){
			$tglawal = "";
			$tglakhir = "";
		}else{
			$tglawal = date_format(date_create($this->input->post("tgl_mulai")),'Y-m-d');
			$tglakhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d');
		}
		
		if($cabang!=""){
			$and .= " and kodeBank = '$cabang'";
		}

		if($capem!=""){
			$and .= " and kodeBank = '$capem'";
		}

		if($produk!=""){
			$and .= " and idProduk = '$produk'";
		}

		if($nopin!=""){
			$and .= " and NomorPinjaman = '$nopin'";
		}

		if($nama!=""){
			$and .= " and NamaDebitur like '%$nama%'";
		}

		if($tglawal!=""){
			$and .= " and convert(varchar(10), a.create_date,120) between '$tglawal' and '$tglakhir'";
		} 

		$query = $this->db->query("select a.NomorRegistrasi, a.idDebitur, a.cif, plafon, TglAkadKredit, TglAkhirKredit, TenorTahun, TcKet, NamaDebitur, UsiaDebitur, KodePekerjaan, a.TglLahir, NomorPK, TarifPremi, premi, JumlahPremiTenor, NomorReferensiPremi, idProduk, keputusan, kodeBank, kodeAsuransi, NomorPolis, TglPolis, StatusKlaim, StatusRestitusi, a.create_date, spv, create_selesai, TglKlaim,max(NomorPinjaman) NomorPinjaman from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) left join t_pinjaman_polis c on (a.NomorRegistrasi=c.NomorRegistrasi) join t_pinjaman_klaim d on(a.NomorRegistrasi=d.NomorRegistrasi) where StatusKlaim = 1 and keputusan = 0 ".$and." group by a.NomorRegistrasi, a.idDebitur, a.cif, plafon, TglAkadKredit, TglAkhirKredit, TenorTahun, TcKet, NamaDebitur, UsiaDebitur, KodePekerjaan, a.TglLahir, NomorPK, TarifPremi, premi, JumlahPremiTenor, NomorReferensiPremi, idProduk, kodeBank, kodeAsuransi, NomorPolis, TglPolis,StatusKlaim, StatusRestitusi, a.create_date,keputusan,spv, TglKlaim,create_selesai");
		return $query;
	}

	public function debiturklaimditangguhkan(){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		if ($this->session->userdata("roles")==3) {
		      $and .= " and spv=1 ";
	    } else if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and pialang=1 and kodeAsuransi = '$kodeAngka'";
	    } else{
		    $and = "";
	    }

		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		$produk = $this->input->post("produk");
		$status = $this->input->post("status");
		$nopin = $this->input->post("nopin");
		$nama = $this->input->post("nama");
		if(empty($this->input->post("tgl_mulai")) && empty($this->input->post("tgl_akhir"))){
			$tglawal = "";
			$tglakhir = "";
		}else{
			$tglawal = date_format(date_create($this->input->post("tgl_mulai")),'Y-m-d');
			$tglakhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d');
		}
		
		if($cabang!=""){
			$and .= " and kodeBank = '$cabang'";
		}

		if($capem!=""){
			$and .= " and kodeBank = '$capem'";
		}

		if($produk!=""){
			$and .= " and idProduk = '$produk'";
		}

		if($nopin!=""){
			$and .= " and NomorPinjaman = '$nopin'";
		}

		if($nama!=""){
			$and .= " and NamaDebitur like '%$nama%'";
		}

		if($tglawal!=""){
			$and .= " and convert(varchar(10), a.create_date,120) between '$tglawal' and '$tglakhir'";
		} 
		$query = $this->db->query("select a.NomorRegistrasi, a.idDebitur, a.cif, plafon, TglAkadKredit, TglAkhirKredit, TenorTahun, TcKet, NamaDebitur, UsiaDebitur, KodePekerjaan, a.TglLahir, NomorPK, TarifPremi, premi, JumlahPremiTenor, NomorReferensiPremi, idProduk, kodeBank, kodeAsuransi, NomorPolis, TglPolis, StatusKlaim, StatusRestitusi, max(NomorPinjaman) NomorPinjaman from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) left join t_pinjaman_polis c on (a.NomorRegistrasi=c.NomorRegistrasi) join t_pinjaman_klaim d on(a.NomorRegistrasi=d.NomorRegistrasi) where StatusKlaim = 2 and keputusan = 0 ".$and." group by a.NomorRegistrasi, a.idDebitur, a.cif, plafon, TglAkadKredit, TglAkhirKredit, TenorTahun, TcKet, NamaDebitur, UsiaDebitur, KodePekerjaan, a.TglLahir, NomorPK, TarifPremi, premi, JumlahPremiTenor, NomorReferensiPremi, idProduk, kodeBank, kodeAsuransi, NomorPolis, TglPolis,StatusKlaim, StatusRestitusi");
		return $query;
	}

	public function debiturklaimditolak(){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		if ($this->session->userdata("roles")==3) {
		      $and .= " and spv=1 ";
	    } else if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and pialang=1 and kodeAsuransi = '$kodeAngka'";
	    } else{
		    $and = "";
	    }

		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		$produk = $this->input->post("produk");
		$status = $this->input->post("status");
		$nopin = $this->input->post("nopin");
		$nama = $this->input->post("nama");
		if(empty($this->input->post("tgl_mulai")) && empty($this->input->post("tgl_akhir"))){
			$tglawal = "";
			$tglakhir = "";
		}else{
			$tglawal = date_format(date_create($this->input->post("tgl_mulai")),'Y-m-d');
			$tglakhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d');
		}
		
		if($cabang!=""){
			$and .= " and kodeBank = '$cabang'";
		}

		if($capem!=""){
			$and .= " and kodeBank = '$capem'";
		}

		if($produk!=""){
			$and .= " and idProduk = '$produk'";
		}

		if($nopin!=""){
			$and .= " and NomorPinjaman = '$nopin'";
		}

		if($nama!=""){
			$and .= " and NamaDebitur like '%$nama%'";
		}

		if($tglawal!=""){
			$and .= " and convert(varchar(10), a.create_date,120) between '$tglawal' and '$tglakhir'";
		} 
		$query = $this->db->query("select a.NomorRegistrasi, a.idDebitur, a.cif, plafon, TglAkadKredit, TglAkhirKredit, TenorTahun, TcKet, NamaDebitur, UsiaDebitur, KodePekerjaan, a.TglLahir, spv, NomorPK, TarifPremi, premi, JumlahPremiTenor, NomorReferensiPremi, idProduk, kodeBank, kodeAsuransi, NomorPolis, TglPolis, StatusKlaim, StatusRestitusi, max(NomorPinjaman) NomorPinjaman from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) left join t_pinjaman_polis c on (a.NomorRegistrasi=c.NomorRegistrasi) join t_pinjaman_klaim d on(a.NomorRegistrasi=d.NomorRegistrasi) where StatusKlaim = '1' and keputusan = -2 ".$and." group by a.NomorRegistrasi, a.idDebitur, a.cif, plafon, TglAkadKredit, TglAkhirKredit, TenorTahun, TcKet, NamaDebitur, UsiaDebitur, KodePekerjaan, a.TglLahir, NomorPK, TarifPremi, premi, JumlahPremiTenor, NomorReferensiPremi, idProduk, kodeBank, kodeAsuransi, NomorPolis, TglPolis,StatusKlaim, StatusRestitusi, spv");
		return $query;
	}

	public function debiturklaimdisetujui(){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		if ($this->session->userdata("roles")==3) {
		      $and .= " and spv=1 ";
	    } else if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and pialang=1 and kodeAsuransi = '$kodeAngka'";
	    } else{
		    $and = "";
	    }

		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		$produk = $this->input->post("produk");
		$status = $this->input->post("status");
		$nopin = $this->input->post("nopin");
		$nama = $this->input->post("nama");
		if(empty($this->input->post("tgl_mulai")) && empty($this->input->post("tgl_akhir"))){
			$tglawal = "";
			$tglakhir = "";
		}else{
			$tglawal = date_format(date_create($this->input->post("tgl_mulai")),'Y-m-d');
			$tglakhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d');
		}
		
		if($cabang!=""){
			$and .= " and kodeBank = '$cabang'";
		}

		if($capem!=""){
			$and .= " and kodeBank = '$capem'";
		}

		if($produk!=""){
			$and .= " and idProduk = '$produk'";
		}

		if($nopin!=""){
			$and .= " and NomorPinjaman = '$nopin'";
		}

		if($nama!=""){
			$and .= " and NamaDebitur like '%$nama%'";
		}

		if($tglawal!=""){
			$and .= " and convert(varchar(10), a.create_date,120) between '$tglawal' and '$tglakhir'";
		}
		$query = $this->db->query("select a.NomorRegistrasi, a.NomorPinjaman, a.idDebitur, a.cif, plafon, TglAkadKredit, TglAkhirKredit, TenorTahun, TcKet, NamaDebitur, UsiaDebitur, KodePekerjaan, a.TglLahir, NomorPK, TarifPremi, premi, JumlahPremiTenor, NomorReferensiPremi, idProduk, kodeBank, kodeAsuransi, NomorPolis, TglPolis, TglKlaim, create_selesai, NominalDisetujui,Resiko, b.KodeKlaim,keputusan, TglKlaim,create_selesai from t_pinjaman a join t_pinjaman_klaim b on(a.NomorRegistrasi=b.NomorRegistrasi) left join t_pinjaman_polis c on (a.NomorRegistrasi=c.NomorRegistrasi) join t_debitur d on(a.idDebitur=d.idDebitur and a.cif=d.cif) join t_resiko e on (e.idResiko=b.idResiko) where keputusan = 1 ".$and."");
		return $query;
	}


	public function saveKeputusan(){
		$kode = $this->input->post("KodeKlaim");
		$keputusan = $this->input->post("keputusan");
		$nominaldisetujui = str_replace(".", "", $this->input->post("nominaldisetujui"));
		$keterangan = $this->input->post("keterangan");
		$hariini = hariini();
		$user = create_user();

		$query = $this->db->query("update t_pinjaman_klaim set asuransi = 1, selesai = 1, create_asuransi = '$hariini', create_selesai = '$hariini', NominalDisetujui = '$nominaldisetujui', keterangan='$keterangan',keputusan = '$keputusan', user_asuransi = '$user' where KodeKlaim = '$kode'");
	}


}