<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_restitusi extends CI_Model{

	public function __Construct(){
		parent::__Construct();
		$this->load->database();
	}

	public function nomorpinjaman($noreg){
		$query = $this->db->query("select NomorPinjaman from t_pinjaman where NomorRegistrasi = '$noreg'");
		return $query->row()->NomorPinjaman;
	}

	public function debitur($noreg){
		
		
		$query = $this->db->query("select *,a.NomorRegistrasi from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) left join t_pinjaman_polis c on(a.NomorRegistrasi=c.NomorRegistrasi) where a.NomorRegistrasi = '$noreg'");
		return $query->row();
	}

	public function DokUmum(){
		$query = $this->db->query("select * from t_resiko_dokumen where idResiko = 0");
		return $query->result();
	}

	public function DokKhusus($idResiko){
		$query = $this->db->query("select * from t_resiko_dokumen where idResiko = $idResiko");
		return $query->result();
	}

	public function DokKhusus2($noreg){
		$query = $this->db->query("select * from t_pinjaman_restitusi_dokumen where KodeRestitusi = '$noreg'");
		return $query->result();
	}

	public function saveRestitusi(){
		$KodeRestitusi = $this->input->post("KodeRestitusi");
		$noreg = $this->input->post("NomorRegistrasi");
		$valPremi = str_replace(".","",$this->input->post("premi"));
		$valPeriode = $this->input->post("periode");
		$valSisa = $this->input->post("sisa");
		$tglpelunasan = date_format(date_create($this->input->post("tglpelunasan")),'Y-m-d 00:00:00');
		$presen = "0.5";
		$user = create_user();
		$hariini = hariini();
		

		$cek = $this->db->query("select KodeRestitusi from t_pinjaman_restitusi where KodeRestitusi = '$KodeRestitusi'")->row()->KodeRestitusi;
		if(empty($cek)){
			$update = $this->db->query("update t_pinjaman set StatusRestitusi = 1 where NomorRegistrasi = '$noreg'");
			if($this->db->affected_rows()>0){
				$query = $this->db->query("insert into t_pinjaman_restitusi values ('$KodeRestitusi','$noreg','$hariini','$tglpelunasan','$valPremi','$valPeriode','$valSisa','$presen','1','0','0','0','0','$user','$hariini','','','','','','','','','',0,'','','','','','','')");
				if($query){
					return TRUE;
				}else{
					return FALSE;
				}
			}
		}else{
			if(empty($valPremi)){
			$query = $this->db->query("update t_pinjaman_restitusi set update_user = '$user', update_date = '$hariini' where KodeRestitusi = '$KodeRestitusi'");

			}else{
			$query = $this->db->query("update t_pinjaman_restitusi set TglPengajuanRestitusi = '$hariini',TglPelunasanRestitusi = '$tglpelunasan',PengembalianPremi = '$valPremi',BulanBerjalan = '$valPeriode',SisaBulan = '$valSisa', update_user = '$user', update_date = '$hariini' where KodeRestitusi = '$KodeRestitusi'");
			}
			if($query){
				return TRUE;
			}else{
				return FALSE;
			}
		}

	}

	public function saveRestitusiDokumen($KodeRestitusi,$dokumen,$nama,$urut){
		$cek = $this->db->query("select top 1 KodeRestitusi from t_pinjaman_restitusi_dokumen where KodeRestitusi = '$KodeRestitusi'");
		if(empty($cek->row()->KodeRestitusi)){
			if(empty($dokumen)){
				$dokumen = $dokumen;
			}
			$query = $this->db->query("insert into t_pinjaman_restitusi_dokumen values ('$KodeRestitusi','$dokumen','$nama','$urut')");
		}else{
			$cek_dok = $this->db->query("select top 1 dokumen, urut from t_pinjaman_restitusi_dokumen where KodeRestitusi = '$KodeRestitusi' and urut = '$urut' ");
			if(empty($cek_dok->row()->dokumen)){
				if(empty($cek_dok->row()->urut)){
					$query = $this->db->query("insert into t_pinjaman_restitusi_dokumen values ('$KodeRestitusi','$dokumen','$nama','$urut')");
				}else{
					$query = $this->db->query("update t_pinjaman_restitusi_dokumen set NamaFile = '$nama', dokumen = '$dokumen' where KodeRestitusi = '$KodeRestitusi' and urut = '$urut'");
				}
			}else{
				$query = $this->db->query("update t_pinjaman_restitusi_dokumen set NamaFile = '$nama', dokumen = '$dokumen' where KodeRestitusi = '$KodeRestitusi' and urut = '$urut'");
			}
		}

		
	}

	public function pinjaman($noreg){
		$query = $this->db->query("select * from t_pinjaman_restitusi where NomorRegistrasi = '$noreg'");
		if(!empty($query->row())){
			return $query->row();
		}
	}

	public function resdok($noreg){
		$query = $this->db->query("select * from t_pinjaman_restitusi_dokumen where KodeRestitusi = '$noreg'");
		return $query;
	}

	public function debiturrestitusi(){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		if ($this->session->userdata("roles")==3) {
		      $and .= " and spv=1 ";
	    } else if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and pialang=1 and kodeAsuransi = '$kodeAngka'";
	    } else{
		    $and = "";
	    }

		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		$produk = $this->input->post("produk");
		$status = $this->input->post("status");
		$nopin = $this->input->post("nopin");
		$nama = $this->input->post("nama");
		if(empty($this->input->post("tgl_mulai")) && empty($this->input->post("tgl_akhir"))){
			$tglawal = "";
			$tglakhir = "";
		}else{
			$tglawal = date_format(date_create($this->input->post("tgl_mulai")),'Y-m-d');
			$tglakhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d');
		}
		
		if($cabang!=""){
			$and .= " and kodeBank = '$cabang'";
		}

		if($capem!=""){
			$and .= " and kodeBank = '$capem'";
		}

		if($produk!=""){
			$and .= " and idProduk = '$produk'";
		}

		if($nopin!=""){
			$and .= " and NomorPinjaman = '$nopin'";
		}

		if($nama!=""){
			$and .= " and NamaDebitur like '%$nama%'";
		}

		if($tglawal!=""){
			$and .= " and convert(varchar(10), a.create_date,120) between '$tglawal' and '$tglakhir'";
		} 
		$query = $this->db->query("select a.NomorRegistrasi, keputusan, a.idDebitur, a.cif, plafon, TglAkadKredit, TglAkhirKredit, TenorTahun, TcKet, NamaDebitur, UsiaDebitur, KodePekerjaan, a.TglLahir, NomorPK, TarifPremi, premi, JumlahPremiTenor, NomorReferensiPremi, TglPengajuanRestitusi, TglPelunasanRestitusi, idProduk, kodeBank, kodeAsuransi, NomorPolis, TglPolis, StatusRestitusi, spv,StatusRestitusi, max(NomorPinjaman) NomorPinjaman from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) left join t_pinjaman_polis c on (a.NomorRegistrasi=c.NomorRegistrasi) join t_pinjaman_restitusi d on(a.NomorRegistrasi=d.NomorRegistrasi) where StatusRestitusi = 1 and keputusan = 0 ".$and." group by a.NomorRegistrasi, a.idDebitur, a.cif, plafon, TglAkadKredit, TglAkhirKredit, TenorTahun, TcKet, NamaDebitur, UsiaDebitur, KodePekerjaan, a.TglLahir, NomorPK, TarifPremi, premi, JumlahPremiTenor, NomorReferensiPremi, idProduk, kodeBank, kodeAsuransi, keputusan,NomorPolis, TglPolis,StatusRestitusi, TglPengajuanRestitusi,spv, TglPelunasanRestitusi,StatusRestitusi");
		return $query;
	}

	public function debiturrestitusiditangguhkan(){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if ($this->session->userdata("roles")==3) {
		      $and .= " and spv=1 ";
	    } else if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and pialang=1 and kodeAsuransi = '$kodeAngka'";
	    } else{
		    $and = "";
	    }
		$query = $this->db->query("select a.NomorRegistrasi, a.idDebitur, a.cif, plafon, TglAkadKredit, TglAkhirKredit, TenorTahun, TcKet, NamaDebitur, UsiaDebitur, KodePekerjaan, a.TglLahir, NomorPK, TarifPremi, premi, JumlahPremiTenor, NomorReferensiPremi, idProduk, kodeBank, kodeAsuransi, NomorPolis, TglPolis, StatusRestitusi, StatusRestitusi, max(NomorPinjaman) NomorPinjaman from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) left join t_pinjaman_polis c on (a.NomorRegistrasi=c.NomorRegistrasi) join t_pinjaman_restitusi d on(a.NomorRegistrasi=d.NomorRegistrasi) where StatusRestitusi = 2 and keputusan = 0 ".$and."  group by a.NomorRegistrasi, a.idDebitur, a.cif, plafon, TglAkadKredit, TglAkhirKredit, TenorTahun, TcKet, NamaDebitur, UsiaDebitur, KodePekerjaan, a.TglLahir, NomorPK, TarifPremi, premi, JumlahPremiTenor, NomorReferensiPremi, idProduk, kodeBank, kodeAsuransi, NomorPolis, TglPolis,StatusRestitusi, StatusRestitusi");
		return $query->result();
	}

	public function debiturrestitusiditolak(){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if ($this->session->userdata("roles")==3) {
		      $and .= " and spv=1 ";
	    } else if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and pialang=1 and kodeAsuransi = '$kodeAngka'";
	    } else{
		    $and = "";
	    }
		$query = $this->db->query("select a.NomorRegistrasi, a.idDebitur, a.cif, plafon, TglAkadKredit, TglAkhirKredit, TenorTahun, TcKet, NamaDebitur, UsiaDebitur, KodePekerjaan, a.TglLahir, NomorPK, TarifPremi, premi, JumlahPremiTenor, NomorReferensiPremi, idProduk, kodeBank, kodeAsuransi, NomorPolis, TglPolis, StatusRestitusi, StatusRestitusi, max(NomorPinjaman) NomorPinjaman from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) left join t_pinjaman_polis c on (a.NomorRegistrasi=c.NomorRegistrasi) join t_pinjaman_restitusi d on(a.NomorRegistrasi=d.NomorRegistrasi) where StatusRestitusi = '-1' and keputusan = 0 ".$and."  group by a.NomorRegistrasi, a.idDebitur, a.cif, plafon, TglAkadKredit, TglAkhirKredit, TenorTahun, TcKet, NamaDebitur, UsiaDebitur, KodePekerjaan, a.TglLahir, NomorPK, TarifPremi, premi, JumlahPremiTenor, NomorReferensiPremi, idProduk, kodeBank, kodeAsuransi, NomorPolis, TglPolis,StatusRestitusi, StatusRestitusi");
		return $query->result();
	}

	public function debiturrestitusidisetujui(){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if ($this->session->userdata("roles")==3) {
		      $and .= " and spv=1 ";
	    } else if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and pialang=1 and kodeAsuransi = '$kodeAngka'";
	    } else{
		    $and = "";
	    }
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		$produk = $this->input->post("produk");
		$status = $this->input->post("status");
		$nopin = $this->input->post("nopin");
		$nama = $this->input->post("nama");
		if(empty($this->input->post("tgl_mulai")) && empty($this->input->post("tgl_akhir"))){
			$tglawal = "";
			$tglakhir = "";
		}else{
			$tglawal = date_format(date_create($this->input->post("tgl_mulai")),'Y-m-d');
			$tglakhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d');
		}
		
		if($cabang!=""){
			$and .= " and kodeBank = '$cabang'";
		}

		if($capem!=""){
			$and .= " and kodeBank = '$capem'";
		}

		if($produk!=""){
			$and .= " and idProduk = '$produk'";
		}

		if($nopin!=""){
			$and .= " and NomorPinjaman = '$nopin'";
		}

		if($nama!=""){
			$and .= " and NamaDebitur like '%$nama%'";
		}

		if($tglawal!=""){
			$and .= " and convert(varchar(10), a.create_date,120) between '$tglawal' and '$tglakhir'";
		} 
		$query = $this->db->query("select a.NomorRegistrasi, TglPengajuanRestitusi, a.idDebitur, a.cif, plafon, TglAkadKredit, TglAkhirKredit, TenorTahun, TcKet, NamaDebitur, UsiaDebitur, KodePekerjaan, a.TglLahir, NomorPK, TarifPremi, premi, JumlahPremiTenor, NomorReferensiPremi, idProduk, kodeBank, keputusan, a.NomorPinjaman, kodeAsuransi, NomorPolis, TglPolis, TglPelunasanRestitusi, create_selesai, PengembalianPremi, b.KodeRestitusi from t_pinjaman a join t_pinjaman_restitusi b on(a.NomorRegistrasi=b.NomorRegistrasi) left join t_pinjaman_polis c on (a.NomorRegistrasi=c.NomorRegistrasi) join t_debitur d on(a.idDebitur=d.idDebitur and a.cif=d.cif)  where keputusan = 1 ".$and." ");
		return $query;
	}


	public function saveKeputusan(){
		$kode = $this->input->post("KodeRestitusi");
		$keputusan = $this->input->post("keputusan");
		$keterangan = $this->input->post("keterangan");
		$hariini = hariini();
		$user = create_user();

		$query = $this->db->query("update t_pinjaman_restitusi set asuransi = 1, selesai = 1, create_asuransi = '$hariini', create_selesai = '$hariini', keterangan='$keterangan',keputusan = '$keputusan', user_asuransi = '$user' where KodeRestitusi = '$kode'");
	}


}