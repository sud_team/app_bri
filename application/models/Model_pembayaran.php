<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pembayaran extends CI_Model{

	public function __Construct(){
		parent::__Construct();
		$this->load->database();
	}

	public function filter_internalmemo_klaim(){
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		$produk = $this->input->post("produk");
		$jenis_im = $this->input->post("jenis_im");
		$nopin = $this->input->post("nopin");
		$nama = $this->input->post("nama");
		if(empty($this->input->post("tgl_mulai")) && empty($this->input->post("tgl_akhir"))){
			$tglawal = "";
			$tglakhir = "";
		}else{
			$tglawal = date_format(date_create($this->input->post("tgl_mulai")),'Y-m-d');
			$tglakhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d');
		}
		
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		
		if($cabang!=""){
			$and .= " and kodeBank = '$cabang'";
		}

		if($capem!=""){
			$and .= " and kodeBank = '$capem'";
		}

		if($produk!=""){
			$and .= " and idProduk = '$produk'";
		}

		if($nopin!=""){
			$and .= " and NomorPinjaman = '$nopin'";
		}

		if($nama!=""){
			$and .= " and NamaDebitur like '%$nama%'";
		}

		if($tglawal!=""){
			$and .= " and convert(varchar(10), a.create_date,120) between '$tglawal' and '$tglakhir'";
		}

		$query = $this->db->query("select NomorPinjaman, a.NomorRegistrasi, NamaDebitur, create_selesai, kodeBank, kodeAsuransi,NominalDisetujui as nominal,'Klaim' as jenis from t_debitur c join t_pinjaman b on b.cif=c.cif and b.idDebitur=c.idDebitur  left join t_pinjaman_klaim a on a.NomorRegistrasi=b.NomorRegistrasi where StatusRekonsel = '1' and selesai = '1' and keputusan = '1' and StatusPembayaran = 0 ".$and);
		return $query;
		// return $query->result();
	}

	public function filter_internalmemo_restitusi(){
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		$produk = $this->input->post("produk");
		$jenis_im = $this->input->post("jenis_im");
		$nopin = $this->input->post("nopin");
		$nama = $this->input->post("nama");
		if(empty($this->input->post("tgl_mulai")) && empty($this->input->post("tgl_akhir"))){
			$tglawal = "";
			$tglakhir = "";
		}else{
			$tglawal = date_format(date_create($this->input->post("tgl_mulai")),'Y-m-d');
			$tglakhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d');
		}
		
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		
		if($cabang!=""){
			$and .= " and kodeBank = '$cabang'";
		}

		if($capem!=""){
			$and .= " and kodeBank = '$capem'";
		}

		if($produk!=""){
			$and .= " and idProduk = '$produk'";
		}

		if($nopin!=""){
			$and .= " and NomorPinjaman = '$nopin'";
		}

		if($nama!=""){
			$and .= " and NamaDebitur like '%$nama%'";
		}

		if($tglawal!=""){
			$and .= " and convert(varchar(10), a.create_date,120) between '$tglawal' and '$tglakhir'";
		}

		$query = $this->db->query("select NomorPinjaman, a.NomorRegistrasi, NamaDebitur, create_selesai, kodeBank, kodeAsuransi, PengembalianPremi as nominal,'Restitusi' as jenis from t_debitur c join t_pinjaman b on b.cif=c.cif and b.idDebitur=c.idDebitur left join t_pinjaman_restitusi a on a.NomorRegistrasi=b.NomorRegistrasi where StatusRekonsel = '1' and selesai = '1' and keputusan = '1'  and StatusPembayaran = 0 ".$and);
		return $query;
		// return $query->result();
	}

	public function savePembayaran(){
		$kodePembayaran = $this->input->post("kode");
		$jenis = $this->input->post("jenis");
		$tgl = date_format(date_create($this->input->post("tgl")),'Y-m-d 00:00:00');
		$nominal = str_replace(".","",$this->input->post("nominal"));
		$NomorPinjaman = $this->input->post("NomorPinjaman");
		$hariini = hariini();
		$user = create_user();

		if($jenis=="Klaim"){
			$table = "t_pinjaman_klaim";
		}else{
			$table = "t_pinjaman_restitusi";
		}
		
		$update = $this->db->query("update ".$table." set KodePembayaran = '$kodePembayaran', TanggalBayar ='$tgl', NominalPembayaran = '$nominal', StatusPembayaran = '1', bayar_user = '$user', bayar_date = '$hariini' where NomorRegistrasi = (select NomorRegistrasi from t_pinjaman where NomorPinjaman = '$NomorPinjaman') ");

		if($update){
			return TRUE;
		}else{
			return FALSE;
		}
		
	}

	public function filter_pembayaran_klaim(){
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		$produk = $this->input->post("produk");
		$jenis_im = $this->input->post("jenis_im");
		$nopin = $this->input->post("nopin");
		$nama = $this->input->post("nama");
		if(empty($this->input->post("tgl_mulai")) && empty($this->input->post("tgl_akhir"))){
			$tglawal = "";
			$tglakhir = "";
		}else{
			$tglawal = date_format(date_create($this->input->post("tgl_mulai")),'Y-m-d');
			$tglakhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d');
		}
		
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		
		if($cabang!=""){
			$and .= " and kodeBank = '$cabang'";
		}

		if($capem!=""){
			$and .= " and kodeBank = '$capem'";
		}

		if($produk!=""){
			$and .= " and idProduk = '$produk'";
		}

		if($nopin!=""){
			$and .= " and NomorPinjaman = '$nopin'";
		}

		if($nama!=""){
			$and .= " and NamaDebitur like '%$nama%'";
		}

		if($tglawal!=""){
			$and .= " and convert(varchar(10), a.create_date,120) between '$tglawal' and '$tglakhir'";
		}

		$query = $this->db->query("select KodePembayaran, NomorPinjaman, a.NomorRegistrasi, NamaDebitur, TanggalBayar, kodeBank, kodeAsuransi,NominalPembayaran as nominal,'Klaim' as jenis from t_debitur c join t_pinjaman b on b.cif=c.cif and b.idDebitur=c.idDebitur  left join t_pinjaman_klaim a on a.NomorRegistrasi=b.NomorRegistrasi where StatusRekonsel = '1' and selesai = '1' and StatusPembayaran = 1 ".$and);
		return $query;
		// return $query->result();
	}

	public function filter_pembayaran_restitusi(){
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		$produk = $this->input->post("produk");
		$jenis_im = $this->input->post("jenis_im");
		$nopin = $this->input->post("nopin");
		$nama = $this->input->post("nama");
		if(empty($this->input->post("tgl_mulai")) && empty($this->input->post("tgl_akhir"))){
			$tglawal = "";
			$tglakhir = "";
		}else{
			$tglawal = date_format(date_create($this->input->post("tgl_mulai")),'Y-m-d');
			$tglakhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d');
		}
		
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		
		if($cabang!=""){
			$and .= " and kodeBank = '$cabang'";
		}

		if($capem!=""){
			$and .= " and kodeBank = '$capem'";
		}

		if($produk!=""){
			$and .= " and idProduk = '$produk'";
		}

		if($nopin!=""){
			$and .= " and NomorPinjaman = '$nopin'";
		}

		if($nama!=""){
			$and .= " and NamaDebitur like '%$nama%'";
		}

		if($tglawal!=""){
			$and .= " and convert(varchar(10), a.create_date,120) between '$tglawal' and '$tglakhir'";
		}

		$query = $this->db->query("select KodePembayaran, NomorPinjaman, a.NomorRegistrasi, NamaDebitur, TanggalBayar, kodeBank, kodeAsuransi, NominalPembayaran as nominal,'Restitusi' as jenis from t_debitur c join t_pinjaman b on b.cif=c.cif and b.idDebitur=c.idDebitur left join t_pinjaman_restitusi a on a.NomorRegistrasi=b.NomorRegistrasi where StatusRekonsel = '1' and selesai = '1'  and StatusPembayaran = 1 ".$and);
		return $query;
		// return $query->result();
	}

	
}