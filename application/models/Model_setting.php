<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_setting extends CI_Model{

	public function __Construct(){
		parent::__Construct();
		$this->load->database();
	}

	public function t_asuransi(){
		$query = $this->db->query("select * from t_asuransi where kodeAngka != 99");
		return $query->result();
	}

	public function list_debitur_prapen(){
		$nopin = $this->input->post("nopin");
		$query = $this->db->query("select NomorPinjaman, plafon, JumlahPremiTenor, TglAkadKredit, TglAkhirKredit, TenorTahun, TenorBulan, NamaDebitur,StatusPrapen from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) where NomorPinjaman = '$nopin'");
		return $query;
	}

	public function updatePrapen(){
		$user = create_user();
		$hariini = hariini();
		$nopin = $this->input->post("NomorPinjaman");
		$query = $this->db->query("update t_pinjaman set StatusPrapen = '1' where NomorPinjaman = '$nopin'");
		if($query){
			$insert = $this->db->query("insert into t_log_setting values ('$user','$hariini','Update Status Prapen Nomor Pinjaman $nopin')");
			return 1;
		}else{
			return 0;
		}
	}

	public function list_debitur_prorate(){
		$nopin = $this->input->post("nopin");
		$query = $this->db->query("select NomorPinjaman, plafon, JumlahPremiTenor, TglAkadKredit, TglAkhirKredit, TenorTahun, TenorBulan, NamaDebitur,StatusPrapen from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) where NomorPinjaman = '$nopin'");
		return $query;
	}

	public function updateProrate(){
		$user = create_user();
		$hariini = hariini();
		$nopin = $this->input->post("NomorPinjaman");
		$premi = str_replace('.','',$this->input->post("JumlahPremiTenor"));
		$fee = $premi*0.1;
		$query = $this->db->query("update t_pinjaman set JumlahPremiTenor = '$premi', FeePremiBroker = '$fee', FeePremiBank = '$fee', TotalSeharusnya = '$premi' where NomorPinjaman = '$nopin'");
		if($query){
			$insert = $this->db->query("insert into t_log_setting values ('$user','$hariini','Update Premi Prorate Nomor Pinjaman $nopin')");
			return 1;
		}else{
			return 0;
		}
	}

	public function list_debitur_pindahan(){
		$nopin = $this->input->post("nopin");
		$query = $this->db->query("select a.NomorPinjaman, a.TglAkadKredit, a.TglAkhirKredit, TenorTahun, TahunKe, TenorBulan, NamaDebitur, kodeAngka, asuransi, a.kodeAsuransi, StatusBayar from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) join t_asuransi c on c.kodeAngka=a.kodeAsuransi join t_pinjaman_bj d on a.NomorPinjaman=d.NomorPinjaman where a.NomorPinjaman = '$nopin'");
		return $query;
	}

	public function updatePindahan(){
		$user = create_user();
		$hariini = hariini();
		$nopin = $this->input->post("NomorPinjaman");
		$tahun = $this->input->post("TahunKe");
		$kodeAsuransiBaru = $this->input->post("kodeAsuransi");
		$tahunsebelumnya = $tahun-1;
		$query = $this->db->query("select NomorRegistrasi, kodeAsuransi from t_pinjaman where NomorPinjaman = '$nopin'");
		$kodeSebelumnya = $query->row()->kodeAsuransi;
		$noreg = $query->row()->NomorRegistrasi;

		$query = $this->db->query("update t_pinjaman set kodeAsuransi = '$kodeAsuransiBaru' where NomorPinjaman = '$nopin'");

		if($query){
			$insert = $this->db->query("insert into t_pinjaman_pindahan_history values ('$noreg','$nopin','$tahunsebelumnya','$kodeSebelumnya','$tahun','$kodeAsuransiBaru','$user','$hariini')");
			$log = $this->db->query("insert into t_log_setting values ('$user','$hariini','Update Perpindahan Asuransi Nomor Pinjaman $nopin')");
			return 1;
		}else{
			return 0;
		}
	}

	
}