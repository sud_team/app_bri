<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_debitur extends CI_Model{

	public function __Construct(){
		parent::__Construct();
		$this->load->database();
	}


	public function debitur(){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		$produk = $this->input->post("produk");
		$status = $this->input->post("status");
		$nopin = $this->input->post("nopin");
		$nama = $this->input->post("nama");
		if(empty($this->input->post("tgl_mulai")) && empty($this->input->post("tgl_akhir"))){
			$tglawal = "";
			$tglakhir = "";
		}else{
			$tglawal = date_format(date_create($this->input->post("tgl_mulai")),'Y-m-d');
			$tglakhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d');
		}
		
		if($cabang!="" && empty($capem)){
			$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang')";
		}

		if($capem!=""){
			$and .= " and kodeBank = '$capem'";
		}

		if($produk!=""){
			$and .= " and idProduk = '$produk'";
		}

		if($nopin!=""){
			$and .= " and NomorPinjaman = '$nopin'";
		}

		if($nama!=""){
			$and .= " and NamaDebitur like '%$nama%'";
		}

		if($tglawal!=""){
			$and .= " and convert(varchar(10), a.create_date,120) between '$tglawal' and '$tglakhir'";
		}

		$query = $this->db->query("select a.NomorRegistrasi, a.NomorPinjaman, a.idDebitur, a.cif, plafon, TglAkadKredit, TglAkhirKredit, TenorTahun, TenorBulan, TcKet, NamaDebitur, UsiaDebitur, KodePekerjaan, a.TglLahir, NomorPK, TarifPremi, premi, JumlahPremiTenor, NomorReferensiPremi, idProduk, kodeBank, kodeAsuransi, NomorPolis, TglPolis, StatusKlaim, StatusRestitusi, StatusRekonsel, TcKet from t_pinjaman a left join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) left join t_pinjaman_polis c on (a.NomorRegistrasi=c.NomorRegistrasi) where StatusDok = 1 ".$and);
		return $query;
		// return $query->result();
	}

	public function cari_debitur(){
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		$produk = $this->input->post("produk");
		$status = $this->input->post("status");
		$nopin = $this->input->post("nopin");
		$nama = $this->input->post("nama");
		$tglawal = date_format(date_create($this->input->post("tglawal")),'Y-m-d');
		$tglakhir = date_format(date_create($this->input->post("tglakhir")),'Y-m-d');
		
		$and = "";
		if($cabang!="" && empty($capem)){
			$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang')";
		}

		if($capem!=""){
			$and .= " and kodeBank = '$capem'";
		}

		if($produk!=""){
			$and .= " and idProduk = '$produk'";
		}

		if($nopin!=""){
			$and .= " and NomorPinjaman = '$nopin'";
		}

		if($nama!=""){
			$and .= " and NamaDebitur = '$nama'";
		}

		if($tglawal!=""){
			$and .= " and TglAkadKredit >= '$tglawal'";
		}

		if($tglakhir!=""){
			$and .= " and TglAkhirKredit <= '$tglakhir'";
		}

		$query = $this->db->query("select NomorRegistrasi, a.idDebitur, a.cif, NomorPinjaman, plafon, TglAkadKredit, TglAkhirKredit, TenorTahun, TcKet, NamaDebitur from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) where StatusDok = 0 ".$and);
		return $query->result();
	}

	
}