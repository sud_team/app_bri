<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_helper extends CI_Model{

	public function __Construct(){
		parent::__Construct();
		$this->load->database();
	}

	public function updateLogin($id, $login){
		$query = $this->db->query("update t_user set is_online = '1', login_at = '$login' where idUser = '$id'");
	}

	public function updateLogout($id, $logout){
		$query = $this->db->query("update t_user set is_online = '0', login_at = '$logout' where idUser = '$id'");
	}	

	public function logdata($id, $akses, $action){
		$query = $this->db->query("insert into t_log values ('$id','$akses','$action')");
	}

	public function is_online(){
		$idUser = create_user();
		$roles = $this->session->userdata("roles");
		$and = "";
		if($roles=="2"){
			$and .= " and (roles = '2' or roles = '3')";
		}else if($roles=="4"){
			$and .= " and (roles = '4' or roles = '3')";
		}else {
			$and .= "";
		}

		// $query = $this->db->query("select count(*) jml, a.idUser, b.send_to, is_online, username from t_user a left join t_chat b on a.idUser=b.send_to where idUser != '$idUser' and count_read = 0 and is_online = 1 ".$and." group by a.idUser, b.send_to, username,is_online");
        $query = $this->db->query("select * from t_user where idUser != '$idUser' and is_online = '1' ".$and);
        return $query;
	}

	public function not_online(){
		$idUser = create_user();
		$roles = $this->session->userdata("roles");
		$and = "";
		if($roles=="2"){
			$and .= " and (roles = '2' or roles = '3')";
		}else if($roles=="4"){
			$and .= " and (roles = '4' or roles = '3')";
		}else {
			$and .= "";
		}

        $query = $this->db->query("select count(*) jml, b.send_to, username from t_user a join t_chat b on a.idUser=b.send_to where idUser != '$idUser' and is_online = '0' and count_read = 0 ".$and." group by b.send_to, username");
        return $query;
	}

	//==== Roles and Menus ====\\
	public function Controller($id_user){
		$this->db->distinct();
		$this->db->join('t_menu_bri','t_menu_bri.idMenu = t_menu_akses.idMenu');
		$this->db->join('t_user','t_user.idUser = t_menu_akses.idUser');
		$this->db->select('t_menu_bri.controller');
		return $this->db->get('t_menu_akses')->result();
	}

	public function user_menu($id = null){
		if(!$id){
			$id = $this->session->userdata('idUser');
		}
		$this->db->distinct();
		$this->db->order_by('t_menu_akses.idMenu','asc');
		$this->db->select('t_menu_akses.idMenu AS MENU_ID');
		$this->db->where('t_menu_akses.idUser',$id);
		$this->db->join('t_menu_bri','t_menu_bri.idMenu = t_menu_akses.idMenu');
		$result = $this->db->get('t_menu_akses')->result_array();
		return  array_column($result, 'MENU_ID');
	}

	//==== Parent Menu ====\\
	public function get_menu($all = false,$selected = null){
		$menus = $this->user_menu();
		if($selected){
			$menus = $selected;
		}
		$this->db->where('parent_id',null);

		$this->db->where_in('idMenu',$menus);
		$this->db->order_by('urut','asc');
		$data = $this->db->get('t_menu_bri')->result_array();
		foreach($data as $d){
			$abc = "";
			if ($d["kategori"]=="H") {
				$abc .= '<li class="nav-label">Home</li>';
			}
			if ($d["kategori"]=="A") {
				$abc .= '<li class="nav-label">Aktifitas</li>';
			}
			if ($d["kategori"]=="L") {
				$abc .= '<li class="nav-label">Laporan</li>';
			}
			if ($d["kategori"]=="E") {
				$abc .= '<li class="nav-label">Master</li>';
			}
			if ($this->session->userdata("roles")==4) {
				$abc .= '<br>';
			}
			echo $abc;
			$this->get_menu_child($d,$menus,$all);			
			// die();
		}
	}

	//==== Child Menu ====\\
	public function get_menu_child($parent,$menus,$type){
		$this->db->where('parent_id',$parent["idMenu"]);
		$this->db->where('status','1');
		$this->db->where_in('idMenu',$menus);
		$this->db->order_by('urut','asc');
		$result = $this->db->get('t_menu_bri')->result_array();
		$link = base_url().''.$parent['link'];
		$icon = $parent["icon"] ? $parent["icon"] : "";
		if(count($result)>0){
			if(!$type){
				echo '<li>';
					echo '<a class="has-arrow" href="#" aria-expanded="false">';
						echo '<i class="fa '.$icon.'"></i> <span class="hide-menu">'.$parent["menu"].'</span>';
					echo '</a>';
					echo '<ul aria-expanded="false" class="collapse">';
					foreach($result as $rs){
						$this->get_menu_child($rs,$menus,$type); 
					}
					echo '</ul>';
				echo '</a>';
			}else{
				echo '<li class="">';
					echo '<input type="checkbox" class="flat-red parent parent_'.$parent["idMenu"].'" value="'.$parent["idMenu"].'" name="menu[]" id="menu" /> <span class="hide-menu">'.$parent["menu"].'</span>';
					echo '<ul class="">';
					foreach($result as $rs){
						$this->get_menu_child($rs,$menus,$type); 
					}
					echo '</ul>';
				echo '</a>';
			}
			
		}else{
			if(!$type){
				if(empty($parent["parent_id"])){
					echo '<li><a href="'.$link.'"><i class="fa '.$icon.'"></i> <span class="hide-menu">'.$parent["menu"].'</span></a></li>';
				}else{
					$notifnomor = "";
					if($parent["kategori"]=="K" || $parent["kategori"]=="R"){
						$jml = "0";
						$jmlh = "0";
						$jmlt = "0";
						$jmls = "0";
						if($parent["kategori"]=="K"){
							$jml = MenungguKlaim();
							$jmlh = DitangguhkanKlaim();
							$jmlt = DitolakKlaim();
							$jmls = DisetujuiKlaim();
						}

						if($parent["kategori"]=="R"){
							$jml = MenungguRestitusi();
							$jmlh = DitangguhkanRestitusi();
							$jmlt = DitolakRestitusi();
							$jmls = DisetujuiRestitusi();
						}
						if($parent["menu"]=="Menunggu Keputusan"){
							$notifnomor = '<span class="label label-rounded label-danger"> '.$jml.' </span>';
						}else if($parent["menu"]=="Ditangguhkan"){
							$notifnomor = '<span class="label label-rounded label-danger"> '.$jmlh.' </span>';
						}else if($parent["menu"]=="Ditolak"){
							$notifnomor = '<span class="label label-rounded label-warning"> '.$jmlt.' </span>';
						}else{
							$notifnomor = '<span class="label label-rounded label-success"> '.$jmls.' </span>';
						}
					}
					echo '<li><a href="'.$link.'"><i class="fa '.$icon.'"></i> <span>'.$parent["menu"].'</span> '.$notifnomor.'</a></li>';
				}
			}else{
				echo '<li><input type="checkbox"  id="parent_'.$parent["idMenu"].'_'.$parent["parent_id"].'" class="flat-red child child_'.$parent["parent_id"].'" value="'.$parent["idMenu"].'" name="menu[]" id="menu" /> '.$parent["menu_name"].'</li>';
			}
		}
		echo '</li>';
	}

	public function StatusPinjaman($umur,$plafon,$batas,$KodePekerjaan){
		$plafon = str_replace(".", "", $plafon);
		
		if($KodePekerjaan=="BJ"){
			$KodePekerjaan = "BJ";
		}else if($KodePekerjaan==45){
			$KodePekerjaan = 45;
		}else{
			$KodePekerjaan = 77;
		}

		$query = $this->db->query("select idTc, keterangan, ac, KodePekerjaan from t_tc where umur1 <= '$umur' and umur2 >= '$umur' and uang1 <= '$plafon' and uang2 >= '$plafon' and umurMax >= '$batas' and KodePekerjaan = '$KodePekerjaan' order by idTc asc");
		if(empty($query)){
			return FALSE;
		}else{
			return $query->row();
		}
	}

	public function StatusPinjamanPrapen($umur,$plafon,$batas,$KodePekerjaan){
		$plafon = str_replace(".", "", $plafon);
		
		if($KodePekerjaan=="BJ"){
			$KodePekerjaan = "BJ";
		}else if($KodePekerjaan==45){
			$KodePekerjaan = 45;
		}else{
			$KodePekerjaan = 77;
		}

		$query = $this->db->query("select idTc, keterangan, umur1, umur2, umurMax, ac, KodePekerjaan from t_tc where uang1 <= '$plafon' and uang2 >= '$plafon' and umurMax >= '$batas' and KodePekerjaan = '$KodePekerjaan' order by idTc asc");
		if(empty($query)){
			return FALSE;
		}else{
			return $query->row();
		}
	}

	public function cekResiko($idResiko){
		$query = $this->db->query("select Resiko from t_resiko where idResiko = '$idResiko'");
		return $query->row()->Resiko;
	}

	public function cekBank($kode){
		$query = $this->db->query("select kodeBank from t_bank where kodeBank = '$kode' LIMIT 1");
		if(empty($query->row())){
			$query = $this->db->query("select kodeBank from t_bank_cabang where kodeBank = '$kode' LIMIT 1");
			if(empty($query->row())){
				$query = $this->db->query("select kodeBank from t_bank_capem where kodeBank = '$kode' LIMIT 1");
				if(empty($query->row())){
					return FALSE;
				}else{
					return TRUE;	
				}
			}else{
				return TRUE;
			}
		}else{
			return TRUE;
		}
	}

	public function cekOpBank($kode){
		$query = $this->db->query("select kodeBank from t_bank where kodeBank = '$kode'");
		if(empty($query->row())){
			$query = $this->db->query("select kodeBank from t_bank_cabang where kodeBank = '$kode'");
			if(empty($query->row())){
				$query = $this->db->query("select kodeBank from t_bank_capem where kodeBank = '$kode'");
				if(empty($query->row())){
					return FALSE;
				}else{
					return "CAPEM";	
				}
			}else{
				return "CABANG";
			}
		}else{
			return "JATIM";
		}
	}

	public function cekNamaBank($kode){
		$query = $this->db->query("select bank from t_bank where kodeBank = '$kode'");
		if(empty($query->row())){
			$query = $this->db->query("select cabang from t_bank_cabang where kodeBank = '$kode'");
			if(empty($query->row())){
				$query = $this->db->query("select capem from t_bank_capem where kodeBank = '$kode'");
				if(empty($query->row())){
					return FALSE;
				}else{
					return $query->row()->capem;	
				}
			}else{
				return $query->row()->cabang;
			}
		}else{
			return $query->row()->bank;
		}
	}

	public function cekNamaBankNyaCapem($kode){
		$query = $this->db->query("select bank from t_bank where kodeBank = '$kode'");
		if(empty($query->row())){
			$query = $this->db->query("select cabang from t_bank_cabang a join t_bank_capem b on(b.idCabang=a.kodeBank) where b.kodeBank = '$kode'");
			if(empty($query->row())){
				return NULL;
			}else{
				return $query->row()->cabang;
			}
		}else{
			return $query->row()->bank;
		}
	}

	public function cekAsdur($asdur){
		$query = $this->db->query("select asuransi from t_asuransi where kodeAngka = '$asdur'");
		if(empty($query->row())){
			return FALSE;
		}else{
			return TRUE;
		}
	}

	public function cekNamaAsdur($asdur){
		$query = $this->db->query("select asuransi from t_asuransi where kodeAngka = '$asdur'");
		if(empty($query->row())){
			return FALSE;
		}else{
			return $query->row()->asuransi;
		}
	}

	public function cekNoPinjaman($nopin){
		$query = $this->db->query("select NomorPinjaman from t_pinjaman where NomorPinjaman = '$nopin'");
		if(empty($query->row())){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function cekStatusPinjaman($cif){
		$query = $this->db->query("select cif from t_pinjaman where cif = '$cif'");
		if(empty($query->row())){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function cekKodePekerjaan($kode){
		$query = $this->db->query("select kodePremi from t_tc_premi where kodePremi = '$kode'");
		if(empty($query->row())){
			return FALSE;
		}else{
			return $query->row()->kodePremi;
		}
	}

	public function cekRatePremi($KodePekerjaan){
		$query = $this->db->query("select PremiTahun from t_tc_premi where kodePremi = '$KodePekerjaan'");
		return $query->row()->PremiTahun;
	}

	public function cekRatePremiProduk($idProduk){
		$query = $this->db->query("select FeeBroker, FeeBank from t_asuransi where kodeAngka = '$idProduk'");
		return $query->row();
	}

	public function cabang(){
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$query = $this->db->query("select idCabang, kodeBank, cabang from t_bank_cabang");
		}else{
			$kodeBank = $this->session->userdata("kodeBank");
			$query = $this->db->query("select idCabang, kodeBank, cabang from t_bank_cabang where kodeBank = '$kodeBank'");
		}
		return $query->result();
	}

	public function asuradur(){
		$query = $this->db->query("select * from t_asuransi where  kodeAngka != 99");
		return $query->result();
	}

	public function asuradurNot99(){
		$query = $this->db->query("select * from t_asuransi where kodeAngka != 99");
		return $query->result();
	}

	public function capem($kode){
		$query = $this->db->query("select idCapem, capem, idCabang,kodeBank from t_bank_capem where idCabang = '$kode'");
		return $query->result();
	}

	public function produk(){
		$query = $this->db->query("select * from t_produk");
		return $query->result();
	}

	public function cekPekerjaan($kode){
		$query = $this->db->query("select NamaPremi from t_tc_premi where kodePremi = '$kode'");
		if(empty($query->row())){
			return FALSE;
		}else{
			return $query->row()->NamaPremi;
		}
	}

	public function cekProduk($kode){
		$query = $this->db->query("select NamaProduk from t_produk where kodeProduk = '$kode'");
		if(empty($query->row())){
			return FALSE;
		}else{
			return $query->row()->NamaProduk;
		}
	}

	public function Resiko(){
		$query = $this->db->query("select * from t_resiko");
		return $query->result();
	}


	public function cekDokumenUmum($noreg,$dok){
		$query = $this->db->query("select NamaFile from t_pinjaman_klaim_dokumen where KodeKlaim = '$noreg' and dokumen = '$dok'");
		if(!empty($query->row())){
			return $query->row()->NamaFile;
		}
	}

	public function cekDokumenKhusus($noreg,$dok){
		$query = $this->db->query("select NamaFile from t_pinjaman_klaim_dokumen where KodeKlaim = '$noreg' and dokumen = '$dok'");
		if(!empty($query->row())){
			return $query->row()->NamaFile;
		}
	}

	public function MenungguKlaim(){
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$where = "";
		}else{
			$kodeBank = $this->session->userdata("kodeBank");
			$where = " and a.kodeBank='$kodeBank' ";
		}

		if ($this->session->userdata("roles")==3) {
		      $where .= " and spv=1 ";
	    } else if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $where .= " and pialang=1 and kodeAsuransi = '$kodeAngka'";
	    } else{
		    $where = "";
	    }

		$query = $this->db->query("select count(*) jml from t_pinjaman_klaim b join t_pinjaman a on(a.NomorRegistrasi=b.NomorRegistrasi) where keputusan = 0 and diajukan = '1' ".$where);
		return $query->row()->jml;
	}

	public function DitangguhkanKlaim(){
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$where = "";
		}else{
			$kodeBank = $this->session->userdata("kodeBank");
			$where = " and a.kodeBank='$kodeBank' ";
		}
		if ($this->session->userdata("roles")==3) {
		      $where .= " and spv=1 ";
	    } else if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $where .= " and pialang=1 and kodeAsuransi = '$kodeAngka'";
	    } else{
		    $where = "";
	    }
		$query = $this->db->query("select count(*) jml from t_pinjaman_klaim b join t_pinjaman a on(a.NomorRegistrasi=b.NomorRegistrasi) where keputusan = 2".$where);
		return $query->row()->jml;
	}

	public function DitolakKlaim(){
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$where = "";
		}else{
			$kodeBank = $this->session->userdata("kodeBank");
			$where = " and a.kodeBank='$kodeBank' ";
		}
		if ($this->session->userdata("roles")==3) {
		      $where .= " and spv=1 ";
	    } else if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $where .= " and pialang=1 and kodeAsuransi = '$kodeAngka'";
	    } else{
		    $where = "";
	    }
		$query = $this->db->query("select count(*) jml from t_pinjaman_klaim b join t_pinjaman a on(a.NomorRegistrasi=b.NomorRegistrasi) where keputusan = '-2'".$where);
		return $query->row()->jml;
	}

	public function DisetujuiKlaim(){
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$where = "";
		}else{
			$kodeBank = $this->session->userdata("kodeBank");
			$where = " and a.kodeBank='$kodeBank' ";
		}
		if ($this->session->userdata("roles")==3) {
		      $where .= " and spv=1 ";
	    } else if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $where .= " and pialang=1 and kodeAsuransi = '$kodeAngka'";
	    } else{
		    $where = "";
	    }
		$query = $this->db->query("select count(*) jml from t_pinjaman_klaim b join t_pinjaman a on(a.NomorRegistrasi=b.NomorRegistrasi) where keputusan = 1".$where);
		return $query->row()->jml;
	}

	public function MenungguRestitusi(){
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$where = "";
		}else{
			$kodeBank = $this->session->userdata("kodeBank");
			$where = " and a.kodeBank='$kodeBank' ";
		}
		if ($this->session->userdata("roles")==3) {
		      $where .= " and spv=1 ";
	    } else if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $where .= " and pialang=1 and kodeAsuransi = '$kodeAngka'";
	    } else{
		    $where = "";
	    }
		$query = $this->db->query("select count(*) jml from t_pinjaman_restitusi b join t_pinjaman a on(a.NomorRegistrasi=b.NomorRegistrasi) where keputusan = 0 and diajukan = '1' ".$where);
		return $query->row()->jml;
	}

	public function DitangguhkanRestitusi(){
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$where = "";
		}else{
			$kodeBank = $this->session->userdata("kodeBank");
			$where = " and a.kodeBank='$kodeBank' ";
		}
		if ($this->session->userdata("roles")==3) {
		      $where .= " and spv=1 ";
	    } else if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $where .= " and pialang=1 and kodeAsuransi = '$kodeAngka'";
	    } else{
		    $where = "";
	    }
		$query = $this->db->query("select count(*) jml from t_pinjaman_restitusi b join t_pinjaman a on(a.NomorRegistrasi=b.NomorRegistrasi) where keputusan = 2".$where);
		return $query->row()->jml;
	}

	public function DitolakRestitusi(){
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$where = "";
		}else{
			$kodeBank = $this->session->userdata("kodeBank");
			$where = " and a.kodeBank='$kodeBank' ";
		}
		if ($this->session->userdata("roles")==3) {
		      $where .= " and spv=1 ";
	    } else if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $where .= " and pialang=1 and kodeAsuransi = '$kodeAngka'";
	    } else{
		    $where = "";
	    }
		$query = $this->db->query("select count(*) jml from t_pinjaman_restitusi b join t_pinjaman a on(a.NomorRegistrasi=b.NomorRegistrasi) where keputusan = '-1'".$where);
		return $query->row()->jml;
	}

	public function DisetujuiRestitusi(){
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$where = "";
		}else{
			$kodeBank = $this->session->userdata("kodeBank");
			$where = " and a.kodeBank='$kodeBank' ";
		}
		if ($this->session->userdata("roles")==3) {
		      $where .= " and spv=1 ";
	    } else if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $where .= " and pialang=1 and kodeAsuransi = '$kodeAngka'";
	    } else{
		    $where = "";
	    }
		$query = $this->db->query("select count(*) jml from t_pinjaman_restitusi b join t_pinjaman a on(a.NomorRegistrasi=b.NomorRegistrasi) where keputusan = 1".$where);
		return $query->row()->jml;
	}

	public function bangpem($kode){
		$query = $this->db->query("select capem, cabang from t_bank_capem a join t_bank_cabang b on(a.idCabang=b.kodeBank) where a.kodeBank = '$kode'");
		if(empty($query->row())){
			$query = $this->db->query("select null capem, cabang from t_bank_cabang where kodeBank = '$kode'");
			return $query->row();
		}else{
			return $query->row();
		}
	}

	public function cekDokKurang($noreg){
		$query = $this->db->query("select dokumen from t_pinjaman_dok where NomorRegistrasi = '$noreg' and terpenuhi = 0");
		return $query;
	}

	public function cekDokLengkap($noreg){
		$query = $this->db->query("select dokumen,namafile from t_pinjaman_dok where NomorRegistrasi = '$noreg' and terpenuhi = 1");
		return $query;
	}	

	public function roles(){
		$query = $this->db->query("select * from t_roles where idRoles != 1");
		return $query->result();
	}

	public function hak_akses(){
		$query = $this->db->query("select * from t_tingkat_hak_akses where idHakAkses != 1");
		return $query->result();
	}

	public function cekDokKlaim($noreg){
		$query = $this->db->query("select count(*) jml, sum(upload) upload from t_pinjaman_klaim_dokumen where KodeKlaim = '$noreg'");
		return $query->row();
	}

	public function cekDebiturAsdur($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select count(a.idDebitur) jmldeb from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur where StatusRekonsel = '1' and  CONVERT(CHAR(7),b.create_date,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and." group by CONVERT(CHAR(2),b.create_date,110),CONVERT(CHAR(7),b.create_date,120)");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jmldeb;
		}
	}

	public function cekDebiturAsdurPolis($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select count(a.idDebitur) jmldeb from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur where StatusRekonsel = '1' and StatusPolis = '1' and  CONVERT(CHAR(7),b.create_date,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and." group by CONVERT(CHAR(2),b.create_date,110),CONVERT(CHAR(7),b.create_date,120)");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jmldeb;
		}
	}

	public function cekDebiturResAsdur($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select count(a.idDebitur) jmldeb from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur join t_pinjaman_restitusi c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and StatusRestitusi = '1'  and  CONVERT(CHAR(7),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and." group by CONVERT(CHAR(2),create_spv,110),CONVERT(CHAR(7),create_spv,120)");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jmldeb;
		}
	}

	public function cekDebiturResAsdurBayar($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select count(a.idDebitur) jmldeb from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur join t_pinjaman_restitusi c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and StatusPembayaran = '1'  and  CONVERT(CHAR(7),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and." group by CONVERT(CHAR(2),create_spv,110),CONVERT(CHAR(7),create_spv,120)");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jmldeb;
		}
	}

	public function cekPremiAsdur($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select sum(JumlahPremiTenor) premi from t_pinjaman where StatusRekonsel = '1' and  CONVERT(CHAR(7),create_date,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and." group by CONVERT(CHAR(2),create_date,110),CONVERT(CHAR(7),create_date,120)");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->premi;
		}
	}

	public function cekUpAsdur($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select sum(plafon) plafon from t_pinjaman where StatusRekonsel = '1' and  CONVERT(CHAR(7),create_date,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and." group by CONVERT(CHAR(2),create_date,110),CONVERT(CHAR(7),create_date,120)");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->plafon;
		}
	}

	public function cekUpAsdurPolis($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select sum(plafon) plafon from t_pinjaman where StatusRekonsel = '1' and StatusPolis = '1' and  CONVERT(CHAR(7),create_date,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and." group by CONVERT(CHAR(2),create_date,110),CONVERT(CHAR(7),create_date,120)");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->plafon;
		}
	}

	public function cekNomAsdur($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select sum(PengembalianPremi) plafon from t_pinjaman b join t_pinjaman_restitusi c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and StatusRestitusi = '1'  and  CONVERT(CHAR(7),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and." group by CONVERT(CHAR(2),create_spv,110),CONVERT(CHAR(7),create_spv,120)");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->plafon;
		}
	}

	public function cekNomAsdurBayar($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select sum(PengembalianPremi) plafon from t_pinjaman b join t_pinjaman_restitusi c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and StatusPembayaran = '1' and  CONVERT(CHAR(7),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and." group by CONVERT(CHAR(2),create_spv,110),CONVERT(CHAR(7),create_spv,120)");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->plafon;
		}
	}

	public function cekDebiturAsdurTahun($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select count(a.idDebitur) jmldeb from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur where StatusRekonsel = '1' and  CONVERT(CHAR(4),b.create_date,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and);
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jmldeb;
		}
	}

	public function cekDebiturAsdurPolisTahun($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select count(a.idDebitur) jmldeb from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur where StatusRekonsel = '1' and StatusPolis = '1' and  CONVERT(CHAR(4),b.create_date,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and);
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jmldeb;
		}
	}

	public function cekDebiturResAsdurTahun($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select count(a.idDebitur) jmldeb from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur join t_pinjaman_restitusi c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and StatusRestitusi = '1' and CONVERT(CHAR(4),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and);
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jmldeb;
		}
	}

	public function cekDebiturResAsdurBayarTahun($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select count(a.idDebitur) jmldeb from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur join t_pinjaman_restitusi c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and StatusPembayaran = '1' and CONVERT(CHAR(4),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and);
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jmldeb;
		}
	}

	public function cekUpAsdurTahun($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select sum(plafon) plafon from t_pinjaman where StatusRekonsel = '1' and  CONVERT(CHAR(4),create_date,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and);
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->plafon;
		}
	}

	public function cekUpAsdurPolisTahun($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select sum(plafon) plafon from t_pinjaman where StatusRekonsel = '1' and StatusPolis = '1' and  CONVERT(CHAR(4),create_date,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and);
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->plafon;
		}
	}

	public function cekResAsdurTahun($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select sum(PengembalianPremi) plafon from t_pinjaman b left join t_pinjaman_restitusi c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and StatusPembayaran = '0' and CONVERT(CHAR(4),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and);
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->plafon;
		}
	}

	public function cekResAsdurBayarTahun($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select sum(PengembalianPremi) plafon from t_pinjaman b join t_pinjaman_restitusi c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and StatusPembayaran = '1' and CONVERT(CHAR(7),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and);
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->plafon;
		}
	}

	public function cekPremiAsdurTahun($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select sum(JumlahPremiTenor) premi from t_pinjaman where StatusRekonsel = '1' and  CONVERT(CHAR(4),create_date,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and);
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->premi;
		}
	}

	public function cekSlaDebitur($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, count(a.idDebitur) jml from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur where StatusRekonsel = '1' and StatusPolis = '0' and CONVERT(CHAR(7),b.create_date,120) ='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,TglRekonsel,convert(varchar(10), Getdate(), 120 )) <= 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}

	public function cekNonSlaDebitur($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, count(a.idDebitur) jml from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur where StatusRekonsel = '1' and StatusPolis = '0' and CONVERT(CHAR(7),b.create_date,120) ='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,TglRekonsel,convert(varchar(10), Getdate(), 120 )) > 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}

	public function cekSlaResDebitur($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, count(a.idDebitur) jml from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur join t_pinjaman_restitusi c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and StatusRestitusi = '1' and StatusPembayaran = '0' and CONVERT(CHAR(7),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,create_spv,convert(varchar(10), Getdate(), 120 )) <= 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}

	public function cekNonSlaResDebitur($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, count(a.idDebitur) jml from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur join t_pinjaman_restitusi c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and StatusRestitusi = '1' and StatusPembayaran = '0' and CONVERT(CHAR(7),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,create_spv,convert(varchar(10), Getdate(), 120 )) > 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}

	public function cekSlaKlaimDebitur($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, count(a.idDebitur) jml from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur join t_pinjaman_klaim c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusPembayaran = 0 and StatusKlaim = '1' and keputusan != '-1' and CONVERT(CHAR(7),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,create_spv,convert(varchar(10), Getdate(), 120 )) <= 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}

	public function cekNonSlaKlaimDebitur($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, count(a.idDebitur) jml from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur join t_pinjaman_klaim c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusPembayaran = 0 and StatusKlaim = '1' and keputusan != '-1'  and CONVERT(CHAR(7),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,create_spv,convert(varchar(10), Getdate(), 120 )) > 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}

	public function cekSlaPlafon($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, sum(plafon) jml from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur where StatusRekonsel = '1' and StatusPolis = '0' and CONVERT(CHAR(7),b.create_date,120) ='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,TglRekonsel,convert(varchar(10), Getdate(), 120 )) <= 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}

	public function cekNonSlaPlafon($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, sum(plafon) jml from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur where StatusRekonsel = '1' and StatusPolis = '0' and CONVERT(CHAR(7),b.create_date,120) ='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,TglRekonsel,convert(varchar(10), Getdate(), 120 )) > 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}

	public function cekSlaResPlafon($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, sum(PengembalianPremi) jml from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur join t_pinjaman_restitusi c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and StatusRestitusi = '1' and StatusPembayaran = '0' and CONVERT(CHAR(7),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,create_spv,convert(varchar(10), Getdate(), 120 )) <= 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}

	public function cekNonSlaResPlafon($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, sum(PengembalianPremi) jml from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur join t_pinjaman_restitusi c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and StatusRestitusi = '1'  and StatusPembayaran = '0' and CONVERT(CHAR(7),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,create_spv,convert(varchar(10), Getdate(), 120 )) > 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}

	public function cekSlaKlaimPlafon($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, sum(NominalDiajukan) jml from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur join t_pinjaman_klaim c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusPembayaran = 0 and StatusKlaim = '1' and keputusan != '-1'  and CONVERT(CHAR(7),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,create_spv,convert(varchar(10), Getdate(), 120 )) <= 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}

	public function cekNonSlaKlaimPlafon($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, sum(NominalDiajukan) jml from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur join t_pinjaman_klaim c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusPembayaran = 0 and StatusKlaim = '1' and keputusan != '-1'  and CONVERT(CHAR(7),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,create_spv,convert(varchar(10), Getdate(), 120 )) > 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}

	//
	public function cekSlaDebitur2($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, count(a.idDebitur) jml from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur where StatusRekonsel = '1' and StatusPolis = '0' and CONVERT(CHAR(7),b.create_date,120) <='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,TglRekonsel,convert(varchar(10), Getdate(), 120 )) <= 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}

	public function cekNonSlaDebitur2($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, count(a.idDebitur) jml from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur where StatusRekonsel = '1' and StatusPolis = '0' and CONVERT(CHAR(7),b.create_date,120) <='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,TglRekonsel,convert(varchar(10), Getdate(), 120 )) > 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}

	public function cekSlaResDebitur2($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, count(a.idDebitur) jml from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur join t_pinjaman_restitusi c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and StatusRestitusi = '1' and StatusPembayaran = '0' and CONVERT(CHAR(7),b.create_date,120) <='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,create_spv,convert(varchar(10), Getdate(), 120 )) <= 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}

	public function cekNonSlaResDebitur2($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, count(a.idDebitur) jml from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur join t_pinjaman_restitusi c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and StatusRestitusi = '1' and StatusPembayaran = '0' and CONVERT(CHAR(7),b.create_date,120) <='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,create_spv,convert(varchar(10), Getdate(), 120 )) > 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}

	public function cekSlaPlafon2($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, sum(plafon) jml from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur where StatusRekonsel = '1' and StatusPolis = '0' and CONVERT(CHAR(7),b.create_date,120) <='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,TglRekonsel,convert(varchar(10), Getdate(), 120 )) <= 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}

	public function cekNonSlaPlafon2($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, sum(plafon) jml from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur where StatusRekonsel = '1' and StatusPolis = '0' and CONVERT(CHAR(7),b.create_date,120) <='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,TglRekonsel,convert(varchar(10), Getdate(), 120 )) > 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}

	public function cekSlaResPlafon2($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, sum(PengembalianPremi) jml from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur join t_pinjaman_restitusi c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and StatusRestitusi = '1' and StatusPembayaran = '0' and CONVERT(CHAR(7),b.create_date,120) <='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,create_spv,convert(varchar(10), Getdate(), 120 )) <= 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}

	public function cekNonSlaResPlafon2($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, sum(PengembalianPremi) jml from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur join t_pinjaman_restitusi c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and StatusRestitusi = '1' and StatusPembayaran = '0' and CONVERT(CHAR(7),create_spv,120) <='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,create_spv,convert(varchar(10), Getdate(), 120 )) > 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}


	//
	//
	public function cekSlaKlaimDebitur2($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, count(c.NomorRegistrasi) jml from t_pinjaman b join t_pinjaman_klaim c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusPembayaran = '0' and keputusan != '-1' and CONVERT(CHAR(7),create_spv,120) <='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,create_spv,convert(varchar(10), Getdate(), 120 )) <= 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}

	public function cekNonSlaKlaimDebitur2($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, count(c.NomorRegistrasi) jml from t_pinjaman b join t_pinjaman_klaim c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusPembayaran = '0' and keputusan != '-1' and CONVERT(CHAR(7),create_spv,120) <='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,create_spv,convert(varchar(10), Getdate(), 120 )) > 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}

	public function cekSlaKlaimPlafon2($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, sum(NominalDiajukan) jml from t_pinjaman b join t_pinjaman_klaim c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusPembayaran = '0' and keputusan != '-1' and CONVERT(CHAR(7),create_spv,120) <='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,create_spv,convert(varchar(10), Getdate(), 120 )) <= 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}

	public function cekNonSlaKlaimPlafon2($tahun,$kodeAsuransi){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, sum(NominalDiajukan) jml from t_pinjaman b join t_pinjaman_klaim c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusPembayaran = '0' and keputusan != '-1' and CONVERT(CHAR(7),create_spv,120) <='$tahun' and kodeAsuransi = '$kodeAsuransi' and DATEDIFF(day,create_spv,convert(varchar(10), Getdate(), 120 )) > 14 ".$and." group by kodeAsuransi ");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jml;
		}
	}


	public function cekDebiturKlaimAsdur($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select count(c.NomorRegistrasi) jmldeb from t_pinjaman b join t_pinjaman_klaim c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and keputusan != '-1' and StatusKlaim = '1'  and  CONVERT(CHAR(7),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and." group by CONVERT(CHAR(2),create_spv,110),CONVERT(CHAR(7),create_spv,120)");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jmldeb;
		}
	}

	public function cekNomKlaimAsdur($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select sum(NominalDiajukan) plafon from t_pinjaman b join t_pinjaman_klaim c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and keputusan != '-1' and  CONVERT(CHAR(7),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and." group by CONVERT(CHAR(2),create_spv,110),CONVERT(CHAR(7),create_spv,120)");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->plafon;
		}
	}

	public function cekDebiturKlaimAsdurBayar($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select count(b.NomorRegistrasi) jmldeb from t_pinjaman b join t_pinjaman_klaim c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and keputusan != '-1' and StatusPembayaran = '1'  and  CONVERT(CHAR(7),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and." group by CONVERT(CHAR(2),create_spv,110),CONVERT(CHAR(7),create_spv,120)");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jmldeb;
		}
	}

	public function cekNomKlaimAsdurBayar($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select sum(NominalDisetujui) plafon from t_pinjaman b join t_pinjaman_klaim c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and keputusan != '-1' and StatusPembayaran = '1' and  CONVERT(CHAR(7),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and." group by CONVERT(CHAR(2),create_spv,110),CONVERT(CHAR(7),create_spv,120)");
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->plafon;
		}
	}

	public function cekDebiturKlaimAsdurTahun($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select count(b.NomorRegistrasi) jmldeb from t_pinjaman b join t_pinjaman_klaim c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and keputusan != '-1' and CONVERT(CHAR(4),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and);
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jmldeb;
		}
	}

	public function cekKlaimAsdurTahun($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select sum(NominalDiajukan) plafon from t_pinjaman b left join t_pinjaman_klaim c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and keputusan != '-1' and CONVERT(CHAR(4),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and);
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->plafon;
		}
	}

	public function cekDebiturKlaimAsdurBayarTahun($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select count(c.NomorRegistrasi) jmldeb from t_pinjaman b join t_pinjaman_klaim c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and StatusPembayaran = '1' and CONVERT(CHAR(4),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and);
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->jmldeb;
		}
	}

	public function cekKlaimAsdurBayarTahun($tahun,$kodeAngka){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}

		$query = $this->db->query("select sum(NominalDisetujui) plafon from t_pinjaman b join t_pinjaman_klaim c on(c.NomorRegistrasi=b.NomorRegistrasi) where StatusRekonsel = '1' and keputusan != '-1' and StatusPembayaran = '1' and CONVERT(CHAR(7),create_spv,120) ='$tahun' and kodeAsuransi = '$kodeAngka' ".$and);
		if(empty($query->row())){
			return 0;
		}else{
			return $query->row()->plafon;
		}
	}

	public function restitusiBj($noreg){
		$query = $this->db->query("select ospremi, min(TahunKe) TahunKe from t_pinjaman_bj where NomorRegistrasi = '$noreg' and StatusBayar = 1 group by ospremi");
		return $query->row();
	}

	public function tgl_polis($noreg){
		$query = $this->db->query("select convert(varchar(10),create_date,120) tgl_input from t_pinjaman_polis where NomorRegistrasi = '$noreg'");
		return $query->row()->tgl_input;
	}

	public function cekPolis($noreg){
		$query = $this->db->query("select NomorPolis from t_pinjaman_polis where NomorRegistrasi = '$noreg'");
		if(empty($query->row()->NomorPolis)){
			return "-";
		}else{
			return $query->row()->NomorPolis;
		}
	}	

	public function count_chat_all(){
		$idUser = create_user();
		$query = $this->db->query("select count(*) jumlah from t_chat where send_to = '$idUser' or send_by ='$idUser' and count_read = 0");
		if(empty($query->row()->jumlah)){
			return 0;
		}else{
			return $query->row()->jumlah;
		}
	}

	public function count_chat($idUser){
		$user = create_user();
		$query = $this->db->query("select count(*) jumlah from t_chat where send_to = '$idUser' and send_by ='$user' and count_read = 0");
		if(empty($query->row()->jumlah)){
			return 0;
		}else{
			return $query->row()->jumlah;
		}
	}


}