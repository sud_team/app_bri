<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_laporan extends CI_Model{

	public function __Construct(){
		parent::__Construct();
		$this->load->database();
	}

	public function getProduksi(){
		$tipe = $this->input->post("tipe");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tgl_awal")) && empty($this->input->post("tgl_akhir"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tgl_awal")),'Y-m-d');
			$tgl_akhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d');
		}


		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				if (cekOpBank($kodeBank)!="CABANG"){
				    $and .= " and kodeBank='$kodeBank' ";
				}
			}
		}

		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}
		if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}

		if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and convert(varchar(10),a.create_date,120) between '$tgl_mulai' and '$tgl_akhir'";
		}

		$query = $this->db->query("
			with 
			pinjaman (NomorRegistrasi, kodeBank, kodeAsuransi, TcKet, NomorPK, KodePekerjaan, cif, NamaDebitur, TglLahir, TglAkadKredit, TglAkhirKredit, TenorBulan, StatusDok, plafon, TarifPremi, premi, JumlahPremiTenor, FeePersenBank, FeePremiBank, FeePremiBroker, FeePresenBroker)
			as (
				select NomorRegistrasi, kodeBank, b.kodeAsuransi, TcKet, NomorPK, KodePekerjaan,a.cif, NamaDebitur, a.TglLahir, TglAkadKredit, TglAkhirKredit, TenorBulan, StatusDok, plafon, TarifPremi, premi, JumlahPremiTenor, FeePersenBank, FeePremiBank, FeePremiBroker, FeePresenBroker from t_debitur a join t_pinjaman b on(a.idDebitur=b.idDebitur and a.cif=b.cif) where StatusRekonsel = 1 ".$and."				
			),
			bj (NomorRegistrasi, premiterbayar)
			as (
				select NomorRegistrasi, premiterbayar from t_pinjaman_bj a where StatusBayar = 1 ".$and."				
			)

			select pinjaman.NomorRegistrasi, kodeBank, kodeAsuransi, TcKet, NomorPK, KodePekerjaan, cif, NamaDebitur, TglLahir, TglAkadKredit, TglAkhirKredit, TenorBulan, StatusDok, plafon, TarifPremi, premi, JumlahPremiTenor, FeePersenBank, FeePremiBank, FeePremiBroker, FeePresenBroker, premiterbayar from pinjaman left join bj  on(pinjaman.NomorRegistrasi=bj.NomorRegistrasi)");
		return $query->result();
	}

	public function getProduksi2(){
		$tipe = $this->input->post("tipe");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");

		if(empty($this->input->post("bulan"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
			$awal_bulan = "";
		}else{
			$tgl_mulai = "01-".($this->input->post("bulan"));
		    $tgl_akhir = date('t')."-".($this->input->post("bulan"));
		    $awal_bulan = date_format(date_create($tgl_mulai),'Y-m-d 00:00:00');
		}

		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and convert(varchar, b.create_date,105) between '$tgl_mulai' and '$tgl_akhir'";
		}

		
		$query = $this->db->query("
			WITH berjalan (kodeAsuransi, jmldeb, jmlpremi,premiterima)  
			AS    
			(  
		    select b.kodeAsuransi, count(c.idDebitur) jmldeb, sum(plafon) jmlpremi, 
			sum(case when convert(varchar, b.create_date,105) between '$tgl_mulai' and '$tgl_akhir' 
			then JumlahPremiTenor else 0 end) premiterima from t_pinjaman b 
			join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur) 
			where StatusRekonsel = '1' ".$and."
			group by b.kodeAsuransi 

			)  
			,   
			aktif (kodeAsuransi, jmldebaktif, jmlpremiaktif,premiterimaaktif)  
			AS  
			(  
	        select b.kodeAsuransi, count(c.idDebitur) jmldebaktif, sum(plafon) jmlpremiaktif, 
			sum(case when b.create_date < '$awal_bulan' then JumlahPremiTenor else 0 end) premiterimaaktif 
			from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur) 
			where StatusRekonsel = '1' and StatusKlaim = '0' and StatusRestitusi = '0' and b.create_date < '$awal_bulan' ".$and." 
			group by b.kodeAsuransi   
			)
			,
			aktifbj (kodeAsuransi, jmldebaktifbj, jmlpremiaktifbj, premiterbayarbj)  
			AS  
			(  
	        select b.kodeAsuransi, count(c.idDebitur) jmldebaktifbj, sum(b.plafon) jmlpremiaktifbj, 
			sum(case when d.create_date < '$awal_bulan' then premiterbayar else 0 end) premiterbayarbj 
			from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur) 
			join t_pinjaman_bj d on (b.NomorRegistrasi=d.NomorRegistrasi and b.NomorPinjaman=d.NomorPinjaman) 
			where StatusRekonsel = '1' and StatusBayar = '1' and StatusKlaim = '0' and StatusRestitusi = '0' and d.create_date < '$awal_bulan' ".$and." 
			group by b.kodeAsuransi   
			)
			,   
			bj (kodeAsuransi, jmldebaktifbj, jmlpremiaktifbayar, premiterbayar)  
			AS  
			(  
	        select b.kodeAsuransi, count(c.idDebitur) jmldebaktif, sum(b.plafon) jmlpremiaktifbayar, 
			sum(case when convert(varchar, d.create_date,105) between '$tgl_mulai' and '$tgl_akhir' then premiterbayar else 0 end) premiterbayar 
			from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur) join t_pinjaman_bj d on b.NomorRegistrasi=d.NomorRegistrasi and b.NomorPinjaman=d.NomorPinjaman
			where StatusRekonsel = '1' and StatusKlaim = '0' and StatusRestitusi = '0' and StatusBayar = '1'  ".$and." 
			group by b.kodeAsuransi   
			)  

			SELECT berjalan.kodeAsuransi  
			  , jmldeb  
			  , jmlpremi  
			  , premiterima  
			  , jmldebaktif, jmlpremiaktif,premiterimaaktif,premiterbayar,premiterbayarbj
			FROM berjalan  
			left JOIN aktif ON berjalan.kodeAsuransi= aktif.kodeAsuransi
			left JOIN aktifbj ON berjalan.kodeAsuransi= aktifbj.kodeAsuransi
			left join bj ON berjalan.kodeAsuransi= bj.kodeAsuransi
			");

		return $query;
	}

	public function getProduksi3(){
		$tipe = $this->input->post("tipe");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tgl_awal2")) && empty($this->input->post("tgl_akhir2"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tgl_awal2")),'Y-m-d 00:00:00');
			$tgl_akhir = date_format(date_create($this->input->post("tgl_akhir2")),'Y-m-d 23:59:59');
		}

		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and b.create_date between '$tgl_mulai' and '$tgl_akhir'";
		}


		$query = $this->db->query("
			with 
			pinjaman (kodeBank, jmldeb, pertanggungan, premi, brokerage, net)
			as (select kodeBank, count(idDebitur) jmldeb, sum(plafon) pertanggungan,sum(JumlahPremiTenor) premi, sum(FeePremiBroker) brokerage, sum(JumlahPremiTenor-(FeePremiBroker+FeePremiBank)) net from t_pinjaman b where StatusRekonsel = '1' ".$and." group by kodeBank),
			bj (kodeBank, jmldebbj, premiterbayar, brokeragebj, netbj)
			as (select kodeBank, count(idDebitur) jmldebbj,sum(premiterbayar) premiterbayar, sum(premiterbayar*FeePresenBroker) brokeragebj, sum(premiterbayar-((premiterbayar*FeePresenBroker)+(premiterbayar*FeePersenBank)) netbj from t_pinjaman_bj a join t_pinjaman b on(a.NomorRegistrasi=b.NomorRegistrasi and a.NomorPinjaman=b.NomorPinjaman) where StatusBayar = '1' ".$and." group by kodeBank)
			SELECT pinjaman.kodeBank  
			  , jmldeb  
			  , pertanggungan  
			  , (case when premiterbayar is null then 0 else premiterbayar end)+premi premi 
			  , (case when brokeragebj is null then 0 else brokeragebj end)+brokerage brokerage 
			  , (case when netbj is null then 0 else netbj end)+net net  
			FROM pinjaman  
			left JOIN bj ON pinjaman.kodeBank= bj.kodeBank
			");
		return $query;
	}

	public function getProduksi4($bulan){
		$tipe = $this->input->post("tipe");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");

		if(empty($this->input->post("tahun"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tahun")),'Y-'.$bulan);
		    $tgl_akhir = date_format(date_create($this->input->post("tahun")),'Y-'.$bulan);
		}

		

		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and convert(varchar(7), a.create_date, 120) = '$tgl_mulai' ";
		}


		$query = $this->db->query("
			with 
			pinjaman (bulan, tahun, jmldeb, plafon, premi, brokerage, net )
			as (
				select CONVERT(CHAR(2),create_date,110) bulan,CONVERT(CHAR(7),create_date,120) tahun,count(idDebitur) jmldeb, sum(plafon) plafon,sum(JumlahPremiTenor) premi, sum(FeePremiBroker) brokerage, sum(JumlahPremiTenor-FeePremiBroker) net from t_pinjaman a where StatusRekonsel = '1' ".$and." group by CONVERT(CHAR(2),create_date,110),CONVERT(CHAR(7),create_date,120)
			),
			bj (bulan, tahun, jmldebbj, premibj, brokeragebj, netbj )
			as (
				select CONVERT(CHAR(2),a.create_date,110) bulan,CONVERT(CHAR(7),a.create_date,120) tahun,count(idDebitur) jmldebbj,sum(premiterbayar) premibj, sum(premiterbayar*FeePresenBroker) brokeragebj, sum(premiterbayar-((premiterbayar*FeePresenBroker)+(premiterbayar*FeePersenBank))) netbj from t_pinjaman a left join  t_pinjaman_bj b on(a.NomorRegistrasi=b.NomorRegistrasi and a.NomorPinjaman=b.NomorPinjaman)  where StatusRekonsel = '1' ".$and." group by CONVERT(CHAR(2),a.create_date,110),CONVERT(CHAR(7),a.create_date,120)
			)
			SELECT 
			pinjaman.bulan,  
			pinjaman.tahun  
			  , jmldeb  
			  , plafon  
			  , (case when premibj is null then 0 else premibj end)+premi premi 
			  , (case when brokeragebj is null then 0 else brokeragebj end)+brokerage brokerage 
			  , (case when netbj is null then 0 else netbj end)+net net  
			FROM pinjaman  
			left JOIN bj ON pinjaman.bulan= bj.bulan
			");
		return $query;
	}

	public function getProduksi5(){
		$tipe = $this->input->post("tipe");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");

		if(empty($this->input->post("tahun"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tahun")),'01-'.$bulan.'-Y');
		    $tgl_akhir = date('t')."-".date_format(date_create($this->input->post("tahun")),$bulan.'-Y');
		}

		

		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			$kodeBank = $this->session->userdata("kodeBank");
			$and .= " and kodeBank='$kodeBank' ";
		}
		if(!empty($asuransi)){
			$and = " and KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and convert(varchar, create_date, 105) between '$tgl_mulai' and '$tgl_akhir'";
		}


		$query = $this->db->query("select CONVERT(CHAR(7),create_date,120), count(idDebitur) jmldeb, sum(plafon) plafon,sum(JumlahPremiTenor) premi, sum(FeePremiBroker) brokerage, sum(JumlahPremiTenor-FeePremiBroker) net from t_pinjaman where StatusRekonsel = '1' ".$and." group by CONVERT(CHAR(7),create_date,120)");
		return $query;
	}

	public function getSumProduksi(){
		$tipe = $this->input->post("tipe");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");

		if(empty($this->input->post("tgl_awal")) && empty($this->input->post("tgl_akhir"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tgl_awal")),'Y-m-d 00:00:00');
		    $tgl_akhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d 23:59:59');
		}

		

		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and a.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and a.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and create_date between '$tgl_mulai' and '$tgl_akhir'";
		}


		$query = $this->db->query("select kodeBank,sum(case when NamaPremi!='Pensiunan' or NamaPremi!='Bank Jatim' then 1 else 0 end ) as 'pnsnonpns',sum( case when NamaPremi='Pensiunan' then 1 else 0 end ) as 'pensiunan', sum(case when NamaPremi='Bank Jatim' then 1 else 0 end ) as 'jatim', SUM(plafon) plafon, SUM(premi) premi, sum(JumlahPremiTenor) JumlahPremiTenor from t_pinjaman a join t_tc_premi b on (a.KodePekerjaan=kodePremi) where StatusRekonsel = 1 ".$and." group by kodeBank");
		return $query;
	}

	public function getSumRestitusi(){
		$tipe = $this->input->post("tipe");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tgl_awal")) && empty($this->input->post("tgl_akhir"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tgl_awal")),'Y-m-d 00:00:00');
		    $tgl_akhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d 23:59:59');
		}

		

		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and a.create_date between '$tgl_mulai' and '$tgl_akhir'";
		}

		$query = $this->db->query("select kodeBank, count(KodeRestitusi) diajukan, 
            sum(CASE WHEN selesai = 1  THEN 1 ELSE 0 END) diterima,
            sum(PengembalianPremi) nominal from t_pinjaman_restitusi a join t_pinjaman b on(a.NomorRegistrasi=b.NomorRegistrasi) where StatusRestitusi = 1 ".$and." group by kodeBank");
		return $query;
	}

	public function getSumKlaim(){
		$tipe = $this->input->post("tipe");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tgl_awal")) && empty($this->input->post("tgl_akhir"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tgl_awal")),'Y-m-d 00:00:00');
		    $tgl_akhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d 23:59:59');
		}

		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and a.create_date between '$tgl_mulai' and '$tgl_akhir'";
		}
		$query = $this->db->query("Select kodeBank,
            count(KodeKlaim) diajukan,
            sum(CASE WHEN selesai = 0 THEN 1 ELSE 0 END) proses,
            sum(CASE WHEN keputusan = '-2'  THEN 1 ELSE 0 END) ditolak,
            sum(CASE WHEN keputusan = '1'  THEN 1 ELSE 0 END) diterima,
            sum(NominalDisetujui) nominal
            from t_pinjaman_klaim a
            join t_pinjaman b
            on (a.NomorRegistrasi=b.NomorRegistrasi)
            where StatusKlaim = 1 ".$and."
            group by kodeBank");
		return $query;
	}

	public function getPolis1(){
		$tipe = $this->input->post("tipepolis");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tgl_awal")) && empty($this->input->post("tgl_akhir"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tgl_awal")),'Y-m-d 00:00:00');
		    $tgl_akhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d 23:59:59');
		}

		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and a.create_date between '$tgl_mulai' and '$tgl_akhir'";
		}

		$query = $this->db->query("select kodeBank, NomorPK, NamaDebitur, a.TglLahir, b.create_date, kodeAsuransi, plafon, StatusPolis, TglRekonsel from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur where StatusRekonsel = 1 ".$and);

		return $query;
	}	

	public function getPolis2(){
		$tipe = $this->input->post("tipepolis");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		


		if(empty($this->input->post("bulan"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
			$awal_bulan = "";
			$dateadd = "";
		}else{
			$tgl_mulai = "01-".($this->input->post("bulan"));
		    $tgl_akhir = date('t')."-".($this->input->post("bulan"));
		    $awal_bulan = date_format(date_create($tgl_mulai),'Y-m');
		    $dateadd = date_format(date_create($tgl_mulai),'Y-m-d');
		}

		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		// if(!empty($tgl_mulai) && !empty($tgl_akhir)){
		// 	$and .= " and a.create_date between '$tgl_mulai' and '$tgl_akhir'";
		// }

		$query = $this->db->query("
			WITH sebelumnya (kodeAsuransi, jmldeb, pertanggungan)  
				AS  (  
				    select b.kodeAsuransi, count(c.idDebitur) jmldeb, sum(plafon) pertanggungan 
					from t_pinjaman b 
					join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur) 
					where StatusRekonsel = '1' and StatusPolis = '1' and convert(varchar(7), b.create_date,120) = convert(varchar(7), DATEADD(mm,-1,'$dateadd'),120) ".$and."
					group by b.kodeAsuransi
				),
				pengajuan (kodeAsuransi, jmldebajukan, jmlpengajuanplafon)  
				AS  (  
					select b.kodeAsuransi, count(c.idDebitur) jmldebajukan, sum(plafon) jmlpengajuanplafon
					from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur) 
					where StatusRekonsel = '1' and convert(varchar(7), b.create_date,120) = '$awal_bulan' ".$and."
					group by b.kodeAsuransi   
				),
				berjalan (kodeAsuransi, jmldebjalan, jmlpengajuanjalan)  
				AS  (  
					select b.kodeAsuransi, count(b.idDebitur) jmldebajukan, sum(plafon) jmlpengajuanjalan
					from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur) 
					where StatusRekonsel = '1' and StatusPolis = '1' and convert(varchar(7), b.create_date,120) = '$awal_bulan' ".$and."
					group by b.kodeAsuransi   
				)  

				SELECT pengajuan.kodeAsuransi  
				  , jmldeb  
				  , pertanggungan  
				  , jmldebajukan  
				  , jmlpengajuanplafon
				  , jmldebjalan
				  , jmlpengajuanjalan 
				FROM sebelumnya  
				right JOIN pengajuan ON sebelumnya.kodeAsuransi= pengajuan.kodeAsuransi
				left JOIN berjalan ON pengajuan.kodeAsuransi= berjalan.kodeAsuransi
		");

		return $query;
	}	

	public function getPolis3(){
		$tipe = $this->input->post("tipepolis");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		

		if(empty($this->input->post("tgl_akhir2"))){
			$dateadd = "";
		}else{
			$dateadd = date_format(date_create($this->input->post("tgl_akhir2")),'Y-m-d');
		}

		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}

		$query = $this->db->query("
			WITH pengajuan (kodeAsuransi, jmldebajukan, jmlpengajuanplafon)  
				AS  (  
					select b.kodeAsuransi, count(c.idDebitur) jmldebajukan, sum(plafon) jmlpengajuanplafon
					from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur) 
					where StatusRekonsel = '1' and convert(varchar(10), b.create_date,120) <= '$dateadd' ".$and."
					group by b.kodeAsuransi   
				),
				berjalan (kodeAsuransi, jmldebjalan, jmlpengajuanjalan)  
				AS  (  
					select b.kodeAsuransi, count(c.idDebitur) jmldebajukan, sum(plafon) jmlpengajuanjalan
					from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur) 
					where StatusRekonsel = '1' and StatusPolis = '1' and convert(varchar(10), b.create_date,120) <= '$dateadd' ".$and."
					group by b.kodeAsuransi   
				)  

				SELECT pengajuan.kodeAsuransi  
				  , jmldebajukan  
				  , jmlpengajuanplafon
				  , jmldebjalan
				  , jmlpengajuanjalan 
				FROM pengajuan
				JOIN berjalan ON pengajuan.kodeAsuransi= berjalan.kodeAsuransi
		");

		return $query;
	}	

	public function getPolis4($bulan){
		$tipe = $this->input->post("tipepolis");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tahun"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tahun")),'Y-'.$bulan);
		    $tgl_akhir = date_format(date_create($this->input->post("tahun")),'Y-'.$bulan);
		}

		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}

		$query = $this->db->query("
			with
			pengajuan (bulan,tahun,jmldeb,plafon) as (
				select CONVERT(CHAR(2),b.create_date,110) bulan,CONVERT(CHAR(7),b.create_date,120) tahun,count(a.idDebitur) jmldeb, 
				sum(plafon) plafon from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur
				where StatusRekonsel = 1 and CONVERT(CHAR(7),b.create_date,120) = '$tgl_mulai'
				group by CONVERT(CHAR(2),b.create_date,110),CONVERT(CHAR(7),b.create_date,120)
			),
			terbit (bulanx,tahunx,jmldebx,plafonx) as (
				select CONVERT(CHAR(2),b.create_date,110) bulan,CONVERT(CHAR(7),b.create_date,120) tahun,count(a.idDebitur) jmldeb, 
				sum(plafon) plafon from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur
				where StatusRekonsel = 1 and StatusPolis = 1 and CONVERT(CHAR(7),b.create_date,120) = '$tgl_mulai'
				group by CONVERT(CHAR(2),b.create_date,110),CONVERT(CHAR(7),b.create_date,120)
			)
			select bulan, tahun, jmldeb, plafon, tahunx, jmldebx, plafonx
			from pengajuan join terbit on pengajuan.bulan=terbit.bulanx
		");

		return $query;
	}

	public function getPolis5(){
		$tipe = $this->input->post("tipepolis");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tgl_awal3")) && empty($this->input->post("tgl_akhir3"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tgl_awal3")),'Y-m-d 00:00:00');
		    $tgl_akhir = date_format(date_create($this->input->post("tgl_akhir3")),'Y-m-d 23:59:59');
		}
		

		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and b.create_date between '$tgl_mulai' and '$tgl_akhir'";
		}

		$query = $this->db->query("
			with
			pengajuan (kodeBank,jmldeb,plafon) as (
				select kodeBank, count(b.idDebitur) jmldeb, sum(plafon) pertanggungan from t_debitur a join t_pinjaman b on(a.cif=b.cif and a.idDebitur=b.idDebitur) where StatusRekonsel = '1' ".$and." group by kodeBank
			),
			terbit (kodeBankx,jmldebx,plafonx) as (
				select kodeBank, count(b.idDebitur) jmldeb, sum(plafon) pertanggungan from t_debitur a join t_pinjaman b on(a.cif=b.cif and a.idDebitur=b.idDebitur) where StatusRekonsel = '1' and StatusPolis = '1' ".$and." group by kodeBank
			)
			select kodeBank, jmldeb, plafon, jmldebx, plafonx
			from pengajuan join terbit on pengajuan.kodeBank=terbit.kodeBankx
		");

		return $query;
		
	}

	public function getRestitusi1(){
		$tipe = $this->input->post("tiperes");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tgl_awal")) && empty($this->input->post("tgl_akhir"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tgl_awal")),'Y-m-d 00:00:00');
		    $tgl_akhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d 23:59:59');
		}


		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and c.create_date between '$tgl_mulai' and '$tgl_akhir'";
		}

		$query = $this->db->query("select kodeBank, NomorPinjaman, create_spv,NamaDebitur, c.TglPengajuanRestitusi, TglPelunasanRestitusi, kodeAsuransi, PengembalianPremi, TanggalBayar, keputusan, StatusPembayaran from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur join t_pinjaman_restitusi c on b.NomorRegistrasi=c.NomorRegistrasi where StatusRekonsel = 1  and StatusRestitusi = 1  and spv = 1 ".$and);

		return $query;
	}

	public function getRestitusi2(){
		$tipe = $this->input->post("tiperes");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("bulan"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
			$awal_bulan = "";
			$dateadd = "";
		}else{
			$tgl_mulai = "01-".($this->input->post("bulan"));
		    $tgl_akhir = date('t')."-".($this->input->post("bulan"));
		    $awal_bulan = date_format(date_create($tgl_mulai),'Y-m');
		    $dateadd = date_format(date_create($tgl_mulai),'Y-m-d');
		}
		

		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		// if(!empty($tgl_mulai) && !empty($tgl_akhir)){
		// 	$and .= " and a.create_date between '$tgl_mulai' and '$tgl_akhir'";
		// }

		$query = $this->db->query("
			WITH sebelumnya (kodeAsuransi, jmldeb, nominal)  
				AS  (  
				    select b.kodeAsuransi, count(c.idDebitur) jmldeb, sum(d.PengembalianPremi) nominal 
					from t_pinjaman b 
					join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur)
					join t_pinjaman_restitusi d on (b.NomorRegistrasi=d.NomorRegistrasi) 
					where StatusRekonsel = '1' and StatusRestitusi = '1' and spv = '1' and convert(varchar(7), d.create_spv,120) = convert(varchar(7), DATEADD(mm,-1,'$dateadd'),120) ".$and."
					group by b.kodeAsuransi
				),
				pengajuan (kodeAsuransi, jmldebajukan, nominalajukan)  
				AS  (  
					select b.kodeAsuransi, count(c.idDebitur) jmldebajukan, sum(d.PengembalianPremi) nominalajukan
					from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur)
					join t_pinjaman_restitusi d on (b.NomorRegistrasi=d.NomorRegistrasi)  
					where StatusRekonsel = '1' and StatusRestitusi = '1' and spv = '1'  and convert(varchar(7), d.create_spv,120) = '$awal_bulan' ".$and."
					group by b.kodeAsuransi   
				),
				berjalan (kodeAsuransi, jmldebjalan, nominaljalan)  
				AS  (  
					select b.kodeAsuransi, count(b.idDebitur) jmldebajukan, sum(d.PengembalianPremi) nominaljalan
					from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur)
					join t_pinjaman_restitusi d on (b.NomorRegistrasi=d.NomorRegistrasi)  
					where StatusRekonsel = '1' and StatusPembayaran = '1' and spv = '1'  and convert(varchar(7), d.create_spv,120) = '$awal_bulan' ".$and."
					group by b.kodeAsuransi   
				)  

				SELECT pengajuan.kodeAsuransi  
				  , jmldeb  
				  , nominal  
				  , jmldebajukan  
				  , nominalajukan
				  , jmldebjalan
				  , nominaljalan 
				FROM sebelumnya  
				right JOIN pengajuan ON sebelumnya.kodeAsuransi= pengajuan.kodeAsuransi
				left JOIN berjalan ON pengajuan.kodeAsuransi= berjalan.kodeAsuransi
		");

		return $query;
	}

	public function getRestitusi3(){
		$tipe = $this->input->post("tiperes");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");

		if(empty($this->input->post("tgl_akhir3"))){
			$dateadd = "";
		}else{
			$dateadd = date_format(date_create($this->input->post("tgl_akhir3")),'Y-m-d');
		}


		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}

		$query = $this->db->query("
			WITH pengajuan (kodeAsuransi, jmldebajukan, nominalajukan)  
				AS  (  
					select b.kodeAsuransi, count(c.idDebitur) jmldebajukan, sum(d.PengembalianPremi) nominalajukan
					from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur)
					join t_pinjaman_restitusi d on (b.NomorRegistrasi=d.NomorRegistrasi)  
					where StatusRekonsel = '1' and StatusRestitusi = '1' and spv = '1'  and convert(varchar(7), d.create_spv,120) <= '$dateadd' ".$and."
					group by b.kodeAsuransi   
				),
				berjalan (kodeAsuransi, jmldebjalan, nominaljalan)  
				AS  (  
					select b.kodeAsuransi, count(c.idDebitur) jmldebajukan, sum(d.PengembalianPremi) nominaljalan
					from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur)
					join t_pinjaman_restitusi d on (b.NomorRegistrasi=d.NomorRegistrasi) 
					where StatusRekonsel = '1' and StatusPembayaran = '1' and spv = '1'  and convert(varchar(10), b.create_date,120) <= '$dateadd' ".$and."
					group by b.kodeAsuransi   
				)  

				SELECT pengajuan.kodeAsuransi  
				  , jmldebajukan  
				  , nominalajukan
				  , jmldebjalan
				  , nominaljalan 
				FROM pengajuan
				LEFT JOIN berjalan ON pengajuan.kodeAsuransi= berjalan.kodeAsuransi
		");

		return $query;
	}


	public function getRestitusi4($bulan){
		$tipe = $this->input->post("tiperes");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tahun2"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tahun2")),'Y-'.$bulan);
		    $tgl_akhir = date_format(date_create($this->input->post("tahun2")),'Y-'.$bulan);
		}

		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}

		$query = $this->db->query("
			with
			pengajuan (bulan,tahun,jmldeb,plafon) as (
				select CONVERT(CHAR(2),create_spv,110) bulan,CONVERT(CHAR(7),create_spv,120) tahun,count(a.idDebitur) jmldeb, 
				sum(PengembalianPremi) plafon from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur join t_pinjaman_restitusi d on (b.NomorRegistrasi=d.NomorRegistrasi)  
					where StatusRekonsel = '1' and StatusRestitusi = '1' and spv = '1'  and CONVERT(CHAR(7),create_spv,120) = '$tgl_mulai'
				group by CONVERT(CHAR(2),create_spv,110),CONVERT(CHAR(7),create_spv,120)
			),
			terbit (bulanx,tahunx,jmldebx,plafonx) as (
				select CONVERT(CHAR(2),create_spv,110) bulan,CONVERT(CHAR(7),create_spv,120) tahun,count(a.idDebitur) jmldeb, 
				sum(PengembalianPremi) plafon from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur
				join t_pinjaman_restitusi d on (b.NomorRegistrasi=d.NomorRegistrasi)  
					where StatusRekonsel = '1' and StatusPembayaran = '1' and spv = '1'   and CONVERT(CHAR(7),create_spv,120) = '$tgl_mulai'
				group by CONVERT(CHAR(2),create_spv,110),CONVERT(CHAR(7),create_spv,120)
			)
			select bulan, tahun, jmldeb, plafon, tahunx, jmldebx, plafonx
			from pengajuan left join terbit on pengajuan.bulan=terbit.bulanx
		");

		return $query;
	}

	public function getRestitusi5(){
		$tipe = $this->input->post("tiperes");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tgl_awal2")) && empty($this->input->post("tgl_akhir2"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tgl_awal2")),'Y-m-d 00:00:00');
		    $tgl_akhir = date_format(date_create($this->input->post("tgl_akhir2")),'Y-m-d 23:59:59');
		}
		

		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and create_spv between '$tgl_mulai' and '$tgl_akhir'";
		}

		$query = $this->db->query("
			with
			pengajuan (kodeBank,jmldeb,plafon) as (
				select kodeBank, count(b.idDebitur) jmldeb, sum(PengembalianPremi) pertanggungan from t_debitur a join t_pinjaman b on(a.cif=b.cif and a.idDebitur=b.idDebitur) left join t_pinjaman_restitusi c on(b.NomorRegistrasi=c.NomorRegistrasi) where StatusRekonsel = '1' and StatusRestitusi = '1'  and spv = '1' ".$and." group by kodeBank
			),
			terbit (kodeBankx,jmldebx,plafonx) as (
				select kodeBank, count(b.idDebitur) jmldeb, sum(PengembalianPremi) pertanggungan from t_debitur a join t_pinjaman b on(a.cif=b.cif and a.idDebitur=b.idDebitur) left join t_pinjaman_restitusi c on(b.NomorRegistrasi=c.NomorRegistrasi) where StatusRekonsel = '1' and StatusPembayaran = '1'  and spv = '1' ".$and." group by kodeBank
			)
			select kodeBank, jmldeb, plafon, jmldebx, plafonx
			from pengajuan left join terbit on pengajuan.kodeBank=terbit.kodeBankx
		");

		return $query;
		
	}

	public function getKlaim1(){
		$tipe = $this->input->post("tiperes");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tgl_awal")) && empty($this->input->post("tgl_akhir"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tgl_awal")),'Y-m-d 00:00:00');
		    $tgl_akhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d 23:59:59');
		}

		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and c.create_date between '$tgl_mulai' and '$tgl_akhir'";
		}

		$query = $this->db->query("select kodeBank, NomorPinjaman, NamaDebitur, c.TglKlaim,idResiko, create_spv, TglResiko, kodeAsuransi, NominalDiajukan,NominalDisetujui, keterangan,TanggalBayar, keputusan, StatusPembayaran, keputusan from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur left join t_pinjaman_klaim c on b.NomorRegistrasi=c.NomorRegistrasi where StatusRekonsel = 1  and StatusKlaim = 1 and spv = 1 ".$and);

		return $query;
	}

	public function getKlaim2(){
		$tipe = $this->input->post("tipeklaim");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("bulan"))){
			$tgl_mulai = "01-".date("m-Y");
		    $tgl_akhir = date('t')."-".date("m-Y");
		    $awal_bulan = date_format(date_create($tgl_mulai),'Y-m');
		    $dateadd = date_format(date_create($tgl_mulai),'Y-m-d');
		}else{
			$tgl_mulai = "01-".($this->input->post("bulan"));
		    $tgl_akhir = date('t')."-".($this->input->post("bulan"));
		    $awal_bulan = date_format(date_create($tgl_mulai),'Y-m');
		    $dateadd = date_format(date_create($tgl_mulai),'Y-m-d');
		}

		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		// if(!empty($tgl_mulai) && !empty($tgl_akhir)){
		// 	$and .= " and a.create_date between '$tgl_mulai' and '$tgl_akhir'";
		// }

		$query = $this->db->query("
			WITH sebelumnya (kodeAsuransi, jmldeb, nominal)  
				AS  (  
				    select b.kodeAsuransi, count(c.idDebitur) jmldeb, sum(d.NominalDisetujui) nominal 
					from t_pinjaman b 
					join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur)
					join t_pinjaman_klaim d on (b.NomorRegistrasi=d.NomorRegistrasi) 
					where StatusRekonsel = '1' and StatusKlaim = '1' and StatusPembayaran = '1' and spv = '1'  and convert(varchar(7), d.create_spv,120) = convert(varchar(7), DATEADD(mm,-1,'$dateadd'),120) ".$and."
					group by b.kodeAsuransi
				),
				pengajuan (kodeAsuransi, jmldebajukan, nominalajukan)  
				AS  (  
					select b.kodeAsuransi, count(c.idDebitur) jmldebajukan, sum(d.NominalDiajukan) nominalajukan
					from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur)
					join t_pinjaman_klaim d on (b.NomorRegistrasi=d.NomorRegistrasi)  
					where StatusRekonsel = '1' and StatusKlaim = '1' and spv = '1' and convert(varchar(7), d.create_spv,120) = '$awal_bulan' ".$and."
					group by b.kodeAsuransi   
				),
				berjalan (kodeAsuransi, jmldebjalan, nominaljalan)  
				AS  (  
					select b.kodeAsuransi, count(b.idDebitur) jmldebajukan, sum(d.NominalDisetujui) nominaljalan
					from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur)
					join t_pinjaman_klaim d on (b.NomorRegistrasi=d.NomorRegistrasi)  
					where StatusRekonsel = '1' and StatusPembayaran = '1' and spv = '1'  and convert(varchar(7), d.create_spv,120) = '$awal_bulan' ".$and."
					group by b.kodeAsuransi   
				)  

				SELECT pengajuan.kodeAsuransi  
				  , jmldeb  
				  , nominal  
				  , jmldebajukan  
				  , nominalajukan
				  , jmldebjalan
				  , nominaljalan 
				FROM sebelumnya  
				right JOIN pengajuan ON sebelumnya.kodeAsuransi= pengajuan.kodeAsuransi
				left JOIN berjalan ON pengajuan.kodeAsuransi= berjalan.kodeAsuransi
		");

		return $query;
	}

	public function getKlaim3(){
		$tipe = $this->input->post("tiperes");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tgl_akhir2"))){
			$dateadd = "";
		}else{
			$dateadd = date_format(date_create($this->input->post("tgl_akhir2")),'Y-m-d');
		}

		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}

		$query = $this->db->query("
			WITH pengajuan (kodeAsuransi, jmldebajukan, nominalajukan)  
				AS  (  
					select b.kodeAsuransi, count(d.NomorRegistrasi) jmldebajukan, sum(d.NominalDiajukan) nominalajukan
					from t_pinjaman b join t_pinjaman_klaim d on (b.NomorRegistrasi=d.NomorRegistrasi)  
					where StatusRekonsel = '1' and StatusKlaim = '1' and spv = '1' and keputusan != -1 and convert(varchar(7), d.create_spv,120) <= '$dateadd' ".$and."
					group by b.kodeAsuransi   
				),
				berjalan (kodeAsuransi, jmldebjalan, nominaljalan)  
				AS  (  
					select b.kodeAsuransi, count(d.NomorRegistrasi) jmldebajukan, sum(d.NominalDisetujui) nominaljalan
					from t_pinjaman b join t_pinjaman_klaim d on (b.NomorRegistrasi=d.NomorRegistrasi)  
					where StatusRekonsel = '1' and StatusPembayaran = '1' and spv = '1' and keputusan != -1  and convert(varchar(10), d.create_spv,120) <= '$dateadd' ".$and."
					group by b.kodeAsuransi   
				)  

				SELECT pengajuan.kodeAsuransi  
				  , jmldebajukan  
				  , nominalajukan
				  , jmldebjalan
				  , nominaljalan 
				FROM pengajuan
				LEFT JOIN berjalan ON pengajuan.kodeAsuransi= berjalan.kodeAsuransi
		");

		return $query;
	}


	public function getKlaim4($bulan){
		$tipe = $this->input->post("tipeklaim");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tahun"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tahun")),'Y-'.$bulan);
		    $tgl_akhir = date_format(date_create($this->input->post("tahun")),'Y-'.$bulan);
		}

		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}

		$query = $this->db->query("
			with
			pengajuan (bulan,tahun,jmldeb,plafon) as (
				select CONVERT(CHAR(2),create_spv,110) bulan,CONVERT(CHAR(7),create_spv,120) tahun,count(a.idDebitur) jmldeb, 
				sum(NominalDiajukan) plafon from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur join t_pinjaman_klaim d on (b.NomorRegistrasi=d.NomorRegistrasi)  
					where StatusRekonsel = '1' and StatusKlaim = '1' and keputusan != '-1' and spv = '1' and CONVERT(CHAR(7),create_spv,120) = '$tgl_mulai'
				group by CONVERT(CHAR(2),create_spv,110),CONVERT(CHAR(7),create_spv,120)
			),
			terbit (bulanx,tahunx,jmldebx,plafonx) as (
				select CONVERT(CHAR(2),create_spv,110) bulan,CONVERT(CHAR(7),create_spv,120) tahun,count(a.idDebitur) jmldeb, 
				sum(NominalDisetujui) plafon from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur
				join t_pinjaman_klaim d on (b.NomorRegistrasi=d.NomorRegistrasi)  
					where StatusRekonsel = '1' and StatusPembayaran = '1'  and spv = '1' and keputusan != '-1'  and CONVERT(CHAR(7),create_spv,120) = '$tgl_mulai'
				group by CONVERT(CHAR(2),create_spv,110),CONVERT(CHAR(7),create_spv,120)
			)
			select bulan, tahun, jmldeb, plafon, tahunx, jmldebx, plafonx
			from pengajuan left join terbit on pengajuan.bulan=terbit.bulanx
		");

		return $query;
	}

	public function getKlaim5(){
		$tipe = $this->input->post("tipeklaim");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tgl_awal2")) && empty($this->input->post("tgl_akhir2"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tgl_awal3")),'Y-m-d 00:00:00');
		    $tgl_akhir = date_format(date_create($this->input->post("tgl_akhir3")),'Y-m-d 23:59:59');
		}


		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and create_spv between '$tgl_mulai' and '$tgl_akhir'";
		}

		$query = $this->db->query("
			with
			pengajuan (kodeBank,jmldeb,plafon) as (
				select kodeBank, count(c.NomorRegistrasi) jmldeb, sum(NominalDiajukan) NominalDiajukan from t_pinjaman b left join t_pinjaman_klaim c on(b.NomorRegistrasi=c.NomorRegistrasi) where StatusRekonsel = '1' and spv = '1' ".$and." group by kodeBank
			),
			terbit (kodeBankx,jmldebx,plafonx) as (
				select kodeBank, count(c.NomorRegistrasi) jmldeb, sum(NominalDisetujui) NominalDisetujui from t_pinjaman b left join t_pinjaman_klaim c on(b.NomorRegistrasi=c.NomorRegistrasi) where StatusRekonsel = '1' and StatusPembayaran = '1'  and spv = '1' ".$and." group by kodeBank
			)
			select kodeBank, jmldeb, plafon, jmldebx, plafonx
			from pengajuan left join terbit on pengajuan.kodeBank=terbit.kodeBankx
		");

		return $query;
		
	}

}