<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_produksi extends CI_Model{

	public function __Construct(){
		parent::__Construct();
		$this->load->database();
	}

	public function saveProduksi($NomorRegistrasi,$cif,$nopin,$kode,$nama,$loan,$jk,$ktp,$tmpt,$tgl,$alamat,$pekerjaan,$pk,$tglakad,$tglakhir,$plafon,$premi,$ref,$top,$asdur,$ac,$umur,$tenor,$tenorbulan,$batas,$cekRatePremi,$FeeBroker,$FeeBank,$idTc,$keterangan,$NomorInvoice,$statusprapen,$umur2){
		$tgl = date_format(date_create($tgl),'Y-m-d');
		$tglakad = date_format(date_create($tglakad),'Y-m-d');
		$tglakhir = date_format(date_create($tglakhir),'Y-m-d');
		$hariini = date("Y-m-d H:i:s");
		$plafon1 = str_replace(".","", $plafon);
		$plafon = str_replace(" ","", $plafon1);
		$premi1 = str_replace(".","", $premi);
		$premi = str_replace(" ","", $premi1);
		$nama = str_replace("'"," ",$nama);
		$nama = str_replace('"'," ",$nama);
		$alamat = str_replace("'"," ",$alamat);
		$alamat = str_replace('"'," ",$alamat);
		$pk = str_replace("'"," ",$pk);
		$pk = str_replace('"'," ",$pk);
		if(strlen($ktp)>16){
			$abc = substr($ktp, 0, 16);
			$ktp = "$abc";
		}else{
			$ktp =  "$ktp";
		}
		// $ktp =  settype($ktp, 'string');
		// $ktp =  gettype($ktp);
		$user = $this->session->userdata("idUser");
		$cekcif = $this->db->query("select top 1 idDebitur, cif from t_debitur where cif = '$cif'");
		if(empty($cekcif->row())){
			$idDebitur = $this->db->query("select max(idDebitur) idDebitur from t_debitur")->row()->idDebitur;
			if(empty($idDebitur)){
				$idDebitur = date("Ymd")."001";
			}else{
				$idDebitur = $idDebitur+1;	
			}
			$insertdebitur = $this->db->query("insert into t_debitur values ('$idDebitur','$cif','$nama','$jk','$ktp','$tmpt','$tgl','$alamat','$user','$hariini','','')");
		}

		if($loan=="KMGPA" || $loan=="KMGPP"){
			$keterangan = "AC";
			$ac=1;
		}

		$doklengkap = "";
		if($ac==1){
			$doklengkap = hariini();
		}

		$cekcif2 = $this->db->query("select top 1 idDebitur, cif from t_debitur where cif = '$cif'");
		$idDebitur = $cekcif2->row()->idDebitur;

		if($pekerjaan=="BJ"){
			$hutang = 0;
			$hutangplafon = $plafon;
			$cicilan = $plafon/$tenorbulan;
			$cicilanpertahun = $cicilan*12;
			$tahun = 0;
			$TotalOsPremi = 0;
			// for ($i=1; $i <= $tenor ; $i++) { 
			// 	$premibj = $hutangplafon*$cekRatePremi;
			// 	$hutangplafon = $hutangplafon-$cicilanpertahun;
			// 	$TotalOsPremi = $TotalOsPremi+$premibj;
			// }
			$JumlahPremiTenor = 0;   
			$TotalSeharusnya = $TotalOsPremi; 
			$premi = 0;
		}else{
			if ($statusprapen==1) {
				$premi = 0;
			} else {
				$premi = $plafon*$cekRatePremi;
			}			
			$JumlahPremiTenor = $tenor*$premi;
			$TotalSeharusnya = $JumlahPremiTenor; 
			$TotalOsPremi = 0;
		}
		
		$feepremibroker = $JumlahPremiTenor*$FeeBroker; 
		$feepremibank = $JumlahPremiTenor*$FeeBank; 
		$insertpinjaman = $this->db->query("insert into t_pinjaman values ('$NomorRegistrasi','$idDebitur','$cif','$tgl','$umur','$pekerjaan','$nopin','$kode','$loan','$pk','$tglakad','$tglakhir','$tenor','$tenorbulan','$batas','$plafon','$cekRatePremi','$premi','$JumlahPremiTenor','','$FeeBroker','$feepremibroker','$FeeBank','$feepremibank','$ref','$top','$asdur','$idTc','$keterangan','$ac','0','0','0','0','$user','$hariini','','','$ac','','$doklengkap','','$TotalOsPremi','$NomorInvoice','$TotalSeharusnya','$statusprapen','$umur2','0','0')");

		if($pekerjaan=="BJ"){
			$hutang = 0;
			$hutangplafon = $plafon;
			$cicilan = ceil($plafon/$tenorbulan);
			$cicilanpertahun = $cicilan*12;
			$tahun = 0;
			$tahunberjalan = date_format(date_create($tglakad),'Y');

			for ($i=1; $i <= $tenor ; $i++) { 
				// $hutang = $hutang+$cicilanpertahun;
				$premi = ceil($hutangplafon*$cekRatePremi);
				$tahun = date_format(date_create($tglakad),'Y')+$tahun;
				$tglbayar = date_format(date_create($tglakad),$tahun.'-m-d');
				$tglakad2 = $tglakad." 00:00:00"; 

				$id = $this->db->query("select max(idTbj) id from t_pinjaman_bj")->row()->id;

				$statusbyr = 0;

				if($i==1){
					$insertbj = $this->db->query("insert into t_pinjaman_bj (NomorRegistrasi,NomorPinjaman,TglAkadKredit,TahunKe,TglJatuhTempo,TglTahunBayar,TglRekonsel,plafon,SisaPinjaman,ospremi,premiterbayar,StatusBayar,create_user,create_date) values ('$NomorRegistrasi','$nopin','$tglakad','$i','$tglakad',dateadd(yy,$i, '$tglakad'),'','$plafon','$hutangplafon','$premi','','$statusbyr','$user','$hariini')");
				}else{
					$ii = $i-1;
					$hutangplafon = 0;
					$premi = 0;
					$insertbj = $this->db->query("insert into t_pinjaman_bj (NomorRegistrasi,NomorPinjaman,TglAkadKredit,TahunKe,TglJatuhTempo,TglTahunBayar,TglRekonsel,plafon,SisaPinjaman,ospremi,premiterbayar,StatusBayar,create_user,create_date) values ('$NomorRegistrasi','$nopin','$tglakad','$i',dateadd(yy,$ii,'$tglakad'),dateadd(yy,$i, '$tglakad'),'','$plafon','$hutangplafon','$premi','','$statusbyr','$user','$hariini')");
				}

				// $hutangplafon = $hutangplafon-$cicilanpertahun;
				$tahun=$tahun+$i;
				$tahunberjalan = $tahunberjalan+($i-1);
			}
		}

		if($keterangan!="AC"){
			$dok = explode("+", $keterangan);
			for ($i=0; $i < count($dok); $i++) { 
				$a = $dok[$i];
				$insertpinjamandok = $this->db->query("insert into t_pinjaman_dok values('$NomorRegistrasi','$idTc','$a','','0')");
			}
		}
	

	}

	public function saveProduksiGagal($cif,$nopin,$kode,$nama,$loan,$jk,$ktp,$tmpt,$tgl,$alamat,$pekerjaan,$pk,$tglakad,$tglakhir,$plafon,$premi,$ref,$top,$asdur,$keterangan){
		$user = create_user();
		$hariini = hariini();
		if(empty($premi)){
			$premi = 0;
		}
		$insertdebitur = $this->db->query("insert into t_pengajuan_ditolak values ('$cif','$nopin','$kode','$nama','$loan','$jk','$ktp','$tmpt','$tgl','$alamat','$pekerjaan','$pk','$tglakad','$tglakhir','$plafon','$premi','$ref','$top','$asdur','$keterangan','$user','$hariini')");
	}


	public function file02($nama,$keterangan,$nopin,$alasan){
		$query = $this->db->query("insert into upload02 values ('$nama','$keterangan','$nopin','$alasan')");
	}

	public function file03($nama,$keterangan,$nopin,$noreg){
		$query = $this->db->query("insert into upload03 values ('$nama','$keterangan','$nopin','$noreg')");
	}

	public function file04($nama,$keterangan,$nopin,$nominal){
		$query = $this->db->query("insert into upload04 values ('$nama','$keterangan','$nopin','$nominal')");
	}

	public function file05($nama,$keterangan,$nopin,$status,$alasan){
		$query = $this->db->query("insert into upload05 values ('$nama','$keterangan','$nopin','$status','$alasan')");
	}

	public function t_urut_upload($jml){
		$user = create_user();
		$hariini = hariini();
		$cek = $this->db->query("select max(nourut) urut from t_urut_upload");
		if(empty($cek)){
			$urut = "00001";
		}else{
			$abc = $cek->row()->urut+1;
			if($abc>9){
				$urut = "000".$abc;
			}else if($abc>99){
				$urut = "00".$abc;
			}else if($abc>999){
				$urut = "0".$abc;
			}else if($abc>9999){
				$urut = $abc;
			}else{
				$urut = "0000".$abc;
			}
		}
		$urut = $this->db->query("insert into t_urut_upload values ('$urut','$jml','$user','$hariini')");	
	}

	public function cekNomorUrut(){
		$urut = $this->db->query("select max(nourut) urut from t_urut_upload");
		return $urut->row()->urut;	
	}

	public function NonAc(){
		$query = $this->db->query("select NomorRegistrasi, a.idDebitur, a.cif, NomorPinjaman, plafon, TglAkadKredit, TglAkhirKredit, TenorTahun, TcKet, NamaDebitur from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) where StatusDok = 0");
		return $query->result();
	}

	public function uploadDok($dok,$noreg,$nama){
		$query = $this->db->query("update t_pinjaman_dok set namafile = '$dok', terpenuhi = 1 where NomorRegistrasi = '$noreg' and dokumen='$nama'");

		$update = $this->db->query("select count(*) total, sum(terpenuhi) jml from t_pinjaman_dok where NomorRegistrasi = '$noreg'");
		if($update->row()->total==$update->row()->jml){
			$tgl = hariini();
			$up = $this->db->query("update t_pinjaman set StatusDok = 1, TglDokLengkap = '$tgl' where NomorRegistrasi = '$noreg'");
		}
	}

	public function FilterNonAc(){
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		$produk = $this->input->post("produk");
		$status = $this->input->post("status");
		$nopin = $this->input->post("nopin");
		$nama = $this->input->post("nama");
		if(empty($this->input->post("tgl_mulai")) && empty($this->input->post("tgl_akhir"))){
			$tglawal = "";
			$tglakhir = "";
		}else{
			$tglawal = date_format(date_create($this->input->post("tgl_mulai")),'Y-m-d');
			$tglakhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d');
		}
		
		$and = "";
		
		if($cabang!="" && empty($capem)){
			$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang')";
		}

		if($capem!=""){
			$and .= " and kodeBank = '$capem'";
		}

		if($produk!=""){
			$and .= " and idProduk = '$produk'";
		}

		if($nopin!=""){
			$and .= " and NomorPinjaman = '$nopin'";
		}

		if($nama!=""){
			$and .= " and NamaDebitur like '%$nama%'";
		}

		if($tglawal!=""){
			$and .= " and convert(varchar(10), a.create_date,120) between '$tglawal' and '$tglakhir'";
		}

		$query = $this->db->query("select NomorRegistrasi, a.idDebitur, a.cif, NomorPinjaman, plafon, TglAkadKredit, TglAkhirKredit, TenorTahun, TcKet, NamaDebitur from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) where StatusDok = 0 ".$and);
		return $query;
		// return $query->result();
	}

	public function FilterAlokasi(){
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		$produk = $this->input->post("produk");
		$status = $this->input->post("status");
		$nopin = $this->input->post("nopin");
		$nama = $this->input->post("nama");
		if(empty($this->input->post("tgl_mulai")) && empty($this->input->post("tgl_akhir"))){
			$tglawal = "";
			$tglakhir = "";
		}else{
			$tglawal = date_format(date_create($this->input->post("tgl_mulai")),'Y-m-d');
			$tglakhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d');
		}
		
		$and = "";
		
		if($cabang!="" && empty($capem)){
			$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang')";
		}

		if($capem!=""){
			$and .= " and kodeBank = '$capem'";
		}

		if($produk!=""){
			$and .= " and idProduk = '$produk'";
		}

		if($nopin!=""){
			$and .= " and NomorPinjaman = '$nopin'";
		}

		if($nama!=""){
			$and .= " and NamaDebitur like '%$nama%'";
		}

		if($tglawal!=""){
			$and .= " and convert(varchar(10), a.create_date,120) between '$tglawal' and '$tglakhir'";
		}

		$query = $this->db->query("select NomorRegistrasi, a.idDebitur, NomorPinjaman, plafon, TglAkadKredit, TglAkhirKredit, TenorTahun, TenorBulan, kodeBank, NamaDebitur from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) where kodeAsuransi = '99' ".$and);
		return $query;
		// return $query->result();
	}

	public function filter_rekonsel(){
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		$produk = $this->input->post("produk");
		$status = $this->input->post("status");
		$nopin = $this->input->post("nopin");
		$nama = $this->input->post("nama");
		if(empty($this->input->post("tgl_mulai")) && empty($this->input->post("tgl_akhir"))){
			$tglawal = "";
			$tglakhir = "";
		}else{
			$tglawal = date_format(date_create($this->input->post("tgl_mulai")),'Y-m-d');
			$tglakhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d');
		}
		
		$and = "";
		
		if($cabang!="" && empty($capem)){
			$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang')";
		}

		if($capem!=""){
			$and .= " and kodeBank = '$capem'";
		}

		if($produk!=""){
			$and .= " and idProduk = '$produk'";
		}

		if($nopin!=""){
			$and .= " and NomorPinjaman = '$nopin'";
		}

		if($nama!=""){
			$and .= " and NamaDebitur like '%$nama%'";
		}

		if($tglawal!=""){
			$and .= " and convert(varchar(10), a.create_date,120) between '$tglawal' and '$tglakhir'";
		}

		$query = $this->db->query("select NomorRegistrasi, a.idDebitur, a.cif, NomorPinjaman, plafon, JumlahPremiTenor,TglAkadKredit, TglAkhirKredit, TenorTahun, TenorBulan, TcKet, NamaDebitur, kodeBank, kodeAsuransi from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) where StatusRekonsel = 0 and KodePekerjaan != 'BJ' and StatusPrapen = '0' and StatusDok = 1 ".$and);
		return $query;
		// return $query->result();
	}

	public function validasiRekonsel(){
		$nopin = $this->input->post("NomorPinjaman");
		$hariini = hariini();
		$query = $this->db->query("update t_pinjaman set StatusRekonsel = 1, TglRekonsel = '$hariini' where NomorPinjaman in (".$nopin.")");
		if($this->db->affected_rows()>0){
			return true;
		}else{
			return false;
		}
	}

	public function filter_prapen(){
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		$produk = $this->input->post("produk");
		$status = $this->input->post("status");
		$nopin = $this->input->post("nopin");
		$nama = $this->input->post("nama");
		if(empty($this->input->post("tgl_mulai")) && empty($this->input->post("tgl_akhir"))){
			$tglawal = "";
			$tglakhir = "";
		}else{
			$tglawal = date_format(date_create($this->input->post("tgl_mulai")),'Y-m-d');
			$tglakhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d');
		}
		
		$and = "";
		
		if($cabang!="" && empty($capem)){
			$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang')";
		}

		if($capem!=""){
			$and .= " and kodeBank = '$capem'";
		}

		if($produk!=""){
			$and .= " and idProduk = '$produk'";
		}

		if($nopin!=""){
			$and .= " and NomorPinjaman = '$nopin'";
		}

		if($nama!=""){
			$and .= " and NamaDebitur like '%$nama%'";
		}

		if($tglawal!=""){
			$and .= " and convert(varchar(10), a.create_date,120) between '$tglawal' and '$tglakhir'";
		}

		$query = $this->db->query("select NomorRegistrasi, a.idDebitur, a.cif, NomorPinjaman, plafon, JumlahPremiTenor,TglAkadKredit, TglAkhirKredit, KodePekerjaan, TenorTahun, TenorBulan, umur2,StatusPrapen, UsiaDebitur, TcKet, NamaDebitur, kodeBank, kodeAsuransi from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) where KodePekerjaan != 'BJ' and StatusPrapen = '1' ".$and);
		// $query = $this->db->query("select NomorRegistrasi, a.idDebitur, a.cif, NomorPinjaman, plafon, JumlahPremiTenor,TglAkadKredit, TglAkhirKredit, TenorTahun, umur2,StatusPrapen, UsiaDebitur, TcKet, NamaDebitur, kodeBank, kodeAsuransi from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) where StatusRekonsel = 0 and KodePekerjaan != 'BJ' and StatusPrapen = '1' ".$and);
		return $query;
		// return $query->result();
	}

	public function filter_bunga(){
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		$produk = $this->input->post("produk");
		$status = $this->input->post("status");
		$nopin = $this->input->post("nopin");
		$nama = $this->input->post("nama");
		if(empty($this->input->post("tgl_mulai")) && empty($this->input->post("tgl_akhir"))){
			$tglawal = "";
			$tglakhir = "";
		}else{
			$tglawal = date_format(date_create($this->input->post("tgl_mulai")),'Y-m-d');
			$tglakhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d');
		}
		
		$and = "";
		
		if($cabang!="" && empty($capem)){
			$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang')";
		}

		if($capem!=""){
			$and .= " and kodeBank = '$capem'";
		}

		if($produk!=""){
			$and .= " and idProduk = '$produk'";
		}

		if($nopin!=""){
			$and .= " and NomorPinjaman = '$nopin'";
		}

		if($nama!=""){
			$and .= " and NamaDebitur like '%$nama%'";
		}

		if($tglawal!=""){
			$and .= " and convert(varchar(10), a.create_date,120) between '$tglawal' and '$tglakhir'";
		}

		$query = $this->db->query("select NomorRegistrasi, NomorPinjaman, KodePekerjaan, plafon, TglAkadKredit, TglAkhirKredit, TenorBulan, kodeBank, NamaDebitur, StatusPinjaman, Bunga, StatusBunga from t_pinjaman a join t_debitur b on a.idDebitur=b.idDebitur and a.cif=b.cif where KodePekerjaan = 'BJ' ".$and);
		return $query;
	}

	public function debitur_bunga($noreg){
		$query = $this->db->query("select NomorRegistrasi, NomorPinjaman, KodePekerjaan, plafon, TglAkadKredit, TglAkhirKredit, TenorBulan, kodeBank, NamaDebitur, StatusPinjaman, Bunga, StatusBunga from t_pinjaman a join t_debitur b on a.idDebitur=b.idDebitur and a.cif=b.cif where NomorRegistrasi = '$noreg'");
		return $query->row();
	}

	public function perhitungan_bunga($noreg){
		$query = $this->db->query("select BulanKe,SisaPinjaman,AngsuranBunga,AngsuranPokok,TotalAngsuran,ospremi from t_pinjaman_bj_cicilan where NomorRegistrasi = '$noreg'");
		return $query;
	}

	public function hitungBunga(){
		$noreg = $this->input->post("NomorRegistrasi");
		$bunga = str_replace(",",".",$this->input->post("bunga"));
		$query = $this->db->query("delete from t_pinjaman_bj_cicilan_temp where NomorRegistrasi = '$noreg'");
		$pinjaman = $this->db->query("select plafon, TenorBulan, NomorPinjaman from t_pinjaman where NomorRegistrasi = '$noreg'");
		$plafon = $pinjaman->row()->plafon;
		$plafon_asli = $pinjaman->row()->plafon;
		$TenorBulan = $pinjaman->row()->TenorBulan;
		$nopin = $pinjaman->row()->NomorPinjaman;

		$a = 1;
		$b = 0;

		$bunga_bulan      = ($bunga/12)/100;
		$pembagi          = 1-(1/pow(1+$bunga_bulan,$TenorBulan));
  		$hasil            = $plafon/($pembagi/$bunga_bulan);
		for ($i=0; $i <= $TenorBulan ; $i++) {

			$ang_bunga = $plafon*$bunga_bulan;
			$ang_pokok = $hasil-$ang_bunga;
			$plafon = $plafon - $ang_pokok;
			$cicilan = $ang_bunga+$ang_pokok;

			if ($i==0) {
				$z = $i+1;
				if($z%12==0){
					$a = $a+1;
				}else{
					if($b==0){
						$a = 1;
					}else{
						$a = $b;
					}
				}
			}else{
				if($i%12==0){
					$a = $a+1;
				}else{
					if($b==0){
						$a = 1;
					}else{
						$a = $b;
					}
				}
			}
			
			

			$b = $a;



			if($i==0){
				$plafon = $plafon_asli;
				$ospremi = $plafon_asli*0.00375;
				$ang_bunga = 0;
				$ang_pokok = 0;
				$cicilan = 0;
			}else{
				$ospremi = $plafon*0.00375;
			}

			if($i==$TenorBulan){
				$plafon = $plafon-$plafon;
				$ospremi = $plafon*0.00375;
			}

			$insert = $this->db->query("insert into t_pinjaman_bj_cicilan_temp values ('$noreg','$nopin',$a,$i,'$plafon_asli','$plafon','$ang_bunga','$ang_pokok','$cicilan','$ospremi')");
		}

		$hasilPerhitungan = $this->db->query("select * from t_pinjaman_bj_cicilan_temp where NomorRegistrasi = '$noreg'");
		return $hasilPerhitungan;
	}

	public function simpanHitungBunga(){
		$noreg = $this->input->post("NomorRegistrasi");
		$bunga = str_replace(",",".",$this->input->post("bunga"));
		$user = create_user();
		$hariini = hariini();
		$delete = $this->db->query("delete from t_pinjaman_bj_cicilan where NomorRegistrasi = '$noreg'");
		$insert = $this->db->query("insert into t_pinjaman_bj_cicilan (NomorRegistrasi,NomorPinjaman,TahunKe,BulanKe,plafon,SisaPinjaman,AngsuranBunga,AngsuranPokok,TotalAngsuran,ospremi,create_user,create_date) select *, '$user','$hariini' from t_pinjaman_bj_cicilan_temp where NomorRegistrasi = '$noreg'");
		
		$bj = $this->db->query("select max(TahunKe) tahun from t_pinjaman_bj where NomorRegistrasi = '$noreg'");
		for ($i=1; $i <= $bj->row()->tahun; $i++) { 
			$premicicilan = $this->db->query("select top 1 TahunKe,SisaPinjaman,ospremi from t_pinjaman_bj_cicilan where NomorRegistrasi = '$noreg' and TahunKe = '$i' order by TahunKe asc");
			if(!empty($premicicilan->row())){
				$SisaPinjaman = $premicicilan->row()->SisaPinjaman;
				$ospremi = $premicicilan->row()->ospremi;
				$update_bj = $this->db->query("update t_pinjaman_bj set SisaPinjaman = '$SisaPinjaman', ospremi = '$ospremi' where NomorRegistrasi = '$noreg' and TahunKe = '$i' and StatusBayar = '0'");
			} 
		}

		$premibj = $this->db->query("select sum(ospremi) premi from t_pinjaman_bj where NomorRegistrasi = '$noreg'");
		$TotalOsPremi = $premibj->row()->premi; 

		$update = $this->db->query("update t_pinjaman set TotalOsPremi = '$TotalOsPremi', TotalSeharusnya = '$TotalOsPremi', StatusBunga = 1, Bunga = '$bunga' where NomorRegistrasi = '$noreg'");

		$query = $this->db->query("delete from t_pinjaman_bj_cicilan_temp where NomorRegistrasi = '$noreg'");
		$hasilPerhitungan = $this->db->query("select NomorRegistrasi,NomorPinjaman,TahunKe,BulanKe,plafon,SisaPinjaman,AngsuranBunga,AngsuranPokok,TotalAngsuran,ospremi from t_pinjaman_bj_cicilan where NomorRegistrasi = '$noreg'");
		return $hasilPerhitungan;
	}

	public function filter_rekonsel_bj(){
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		$produk = $this->input->post("produk");
		$status = $this->input->post("status");
		$nopin = $this->input->post("nopin");
		$nama = $this->input->post("nama");
		if(empty($this->input->post("tgl_mulai")) && empty($this->input->post("tgl_akhir"))){
			$tglawal = "";
			$tglakhir = "";
		}else{
			$tglawal = date_format(date_create($this->input->post("tgl_mulai")),'Y-m-d');
			$tglakhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d');
		}
		
		$and = "";
		
		if($cabang!="" && empty($capem)){
			$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang')";
		}

		if($capem!=""){
			$and .= " and kodeBank = '$capem'";
		}

		if($produk!=""){
			$and .= " and idProduk = '$produk'";
		}

		if($nopin!=""){
			$and .= " and c.NomorPinjaman = '$nopin'";
		}

		if($nama!=""){
			$and .= " and NamaDebitur like '%$nama%'";
		}

		if($tglawal!=""){
			$and .= " and convert(varchar(10), a.create_date,120) between '$tglawal' and '$tglakhir'";
		}

		$hariini = hariini();

		$query = $this->db->query("
			select c.NomorRegistrasi, c.NomorPinjaman,c.TglAkadKredit, TenorTahun, a.TenorBulan, NamaDebitur, kodeBank, kodeAsuransi, min(TahunKe) tahun from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) join t_pinjaman_bj c on (a.NomorRegistrasi=c.NomorRegistrasi and a.NomorPinjaman=c.NomorPinjaman) where StatusBayar = 0 and KodePekerjaan = 'BJ' ".$and." group by c.NomorRegistrasi, c.NomorPinjaman,c.TglAkadKredit, TenorTahun, a.TenorBulan,NamaDebitur, kodeBank, kodeAsuransi");
		return $query;
		// return $query->result();
	}

	public function filter_rekonsel_bj_detail($noreg,$tahunke){
		$query = $this->db->query("select idTbj, SisaPinjaman, TglJatuhTempo, ospremi, plafon from t_pinjaman_bj where NomorRegistrasi = '$noreg' and TahunKe = '$tahunke'");
		return $query->row();
	}

	public function validasiRekonselBj(){
		$nopin = $this->input->post("NomorPinjaman");
		$NomorPinjaman = $this->db->query("select NomorPinjaman,ospremi from t_pinjaman_bj where idTbj in (".$nopin.")");
		$hariini = hariini();
		foreach ($NomorPinjaman->result() as $key) {
			$nox = $key->NomorPinjaman;
			$ospremi = $key->ospremi;
			$query = $this->db->query("update t_pinjaman set StatusRekonsel = 1, TglRekonsel = '$hariini', TotalOsPremi = TotalOsPremi-'$ospremi', JumlahPremiTenor='$ospremi', FeePremiBroker=FeePresenBroker*'$ospremi', FeePremiBank=FeePersenBank*'$ospremi' where NomorPinjaman = '$nox'");
		}

		$update = $this->db->query("update t_pinjaman_bj set StatusBayar = 1, TglRekonsel = '$hariini', create_date = '$hariini', premiterbayar = ospremi where idTbj in (".$nopin.")");
		if($this->db->affected_rows()>0){
			return true;
		}else{
			return false;
		}
	}

	public function filter_polis(){
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		$produk = $this->input->post("produk");
		$status = $this->input->post("status");
		$nopin = $this->input->post("nopin");
		$nama = $this->input->post("nama");
		if(empty($this->input->post("tgl_mulai")) && empty($this->input->post("tgl_akhir"))){
			$tglawal = "";
			$tglakhir = "";
		}else{
			$tglawal = date_format(date_create($this->input->post("tgl_mulai")),'Y-m-d');
			$tglakhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d');
		}
		
		$and = "";
		
		if($cabang!="" && empty($capem)){
			$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang')";
		}

		if($capem!=""){
			$and .= " and kodeBank = '$capem'";
		}

		if($produk!=""){
			$and .= " and idProduk = '$produk'";
		}

		if($nopin!=""){
			$and .= " and NomorPinjaman = '$nopin'";
		}

		if($nama!=""){
			$and .= " and NamaDebitur like '%$nama%'";
		}

		if($tglawal!=""){
			$and .= " and convert(varchar(10), a.create_date,120) between '$tglawal' and '$tglakhir'";
		}

		$query = $this->db->query("select NomorRegistrasi, a.idDebitur, a.cif, NomorPinjaman, plafon, TglAkadKredit, TglAkhirKredit, TenorTahun,TenorBulan, TcKet, NamaDebitur, kodeBank, kodeAsuransi from t_pinjaman a join t_debitur b on(a.idDebitur=b.idDebitur and a.cif=b.cif) where StatusDok = 1 and StatusRekonsel = 1 and StatusPolis = 0 ".$and);
		return $query;
	}

	public function terbitkanPolis(){
		$noreg = $this->input->post("NomorRegistrasi");
		$nopolis = $this->input->post("nopolis");
		$tglpolis = date_format(date_create($this->input->post("tglpolis")),'Y-m-d 00:00:00');
		$user = $this->session->userdata("idUser");
		$hariini = date("Y-m-d H:i:s");
		$query = $this->db->query("update t_pinjaman set StatusPolis = 1 where NomorRegistrasi = '$noreg'");
		if($this->db->affected_rows()>0){
			$insert = $this->db->query("insert into t_pinjaman_polis values ('$noreg','$nopolis','$tglpolis','$user','$hariini','','')");
			if($this->db->affected_rows()>0){
				return "1";
			}else{
				return "-1";
			}
		}else{
			return "0";
		}
	}

	public function simpanAsdur(){
		$noreg = $this->input->post("noreg");
		$asdur = $this->input->post("asdur");
		$user = create_user();
		$hariini = hariini();
		$insert = $this->db->query("update t_pinjaman set kodeAsuransi = '$asdur', TglAlokasi = '$hariini' where NomorRegistrasi = '$noreg'");
		if($insert){
			return "1";
		}else{
			return "0";
		}
	}
}