<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model_laporan extends CI_Model{

	public function __Construct(){
		parent::__Construct();
		$this->load->database();
		
	}

	public function getProduksi(){
		$tipe = $this->input->post("tipe");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tgl_awal")) && empty($this->input->post("tgl_akhir"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tgl_awal")),'Y-m-d');
			$tgl_akhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d');
		}


		$and = "";

		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }

		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				if (cekOpBank($kodeBank)!="CABANG"){
				    $and .= " and kodeBank='$kodeBank' ";
				}
			}
		}

		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}
		if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}

		if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and convert(varchar(10),a.create_date,120) between '$tgl_mulai' and '$tgl_akhir'";
		}

		$query = $this->db->query("select kodeBank, b.kodeAsuransi, NomorPinjaman, TcKet, NomorPK, KodePekerjaan,a.cif, NamaDebitur, a.TglLahir, TglAkadKredit, TglAkhirKredit, TenorBulan, TotalOsPremi, StatusDok, plafon, TarifPremi, premi, JumlahPremiTenor, FeePersenBank, FeePremiBank, FeePremiBroker, FeePresenBroker from t_debitur a join t_pinjaman b on(a.idDebitur=b.idDebitur and a.cif=b.cif) where StatusRekonsel = 1 ".$and);
		return $query->result();
	}

	public function getProduksi2(){
		$tipe = $this->input->post("tipe");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");

		if(empty($this->input->post("bulan"))){
			$tgl_mulai = "01-".date("m-Y");
		    $tgl_akhir = date('t')."-".date("m-Y");
		    $tgl_input = date("Y-m");
		    $awal_bulan = date_format(date_create($tgl_mulai),'Y-m-d 00:00:00');
		}else{
			$tgl_mulai = "01-".($this->input->post("bulan"));
			$tgl_input = date_format(date_create($tgl_mulai),'Y-m');
		    $tgl_akhir = date('t')."-".($this->input->post("bulan"));
		    $awal_bulan = date_format(date_create($tgl_mulai),'Y-m-d 00:00:00');
		}

		$and = "";

		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }

		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and convert(varchar(7), b.create_date,120) = '$tgl_input'";
		}

		
		$query = $this->db->query("
			WITH berjalan (kodeAsuransi, jmldeb, jmlpremi,premiterima)  
			AS    
			(  
		    select b.kodeAsuransi, count(c.idDebitur) jmldeb, sum(plafon) jmlpremi, 
			sum(case when convert(varchar(7), b.create_date,120) = '$tgl_input' 
			then JumlahPremiTenor else 0 end) premiterima from t_pinjaman b 
			join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur) 
			where StatusRekonsel = '1' ".$and."
			group by b.kodeAsuransi 

			)  
			,   
			aktif (kodeAsuransi, jmldebaktif, jmlpremiaktif,premiterimaaktif)  
			AS  
			(  
	        select b.kodeAsuransi, count(c.idDebitur) jmldebaktif, sum(plafon) jmlpremiaktif, 
			sum(case when b.create_date < '$awal_bulan' then JumlahPremiTenor else 0 end) premiterimaaktif 
			from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur) 
			where StatusRekonsel = '1' and StatusKlaim = '0' and StatusRestitusi = '0' and b.create_date < '$awal_bulan' ".$and." 
			group by b.kodeAsuransi   
			)  

			SELECT berjalan.kodeAsuransi  
			  , jmldeb  
			  , jmlpremi  
			  , premiterima  
			  , jmldebaktif, jmlpremiaktif,premiterimaaktif 
			FROM berjalan  
			left JOIN aktif ON berjalan.kodeAsuransi= aktif.kodeAsuransi

			");

		return $query;
	}

	public function getProduksi3(){
		$tipe = $this->input->post("tipe");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tgl_awal2")) && empty($this->input->post("tgl_akhir2"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tgl_awal2")),'Y-m-d 00:00:00');
			$tgl_akhir = date_format(date_create($this->input->post("tgl_akhir2")),'Y-m-d 23:59:59');
		}

		$and = "";

		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }

		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and create_date between '$tgl_mulai' and '$tgl_akhir'";
		}


		$query = $this->db->query("select kodeBank, count(idDebitur) jmldeb, sum(plafon) pertanggungan,sum(JumlahPremiTenor) premi, sum(FeePremiBroker) brokerage, sum(JumlahPremiTenor-FeePremiBroker-FeePremiBank) net from t_pinjaman where StatusRekonsel = '1' ".$and." group by kodeBank");
		return $query;
	}

	public function getProduksi4($bulan){
		$tipe = $this->input->post("tipe");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");

		if(empty($this->input->post("tahun"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			// $tgl_mulai = date_format(date_create($this->input->post("tahun")),'Y-'.$bulan);
			$tgl_mulai = $this->input->post("tahun").'-'.$bulan;
		    $tgl_akhir = date_format(date_create($this->input->post("tahun")),'Y-'.$bulan);
		}

		$and = "";

		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }

		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and idProduk=".$produk;
		}
		if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}
		if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and convert(varchar(7), create_date, 120) = '$tgl_mulai' ";
		}


		$query = $this->db->query("select CONVERT(CHAR(2),create_date,110) bulan,CONVERT(CHAR(7),create_date,120) tahun,count(idDebitur) jmldeb, sum(plafon) plafon,sum(JumlahPremiTenor) premi, sum(FeePremiBroker) brokerage, sum(JumlahPremiTenor-FeePremiBroker-FeePremiBank) net from t_pinjaman where StatusRekonsel = '1' ".$and." group by CONVERT(CHAR(2),create_date,110),CONVERT(CHAR(7),create_date,120)");
		return $query;
	}

	public function getProduksi5(){
		$tipe = $this->input->post("tipe");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");

		if(empty($this->input->post("tahun"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tahun")),'01-'.$bulan.'-Y');
		    $tgl_akhir = date('t')."-".date_format(date_create($this->input->post("tahun")),$bulan.'-Y');
		}

		

		$and = "";

		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }

		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			$kodeBank = $this->session->userdata("kodeBank");
			$and .= " and kodeBank='$kodeBank' ";
		}
		if(!empty($asuransi)){
			$and = " and KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and convert(varchar, create_date, 105) between '$tgl_mulai' and '$tgl_akhir'";
		}


		$query = $this->db->query("select CONVERT(CHAR(7),create_date,120), count(idDebitur) jmldeb, sum(plafon) plafon,sum(JumlahPremiTenor) premi, sum(FeePremiBroker) brokerage, sum(JumlahPremiTenor-FeePremiBroker-FeePremiBank) net from t_pinjaman where StatusRekonsel = '1' ".$and." group by CONVERT(CHAR(7),create_date,120)");
		return $query;
	}

	public function getSumProduksi(){
		$tipe = $this->input->post("tipe");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");

		if(empty($this->input->post("tgl_awal")) && empty($this->input->post("tgl_akhir"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tgl_awal")),'Y-m-d 00:00:00');
		    $tgl_akhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d 23:59:59');
		}

		

		$and = "";

		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }

		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and a.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and a.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and create_date between '$tgl_mulai' and '$tgl_akhir'";
		}


		$query = $this->db->query("select kodeBank,sum(case when kodePremi!='45' and kodePremi!='BJ' then 1 else 0 end ) as 'pnsnonpns',sum( case when kodePremi='45' then 1 else 0 end ) as 'pensiunan', sum(case when kodePremi='BJ' then 1 else 0 end ) as 'jatim', SUM(plafon) plafon, SUM(premi) premi, sum(JumlahPremiTenor) JumlahPremiTenor from t_pinjaman a join t_tc_premi b on (a.KodePekerjaan=b.kodePremi) where StatusRekonsel = 1 ".$and." group by kodeBank");
		return $query;
	}

	public function getSumRestitusi(){
		$tipe = $this->input->post("tipe");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tgl_awal")) && empty($this->input->post("tgl_akhir"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tgl_awal")),'Y-m-d 00:00:00');
		    $tgl_akhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d 23:59:59');
		}

		

		$and = "";

		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }

		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and a.create_date between '$tgl_mulai' and '$tgl_akhir'";
		}

		$query = $this->db->query("select kodeBank, count(KodeRestitusi) diajukan, 
            sum(CASE WHEN selesai = 1  THEN 1 ELSE 0 END) diterima,
            sum(case when keputusan = 1 then PengembalianPremi else 0 end) nominal from t_pinjaman_restitusi a join t_pinjaman b on(a.NomorRegistrasi=b.NomorRegistrasi) where StatusRestitusi = 1 ".$and." group by kodeBank");
		return $query;
	}

	public function getSumKlaim(){
		$tipe = $this->input->post("tipe");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tgl_awal")) && empty($this->input->post("tgl_akhir"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tgl_awal")),'Y-m-d 00:00:00');
		    $tgl_akhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d 23:59:59');
		}

		$and = "";

		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }

		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and a.create_date between '$tgl_mulai' and '$tgl_akhir'";
		}
		$query = $this->db->query("Select kodeBank,
            count(KodeKlaim) diajukan,
            sum(CASE WHEN selesai = 0 THEN 1 ELSE 0 END) proses,
            sum(CASE WHEN keputusan = '-2'  THEN 1 ELSE 0 END) ditolak,
            sum(CASE WHEN keputusan = '1'  THEN 1 ELSE 0 END) diterima,
            sum(NominalDisetujui) nominal
            from t_pinjaman_klaim a
            join t_pinjaman b
            on (a.NomorRegistrasi=b.NomorRegistrasi)
            where StatusKlaim = 1 ".$and."
            group by kodeBank");
		return $query;
	}

	public function getPolis1(){
		$tipe = $this->input->post("tipepolis");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$status = $this->input->post("status");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tgl_awal")) && empty($this->input->post("tgl_akhir"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tgl_awal")),'Y-m-d 00:00:00');
		    $tgl_akhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d 23:59:59');
		}

		$and = "";
		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }

		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}if($status!=""){
			$and .= " and StatusPolis=".$status;
		}
		if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and a.create_date between '$tgl_mulai' and '$tgl_akhir'";
		}

		$query = $this->db->query("select kodeBank, NomorPK, NamaDebitur, a.TglLahir, b.create_date, kodeAsuransi, plafon, StatusPolis, TglRekonsel from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur where StatusRekonsel = 1 ".$and);

		return $query;
	}	

	public function getPolis2(){
		$tipe = $this->input->post("tipepolis");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		


		if(empty($this->input->post("bulan"))){
			$tgl_mulai = "01-".date("m-Y");
		    $tgl_akhir = date('t')."-".date("m-Y");
		    $awal_bulan = date_format(date_create($tgl_mulai),'Y-m');
		    $dateadd = date_format(date_create($tgl_mulai),'Y-m-d');
		}else{
			$tgl_mulai = "01-".($this->input->post("bulan"));
		    $tgl_akhir = date('t')."-".($this->input->post("bulan"));
		    $awal_bulan = date_format(date_create($tgl_mulai),'Y-m');
		    $dateadd = date_format(date_create($tgl_mulai),'Y-m-d');
		}

		$and = "";
		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		// if(!empty($tgl_mulai) && !empty($tgl_akhir)){
		// 	$and .= " and a.create_date between '$tgl_mulai' and '$tgl_akhir'";
		// }

		$query = $this->db->query("
			WITH sebelumnya (kodeAsuransi, jmldeb, pertanggungan)  
				AS  (  
				    select b.kodeAsuransi, count(c.idDebitur) jmldeb, sum(plafon) pertanggungan 
					from t_pinjaman b 
					join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur) 
					where StatusRekonsel = '1' and StatusPolis = '1' and convert(varchar(7), b.create_date,120) = convert(varchar(7), DATEADD(mm,-1,'$dateadd'),120) ".$and."
					group by b.kodeAsuransi
				),
				pengajuan (kodeAsuransi, jmldebajukan, jmlpengajuanplafon)  
				AS  (  
					select b.kodeAsuransi, count(c.idDebitur) jmldebajukan, sum(plafon) jmlpengajuanplafon
					from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur) 
					where StatusRekonsel = '1' and convert(varchar(7), b.create_date,120) = '$awal_bulan' ".$and."
					group by b.kodeAsuransi   
				),
				berjalan (kodeAsuransi, jmldebjalan, jmlpengajuanjalan)  
				AS  (  
					select b.kodeAsuransi, count(b.idDebitur) jmldebajukan, sum(plafon) jmlpengajuanjalan
					from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur) 
					where StatusRekonsel = '1' and StatusPolis = '1' and convert(varchar(7), b.create_date,120) = '$awal_bulan' ".$and."
					group by b.kodeAsuransi   
				)  

				SELECT pengajuan.kodeAsuransi  
				  , jmldeb  
				  , pertanggungan  
				  , jmldebajukan  
				  , jmlpengajuanplafon
				  , jmldebjalan
				  , jmlpengajuanjalan 
				FROM sebelumnya  
				right JOIN pengajuan ON sebelumnya.kodeAsuransi= pengajuan.kodeAsuransi
				left JOIN berjalan ON pengajuan.kodeAsuransi= berjalan.kodeAsuransi
		");

		return $query;
	}	

	public function getPolis3(){
		$tipe = $this->input->post("tipepolis");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		

		if(empty($this->input->post("tgl_akhir2"))){
			$dateadd = "";
		}else{
			$dateadd = date_format(date_create($this->input->post("tgl_akhir2")),'Y-m-d');
		}

		$and = "";
		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}

		$query = $this->db->query("
			WITH pengajuan (kodeAsuransi, jmldebajukan, jmlpengajuanplafon)  
				AS  (  
					select b.kodeAsuransi, count(c.idDebitur) jmldebajukan, sum(plafon) jmlpengajuanplafon
					from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur) 
					where StatusRekonsel = '1' and convert(varchar(10), b.create_date,120) <= '$dateadd' ".$and."
					group by b.kodeAsuransi   
				),
				berjalan (kodeAsuransi, jmldebjalan, jmlpengajuanjalan)  
				AS  (  
					select b.kodeAsuransi, count(c.idDebitur) jmldebajukan, sum(plafon) jmlpengajuanjalan
					from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur) 
					where StatusRekonsel = '1' and StatusPolis = '1' and convert(varchar(10), b.create_date,120) <= '$dateadd' ".$and."
					group by b.kodeAsuransi   
				)  

				SELECT pengajuan.kodeAsuransi  
				  , jmldebajukan  
				  , jmlpengajuanplafon
				  , jmldebjalan
				  , jmlpengajuanjalan 
				FROM pengajuan
				left JOIN berjalan ON pengajuan.kodeAsuransi= berjalan.kodeAsuransi
		");

		return $query;
	}	

	public function getPolis4($bulan){
		$tipe = $this->input->post("tipepolis");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tahun"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			// $tgl_mulai = date_format(date_create($this->input->post("tahun")),'Y-'.$bulan);
			$tgl_mulai = $this->input->post("tahun").'-'.$bulan;
		    $tgl_akhir = date_format(date_create($this->input->post("tahun")),'Y-'.$bulan);
		}

		$and = "";
		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}

		$query = $this->db->query("
			with
			pengajuan (bulan,tahun,jmldeb,plafon) as (
				select CONVERT(CHAR(2),b.create_date,110) bulan,CONVERT(CHAR(7),b.create_date,120) tahun,count(a.idDebitur) jmldeb, 
				sum(plafon) plafon from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur
				where StatusRekonsel = 1 and CONVERT(CHAR(7),b.create_date,120) = '$tgl_mulai' ".$and."
				group by CONVERT(CHAR(2),b.create_date,110),CONVERT(CHAR(7),b.create_date,120)
			),
			terbit (bulanx,tahunx,jmldebx,plafonx) as (
				select CONVERT(CHAR(2),b.create_date,110) bulan,CONVERT(CHAR(7),b.create_date,120) tahun,count(a.idDebitur) jmldeb, 
				sum(plafon) plafon from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur
				where StatusRekonsel = 1 and StatusPolis = 1 and CONVERT(CHAR(7),b.create_date,120) = '$tgl_mulai' ".$and."
				group by CONVERT(CHAR(2),b.create_date,110),CONVERT(CHAR(7),b.create_date,120)
			)
			select bulan, tahun, jmldeb, plafon, tahunx, jmldebx, plafonx
			from pengajuan left join terbit on pengajuan.bulan=terbit.bulanx
		");

		return $query;
	}

	public function getPolis5(){
		$tipe = $this->input->post("tipepolis");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tgl_awal3")) && empty($this->input->post("tgl_akhir3"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tgl_awal3")),'Y-m-d 00:00:00');
		    $tgl_akhir = date_format(date_create($this->input->post("tgl_akhir3")),'Y-m-d 23:59:59');
		}
		

		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and b.create_date between '$tgl_mulai' and '$tgl_akhir'";
		}

		$query = $this->db->query("
			with
			pengajuan (kodeBank,jmldeb,plafon) as (
				select kodeBank, count(b.idDebitur) jmldeb, sum(plafon) pertanggungan from t_debitur a join t_pinjaman b on(a.cif=b.cif and a.idDebitur=b.idDebitur) where StatusRekonsel = '1' ".$and." group by kodeBank
			),
			terbit (kodeBankx,jmldebx,plafonx) as (
				select kodeBank, count(b.idDebitur) jmldeb, sum(plafon) pertanggungan from t_debitur a join t_pinjaman b on(a.cif=b.cif and a.idDebitur=b.idDebitur) where StatusRekonsel = '1' and StatusPolis = '1' ".$and." group by kodeBank
			)
			select kodeBank, jmldeb, plafon, jmldebx, plafonx
			from pengajuan left join terbit on pengajuan.kodeBank=terbit.kodeBankx
		");

		return $query;
		
	}

	public function getRestitusi1(){
		$tipe = $this->input->post("tiperes");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tgl_awal")) && empty($this->input->post("tgl_akhir"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tgl_awal")),'Y-m-d 00:00:00');
		    $tgl_akhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d 23:59:59');
		}


		$and = "";
		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and c.create_date between '$tgl_mulai' and '$tgl_akhir'";
		}

		$query = $this->db->query("select kodeBank, NomorPinjaman, create_spv,NamaDebitur, c.TglPengajuanRestitusi, TglPelunasanRestitusi, kodeAsuransi, PengembalianPremi, TanggalBayar, keputusan, spv, pialang, asuransi, StatusPembayaran from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur join t_pinjaman_restitusi c on b.NomorRegistrasi=c.NomorRegistrasi where StatusRekonsel = 1  and StatusRestitusi = 1  and spv = 1 ".$and);

		return $query;
	}

	public function getRestitusi2(){
		$tipe = $this->input->post("tiperes");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("bulan"))){
			$tgl_mulai = "01-".date("m-Y");
		    $tgl_akhir = date('t')."-".date("m-Y");
		    $awal_bulan = date_format(date_create($tgl_mulai),'Y-m');
		    $dateadd = date_format(date_create($tgl_mulai),'Y-m-d');
		}else{
			$tgl_mulai = "01-".($this->input->post("bulan"));
		    $tgl_akhir = date('t')."-".($this->input->post("bulan"));
		    $awal_bulan = date_format(date_create($tgl_mulai),'Y-m');
		    $dateadd = date_format(date_create($tgl_mulai),'Y-m-d');
		}
		

		$and = "";
		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		// if(!empty($tgl_mulai) && !empty($tgl_akhir)){
		// 	$and .= " and a.create_date between '$tgl_mulai' and '$tgl_akhir'";
		// }

		$query = $this->db->query("
			WITH sebelumnya (kodeAsuransi, jmldeb, nominal)  
				AS  (  
				    select b.kodeAsuransi, count(c.idDebitur) jmldeb, sum(d.PengembalianPremi) nominal 
					from t_pinjaman b 
					join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur)
					join t_pinjaman_restitusi d on (b.NomorRegistrasi=d.NomorRegistrasi) 
					where StatusRekonsel = '1' and StatusRestitusi = '1' and spv = '1' and convert(varchar(7), d.create_spv,120) = convert(varchar(7), DATEADD(mm,-1,'$dateadd'),120) ".$and."
					group by b.kodeAsuransi
				),
				pengajuan (kodeAsuransi, jmldebajukan, nominalajukan)  
				AS  (  
					select b.kodeAsuransi, count(c.idDebitur) jmldebajukan, sum(d.PengembalianPremi) nominalajukan
					from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur)
					join t_pinjaman_restitusi d on (b.NomorRegistrasi=d.NomorRegistrasi)  
					where StatusRekonsel = '1' and StatusRestitusi = '1' and spv = '1'  and convert(varchar(7), d.create_spv,120) = '$awal_bulan' ".$and."
					group by b.kodeAsuransi   
				),
				berjalan (kodeAsuransi, jmldebjalan, nominaljalan)  
				AS  (  
					select b.kodeAsuransi, count(b.idDebitur) jmldebajukan, sum(d.PengembalianPremi) nominaljalan
					from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur)
					join t_pinjaman_restitusi d on (b.NomorRegistrasi=d.NomorRegistrasi)  
					where StatusRekonsel = '1' and StatusPembayaran = '1' and spv = '1'  and convert(varchar(7), d.create_spv,120) = '$awal_bulan' ".$and."
					group by b.kodeAsuransi   
				)  

				SELECT pengajuan.kodeAsuransi  
				  , jmldeb  
				  , nominal  
				  , jmldebajukan  
				  , nominalajukan
				  , jmldebjalan
				  , nominaljalan 
				FROM sebelumnya  
				right JOIN pengajuan ON sebelumnya.kodeAsuransi= pengajuan.kodeAsuransi
				left JOIN berjalan ON pengajuan.kodeAsuransi= berjalan.kodeAsuransi
		");

		return $query;
	}

	public function getRestitusi3(){
		$tipe = $this->input->post("tiperes");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");

		if(empty($this->input->post("tgl_akhir3"))){
			$dateadd = "";
		}else{
			$dateadd = date_format(date_create($this->input->post("tgl_akhir3")),'Y-m-d');
		}


		$and = "";
		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}

		$query = $this->db->query("
			WITH pengajuan (kodeAsuransi, jmldebajukan, nominalajukan)  
				AS  (  
					select b.kodeAsuransi, count(c.idDebitur) jmldebajukan, sum(d.PengembalianPremi) nominalajukan
					from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur)
					join t_pinjaman_restitusi d on (b.NomorRegistrasi=d.NomorRegistrasi)  
					where StatusRekonsel = '1' and StatusRestitusi = '1' and spv = '1'  and convert(varchar(7), d.create_spv,120) <= '$dateadd' ".$and."
					group by b.kodeAsuransi   
				),
				berjalan (kodeAsuransi, jmldebjalan, nominaljalan)  
				AS  (  
					select b.kodeAsuransi, count(c.idDebitur) jmldebajukan, sum(d.PengembalianPremi) nominaljalan
					from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur)
					join t_pinjaman_restitusi d on (b.NomorRegistrasi=d.NomorRegistrasi) 
					where StatusRekonsel = '1' and StatusPembayaran = '1' and spv = '1'  and convert(varchar(10), b.create_date,120) <= '$dateadd' ".$and."
					group by b.kodeAsuransi   
				)  

				SELECT pengajuan.kodeAsuransi  
				  , jmldebajukan  
				  , nominalajukan
				  , jmldebjalan
				  , nominaljalan 
				FROM pengajuan
				LEFT JOIN berjalan ON pengajuan.kodeAsuransi= berjalan.kodeAsuransi
		");

		return $query;
	}


	public function getRestitusi4($bulan){
		$tipe = $this->input->post("tiperes");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tahun2"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			// $tgl_mulai = date_format(date_create($this->input->post("tahun2")),'Y-'.$bulan);
			$tgl_mulai = $this->input->post("tahun").'-'.$bulan;
		    $tgl_akhir = date_format(date_create($this->input->post("tahun2")),'Y-'.$bulan);
		}

		$and = "";
		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}

		$query = $this->db->query("
			with
			pengajuan (bulan,tahun,jmldeb,plafon) as (
				select CONVERT(CHAR(2),create_spv,110) bulan,CONVERT(CHAR(7),create_spv,120) tahun,count(a.idDebitur) jmldeb, 
				sum(PengembalianPremi) plafon from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur join t_pinjaman_restitusi d on (b.NomorRegistrasi=d.NomorRegistrasi)  
					where StatusRekonsel = '1' and StatusRestitusi = '1' and spv = '1'  and CONVERT(CHAR(7),create_spv,120) = '$tgl_mulai' ".$and."
				group by CONVERT(CHAR(2),create_spv,110),CONVERT(CHAR(7),create_spv,120)
			),
			terbit (bulanx,tahunx,jmldebx,plafonx) as (
				select CONVERT(CHAR(2),create_spv,110) bulan,CONVERT(CHAR(7),create_spv,120) tahun,count(a.idDebitur) jmldeb, 
				sum(PengembalianPremi) plafon from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur
				join t_pinjaman_restitusi d on (b.NomorRegistrasi=d.NomorRegistrasi)  
					where StatusRekonsel = '1' and StatusPembayaran = '1' and spv = '1'   and CONVERT(CHAR(7),create_spv,120) = '$tgl_mulai' ".$and."
				group by CONVERT(CHAR(2),create_spv,110),CONVERT(CHAR(7),create_spv,120)
			)
			select bulan, tahun, jmldeb, plafon, tahunx, jmldebx, plafonx
			from pengajuan left join terbit on pengajuan.bulan=terbit.bulanx
		");

		return $query;
	}

	public function getRestitusi5(){
		$tipe = $this->input->post("tiperes");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tgl_awal2")) && empty($this->input->post("tgl_akhir2"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tgl_awal2")),'Y-m-d 00:00:00');
		    $tgl_akhir = date_format(date_create($this->input->post("tgl_akhir2")),'Y-m-d 23:59:59');
		}
		

		$and = "";
		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and create_spv between '$tgl_mulai' and '$tgl_akhir'";
		}

		$query = $this->db->query("
			with
			pengajuan (kodeBank,jmldeb,plafon) as (
				select kodeBank, count(b.idDebitur) jmldeb, sum(PengembalianPremi) pertanggungan from t_debitur a join t_pinjaman b on(a.cif=b.cif and a.idDebitur=b.idDebitur) left join t_pinjaman_restitusi c on(b.NomorRegistrasi=c.NomorRegistrasi) where StatusRekonsel = '1' and StatusRestitusi = '1'  and spv = '1' ".$and." group by kodeBank
			),
			terbit (kodeBankx,jmldebx,plafonx) as (
				select kodeBank, count(b.idDebitur) jmldeb, sum(PengembalianPremi) pertanggungan from t_debitur a join t_pinjaman b on(a.cif=b.cif and a.idDebitur=b.idDebitur) left join t_pinjaman_restitusi c on(b.NomorRegistrasi=c.NomorRegistrasi) where StatusRekonsel = '1' and StatusPembayaran = '1'  and spv = '1' ".$and." group by kodeBank
			)
			select kodeBank, jmldeb, plafon, jmldebx, plafonx
			from pengajuan left join terbit on pengajuan.kodeBank=terbit.kodeBankx
		");

		return $query;
		
	}

	public function getKlaim1(){
		$tipe = $this->input->post("tiperes");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		$status = $this->input->post("status");
		if(empty($this->input->post("tgl_awal")) && empty($this->input->post("tgl_akhir"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tgl_awal")),'Y-m-d 00:00:00');
		    $tgl_akhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d 23:59:59');
		}

		$and = "";
		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and c.create_date between '$tgl_mulai' and '$tgl_akhir'";
		}

		if(!empty($status)){
			$and .= " and keputusan = '$status'";
		}

		if($status==0){
			$and .= " and keputusan = '0'";
		}

		$query = $this->db->query("select kodeBank, NomorPinjaman, NamaDebitur, c.TglKlaim,idResiko, create_spv, TglResiko, kodeAsuransi, NominalDiajukan,NominalDisetujui, TanggalBayar, keterangan, keputusan, spv, pialang, asuransi, StatusPembayaran from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur left join t_pinjaman_klaim c on b.NomorRegistrasi=c.NomorRegistrasi where StatusRekonsel = 1  and StatusKlaim = 1 and spv = 1 ".$and);

		return $query;
	}

	public function getKlaim2(){
		$tipe = $this->input->post("tipeklaim");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("bulan"))){
			$tgl_mulai = "01-".date("m-Y");
		    $tgl_akhir = date('t')."-".date("m-Y");
		    $awal_bulan = date_format(date_create($tgl_mulai),'Y-m');
		    $dateadd = date_format(date_create($tgl_mulai),'Y-m-d');
		}else{
			$tgl_mulai = "01-".($this->input->post("bulan"));
		    $tgl_akhir = date('t')."-".($this->input->post("bulan"));
		    $awal_bulan = date_format(date_create($tgl_mulai),'Y-m');
		    $dateadd = date_format(date_create($tgl_mulai),'Y-m-d');
		}

		$and = "";
		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}
		// if(!empty($tgl_mulai) && !empty($tgl_akhir)){
		// 	$and .= " and a.create_date between '$tgl_mulai' and '$tgl_akhir'";
		// }

		$query = $this->db->query("
			WITH sebelumnya (kodeAsuransi, jmldeb, nominal)  
				AS  (  
				    select b.kodeAsuransi, count(c.idDebitur) jmldeb, sum(d.NominalDisetujui) nominal 
					from t_pinjaman b 
					join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur)
					join t_pinjaman_klaim d on (b.NomorRegistrasi=d.NomorRegistrasi) 
					where StatusRekonsel = '1' and StatusKlaim = '1' and StatusPembayaran = '1' and spv = '1' and keputusan != -2  and convert(varchar(7), d.create_spv,120) = convert(varchar(7), DATEADD(mm,-1,'$dateadd'),120) ".$and."
					group by b.kodeAsuransi
				),
				pengajuan (kodeAsuransi, jmldebajukan, nominalajukan)  
				AS  (  
					select b.kodeAsuransi, count(c.idDebitur) jmldebajukan, sum(d.NominalDiajukan) nominalajukan
					from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur)
					join t_pinjaman_klaim d on (b.NomorRegistrasi=d.NomorRegistrasi)  
					where StatusRekonsel = '1' and StatusKlaim = '1' and spv = '1' and keputusan != -2 and convert(varchar(7), d.create_spv,120) = '$awal_bulan' ".$and."
					group by b.kodeAsuransi   
				),
				berjalan (kodeAsuransi, jmldebjalan, nominaljalan)  
				AS  (  
					select b.kodeAsuransi, count(b.idDebitur) jmldebajukan, sum(d.NominalDisetujui) nominaljalan
					from t_pinjaman b join t_debitur c on (b.cif=c.cif and b.idDebitur=c.idDebitur)
					join t_pinjaman_klaim d on (b.NomorRegistrasi=d.NomorRegistrasi)  
					where StatusRekonsel = '1' and StatusPembayaran = '1' and spv = '1' and keputusan = '0'  and convert(varchar(7), d.create_spv,120) = '$awal_bulan' ".$and."
					group by b.kodeAsuransi   
				)  

				SELECT pengajuan.kodeAsuransi  
				  , jmldeb  
				  , nominal  
				  , jmldebajukan  
				  , nominalajukan
				  , jmldebjalan
				  , nominaljalan 
				FROM sebelumnya  
				right JOIN pengajuan ON sebelumnya.kodeAsuransi= pengajuan.kodeAsuransi
				left JOIN berjalan ON pengajuan.kodeAsuransi= berjalan.kodeAsuransi
		");

		return $query;
	}

	public function getKlaim3(){
		$tipe = $this->input->post("tiperes");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tgl_akhir2"))){
			$dateadd = "";
		}else{
			$dateadd = date_format(date_create($this->input->post("tgl_akhir2")),'Y-m-d');
		}

		$and = "";
		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}

		$query = $this->db->query("
			WITH pengajuan (kodeAsuransi, jmldebajukan, nominalajukan)  
				AS  (  
					select b.kodeAsuransi, count(d.NomorRegistrasi) jmldebajukan, sum(d.NominalDiajukan) nominalajukan
					from t_pinjaman b join t_pinjaman_klaim d on (b.NomorRegistrasi=d.NomorRegistrasi)  
					where StatusRekonsel = '1' and StatusKlaim = '1' and spv = '1' and keputusan != -2 and convert(varchar(7), d.create_spv,120) <= '$dateadd' ".$and."
					group by b.kodeAsuransi   
				),
				berjalan (kodeAsuransi, jmldebjalan, nominaljalan)  
				AS  (  
					select b.kodeAsuransi, count(d.NomorRegistrasi) jmldebajukan, sum(d.NominalDisetujui) nominaljalan
					from t_pinjaman b join t_pinjaman_klaim d on (b.NomorRegistrasi=d.NomorRegistrasi)  
					where StatusRekonsel = '1' and StatusPembayaran = '1' and spv = '1' and keputusan = '1'  and convert(varchar(10), d.create_spv,120) <= '$dateadd' ".$and."
					group by b.kodeAsuransi   
				)  

				SELECT pengajuan.kodeAsuransi  
				  , jmldebajukan  
				  , nominalajukan
				  , jmldebjalan
				  , nominaljalan 
				FROM pengajuan
				LEFT JOIN berjalan ON pengajuan.kodeAsuransi= berjalan.kodeAsuransi
		");

		return $query;
	}


	public function getKlaim4($bulan){
		$tipe = $this->input->post("tipeklaim");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tahun"))){
			$tgl_mulai = date("Y")."-".$bulan;
			$tgl_akhir = date("Y")."-".$bulan;
		}else{
			// $tgl_mulai = date_format(date_create($this->input->post("tahun")),'Y-'.$bulan);
			$tgl_mulai = $this->input->post("tahun").'-'.$bulan;
		    $tgl_akhir = date_format(date_create($this->input->post("tahun")),'Y-'.$bulan);
		}

		$and = "";
		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}

		$query = $this->db->query("
			with
			pengajuan (bulan,tahun,jmldeb,plafon) as (
				select CONVERT(CHAR(2),create_spv,110) bulan,CONVERT(CHAR(7),create_spv,120) tahun,count(a.idDebitur) jmldeb, 
				sum(NominalDiajukan) plafon from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur join t_pinjaman_klaim d on (b.NomorRegistrasi=d.NomorRegistrasi)  
					where StatusRekonsel = '1' and StatusKlaim = '1' and keputusan != '-1' and spv = '1' and CONVERT(CHAR(7),create_spv,120) = '$tgl_mulai' ".$and."
				group by CONVERT(CHAR(2),create_spv,110),CONVERT(CHAR(7),create_spv,120)
			),
			terbit (bulanx,tahunx,jmldebx,plafonx) as (
				select CONVERT(CHAR(2),create_spv,110) bulan,CONVERT(CHAR(7),create_spv,120) tahun,count(a.idDebitur) jmldeb, 
				sum(NominalDisetujui) plafon from t_debitur a join t_pinjaman b on a.cif=b.cif and a.idDebitur=b.idDebitur
				join t_pinjaman_klaim d on (b.NomorRegistrasi=d.NomorRegistrasi)  
					where StatusRekonsel = '1' and StatusPembayaran = '1'  and spv = '1' and keputusan != '-1'  and CONVERT(CHAR(7),create_spv,120) = '$tgl_mulai' ".$and."
				group by CONVERT(CHAR(2),create_spv,110),CONVERT(CHAR(7),create_spv,120)
			)
			select bulan, tahun, jmldeb, plafon, tahunx, jmldebx, plafonx
			from pengajuan left join terbit on pengajuan.bulan=terbit.bulanx
		");

		return $query;
	}

	public function getKlaim5(){
		$tipe = $this->input->post("tipeklaim");
		$asuransi = $this->input->post("asuransi");
		$produk = $this->input->post("produk");
		$cabang = $this->input->post("cabang");
		$capem = $this->input->post("capem");
		if(empty($this->input->post("tgl_awal2")) && empty($this->input->post("tgl_akhir2"))){
			$tgl_mulai = "";
			$tgl_akhir = "";
		}else{
			$tgl_mulai = date_format(date_create($this->input->post("tgl_awal3")),'Y-m-d 00:00:00');
		    $tgl_akhir = date_format(date_create($this->input->post("tgl_akhir3")),'Y-m-d 23:59:59');
		}


		$and = "";
		if($this->session->userdata("roles")==4) {
	    	$user = $this->session->userdata("username");
	    	$kodeAngka = $this->db->query("select kodeAngka from t_asuransi where lower(kodeAsuransi) = '$user'")->row()->kodeAngka;
		    $and .= " and kodeAsuransi = '$kodeAngka'";
	    }
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="3"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$kodeBank') or kodeBank = '$kodeBank') ";
			}else{
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank='$kodeBank' ";
			}
		}
		if(!empty($asuransi)){
			$and = " and b.KodeAsuransi =".$asuransi;
		}if(!empty($produk)){
			$and .= " and b.idProduk=".$produk;
		}if(!empty($cabang) && empty($capem)){
			if($this->session->userdata("roles")=="2" && $this->session->userdata("hak_akses")=="5"){
				$and .= " and kodeBank='$cabang'";
			}else{
				$and .= " and (kodeBank in (select kodeBank from t_bank_capem where idCabang= '$cabang') or kodeBank = '$cabang') ";
			}
		}if(!empty($capem)){
			$and .= " and kodeBank=".$capem;
		}if(!empty($tgl_mulai) && !empty($tgl_akhir)){
			$and .= " and create_spv between '$tgl_mulai' and '$tgl_akhir'";
		}

		$query = $this->db->query("
			with
			pengajuan (kodeBank,jmldeb,plafon) as (
				select kodeBank, count(c.NomorRegistrasi) jmldeb, sum(NominalDiajukan) NominalDiajukan from t_pinjaman b left join t_pinjaman_klaim c on(b.NomorRegistrasi=c.NomorRegistrasi) where StatusRekonsel = '1' and spv = '1' ".$and." group by kodeBank
			),
			terbit (kodeBankx,jmldebx,plafonx) as (
				select kodeBank, count(c.NomorRegistrasi) jmldeb, sum(NominalDisetujui) NominalDisetujui from t_pinjaman b left join t_pinjaman_klaim c on(b.NomorRegistrasi=c.NomorRegistrasi) where StatusRekonsel = '1' and StatusPembayaran = '1'  and spv = '1' ".$and." group by kodeBank
			)
			select kodeBank, jmldeb, plafon, jmldebx, plafonx
			from pengajuan left join terbit on pengajuan.kodeBank=terbit.kodeBankx
		");

		return $query;
		
	}

	public function dataInvoice(){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank = '$kodeBank' ";
			}
		}

		$query = $this->db->query("select convert(varchar(10),create_date, 120) tgl, count(NomorPinjaman) deb, sum(JumlahPremiTenor) premi from t_pinjaman where kodeAsuransi != 99 and convert(varchar(10),create_date, 120) < convert(varchar(10),CURRENT_TIMESTAMP, 120) ".$and." group by convert(varchar(10),create_date, 120)");
		return $query;
	}

	public  function invoice($tgl){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank = '$kodeBank' ";
			}
		}

		$query = $this->db->query("select rank() OVER (ORDER BY NomorPinjaman, NamaDebitur, JumlahPremiTenor, kodeAsuransi, KodePekerjaan, TglAkadKredit, TenorTahun) as rank, NomorPinjaman, NamaDebitur, JumlahPremiTenor, kodeAsuransi, KodePekerjaan, TglAkadKredit, TenorTahun from t_pinjaman a join t_debitur b on(a.cif=b.cif and a.idDebitur=b.idDebitur) where kodeAsuransi != 99 and StatusRekonsel = 1 and convert(varchar(10), a.create_date, 120) = '$tgl' ".$and." order by rank asc");
		return $query->result();
	}

	public  function NomorInvoice($tgl){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank = '$kodeBank' ";
			}
		}

		$query = $this->db->query("select top 1 NomorInvoice from t_pinjaman a where kodeAsuransi != 99 and convert(varchar(10), a.create_date, 120) = '$tgl' ".$and);
		return $query->row()->NomorInvoice;
	}

	public  function OsPremi($nopin){
		$query = $this->db->query("select ospremi, max(TahunKe) TahunKe from t_pinjaman_bj where NomorPinjaman = '$nopin' and StatusBayar = 1 group by ospremi");
		return $query->row()->ospremi;
	}

	public  function invoiceTotal($tgl){
		$and = "";
		if (empty($this->session->userdata("kodeBank")) || $this->session->userdata("kodeBank")=="000") {
			$and .= "";
		}else{
			if($this->session->userdata("roles")=="2"){
				$kodeBank = $this->session->userdata("kodeBank");
				$and .= " and kodeBank = '$kodeBank' ";
			}
		}

		$query = $this->db->query("select kodeAsuransi, count(NomorPinjaman) deb, sum(JumlahPremiTenor) premi from t_pinjaman where kodeAsuransi != 99 and StatusRekonsel = 1 and convert(varchar(10), create_date, 120) = '$tgl' ".$and." group by kodeAsuransi");
		return $query->result();
	}

	public function lap_asuransi(){
		$tgl_mulai = date_format(date_create($this->input->post("tgl_mulai")),'Y-m-d');
		$tgl_akhir = date_format(date_create($this->input->post("tgl_akhir")),'Y-m-d');
		$query = $this->db->query("select asuransi, sum(plafon) plafon, sum(JumlahPremiTenor) JumlahPremiTenor, sum(TotalOsPremi) TotalOsPremi, sum(FeePremiBank) FeePremiBank, sum(FeePremiBroker) FeePremiBroker, sum(JumlahPremiTenor-FeePremiBank-FeePremiBroker) NetPremi from t_asuransi a join t_pinjaman b on a.kodeAngka=b.kodeAsuransi where convert(varchar(10), create_date, 120) between '$tgl_mulai' and '$tgl_akhir' group by asuransi");
		return $query;
	}

}