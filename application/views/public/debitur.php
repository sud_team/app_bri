<div class="row">
  <div class="col-12">
      <div class="card">
          <div class="card-body">
          <h4 class="card-title">Cari Data Debitur</h4>
              <h6 class="card-subtitle">Untuk menampilkan detail data Peserta termasuk proses pengajuan Klaim dan Restitusi</h6>
<!-- Menu Pencairan -->
            <form id="FormSubmit" method="post">
              <div class="col-md-7">
                  <div class="row m-b-10">
                <?php if (cekOpBank($this->session->userdata("kodeBank"))!="CAPEM"): ?>
                  <?php if (cekOpBank($this->session->userdata("kodeBank"))=="CABANG"): ?>
                  <div class="text-left text-megna col-sm-5">
                        <small class="form-control-feedback f-s-10">Cabang  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                        <input type="hidden" placeholder="Nama" id="cabang" name="cabang" class=" custom-select f-s-12 " value="<?php echo $this->session->userdata('kodeBank');?>" readonly>
                        <input type="text" placeholder="Nama" id="cabang1" name="cabang1" class=" custom-select f-s-12 " value="<?php echo cekNamaBank($this->session->userdata('kodeBank'));?>" readonly>
                    </div>
                    <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Kantor Operasional</small>
                            <select class="js-example-basic-cabang  custom-select f-s-12" id="capem" name="capem">
                                <option value="">Semua</option>
                            </select>
                        </div>
                  <?php else: ?>
                  <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Cabang  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                            <select class="js-example-basic-cabang  custom-select f-s-12" id="cabang" name="cabang">
                                <option value="">Semua</option>
                                <?php foreach ($cabang as $key): ?>
                                <option value="<?php echo $key->kodeBank?>"><?php echo $key->kodeBank.'-'.$key->cabang;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Kantor Operasional</small>
                            <select class="js-example-basic-cabang  custom-select f-s-12" id="capem" name="capem">
                                <option value="">Semua</option>
                            </select>
                        </div>
                  <?php endif ?>
                <?php endif ?>
    
                    </div>

                    <div class="row m-b-10">
                    <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Produk  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                            <select class="js-example-basic-cabang  custom-select f-s-12" name="produk" id="produk">
                                    <option value="">Semua</option>
                                    <?php foreach ($produk as $pd): ?>
                                    <option value="<?php echo $pd->idProduk?>"><?php echo $pd->NamaProduk?></option>
                                    <?php endforeach ?>
                                      
                                </select>
                        </div>
                        <div class="text-left text-megna col-sm-5">
                        </div>
                    </div>
                    <div class="row m-b-20">
                    <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Nama</small>

                            <input type="text" placeholder="Nama" name="nama" id="nama" class=" custom-select f-s-12 ">
                        </div>
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">No. Pinjaman</small>
                            <input type="text" placeholder="No. Pinjaman" name="nopin" id="nopin" class=" custom-select f-s-12 ">
                        </div>
                    </div>
                    <div class="row m-b-20">
                    <div class="text-left text-megna col-sm-4">
                            <small class="form-control-feedback f-s-10 text-megna">Periode upload dari</small>
                            <input type="text" class="custom-select f-s-12 datepicker" placeholder="dd-mm-yyyy" name="tgl_mulai" id="tgl_mulai">
                        </div>
                        <div class="text-left text-megna col-sm-4">
                            <small class="form-control-feedback f-s-10 text-megna">Sampai dengan</small>
                            <input type="text" class="custom-select f-s-12 datepicker" placeholder="dd-mm-yyyy" name="tgl_akhir" id="tgl_akhir">
                        </div>
                        <div class="col-sm-2 m-t-20 text-right">
                            <a id="cek" class="btn btn-info f-s-12 text-light">
                                <i class="fa fa-search"></i> Cari
                            </a>
                        </div>
                    </div>
              </div>
            </form>
<!-- Menu Pencairan Akhir -->

              <div class="table-responsive">
                  <table id="debitur" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                      <thead >
                          <tr >
                              <th>#</th>
                              <th>No. Pinjaman</th>
                              <th>Nama</th>
                              <th>Tgl Lahir</th>
                              <th>Usia</th>
                              <th>Pekerjaan</th>
                              <th>Tgl Kredit</th>
                              <th>Tenor</th>
                              <th>Plafon</th>
                              <th>Produk</th>
                              <th>Klaim/Restitusi</th>
                              <th>Status</th>
                              <th>Bank</th>
                              <th>Asuradur</th>
                              <th>Tracking</th>
                          </tr>
                      </thead>
                      <tbody>
                        
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>

<div class="modal fade" id="tracking" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h6 class="modal-title font-weight-bold" id="exampleModalLabel">Tracking Debitur</h6>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body f-s-12 p-l-30 p-r-30">
              <div class="row text-left">
                  <lable class="col-sm-6 control-label text-megna font-weight-bold">Data Debitur</lable>
              </div>
              <div class="col-xs-12" id="dok">
                          
              </div>
          </div>
          <div class="modal-footer ">
              <button type="button" class="btn btn-secondary btn-sm " data-dismiss="modal">Close</button>
          </div>
      </div>
  </div>
</div>
