<div class="row">
  <div class="col-12">
      <div class="card">
          <div class="card-body">
            <h4 class="card-title">Rekonsel Bank Jatim</h4>
            <h6 class="card-subtitle">Pencocokan data peserta dengan Premi yang diterima, khusus Debitur Pegawai Bank Jatim</h6>
            <!-- Panel Utama -->
            <form id="FormSubmit" method="post">
            <div class="row">
                <!-- Menu Pencairan -->
                <div class="col-md-7">
                  <input type="hidden" id="link" value="filter_rekonsel_bj">
                    <div class="row m-b-10">
                    <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Cabang  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                            <select class="js-example-basic-cabang  custom-select f-s-12" id="cabang" name="cabang">
                                <option value="">Semua</option>
                                <?php foreach ($cabang as $key): ?>
                                <option value="<?php echo $key->kodeBank?>"><?php echo $key->kodeBank.'-'.$key->cabang;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Kantor Operasional</small>
                            <select class="js-example-basic-cabang  custom-select f-s-12" id="capem" name="capem">
                                <option value="">Semua</option>
                            </select>
                        </div>
                    </div>
                    <div class="row m-b-10">
                    <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Produk  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                            <select class="js-example-basic-cabang  custom-select f-s-12" name="produk" id="produk">
                                    <option value="">Semua</option>
                                    <?php foreach ($produk as $pd): ?>
                                    <option value="<?php echo $pd->idProduk?>"><?php echo $pd->NamaProduk?></option>
                                    <?php endforeach ?>
                                      
                                </select>
                        </div>
                        <div class="text-left text-megna col-sm-5">
                        </div>
                    </div>
                    <div class="row m-b-20">
                    <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Nama</small>

                            <input type="text" placeholder="Nama" name="nama" id="nama" class=" custom-select f-s-12 ">
                        </div>
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">No. Pinjaman</small>
                            <input type="text" placeholder="No. Pinjaman" name="nopin" id="nopin" class=" custom-select f-s-12 ">
                        </div>
                    </div>
                    <div class="row m-b-20">
                    <div class="text-left text-megna col-sm-4">
                            <small class="form-control-feedback f-s-10 text-megna">Periode upload dari</small>
                            <input type="text" class="custom-select f-s-12 datepicker" placeholder="dd-mm-yyyy" name="tgl_mulai" id="tgl_mulai" required>
                        </div>
                        <div class="text-left text-megna col-sm-4">
                            <small class="form-control-feedback f-s-10 text-megna">Sampai dengan</small>
                            <input type="text" class="custom-select f-s-12 datepicker" placeholder="dd-mm-yyyy" name="tgl_akhir" id="tgl_akhir" required>
                        </div>
                        <div class="col-sm-2 m-t-20 text-right">
                            <a id="cari" class="btn btn-info f-s-12 text-light">
                            <i class="fa fa-search"></i> Tampilkan
                            </a>
                        </div>
                    </div>
                </div>
              </form>
                <!-- Menu Pencairan Akhir -->
                <div class="col-md-4 bg-light-warning radius p-10 m-b-30 animated animated bounceIn">
                    <div class="box-title text-center">
                        <h6><b>Perhatian!</b></h6>
                    </div>
                    <hr>
                    <div class=" m-l-10 m-b-0 text-red f-s-12">  
                    Selalu lakukan pengecekan tanggal <b>Jatuh Tempo</b> pada Debitur Pegawai Bank Jatim dibawah<br> <hr>
                    Reminder tagihan Premi kepada Cabang/Capem atas data Debitur 1 bulan sebelum tanggal Jatuh Tempo<br> 
                    </div>
                    <hr>
                    <!-- <div class=" m-l-10 m-b-20 text-muted f-s-10">
                            Pilih peserta yang akan direkonsel pada kolom sebelah kiri tabel debitur dibawah</div> -->
                    <div class="pull-right text-light">
                    <a class="btn btn-sm btn-danger text-light" id="validasi">Konfirmasi<i class="fa fa-check-square-o"></i></a>
                    </div>
                    <div class="row m-l-10 text-dark">
                        <div class="col-sm-12">Tanggal Rekonsel</div>
                        <div class="col-sm-12">
                            <b><?php echo my_date_indo(date("Y-m-d"))?></b>
                        </div>
                    </div>
                    <?php /*<div class="row m-l-10 text-dark">
                        <div class="w-40">Jumlah Calon Debitur</div>
                        <div class="w-30">
                            <b><span id="jmldebitur"></span></b></div>
                    </div> */?>
                    <!-- <div class="row m-l-10 m-b-20 text-dark">
                        <div class="w-40">Total Premi</div>
                        <div class="w-30">
                            <b>Rp.21.200.000</b>
                        </div>
                    </div> -->
                    <div class="row m-l-10 m-b-20 w-90"></div>
                </div>
            </div>
            </form>
              <div class="table-responsive">
                  <table id="DataPeserta" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                      <thead >
                          <tr >
                              <th><input type="checkbox" id="chkSelectAll"></th>
                              <th>No.Pinjaman</th>
                              <th>Nama</th>
                              <th>Tgl Kredit</th>
                              <th>Jatuh Tempo</th>
                              <th>Tenor</th>
                              <th>Plafon</th>
                              <th>Sisa Pinjaman</th>
                              <th>Tahun ke</th>
                              <th>Premi</th>
                              <th>Bank</th>
                              <th>Asuradur</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>