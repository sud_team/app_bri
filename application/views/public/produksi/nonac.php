<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Debitur Non-Automatic Cover</h4>
                <h6 class="card-subtitle">Untuk menampilkan dan melengkapi dokumen Debitur yang termasuk kedalam Non-Automatic Cover</h6>
                <form id="FormSubmit" method="post">
                <div class="col-8">

                <div class="row m-b-10">
                  <input type="hidden" id="link" value="filter_nonac">

                                                <div class="text-left text-megna col-md-5">
                            <small class="form-control-feedback f-s-10">Cabang</small>

                            <select class="js-example-basic-cabang  custom-select f-s-12" id="cabang" name="cabang">
                                <option value="">Semua</option>
                                <?php foreach ($cabang as $key): ?>
                                <option value="<?php echo $key->kodeBank?>"><?php echo $key->kodeBank.'-'.$key->cabang;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="text-left text-megna col-md-5">
                            <small class="form-control-feedback f-s-10">Kantor Operasional</small>
                            <select class="js-example-basic-cabang  custom-select f-s-12" id="capem" name="capem">
                                <option value="">Semua</option>
                            </select>
                        </div>
                    </div>
                    <div class="row m-b-10">
                    <div class="text-left text-megna col-md-5">
                            <small class="form-control-feedback f-s-10">Produk</small>
                            <select class=" custom-select f-s-12" name="produk" id="produk">
                                <option value="">Semua</option>
                                <?php foreach ($produk as $pd): ?>
                                <option value="<?php echo $pd->idProduk?>"><?php echo $pd->NamaProduk?></option>
                                <?php endforeach ?>
                                  
                            </select>
                        </div>
                        <div class="text-left text-megna col-md-5 m-l-10">
                        </div>
                    </div>
                    <div class="row m-b-20">
                    <div class="text-left text-megna col-md-5">
                            <small class="form-control-feedback f-s-10">Nama</small>

                            <input type="text" placeholder="Nama" name="nama" id="nama" class=" custom-select f-s-12 ">
                        </div>
                        <div class="text-left text-megna col-md-5">
                            <small class="form-control-feedback f-s-10">No. Pinjaman</small>
                            <input type="text" placeholder="No. Pinjaman" name="nopin" id="nopin" class=" custom-select f-s-12 ">
                        </div>
                    </div>

                    <div class="row m-b-20">
                    <div class="text-left text-megna col-md-4">
                            <small class="form-control-feedback f-s-10 text-megna">Periode upload </small>
                            <input type="text" class=" custom-select f-s-12 datepicker" placeholder="dd/mm/yyyy" name="tgl_mulai" id="tgl_mulai" required>
                        </div>
                        <div class="text-left text-megna col-md-4">
                            <small class="form-control-feedback f-s-10 text-megna">Sampai dengan</small>
                            <input type="text" class="custom-select f-s-12 datepicker" placeholder="dd/mm/yyyy" name="tgl_akhir" id="tgl_akhir" required>
                        </div>
                        <div class="col-md-2 m-t-20 text-right">
                            <a id="cari" class="btn btn-info f-s-12" style="color: #ffffff;">
                                <i class="fa fa-search"></i> Cari
                            </a>
                        </div>
                    </div>
</div>
                  </form>
                <div class="table-responsive">

                    <table id="DataPeserta" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                        <thead >
                            <tr >
                                <th>No.</th>
                                <th>No.Pinjaman</th>
                                <th>No.CIF</th>
                                <th>Nama</th>
                                <th>Tgl Kredit</th>
                                <th>Tgl Akhir</th>
                                <th>Tenor</th>
                                <th>Plafon</th>
                                <th>View</th>
                            </tr>
                        </thead>
                        <tbody id="bodynonac">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form action="<?php echo base_url()?>produksi/uploadDok" method="post" enctype="multipart/form-data">
          <div class="modal-header">
              <h6 class="modal-title font-weight-bold" id="exampleModalLabel">Detail Debitur Non-Automatic Cover</h6>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body f-s-12 ">
            
              <div class="col-sm-12" id="dok">


              
              </div>
          </div>
          <div class="modal-footer ">
              <div class="col-sm control-label text-left f-s-10">Kekurangan dokumen yang harus dilengkapi karena tidak
                  sesuai dengan term & condition
              </div>

              <button type="button" class="btn btn-secondary btn-sm " data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-info btn-sm " value="Kirim" style="color: #ffffff">
          </div>
        </form>
      </div>
  </div>
</div>



