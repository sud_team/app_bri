<div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Upload Produksi</h4>
                    <h6 class="card-subtitle">Unggah file GET-01 penutupan Premi dari <code>Bank</code></h6>
                   <!--  <h6 class="card-subtitle">Template pendaftaran debitur dalam format Excel <button class="badge badge-info">Download</button>
                    <div>&nbsp;</div> -->
                    <!-- <form action="#" class="dropzone"> -->
                        <!-- <div class="fallback"> -->
                            <!-- <input name="file" type="file" multiple /> -->
                        <!-- </div> -->
                    <!-- </form> -->
                    <form action="<?php echo base_url()?>produksi/upload" method="post" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <input type="file" required class="btn btn-xs" name="upload_file">
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" class="btn btn-xs btn-info" name="Upload" value="Upload">
                            </div>
                            <div class="col-sm-6 text-muted text-right">Download template data input
                                <a type="" class="btn btn-xs btn-info" ><i class="fa fa-download text-light"></i></a>

                            </div>
                        </div>
                    </form>
                    <div class="col-sm-4 alert alert-danger text-center animated zoomInLeft">
                                        Jangan lupa <b>simpan</b> file <b>Export</b> setelah upload!
                                    </div>
                    <div class="table-responsive">
                        <?php if(!empty($debitur)){ ?>
                            <?php echo $gagal;?>
                        <?php } ?>
                    </div>
                    <br>
                    <br>
                    <div class="table-responsive">
                        <?php if(!empty($debitur)){ ?>
                            <?php echo $detail;?>
                        <?php } ?>
                    </div>
                    <br>
                    <br>
                    <div class="table-responsive">
                        <?php // if(!empty($debitur)){ ?>
                            <!-- <a class="btn btn-xs btn-success" id="import" style="color:#ffffff;">Import</a>
                            <br> -->
                        <?php // } ?>
                        <!-- <br> -->
                        <table id="upload" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                            <thead >
                                <tr >
                                    <th class="w-3">#</th>
                                    <th>No. Registrasi</th>
                                    <th class="w-6">CIF</th>
                                    <th class="w-6">No. Pinjaman</th>
                                    <th>Kode Bank</th>
                                    <th>Nama</th>
                                    <th>Tipe Loan</th>
                                    <th>Jenis Kelamin</th>
                                    <th class="w-6">KTP</th>
                                    <th>Tempat Lahir</th>
                                    <th>Tgl. Lahir</th>
                                    <th>Alamat</th>
                                    <th>Pekerjaan</th>
                                    <th class="w-6">No. PK</th>
                                    <th>Tgl. Akad</th>
                                    <th>Tgl. Akhir</th>
                                    <th>Plafon</th>
                                    <th>Premi</th>
                                    <th class="w-6">No. Referensi <br>Premi</th>
                                    <th>New/Topup<br></th>
                                    <th>Kode Asuransi<br></th>
                                    <th>Status<br></th>
                                </tr>
                            </thead>
                            <?php echo $debitur;?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <!-- End PAge Content -->
</div>