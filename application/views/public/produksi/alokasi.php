<div class="row">
  <div class="col-12">
      <div class="card">
          <div class="card-body">
              <h4 class="card-title">Alokasi Asuradur</h4>
              <h6 class="card-subtitle">Menentukan Asuradur pada setiap data peserta</h6>

<!-- Menu Pencairan -->
            <form id="FormSubmit" method="post">
              <input type="hidden" id="link" value="filter_alokasi">
              <div class="col-md-7">
                <div class="row m-b-10">
                <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Cabang &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                            <select class="js-example-basic-cabang  custom-select f-s-12" id="cabang" name="cabang">
                                <option value="">Semua</option>
                                <?php foreach ($cabang as $key): ?>
                                <option value="<?php echo $key->kodeBank?>"><?php echo $key->kodeBank.'-'.$key->cabang;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Kantor Operasional</small>
                            <select class="js-example-basic-cabang  custom-select f-s-12" id="capem" name="capem">
                                <option value="">Semua</option>
                            </select>
                        </div>
                    </div>
                    <div class="row m-b-10">
                            <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Produk  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                            <select class="js-example-basic-cabang  custom-select f-s-12" name="produk" id="produk">
                                    <option value="">Semua</option>
                                    <?php foreach ($produk as $pd): ?>
                                    <option value="<?php echo $pd->idProduk?>"><?php echo $pd->NamaProduk?></option>
                                    <?php endforeach ?>
                            </select>
                        </div>
                        <div class="text-left text-megna col-sm-5 m-l-10">
                        </div>
                    </div>
                    <div class="row m-b-20">
                    <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Nama</small>

                            <input type="text" placeholder="Nama" name="nama" id="nama" class=" custom-select f-s-12 ">
                        </div>
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">No. Pinjaman</small>
                            <input type="text" placeholder="No. Pinjaman" name="nopin" id="nopin" class=" custom-select f-s-12 ">
                        </div>
                    </div>
                    <div class="row m-b-20">
                    <div class="text-left text-megna col-sm-4">
                            <small class="form-control-feedback f-s-10 text-megna">Periode upload dari</small>
                            <input type="text" class="custom-select f-s-12 datepicker" placeholder="dd-mm-yyyy" name="tgl_mulai" id="tgl_mulai" required>
                        </div>
                        <div class="text-left text-megna col-sm-4">
                            <small class="form-control-feedback f-s-10 text-megna">Sampai dengan</small>
                            <input type="text" class="custom-select f-s-12 datepicker" placeholder="dd-mm-yyyy" name="tgl_akhir" id="tgl_akhir" required>
                        </div>
                        <div class="col-sm-2 m-t-20 text-right">
                            <a id="cari" class="btn btn-info f-s-12 text-light">
                            <i class="fa fa-search"></i> Tampilkan
                            </a>
                        </div>
                    </div>
              </div>
            </form>
<!-- Menu Pencairan Akhir -->

              <div class="table-responsive">
                  <table id="DataPeserta" class="display nowrap table-hover table-bordered f-s-12 h-100" cellspacing="0" width="100%">
                      <thead >
                          <tr >
                              <th class="w-6">No.Pinjaman</th>
                              <th class="w-18">Nama</th>
                              <th>Tgl Kredit</th>
                              <th >Tgl Akhir</th>
                              <th>Tenor</th>
                              <th>Plafon</th>
                              <th>Bank</th>
                              <th>Asuradur</th>
                              <th> <i class="fa fa-paper-plane
" aria-hidden="true"></i>
</i></th>
                          </tr>
                      </thead>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>