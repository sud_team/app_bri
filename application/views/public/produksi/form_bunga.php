<div class="row">
    <div class="col-lg-12 m-t-0">

        <div class="card ">
            <!-- Data Debitur -->
            <div class="card-outline-green p-t-30 p-b-20">
                <div class="card-header">
                    <h5 class="m-b-0 text-white">Data Debitur Pegawai Bank Jatim
                        <span class="m-l-40 m-t-10 m-b-10 text-right">No. Pinjaman : <?php echo $debitur->NomorPinjaman?></span>
                    </h5>
                </div>
            </div>
            <div class="card-body">
                <form id="FormSubmit" method="post">
                <div class="form-body">
                    <div class="table-responsive data-toggle">
                        <table id="my-lg-0 " class="display narrow table-hover table-sm f-s-14" cellspacing="0" width="100%">
                            <tbody>
                                <tr>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-12 text-right">Nama Debitur :</td>
                                    <td class="text-dark w-25"><?php echo $debitur->NamaDebitur;?></td>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-12 text-right">Kantor Operasional :</td>
                                    <td class="text-dark w-25"><?php echo cekNamaBank($debitur->kodeBank);?></td>
                                    <td class="w-3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-12 text-right">Tgl. Kredit :</td>
                                    <td class="text-dark w-25"><?php echo my_date_indo($debitur->TglAkadKredit);?></td>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-12 text-right">Tgl. Akhir Kredit :</td>
                                    <td class="text-dark w-25"><?php echo my_date_indo($debitur->TglAkhirKredit);?></td>
                                    <td class="w-3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-12 text-right">Plafon :</td>
                                    <td class="text-dark w-25"><?php echo price($debitur->plafon);?></td>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-12 text-right">Status Pinjaman :</td>
                                    <td class="text-dark w-25"><?php if($debitur->StatusPinjaman==0){ echo "NEW";}else{ echo "TOP UP";};?></td>
                                    <td class="w-3"></td>
                                </tr>
                                <tr>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-12  text-right align-top">Tenor :</td>
                                    <td class="text-dark align-top w-25"><?php echo $debitur->TenorBulan." Bulan";?></td>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-12 text-right align-top">Interest :</td>
                                    <td class="text-dark w-25">
                                        <?php if ($debitur->StatusBunga==0): ?>
                                        <input type="text" name="bunga" id="bunga" class=" custom-select custom-select-sm f-s-12" placeholder="%" ><small class="form-control-feedback f-s-10 text-muted">Gunakan titik (.) untuk penulisan desimal</small>
                                        <?php else: ?>
                                        <?php echo $debitur->Bunga;?>
                                        <?php endif ?>

                                    </td>
                                    <td class="w-3">&nbsp;</td>
                                </tr>
                                <?php if ($debitur->StatusBunga==0): ?>
                                <tr>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-12 text-right">&nbsp;</td>
                                    <td class="text-dark w-25">&nbsp;</td>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-12 text-right"></td>
                                    <td class="w-25">
                                        <a id="hitung" class="btn btn-sm btn-info text-light">Hitung</a> <a id="simpan" class="btn btn-sm btn-warning text-light"><i class="fa fa-save"></i>&nbsp Simpan</a>
                                    </td>
                                    <td class="w-3">&nbsp;</td>
                                </tr>
                                <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <br>
                <div class="table-responsive">
                    <input type="hidden" name="NomorRegistrasi" id="NomorRegistrasi" value="<?php echo $debitur->NomorRegistrasi;?>">
                    <table id="PerhitunganBunga" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                        <thead >
                            <tr >
                                <th>Bulan Ke</th>
                                <th>Sisa Pinjaman</th>
                                <th>Angsuran Bunga</th>
                                <th>Angsuran Pokok</th>
                                <th>Total Angsuran</th>
                                <th>OS Premi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                </form>
            </div>
            <!-- Data Debitur -->
            <!-- Form Klaim (bikin errro=align-self-center) -->
            <?php?>
        </div>
    </div>

</div>