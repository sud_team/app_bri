<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title text-bri font-weight-bold">Upload SPPA</h4>
                <h6 class="card-subtitle">Unggah Surat Permohonan Penutupan Asuransi dalam format Excel <code>(.xls) </code></h6>
                <hr>
                <div class="row">
                    <div class="col-md-3 col-lg-2">
                        <button type="button" class="btn btn-outline-bri btn-block f-s-12" data-toggle="modal" data-target="#exampleModalCenter">
                        <i class="fa fa-upload" aria-hidden="true"></i>  Upload SPPA
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModalCenter" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="card-title text-bri font-weight-bold">Upload SPPA</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <label class="control-label">Pilih SPPA Produk yang akan di upload</label>
                                                <select class="js-example-basic-cabang f-s-12" name="produk" id="produk">
                                                    <option value=""> AJK Asuransi Jiwa Kredit </option>
                                                    <option value=""> FEQ Asuransi Fire/Earthquake </option>
                                                    <option value=""> MVHE Asuransi Motor Vehicle/Heavy Equipment </option>
                                                    <option value=""> CIT Asuransi Cash In Transit </option>
                                                    <option value=""> BON Asuransi Bond</option>
                                                    <option value=""> CAR Construction All Risk</option>
                                                    <option value=""> MC Marine Cargo</option>
                                                    <option value=""> CRG Cargo</option>

                                                </select>
                                            </div>

                                        </div>

                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <label class="control-label">Upload file SPPA (.xls)</label>
                                                <input type="file" name="filenameBuktiTransfer" id="upload-cit" class="inputfilecustom" data-multiple-caption="{count} files selected" multiple />
                                                <label for="upload-cit" class="form-control"><i class="fa fa-upload" aria-hidden="true"></i> <span>Pilih file Excel xls.&hellip;</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary f-s-12 pull-left" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-bri f-s-12">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 p-t-30">
                        <h5  >SPPA Upload History</h4>
                            <div class="table-responsive">

                                <table id="upload" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th class="w-3">No</th>
                                            <th>Tanggal</th>
                                            <th>Bank/Cabang/Pembantu</th>
                                            <th>Kode Bank</th>

                                            <th>Jenis SPPA</th>
                                            <th>Kode File</th>

                                            <th class="w-10">Download </th>
                                            <th class="w-10">Hapus </th>
                                            <!-- button hapus di hidden untuk login Bank -->

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <td>1</td>
                                        <td>20 Feb 2019</td>
                                        <td>BRI Sidoarjo</td>
                                        <td>00022</td>
                                        <td>Asuransi Motor Vehicle</td>
                                        <td>Kode File</td>
                                        <td class="text-center w-10"><a type="button" class="btn btn-sm btn-outline-success"> <i class="fa fa-download"></i></a></td>
                                        <td class="text-center w-10"><button type="button" class="btn btn-sm btn-outline-red"> <i class="fa fa-times"></i></button></td>
                                        <!-- button hapus di hidden untuk login Bank -->
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- End PAge Content -->
</div>