<div class="row">
    <div class="col-lg-12 m-t-0">

        <div class="card ">
            <div class="card-body text-center"> Tracking Restitusi </div>

            <div class="row bs-wizard  " style="border-bottom:0;">
                <div class=" bs-wizard-step complete w-20">
                    <div class="text-center bs-wizard-stepnum">Diajukan</div>
                    <?php if ($pinjaman->diajukan==1): ?>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center col-m-2"><?php echo my_date_indo($pinjaman->create_date);?></div>
                    <?php endif ?>
                </div>

                <div class=" bs-wizard-step complete  w-20">
                    <!-- complete -->
                    <div class="text-center bs-wizard-stepnum">Persetujuan SPV</div>
                    <?php if ($pinjaman->spv==1): ?>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center col-xs-2"><?php echo my_date_indo($pinjaman->create_spv);?> </div>
                    <?php endif ?>
                </div>

                <div class=" bs-wizard-step complete w-20">
                    <!-- complete -->
                    <div class="text-center bs-wizard-stepnum">Persetujuan Pialang</div>
                    <?php if ($pinjaman->pialang==1): ?>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center col-xs-2"><?php echo my_date_indo($pinjaman->create_pialang);?> </div>
                    <?php endif ?>
                </div>

                <div class=" bs-wizard-step complete w-20">
                    <!-- active -->
                    <div class="text-center bs-wizard-stepnum">Penilaian Asuransi</div>
                    <?php if ($pinjaman->asuransi==1): ?>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center "><?php echo my_date_indo($pinjaman->create_asuransi);?> </div>
                    <?php endif ?>
                </div>

                <div class=" bs-wizard-step complete  w-20">
                    <!-- active -->
                    <div class="text-center bs-wizard-stepnum">Penyelesaian</div>
                    <?php if ($pinjaman->selesai==1): ?>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"><?php echo my_date_indo($pinjaman->create_selesai);?> </div>
                    <?php endif ?>
                </div>
            </div>
            <!-- Data Debitur -->
            <div class="card-outline-fade2 p-t-30 p-b-20">
                <div class="card-header">
                    <h5 class="m-b-0 text-white">Data Debitur
                        <span class="m-l-40 m-t-10 m-b-10 text-right">No. Pinjaman : <?php echo $NomorPinjaman?></span>
                    </h5>
                </div>
            </div>
            <div class="card-body">
                    <div class="form-body">
                        <div class="table-responsive data-toggle">
                            <table id="my-lg-0 " class="display narrow table-hover table-sm f-s-14" cellspacing="0" width="100%">
                                <tbody>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">CIF :</td>
                                        <td class="text-dark w-25"><?php echo $debitur->cif;?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Nama Debitur :</td>
                                        <td class="text-dark w-25"><?php echo $debitur->NamaDebitur;?></td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Tgl Kredit :</td>
                                        <td class="text-dark w-25"><?php echo my_date_indo($debitur->TglAkadKredit);?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Tgl. Akhir Kredit :</td>
                                        <td class="text-dark w-25"><?php echo my_date_indo($debitur->TglAkhirKredit);?></td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Tgl. Lahir :</td>
                                        <td class="text-dark w-25"><?php echo my_date_indo($debitur->TglLahir);?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Usia :</td>
                                        <td class="text-dark w-25"><?php echo str_replace('.',' Tahun ',$debitur->UsiaDebitur)." Bulan";?></td>
                                        <td class="w-3"></td>
                                    </tr>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Kategori :</td>
                                        <td class="text-dark w-25"><?php echo cekPekerjaan($debitur->KodePekerjaan);?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Status :</td>
                                        <td class="text-dark w-25"><?php echo $debitur->TcKet;?></td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Tenor :</td>
                                        <td class="text-dark w-25"><?php echo $debitur->TenorTahun." Tahun (".$debitur->TenorBulan." Bulan)";?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Plafon :</td>
                                        <td class="text-dark w-25"><?php echo price($debitur->plafon);?></td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <?php if ($debitur->KodePekerjaan=="BJ"): ?>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Tahun Ke :</td>
                                        <td class="text-dark w-25"><?php echo restitusiBj($debitur->NomorRegistrasi)->TahunKe;?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Premi :</td>
                                        <td class="text-dark w-25"><?php echo price(restitusiBj($debitur->NomorRegistrasi)->ospremi);?></td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>    
                                    <?php else: ?>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Premi :</td>
                                        <td class="text-dark w-25"><?php echo price($debitur->premi);?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Total Premi :</td>
                                        <td class="text-dark w-25"><?php echo price($debitur->JumlahPremiTenor);?></td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <?php endif ?>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td class="w-12 text-right">Asuradur :</td>
                                        <td class="text-dark w-25"><?php echo cekNamaAsdur($debitur->kodeAsuransi)?></td>
                                        <td>&nbsp;</td>
                                        <td class="w-12 text-right">Polis :</td>
                                        <td class="text-dark w-25"><?php echo $debitur->NomorPolis?></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td class="w-12 text-right">Bank :</td>
                                        <td class="text-dark w-25"><?php echo cekNamaBank($debitur->kodeBank)?></td>
                                        <td>&nbsp;</td>
                                        <td class="w-12 text-right">Tgl. Polis :</td>
                                        <td class="text-dark w-25"><?php echo my_date_indo($debitur->TglPolis)?></td>
                                        <td>&nbsp;</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>

                
            </div>
            <!-- Data Debitur -->
            <!-- Form Restitusi -->
            <form class="form-horizontal  col-md-12" role="form" action="<?php echo base_url()?>restitusi/saveRestitusi" method="post" enctype="multipart/form-data">    
            <div class="card-outline-fade3 p-t-30 p-b-20">
                <div class="card-header">
                    <h5 class="m-b-0 text-white">Form Restitusi
                        <span class="m-l-40 m-t-10 m-b-10 text-right">No. Restitusi : RS<?php echo $debitur->NomorRegistrasi?></span>
                    </h5>
                </div>
            </div>
            <div class="card-body" role="form">
                <div class="form-body">
                    <div class="table-responsive">
                        <input type="hidden" name="KodeRestitusi" value="RS<?php echo $debitur->NomorRegistrasi?>">
                        <input type="hidden" name="NomorRegistrasi" id="noreg" value="<?php echo $debitur->NomorRegistrasi?>">
                        <input type="hidden" name="TenorBulan" id="TenorBulan" value="<?php echo $debitur->TenorBulan?>">
                        <input type="hidden" name="TglAkadKredit" id="TglAkadKredit" value="<?php echo date_format(date_create($debitur->TglAkadKredit),'d-m-Y')?>">
                        <input type="hidden" name="JumlahPremiTenor" id="JumlahPremiTenor" value="<?php echo $debitur->JumlahPremiTenor?>">
                        <table id="my-lg-0" class="display narrow table-sm f-s-14" cellspacing="0" width="100%">
                            <tbody>

                                <tr>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-20"> Tanggal Pelunasan </td>
                                    <td class="w-30 ">
                                        <div class="col-xs-4 has-warning">
                                            <?php if ($pinjaman->spv==1): ?>
                                            <?php  if(!empty($pinjaman->TglPelunasanRestitusi)){ echo date_format(date_create($pinjaman->TglPelunasanRestitusi),'d-m-Y'); }?>
                                            <?php else: ?>
                                            <input type="text" class="form-control bg-white f-s-12 datepicker" name="tglpelunasan" id="tglpelunasan" value="<?php  if(!empty($pinjaman->TglPelunasanRestitusi)){ echo date_format(date_create($pinjaman->TglPelunasanRestitusi),'d-m-Y'); }?>" readonly>                                                
                                            <?php endif ?>
                                        </div>
                                    </td>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-20"> Pengembalian Premi </td>
                                    <td class="w-30 ">
                                        <div class="col-xs-4 has-warning">
                                            <?php if ($pinjaman->spv==1): ?>
                                            <?php if(!empty($pinjaman->PengembalianPremi)){ echo $pinjaman->PengembalianPremi;}?>
                                            <?php else: ?>
                                            <span id="premi"><b class="price">Rp. <?php if(!empty($pinjaman->PengembalianPremi)){ echo price($pinjaman->PengembalianPremi);}?></b></span>
                                            <input type="hidden" name="premi" id="valPremi" value="<?php if(!empty($pinjaman->PengembalianPremi)){ echo $pinjaman->PengembalianPremi;}?>">
                                            <?php endif ?>
                                        </div>
                                    </td>
                                    <td class="w-3">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-20"> Periode pinjaman berjalan </td>
                                    <td class="w-30 ">
                                        <div class="col-xs-4 has-warning">
                                            <?php if ($pinjaman->spv==1): ?>
                                            <?php if(!empty($pinjaman->BulanBerjalan)){ echo $pinjaman->BulanBerjalan;}?>
                                            <?php else: ?>
                                            <span id="periode"><b><?php if(!empty($pinjaman->BulanBerjalan)){ echo $pinjaman->BulanBerjalan;}?></b></span>
                                            <input type="hidden" name="periode" id="valPeriode" value="<?php if(!empty($pinjaman->BulanBerjalan)){ echo $pinjaman->BulanBerjalan;}?>">
                                            <?php endif ?>
                                        </div>
                                    </td>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-20"> Sisa periode pinjaman </td>
                                    <td class="w-30">
                                        <div class="col-xs-4 has-warning">
                                            <?php if ($pinjaman->spv==1): ?>
                                                <?php if(!empty($pinjaman->SisaBulan)){ echo $pinjaman->SisaBulan;}?>
                                            <?php else: ?>
                                                
                                            <span id="sisa"><b><?php if(!empty($pinjaman->SisaBulan)){ echo $pinjaman->SisaBulan;}?></b></span>
                                            <input type="hidden" name="sisa" id="valSisa" value="<?php if(!empty($pinjaman->SisaBulan)){ echo $pinjaman->SisaBulan;}?>">
                                            <?php endif ?>
                                        </div>
                                    </td>
                                    <td class="w-3">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <!--/span-->
                    </div>
                </div>

            </div>
            <!-- Form Restitusi -->
            <!-- Dokumen Restitusi -->
            <div class="card-outline-fade3 p-t-30 p-b-20">
                <div class="card-header">
                    <h5 class="m-b-0 text-white">Dokumen Restitusi
                        <span class="m-l-10 m-t-10 m-b-10 text-right">No. Restitusi RS<?php echo $debitur->NomorRegistrasi?></span>
                    </h5>
                </div>
            </div>
            <style type="text/css">
               /*  
                input[type="file"] {
                    display: none;
                } */
            </style>
            <div class="card-body">
                <!-- Dokumen Umum -->
                <h5 class="box-title text-center "> Upload Dokumen </h5>
                <div class="table-responsive">
                    <table id="" class="display narrow table-sm table-striped f-s-14" cellspacing="0" width="100%">
                        <tbody>
                            <?php if (!empty($resdok->result())){ ?>
                                <?php $no=1; foreach ($resdok->result() as $value): ?>
                                <tr>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-50"><input class="form-control col-xs f-s-14" type="text" placeholder="keterangan/nama dokumen" name="namaDokumen<?php echo $no?>" value="<?php echo $value->dokumen;?>"></td>
                                    <td align="right">
                                        <div class="form-actions p-2">
                                            <?php if ($pinjaman->spv==1): ?>
                                            <label class="btn btn-outline-primary btn-sm">
                                                <a target="_blank" href="<?php echo base_url()?>upload/restitusi/<?php echo $value->NamaFile?>"> 
                                                <i class="fa fa-eye"></i> Lihat <?php echo $value->NamaFile?></a>
                                            </label>    
                                            <?php else: ?>
                                            <label  class="btn btn-outline-warning btn-sm">
                                                <input type="file" name="fileumum<?php echo $no?>" >
                                            </label>
                                            <label class="btn btn-outline-primary btn-sm">
                                                <a target="_blank" href="<?php echo base_url()?>upload/restitusi/<?php echo $value->NamaFile?>"> 
                                                <i class="fa fa-eye"></i> Lihat <?php echo $value->NamaFile?></a>
                                            </label>
                                                
                                            <?php endif ?>
                                           <?php if ($this->session->userdata("roles")==3 || $this->session->userdata("roles")==1): ?>
                                                <label  class="btn btn-outline-warning btn-sm">
                                                    <input type="file" name="fileumum<?php echo $no?>" >
                                                </label>
                                            <?php endif ?>
                                        </div>
                                    </td>
                                </tr>    
                                <?php $no++; endforeach ?>
                            <?php } else{  ?>                                
                            <tr>
                                <td class="w-3">&nbsp;</td>
                                <td class="w-50"><input class="form-control col-xs f-s-14" type="text" placeholder="keterangan/nama dokumen" name="namaDokumen1"></td>
                                <td align="right">
                                    <div class="form-actions p-2">
                                        <label  class="btn btn-outline-warning btn-sm">
                                            <input type="file" name="fileumum1" >
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="w-3">&nbsp;</td>
                                <td class="w-50"><input class="form-control col-xs f-s-14" type="text" placeholder="keterangan/nama dokumen" name="namaDokumen2"></td>
                                <td align="right">
                                    <div class="form-actions p-2">
                                        <label  class="btn btn-outline-warning btn-sm">
                                            <input type="file" name="fileumum2" >
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="w-3">&nbsp;</td>
                                <td class="w-50"><input class="form-control col-xs f-s-14" type="text" placeholder="keterangan/nama dokumen" name="namaDokumen3"></td>
                                <td align="right">
                                    <div class="form-actions p-2">
                                        <label  class="btn btn-outline-warning btn-sm">
                                            <input type="file" name="fileumum3" >
                                        </label>
                                    </div>
                                </td>
                            </tr> 
                            <?php } ?>
                            <?php if ($this->session->userdata("roles")==3 || $this->session->userdata("roles")==1): ?>
                                <?php if ($no<3): ?>
                                    <?php for ($a=$no; $a <= 3 ; $a++) { ?>
                                        <tr>
                                            <td class="w-3">&nbsp;</td>
                                            <td class="w-50"><input class="form-control col-xs f-s-14" type="text" placeholder="keterangan/nama dokumen" name="namaDokumen<?php echo $no?>"></td>
                                            <td align="right">
                                                <div class="form-actions p-2">
                                                    <label  class="btn btn-outline-warning btn-sm">
                                                        <input type="file" name="fileumum<?php echo $no?>" >
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php }?>
                                <?php endif ?>
                            <?php endif ?>
                            
                        </tbody>
                    </table>

                </div>
                <div class="col-lg-3">&nbsp;</div>
                <div class="form-actions p-2 pull-left">
                    <?php if ($this->session->userdata("roles")==3 || $this->session->userdata("roles")==1): ?>
                        <a href="<?php echo base_url()?>restitusi/formKeputusan/M<?php echo $debitur->NomorRegistrasi?>" class="btn btn-dark btn-sm"><i class="fa fa-undo"></i> Kembali</a>
                    <?php else: ?>
                        <a href="<?php echo base_url()?>restitusi/menunggu" class="btn btn-dark btn-sm"><i class="fa fa-undo"></i> Kembali</a>
                    <?php endif ?>
                </div>
                <div class="form-actions p-2 pull-right">
                    <?php if ($pinjaman->diajukan==1 && $pinjaman->spv==0 && $pinjaman->pialang==0 && $pinjaman->asuransi==0 && $pinjaman->selesai==0): ?>
                        <a href="<?php echo base_url()?>restitusi/batal/RS<?php echo $debitur->NomorRegistrasi?>" class="btn  btn-danger btn-sm"><i class="fa fa-times"></i> Batal</a>
                        <?php if ($this->session->userdata("roles")==1): ?>
                            <button type="submit" class="btn  btn-warning btn-sm">
                            <i class="fa fa-paper-plane "></i> Ajukan </button>    
                        <?php else: ?>
                            <button type="submit" class="btn  btn-warning btn-sm">
                            <i class="fa fa-paper-plane "></i> Ajukan </button>
                        <?php endif ?>
                    <?php endif ?>
                    <?php if ($this->session->userdata("roles")==3 || $this->session->userdata("roles")==1): ?>
                        <button type="submit" class="btn  btn-info btn-sm" onclick="return confirm('Apakah anda yakin?')">
                            <i class="fa fa-save "></i> Simpan </button>
                    <?php endif ?>
                </div>

                <?php /* if (($this->session->userdata("hak_akses")==4 && $this->session->userdata("roles")==2) || $this->session->userdata("roles")==1): ?>

                <div class="form-actions p-2 pull-right">
                    <?php if ($pinjaman->diajukan==1 && $pinjaman->spv==0 && $pinjaman->pialang==0 && $pinjaman->asuransi==0 && $pinjaman->selesai==0): ?>
                    <a href="<?php echo base_url()?>restitusi/approveSpv/RS<?php echo $pinjaman->NomorRegistrasi?>" class="btn  btn-info btn-sm">
                        <i class="fa fa-paper-plane "></i> Approve </a>
                    <?php endif ?>
                </div>
                <?php endif */ ?>
            </div>
            </form>
        </div>
    </div>

</div>