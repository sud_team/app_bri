<div class="row">
  <div class="col-12">
      <div class="card">
          <div class="card-body">
              <h4 class="card-title">Menunggu Keputusan Restitusi</h4>
              <h6 class="card-subtitle">Data pengajuan restitusi menunggu keputusan level Penyelia atau penilaian Pialang atau penilaian Asuradur</h6>

<!-- Menu Pencairan -->
              <form id="FormSubmit" method="post">
              <input type="hidden" id="link" value="filter_menunggu">
              <div class="col-md-7">
                <div class="row m-b-10">
                       <?php if (cekOpBank($this->session->userdata("kodeBank"))!="CAPEM"): ?>
                  <?php if (cekOpBank($this->session->userdata("kodeBank"))=="CABANG"): ?>
                  <div class="text-left text-megna col-sm-5">
                        <small class="form-control-feedback f-s-10">Cabang &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                        <input type="hidden" placeholder="Nama" id="cabang" name="cabang" class="col-xs-3 custom-select f-s-12 " value="<?php echo $this->session->userdata('kodeBank');?>" readonly>
                        <input type="text" placeholder="Nama" id="cabang1" name="cabang1" class="col-xs-3 custom-select f-s-12 " value="<?php echo cekNamaBank($this->session->userdata('kodeBank'));?>" readonly>
                    </div>
                    <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Kantor Operasional</small>
                            <select class="js-example-basic-cabang custom-select f-s-12" id="capem" name="capem">
                                <option value="">Semua</option>
                            </select>
                        </div>
                  <?php else: ?>
                  <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Cabang &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                            <select class="js-example-basic-cabang custom-select f-s-12" id="cabang" name="cabang">
                                <option value="">Semua</option>
                                <?php foreach ($cabang as $key): ?>
                                <option value="<?php echo $key->kodeBank?>"><?php echo $key->kodeBank.'-'.$key->cabang;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Kantor Operasional</small>
                            <select class="js-example-basic-cabang custom-select f-s-12" id="capem" name="capem">
                                <option value="">Semua</option>
                            </select>
                        </div>
                  <?php endif ?>
                <?php endif ?>
                    </div>
                    <div class="row m-b-10">
                    <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Produk &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                            <select class="js-example-basic-cabang custom-select f-s-12" name="produk" id="produk">
                                    <option value="">Semua</option>
                                    <?php foreach ($produk as $pd): ?>
                                    <option value="<?php echo $pd->idProduk?>"><?php echo $pd->NamaProduk?></option>
                                    <?php endforeach ?>
                                      
                                </select>
                        </div>
                        <div class="text-left text-megna col-sm-5">
                        </div>
                    </div>
                    <div class="row m-b-20">
                    <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Nama</small>

                            <input type="text" placeholder="Nama" name="nama" id="nama" class="col-xs-3 custom-select f-s-12 ">
                        </div>
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">No. Pinjaman</small>
                            <input type="text" placeholder="No. Pinjaman" name="nopin" id="nopin" class="col-xs-3 custom-select f-s-12 ">
                        </div>
                    </div>
                    <div class="row m-b-20">
                    <div class="text-left text-megna col-sm-4">
                            <small class="form-control-feedback f-s-10 text-megna">Periode upload dari</small>
                            <input type="text" class="custom-select f-s-12 datepicker" placeholder="dd-mm-yyyy" name="tgl_mulai" id="tgl_mulai">
                        </div>
                        <div class="text-left text-megna col-sm-4">
                            <small class="form-control-feedback f-s-10 text-megna">Sampai dengan</small>
                            <input type="text" class="custom-select f-s-12 datepicker" placeholder="dd-mm-yyyy" name="tgl_akhir" id="tgl_akhir">
                        </div>
                        <div class="col-sm-2 m-t-20 text-right">
                            <a id="cari" class="btn btn-info f-s-12 text-light">
                            <i class="fa fa-search"></i> Tampilkan
                            </a>
                        </div>
                    </div>
              </div>
            </form>
<!-- Menu Pencairan Akhir -->

              <div class="table-responsive">
                  <table id="debitur" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                      <thead >
                          <tr >
                              <th>#</th>
                              <th>Tgl Pengajuan</th>
                              <th>No.Pinjaman</th>
                              <th>Nama</th>
                              <th>Tgl Lahir</th>
                              <th>Usia</th>
                              <th>Pekerjaan</th>
                              <th>Tgl Kredit</th>
                              <th>Tenor</th>
                              <th>Plafon</th>
                              <th>Produk</th>
                              <th>Bank</th>
                              <th>Asuradur</th>
                              <th>Lama Proses</th>
                              <th><i class="fa fa-folder-open" aria-hidden="true"></i>
                          </tr>
                      </thead>
                      <tbody>
                        <?php /* $no=1; foreach ($debiturrestitusi as $key): ?>
                        <tr>
                          <td style="text-align: center;"><?php echo $key->cif;?></td>
                          <td><?php echo $key->NamaDebitur;?></td>
                          <td><?php echo my_date_indo($key->TglLahir);?></td>
                          <td style="text-align: center;"><?php echo str_replace(".", " Tahun ", $key->UsiaDebitur)." Bulan";?></td>
                          <td><?php echo cekPekerjaan($key->KodePekerjaan);?></td>
                          <td><?php echo my_date_indo($key->TglAkadKredit);?></td>
                          <td><?php echo my_date_indo($key->TglAkhirKredit);?></td>
                          <td style="text-align: center;"><?php echo $key->TenorTahun;?></td>
                          <td><?php echo $key->NomorPK;?></td>
                          <td><?php echo cekProduk($key->idProduk);?></td>
                          <td><?php echo price($key->plafon);?></td>
                          <td>
                              <a href="<?php echo base_url()?>restitusi/formKeputusan/M<?php echo $key->NomorRegistrasi?>" class="btn btn-xs btn-danger" style="color: #ffffff;">Proses 
                              </a>
                          </td>
                          <td><?php echo ($key->TarifPremi*1000)." %";?></td>
                          <td><?php echo price($key->premi);?></td>
                          <td><?php echo price($key->JumlahPremiTenor);?></td>
                          <td><?php echo $key->NomorReferensiPremi;?></td>
                          <td><?php echo cekNamaBank($key->kodeBank);?></td>
                          <td><?php echo cekNamaAsdur($key->kodeAsuransi);?></td>
                          
                        </tr>
                        <?php $no++; endforeach  */?>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>