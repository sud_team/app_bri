<div class="row">
  <div class="col-12">
      <div class="card">
          <div class="card-body">
            <div class="col-md-12">

             <form id="FormSubmit" method="post">
              <!-- <div class="col-7"> -->
                  <div class="row">
                    <?php if (cekOpBank($this->session->userdata("kodeBank"))!="CAPEM"): ?>
                      <?php if (cekOpBank($this->session->userdata("kodeBank"))=="CABANG"): ?>
                        <div class="col-sm-3">
                            <small class="form-control-feedback f-s-10">Cabang &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                            <input type="hidden" placeholder="Nama" id="cabang" name="cabang" class=" custom-select f-s-12 " value="<?php echo $this->session->userdata('kodeBank');?>" readonly>
                            <input type="text" placeholder="Nama" id="cabang1" name="cabang1" class=" custom-select f-s-12 " value="<?php echo cekNamaBank($this->session->userdata('kodeBank'));?>" readonly>
                        </div>
                        <div class="col-sm-3">
                            <small class="form-control-feedback f-s-10">Kantor Operasional</small>
                            <select class="js-example-basic-cabang  custom-select f-s-12" id="capem" name="capem">
                                <option value="">Semua</option>
                            </select>
                        </div>
                      <?php else: ?>
                        <div class="col-sm-3">
                          <small class="form-control-feedback f-s-10">Cabang &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                          <select class="js-example-basic-cabang  custom-select f-s-12" id="cabang" name="cabang">
                            <option value="">Semua</option>
                            <?php foreach ($cabang as $key): ?>
                            <option value="<?php echo $key->kodeBank?>"><?php echo $key->kodeBank.'-'.$key->cabang;?></option>
                            <?php endforeach ?>
                          </select>
                        </div>
                        <div class="col-sm-3">
                          <small class="form-control-feedback f-s-10">Kantor Operasional</small>
                          <select class="js-example-basic-cabang  custom-select f-s-12" id="capem" name="capem">
                            <option value="">Semua</option>
                          </select>
                        </div>
                      <?php endif ?>
                    <?php endif ?>
                    <div class="col-sm-3">
                        <small class="form-control-feedback f-s-10">Tahun</small>
                        <input type="text" class="custom-select f-s-12 datepicker3" placeholder="yyyy" name="tahun" id="tahun">
                    </div>
                    <div class="col-sm-2 m-t-20 m-l-5">
                        <a id="cek" class="btn btn-info f-s-12 text-light">
                            <i class="fa fa-search"></i> Tampilkan
                        </a>
                    </div>
                  </div>
              <!-- </div> -->
            </form>
            </div>
        </div>
    </div>
  </div>
  <div class="col-md-6">
      <div class="card">
          <div class="card-body">
            <div class="col-md-12">
                <div id="container1" style="min-width: 200px; height: 400px; margin: 0 auto"></div>
            </div>
        </div>
    </div>
  </div>
    <div class="col-md-6">
      <div class="card">
          <div class="card-body">
            <div class="col-md-12">
                <div id="container2" style="min-width: 200px; height: 400px; margin: 0 auto"></div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <?php if ($this->session->userdata("roles")!=4): ?>
  <div class="col-12 p-0">
      <div class="card">
          <div class="card-body">
            <div class="col-md-12">
              <h4>Reminder OS Premi Karyawan Bank BRI</h4>                        
              <span class="pull-right"><img src="<?php echo base_url()?>assets/ela/images/logorep.png" alt="" class="logo-text" /></span>

                <div class="table-responsive">
                  <table id="DataPeserta" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                      <thead>
                          <tr>
                              <th>No.Pinjaman</th>
                              <th>Nama</th>
                              <th>Asuransi</th>
                              <th>Kantor Operasional</th>
                              <th>Jatuh Tempo</th>
                              <th>Tagihan Tahun Ke</th>
                              <th>Premi</th>
                          </tr>
                      </thead>
                  </table>
                </div>
            </div>
        </div>
    </div>
  </div>
  <?php endif ?>

</div>

