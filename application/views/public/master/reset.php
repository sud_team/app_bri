<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Reset User Bank</h4>
                <!--TABEL-->
                
                <div class="table-responsive m-t-40">
                    <table id="DataPeserta" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="w-5">#</th>
                                <th>Username</th>
                                <th>Bank</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($user as $key): ?>
                            <tr>
                                <td><?php echo $no;?></td>
                                <td style="text-align: center;"><?php echo $key->username;?></td>
                                <td style="text-align: center;"><?php echo cekNamaBank($key->kodeBank);?></td>
                                <td style="text-align: center;"><a class="btn btn-xs btn-primary" onclick="updatePass('<?php echo $key->idUser;?>')" style="color: #ffffff">Reset</a> <?php if ($key->is_online==1): ?>
                                  - <a class="btn btn-xs btn-warning" onclick="unlockPass('<?php echo $key->idUser;?>')" style="color: #ffffff">Unlock</a></td>
                                <?php endif ?> 
                                  
                            </tr>
                            <?php $no++; endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="reset" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form action="<?php echo base_url()?>master/saveUpdate" method="post" enctype="multipart/form-data">
          <div class="modal-header">
              <h6 class="modal-title font-weight-bold" id="exampleModalLabel">Reset Password</h6>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body f-s-12 p-l-30 p-r-30">
              <div class="col-xs-12" id="dok">              
              </div>
          </div>
          <div class="modal-footer ">
              <button type="button" class="btn btn-secondary btn-sm " data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-info btn-sm " onclick="return confirm('Apakah Anda Yakin?');" value="Save" style="color: #ffffff">
          </div>
        </form>
      </div>
  </div>
</div>