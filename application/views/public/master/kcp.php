<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Daftar Bank Dan Capem</h4>
                <h6 class="card-subtitle">Kantor Bank Cabang dan Cabang Pembantu</h6>

                <!--TABEL-->
                <div class="row">
                <div class="text-left text-info col-md-4 p-b-10">
                    <small class="form-control-feedback f-s-12">Cabang</small>
                    <select class="js-example-basic-cabang col-sm-8 custom-select f-s-12" id="cabang" name="cabang">
                      <option value="0">Semua</option>
                      <?php foreach ($cabang as $key): ?>
                      <option value="<?php echo $key->kodeBank?>"><?php echo $key->kodeBank.'-'.$key->cabang;?></option>
                      <?php endforeach ?>
                    </select>
                    </div>
<div class="col-md-2 p-b-10">
                    <a id="cari" class="btn btn-info f-s-12" style="color: #ffffff;">
                        <i class="fa fa-search"></i> Tampilkan 
                    </a></div></div>

                <div class="table-responsive m-t-40">
                    <table id="example23" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="w-5">#</th>
                                <th>Bank Cabang</th>
                                <th>Bank Capem</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>