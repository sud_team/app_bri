<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Pengaturan User</h4>
                <h6 class="card-subtitle">Untuk menambahkan, merubah, menghapus dan aktifasi data user</h6>
                <div class="nav-item dropdown mega-dropdown">
                    <button aria-expanded="false" aria-haspopup="true" class="btn btn-info btn-sm" data-toggle="dropdown" href="#">Buat User Baru
                        <i class="fa fa-user-plus"></i>
                    </button>
                    <div class="dropdown-menu animated zoomIn alert alert-info">
                        <div class="card-title text-center">
                            <h4>Entry data user</h4>
                        </div>
                        <form action="<?php echo base_url()?>master/saveUser" class="form-valide" method="post">
                            <div class="form-group row">
                                <label class="col-form-label w-15 p-10" for="val-username">Username
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-sm-4 has-success">
                                    <input class="form-control f-s-12" name="username" placeholder="Masukan a username.." type="text" autocomplete="off">
                                </div>
                                <label class="col-form-label w-15 p-10" for="val-password">Password
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-sm-4 has-success">
                                    <input class="form-control f-s-12"  name="password" type="password"  autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label w-15 p-10" for="val-akses">Hak Akses
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-sm-4 has-success">
                                    <select class="js-example-basic-cabang custom-select f-s-12 " id="akses" name="akses">
                                        <option value="">Semua</option>
                                        <?php foreach ($akses as $as): ?>
                                        <option value="<?php echo $as->idHakAkses?>"><?php echo $as->hak_akses?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <label class="col-form-label w-15 p-10" for="val-role">Role
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-sm-4">
                                    <select class="js-example-basic-cabang custom-select f-s-12" id="role" name="role">
                                        <option value="">Semua</option>
                                        <?php foreach ($roles as $rl): ?>
                                        <option value="<?php echo $rl->idRoles?>"><?php echo $rl->roles?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label w-15 p-10" for="val-suggestions">Cabang
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-sm-4">
                                    <select class="js-example-basic-cabang custom-select f-s-12" id="cabang" name="cabang">
                                        <option value="">Semua</option>
                                        <?php foreach ($cabang as $cb): ?>
                                        <option value="<?php echo $cb->kodeBank?>"><?php echo $cb->kodeBank." - ".$cb->cabang?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <label class="col-form-label w-15 p-10" for="val-select2">Capem
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-sm-4 ">
                                    <select class="js-example-basic-cabang col-sm-3 custom-select f-s-12" id="capem" name="capem">
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label w-15 p-10" for="val-suggestions">Menu Akses
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-sm-12">
                                    
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-8 m-t-20">
                                    <button class="btn btn-info btn-sm sweet-success"  type="submit">Simpan
                                        <i class="fa fa-save"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--TABEL-->
                <br>
                <?php /*<form action="<?php echo base_url()?>master/userUpload" method="post" enctype="multipart/form-data">
                    <input type="file" name="user" class="btn btn-xs btn-secondary">
                    <input type="submit" name="upload" value="Upload" class="btn btn-xs btn-info">
                </form>*/?>
                <div class="table-responsive m-t-40">
                    <table id="user" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="w-5">#</th>
                                <th class="w-15">Username</th>
                                <th class="w-15">Cabang</th>
                                <th class="w-15">Capem</th>
                                <th class="w-7">Roles</th>
                                <th class="w-15">Akses</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>