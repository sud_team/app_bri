<div class="row">
  <div class="col-12">
      <div class="card">
          <div class="card-body">
          <h4 class="card-title">Mengatur Premi Debitur Menjadi ProRate</h4>
            <!-- Panel Utama -->
            <form id="FormSubmit" method="post">
            <div class="row">
                <!-- Menu Pencairan -->
                <div class="col-md-7">
                  <input type="hidden" id="link" value="filter_setProrate">
                    <div class="row m-b-20">
                      <div class="text-left text-info col-sm-5">
                        <small class="form-control-feedback f-s-10">No. Pinjaman</small>
                        <input type="text" placeholder="No. Pinjaman" name="nopin" id="nopin" class=" custom-select f-s-12 ">
                      </div>
                    </div>
                    <div class="row m-b-20">
                      <div class="col-sm-2 m-t-20 text-right">
                        <a id="cariProrate" class="btn btn-info f-s-12 text-light">
                        <i class="fa fa-search"></i> Tampilkan
                        </a>
                      </div>
                    </div>
                </div>
              </form>
                    
                <!-- Menu Pencairan Akhir -->
                <!-- Display Konfirmasi Akhir "VISIBLE SETELAH PENCARIAN"-->
            </div>
            </form>
              <div class="table-responsive">
                  <!-- <a class="btn btn-xs btn-primary" id="validasi" style="color: #ffffff">Validasi</a> -->
                  <table id="DataDebitur" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                      <thead >
                          <tr >
                              <th>No.Pinjaman</th>
                              <th>Nama</th>
                              <th>Tgl Kredit</th>
                              <th>Tgl Akhir</th>
                              <th>Tenor Bulan</th>
                              <th>Plafon</th>
                              <th>Premi</th>
                              <th>Adjust</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>


