<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Produk</h4>
                <div class="nav-item dropdown mega-dropdown">
                    <button aria-expanded="false" aria-haspopup="true" class="btn btn-info btn-sm" data-toggle="dropdown" href="#">Tambah Produk
                        <i class="fa fa-user-plus"></i>
                    </button>
                    <div class="dropdown-menu animated zoomIn alert alert-info">
                        <div class="card-title text-center">
                            <h4>Entry data Produk</h4>
                        </div>
                        <form action="<?php echo base_url()?>master/saveProduk" class="form-valide" method="post">
                            <div class="form-group row">
                                <label class="col-form-label w-15 p-10">Kode Produk
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <input class="form-control f-s-12" placeholder="Masukan Kode Produk" name="kode" type="text">
                                </div>
                                <label class="col-form-label w-15 p-10">Nama Produk
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <input class="form-control f-s-12" placeholder="Masukan Nama Produk" name="produk" type="text">
                                </div>
                                <!-- <label class="col-form-label w-15 p-10">Fee Broker
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-s-12 has-success w-30">
                                    <input class="form-control f-s-12" placeholder="Masukan Fee Broker" name="feebroker" type="text">
                                </div>
                                <label class="col-form-label w-15 p-10">Fee Bank
                                    <span class="text-danger">*</span>
                                </label> 
                                <div class="col-s-12 has-success w-30">
                                    <input class="form-control f-s-12" placeholder="Masukan Fee Bank" name="feebank" type="text">
                                </div> -->
                            </div>
                            <div class="form-group row">
                                <div class="col-8 m-t-20">
                                    <button class="btn btn-info btn-sm sweet-success"  type="submit">Simpan
                                        <i class="fa fa-save"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--TABEL-->
                <div class="table-responsive m-t-40">
                    <table id="example23" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="w-5">#</th>
                                <th>Kode Produk</th>
                                <th>Produk</th>
                                <!-- <th>Fee Broker</th>
                                <th>Fee Bank</th> -->
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($produk as $key): ?>
                            <tr>
                                <td><?php echo $no;?></td>
                                <td style="text-align: center;"><?php echo $key->kodeProduk;?></td>
                                <td style="text-align: center;"><?php echo $key->NamaProduk;?></td>
                                <?php /*<td style="text-align: center;"><?php echo $key->FeeBroker;?></td>
                                <td style="text-align: center;"><?php echo $key->FeeBank;?></td> */ ?>
                                <td></td>
                            </tr>
                            <?php $no++; endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
