        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Pengaturan User</h4>
                <h6 class="card-subtitle">Untuk menambahkan, merubah, menghapus dan aktifasi data user</h6>
                    <!-- Button to Open the Modal -->
                    <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#myModal">
                    Buat User Baru &nbsp<i class="fa fa-user-plus"></i></button>

                 <!-- The Modal -->
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content p-10">

                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="text-center">Entry Data User</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                            <!-- Modal body -->
                            <div class="modal-body">
                                    <form action="<?php echo base_url()?>master/saveUser" class="form-valide" method="post">
                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-3 p-10 text-right" for="val-username">Username
                                                <span class="text-danger">*</span>
                                            </label>
                                                <input class="col-sm-6 custom-select f-s-12" name="username" placeholder="Masukan a username.." type="text" autocomplete="off">
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-3 p-10 text-right" for="val-password">Password
                                                <span class="text-danger">*</span>
                                            </label>
                                                <input class="col-sm-6 custom-select f-s-12"  name="password" type="password"  autocomplete="off">
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-3 p-10 text-right" for="val-akses">Hak Akses
                                                <span class="text-danger">*</span>
                                            </label>
                                                <select class="col-sm-6 custom-select f-s-12" id="akses" name="akses">
                                                    <option value="">Semua</option>
                                                    <?php foreach ($akses as $as): ?>
                                                    <option value="<?php echo $as->idHakAkses?>"><?php echo $as->hak_akses?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                            <div class="form-group row">
                                            <label class="col-form-label col-sm-3 p-10 text-right" for="val-role">Role
                                                <span class="text-danger">*</span>
                                            </label>
                                                <select class="col-sm-6 custom-select f-s-12" id="role" name="role">
                                                    <option value="">Semua</option>
                                                    <?php foreach ($roles as $rl): ?>
                                                    <option value="<?php echo $rl->idRoles?>"><?php echo $rl->roles?></option>
                                                    <?php endforeach ?>
                                                </select>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-3 p-10 text-right" for="val-suggestions">Cabang
                                                <span class="text-danger">*</span>
                                            </label>
                                                <select class="col-sm-6 custom-select f-s-12" id="cabang" name="cabang">
                                                    <option value="">Semua</option>
                                                    <?php foreach ($cabang as $cb): ?>
                                                    <option value="<?php echo $cb->kodeBank?>"><?php echo $cb->kodeBank." - ".$cb->cabang?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-3 p-10 text-right" for="val-select2">Capem
                                                <span class="text-danger">*</span>
                                            </label>
                                                <select class="col-sm-6 custom-select f-s-12" id="capem" name="capem">
                                                </select>
                                            </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-3 p-10 text-right" for="val-suggestions">Menu Akses
                                                <span class="text-danger">*</span>
                                            </label>
                                            <!-- MENU AKSES -->
                                            <div class="col-sm-8 row p-r-0 p-t-10">
                                            <div class="col-sm-4">
                                                <ul class="f-s-12 p-b-10">
                                                    <input type="checkbox" /> Dashboard
                                                </ul>
                                                <ul class="f-s-12 p-b-10">
                                                    <input type="checkbox" /> Produksi
                                                    <ul class="p-l-15 f-s-12">
                                                        <li >
                                                        <input type="checkbox" /> Upload Data</li>
                                                        </li>
                                                        <li>
                                                        <input type="checkbox" /> Edit Data Peserta</li>
                                                        </li>
                                                        <li>
                                                        <input type="checkbox" /> Non-TC</li>
                                                        </li>
                                                        <li>
                                                        <input type="checkbox" /> Rekonsel</li>
                                                        </li>
                                                        <li>
                                                        <input type="checkbox" /> Input Polis</li>
                                                        </li>
                                                    </ul>
                                                </ul>
                                                <ul class="f-s-12 p-b-10">
                                                    <input type="checkbox" /> Produksi
                                                    <ul class="p-l-15 f-s-12">
                                                        <li >
                                                        <input type="checkbox" /> Upload Data</li>
                                                        </li>
                                                        <li>
                                                        <input type="checkbox" /> Edit Data Peserta</li>
                                                        </li>
                                                        <li>
                                                        <input type="checkbox" /> Non-TC</li>
                                                        </li>
                                                        <li>
                                                        <input type="checkbox" /> Rekonsel</li>
                                                        </li>
                                                        <li>
                                                        <input type="checkbox" /> Input Polis</li>
                                                        </li>
                                                    </ul>
                                                </ul>
                                                </div>

                                                <div class="col-sm-4">
                                                <ul class="f-s-12 p-b-10">
                                                    <input type="checkbox" /> Klaim
                                                    <ul class="p-l-15 f-s-12">
                                                        <li>
                                                        <input type="checkbox" /> Cari Data Klaim</li>
                                                        </li>
                                                        <li>
                                                        <input type="checkbox" /> Input</li>
                                                        </li>
                                                        <li>
                                                        <input type="checkbox" /> Lihat data</li>
                                                        </li>
                                                        <li>
                                                        <input type="checkbox" /> Persetujuan</li>
                                                        </li>
                                                        <li>
                                                        <input type="checkbox" /> Penilaian Klaim</li>
                                                        </li>
                                                        <li>
                                                        <input type="checkbox" /> Pembayaran</li>
                                                        </li>
                                                        </ul>
                                                    </ul>
                                                    <ul class="f-s-12 p-b-10">
                                                    <input type="checkbox" /> Klaim
                                                    <ul class="p-l-15 f-s-12">
                                                        <li>
                                                        <input type="checkbox" /> Cari Data Klaim</li>
                                                        </li>
                                                        <li>
                                                        <input type="checkbox" /> Input</li>
                                                        </li>
                                                        <li>
                                                        <input type="checkbox" /> Lihat data</li>
                                                        </li>
                                                        <li>
                                                        <input type="checkbox" /> Persetujuan</li>
                                                        </li>
                                                        <li>
                                                        <input type="checkbox" /> Penilaian Klaim</li>
                                                        </li>
                                                        <li>
                                                        <input type="checkbox" /> Pembayaran</li>
                                                        </li>
                                                        </ul>
                                                    </ul>                              
                                                </div>

                                                <div class="col-sm-4">                                        
                                                    <ul class="f-s-12 p-b-10">
                                                        <input type="checkbox" /> Pengaturan User
                                                        <ul class="p-l-15 f-s-12">
                                                        <li>
                                                        <input type="checkbox" /> Upload Data</li>
                                                        </li>
                                                        <li>
                                                        <input type="checkbox" /> Non-TC</li>
                                                        </li>
                                                        <li>
                                                        <input type="checkbox" /> Rekonsel</li>
                                                        </li>
                                                        <li>
                                                        <input type="checkbox" /> Input Polis</li>
                                                        </li>
                                                        <li>
                                                        <input type="checkbox" /> Cari Data Peserta</li>
                                                        </ul>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                            <!-- MENU AKSES -->

                                        <div class="form-group row">
                                            
                                        </div>
                                    </form>
                                </div>

                            <!-- Modal footer -->
                            <div class="modal-footer text-center">
                            <div class="col-sm-6 text-left"><button class="btn btn-info btn-sm sweet-success animated flash"  type="submit">Simpan &nbsp<i class="fa fa-save"></i></button></div> 
                            <div class="text-right col-sm-6"> <button type="button" class="btn btn-warning btn-sm">Tambah User lagi &nbsp<span class="fa fa-plus"></span></button></div>      
                            </div>
                        </div>
                    </div>  
                </div> 
                <!--TABEL-->
                <br>
                <?php /*<form action="<?php echo base_url()?>master/userUpload" method="post" enctype="multipart/form-data">
                    <input type="file" name="user" class="btn btn-xs btn-secondary">
                    <input type="submit" name="upload" value="Upload" class="btn btn-xs btn-info">
                </form>*/?>
                <div class="table-responsive m-t-40">
                    <table id="user" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="w-5">#</th>
                                <th class="w-15">Username</th>
                                <th class="w-15">Cabang</th>
                                <th class="w-15">Capem</th>
                                <th class="w-7">Roles</th>
                                <th class="w-15">Akses</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
