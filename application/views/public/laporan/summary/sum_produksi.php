<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <li class="pull-right">
                <img class="dark-logo" src="<?php echo base_url()?>assets/ela/images/logorep.png">
      </li>
                        <!-- Lebel Laporan  -->
                        <h4 class="card-title">
                            <strong><?php echo strtoupper("summary produksi");?></strong>
                        </h4>
                        <div class="row lable-laporan m-b-20">
                            <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-sm-4" id="judul">
                                        <strong>Periode</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <span>: </span><?php echo $periode?></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4" id="judul">
                                        <strong>Asuradur</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <span>: </span><?php echo $asuransi?></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4" id="judul">
                                        <strong>Produk</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <span>: </span><?php echo $produk?></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <strong>Bank</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <span>: </span><?php echo $bank?></div>
                                </div>
                            </div>
                            <div class="col-lg-4"></div>
                        </div>
                <div class="table-responsive" id="abc">                
                    <table id='DataPeserta2' class='display nowrap table-hover table-bordered f-s-12' cellspacing='0' width='100%'>
                        <thead>
                            <tr class="text-center font-weight-bold">
                                <th> #</th>
                                <th> BANK</th>
                                <th> PEGAWAI (PNS dan Non PNS)</th>
                                <th> PENSIUNAN</th>
                                <th> PEGAWAI BANK BRI</th>
                                <th> PLAFON</th>
                                <th> PREMI</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $i=1; 
                                $totpnsnon = 0;
                                $totpensiunan = 0;
                                $totjatim = 0;
                                $totplafon = 0;
                                $totJumlahPremiTenor = 0;
                                foreach ($debitur->result() as $key): ?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td><?php echo cekNamaBank($key->kodeBank)?></td>
                                <td style="text-align: center;"><?php echo $key->pnsnonpns?></td>
                                <td style="text-align: center;"><?php echo $key->pensiunan?></td>
                                <td style="text-align: center;"><?php echo $key->jatim?></td>
                                <td style="text-align: right;"><?php echo price($key->plafon)?></td>
                                <td style="text-align: right;"><?php echo price($key->JumlahPremiTenor)?></td>
                            </tr>
                            <?php 
                                $i++;
                                $totpnsnon = $totpnsnon+$key->pnsnonpns;
                                $totpensiunan = $totpensiunan+$key->pensiunan;
                                $totjatim = $totjatim+$key->jatim;
                                $totplafon = $totplafon+$key->plafon;
                                $totJumlahPremiTenor = $totJumlahPremiTenor+$key->JumlahPremiTenor;
                            endforeach ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="2" style="text-align: right;">TOTAL</th>
                                <th style="text-align: center;"><?php echo  $totpnsnon?></th>
                                <th style="text-align: center;"><?php echo  $totpensiunan?></th>
                                <th style="text-align: center;"><?php echo  $totjatim?></th>
                                <th style="text-align: right;"><?php echo  price($totplafon)?></th>
                                <th style="text-align: right;"><?php echo  price($totJumlahPremiTenor)?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>