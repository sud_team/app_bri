<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <li class="pull-right">
                <img class="dark-logo" src="<?php echo base_url()?>assets/ela/images/logorep.png">
        </li>
                        <!-- Lebel Laporan  -->
                        <h4 class="card-title">
                            <strong><?php echo strtoupper("SUmmary klaim");?></strong>
                        </h4>
                        <div class="row lable-laporan m-b-20">
                            <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-sm-4" id="judul">
                                        <strong>Periode</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <span>: </span><?php echo $periode?></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4" id="judul">
                                        <strong>Asuradur</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <span>: </span><?php echo $asuransi?></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4" id="judul">
                                        <strong>Produk</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <span>: </span><?php echo $produk?></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <strong>Bank</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <span>: </span><?php echo $bank?></div>
                                </div>
                            </div>
                            <div class="col-lg-4"></div>
                        </div>
                <div class="table-responsive" id="abc">                
                    <table id='DataPeserta2' class='display nowrap table-hover table-bordered f-s-12' cellspacing='0' width='100%'>
                        <thead>
                        <tr class="text-center font-weight-bold">
                                <th> #</th>
                                <th> BANK</th>
                                <th> DIAJUKAN</th>
                                <th> PROSES</th>
                                <th> DITOLAK</th>
                                <th> DISETUJUI</th>
                                <th> NOMINAL PERSETUJUAN</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; $totpnsnon = 0;
                                $totpensiunan = 0;
                                $totjatim = 0;
                                $totplafon = 0;
                                $totJumlahPremiTenor = 0;
                                foreach ($debitur->result() as $key): ?>
                            <tr>
                                <td style="text-align: center;"><?php echo $i;?></td>
                                <td><?php echo cekNamaBank($key->kodeBank)?></td>
                                <td style="text-align: center;"><?php echo $key->diajukan?></td>
                                <td style="text-align: center;"><?php echo $key->proses?></td>
                                <td style="text-align: center;"><?php echo $key->ditolak?></td>
                                <td style="text-align: center;"><?php echo $key->diterima?></td>
                                <td style="text-align: right;"><?php echo price($key->nominal)?></td>
                            </tr>
                            <?php 
                                $i++;
                                $totpnsnon = $totpnsnon+$key->diajukan;
                                $totpensiunan = $totpensiunan+$key->proses;
                                $totjatim = $totjatim+$key->ditolak;
                                $totplafon = $totplafon+$key->diterima;
                                $totJumlahPremiTenor = $totJumlahPremiTenor+$key->nominal;
                            endforeach ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="2" style="text-align: right;">TOTAL</th>
                                <th style="text-align: center;"><?php echo  $totpnsnon?></th>
                                <th style="text-align: center;"><?php echo  $totpensiunan?></th>
                                <th style="text-align: center;"><?php echo  $totjatim?></th>
                                <th style="text-align: center;"><?php echo  $totplafon?></th>
                                <th style="text-align: right;"><?php echo  price($totJumlahPremiTenor)?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>