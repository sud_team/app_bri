 <div class="col-sm-12">
    <div class="card">
        <div class="card-body">
            <form id="FormSubmit" method="post" target="_blank" action="<?php echo base_url()?>laporan/cariProduksi">
            <h4 class="card-title font-weight-bold text-megna">PRODUKSI</h4>
            <!-- Nav tabs -->
            <div class="vtabs">
                <ul class="nav nav-tabs tabs-vertical" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#lapro1" id="tablapro1" role="tab">
                            <span class="hidden-sm-up"> 1
                                <i class="ti-arrow-right"> </i>
                            </span>
                            <span class="hidden-xs-down"> Rincian Data Produksi Asuransi
                            </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lapro2" id="tablapro2" role="tab">
                            <span class="hidden-sm-up"> 2
                                <i class="ti-arrow-right"> </i>
                            </span>
                            <span class="hidden-xs-down"> Rekap Pertanggungan Bulanan Per Asuransi
                            </span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lapro3" id="tablapro3" role="tab">
                            <span class="hidden-sm-up"> 3
                                <i class="ti-arrow-right"> </i>
                            </span>
                            <span class="hidden-xs-down"> Produksi Asuransi per Bank
                            </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lapro4" id="tablapro4" role="tab">
                            <span class="hidden-sm-up"> 4
                                <i class="ti-arrow-right"> </i>
                            </span>
                            <span class="hidden-xs-down"> Rekap Produksi Asuransi Tahunan
                            </span>
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane" id="lapro0" role="tabpanel">
                        <div class="p-20">
                            <h4 class="text-primary">Menampilkan seluruh laporan Produksi</h4>
                            <h6>Penarikan data terhitung dari seluruh Produksi yang telah diterima</h6>
                        </div>
                    </div>
                    <input type="hidden" name="tipe" id="tipe" value="">
                    <div class="tab-pane active" id="lapro1" role="tabpanel">
                        <div>
                            <h6>Menampilkan laporan data produksi per debitur secara rinci</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <!-- <div class="row m-b-20">
                            <div class="col-sm-5 f-s-10 text-megna">Periode upload 
                            <input id="tgl_mulai" type="text" class="custom-select f-s-12 datepicker" name="tgl_awal" placeholder="dd-mm-yyyy"></div>
                            <div class="col-sm-5 f-s-10 text-megna">Sampai dengan
                            <input id="tgl_akhir" type="text" class="custom-select f-s-12 datepicker" name="tgl_akhir" placeholder="dd-mm-yyyy"></div>
                        </div> -->
                        <div class="row m-b-20">
                            <div class="col-sm-5 f-s-10 text-megna">Periode rekonsel 
                            <input id="tgl_awalrekonsel1" type="text" class="custom-select f-s-12 datepicker" name="tgl_awalrekonsel1" placeholder="dd-mm-yyyy"></div>
                            <div class="col-sm-5 f-s-10 text-megna">Sampai dengan
                            <input id="tgl_akhirrekonsel1" type="text" class="custom-select f-s-12 datepicker" name="tgl_akhirrekonsel1" placeholder="dd-mm-yyyy"></div>
                        </div>
                    </div>
                    <div class="tab-pane " id="lapro2" role="tabpanel">
                        <div>
                            <h6>Menampilkan laporan rekap pertanggungan per asuransi dalam sebulan</h6>
                            <p>Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <!-- <div class="row m-b-20">
                            <div class="col-sm-5 f-s-10 text-megna">Periode upload (Bulan)
                            <input type="text" class=" custom-select f-s-12 datepicker2" name="bulan" placeholder="mm-yyyy"></div>
                        </div> </div>-->
                        <div class="row m-b-20">
                            <div class="col-sm-5 f-s-10 text-megna">Periode rekonsel 
                            <input id="tgl_awalrekonsel2" type="text" class="custom-select f-s-12 datepicker" name="tgl_awalrekonsel2" placeholder="dd-mm-yyyy"></div>
                            <div class="col-sm-5 f-s-10 text-megna">Sampai dengan
                            <input id="tgl_akhirrekonsel2" type="text" class="custom-select f-s-12 datepicker" name="tgl_akhirrekonsel2" placeholder="dd-mm-yyyy"></div>
                        </div>
                    </div>
                    <div class="tab-pane " id="lapro3" role="tabpanel">
                        <div>
                            <h6>Laporan produksi asuransi per bank (Kantor Pusat, Wilayah Cabang dan Cabang Pembantu)
                            </h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <!-- <div class="row m-b-20">
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Periode upload</small>
                            <input id="tgl_mulai" type="text" class="custom-select f-s-12 datepicker" name="tgl_awal2" placeholder="dd-mm-yyyy"></div>
                            <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Sampai dengan</small>
                            <input id="tgl_akhir" type="text" class="custom-select f-s-12 datepicker" name="tgl_akhir2" placeholder="dd-mm-yyyy"></div>
                        </div> -->
                        <div class="row m-b-20">
                            <div class="col-sm-5 f-s-10 text-megna">Periode rekonsel 
                            <input id="tgl_awalrekonsel3" type="text" class="custom-select f-s-12 datepicker" name="tgl_awalrekonsel3" placeholder="dd-mm-yyyy"></div>
                            <div class="col-sm-5 f-s-10 text-megna">Sampai dengan
                            <input id="tgl_akhirrekonsel3" type="text" class="custom-select f-s-12 datepicker" name="tgl_akhirrekonsel3" placeholder="dd-mm-yyyy"></div>
                        </div>
                    </div>
                    <div class="tab-pane " id="lapro4" role="tabpanel">
                        <div>
                            <h6>Laporan Produksi Asuransi dalam setahun</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <!-- <div class="row m-b-20">
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Periode upload (Tahun)</small>
                            <input type="text" class="custom-select f-s-12 datepicker3" name="tahun" placeholder="yyyy"></div>
                        </div> -->
                        <div class="row m-b-20">
                            <div class="col-sm-5 f-s-10 text-megna">Periode rekonsel 
                            <input id="tgl_awalrekonsel4" type="text" class="custom-select f-s-12 datepicker" name="tgl_awalrekonsel4" placeholder="dd-mm-yyyy"></div>
                            <div class="col-sm-5 f-s-10 text-megna">Sampai dengan
                            <input id="tgl_akhirrekonsel4" type="text" class="custom-select f-s-12 datepicker" name="tgl_akhirrekonsel4" placeholder="dd-mm-yyyy"></div>
                        </div>
                    </div>
                    <div class="row m-b-20">
                    <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Produk   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                        <select width="100%"  class="js-example-basic-cabang form-control-feedback custom-select f-s-12 " id="produk" name="produk">
                            <option value="">Semua</option>
                            <?php foreach ($produk as $key1): ?>
                            <option value="<?php echo $key1->idProduk?>"><?php echo $key1->NamaProduk?></option>
                            <?php endforeach ?>
                        </select></div>
                        <?php if ($this->session->userdata("roles")!=4): ?>
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Asuradur   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                        <select width="100%"  class="js-example-basic-cabang form-control-feedback custom-select f-s-12 " id="asuransi" name="asuransi">
                            <option value="">Semua</option>
                            <?php foreach ($asuradur as $key): ?>
                            <option value="<?php echo $key->kodeAngka?>"><?php echo $key->asuransi?></option>
                            <?php endforeach ?>
                        </select></div>
                        <?php endif ?>
                    </div>
                    <div class="row m-b-20">
                        

                       <?php if (cekOpBank($this->session->userdata("kodeBank"))!="CAPEM"): ?>
                          <?php if (cekOpBank($this->session->userdata("kodeBank"))=="CABANG"): ?>
                                <input type="hidden" placeholder="Nama" id="cabang" name="cabang" class="col-xs-3 custom-select f-s-12 " value="<?php echo $this->session->userdata('kodeBank');?>" readonly>
                                
                                <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Kantor Cabang</small>
                        <input type="text" placeholder="Nama" id="cabang1" name="cabang1" class=" custom-select f-s-12" value="<?php echo cekNamaBank($this->session->userdata('kodeBank'));?>" readonly>                       
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Kantor Operasional</small>
                        <select width="100%"  class="js-example-basic-cabang  custom-select f-s-12" id="capem" name="capem">
                            <option value="">Semua</option>
                        </select> </div>
                          <?php else: ?>
                          <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Kantor Cabang</small>
                        <select width="100%"  class="js-example-basic-cabang  custom-select f-s-12" id="cabang" name="cabang">
                            <option value="">Semua</option>
                            <?php foreach ($cabang as $key): ?>
                            <option value="<?php echo $key->kodeBank?>"><?php echo $key->kodeBank.'-'.$key->cabang;?></option>
                            <?php endforeach ?>
                        </select></div>                        
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Kantor Operasional</small>
                        <select width="100%"  class="js-example-basic-cabang  custom-select f-s-12" id="capem" name="capem">
                            <option value="">Semua</option>
                        </select></div>
                          <?php endif ?>
                        <?php endif ?>
                        
                    </div>
                    <div class="row m-b-20">
                        <div class="col-sm">
                            <input class="btn btn-info f-s-12" type="submit" name="tampilkan" target="_blank" value="Tampilkan">
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>