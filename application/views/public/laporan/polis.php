<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <form id="FormSubmit" method="post" target="_blank" action="<?php echo base_url()?>laporan/cariPolis">
            <h4 class="card-title font-weight-bold text-megna">POLIS</h4>
            <!-- Nav tabs -->
            <div class="vtabs">
                <ul class="nav nav-tabs tabs-vertical" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" id="tabpolis1" href="#lapol1" role="tab">
                            <span class="hidden-sm-up"> 1
                                <i class="ti-arrow-right"> </i>
                            </span>
                            <span class="hidden-xs-down">Rincian Polis</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lapol2" id="tabpolis2" role="tab">
                            <span class="hidden-sm-up"> 2
                                <i class="ti-arrow-right"> </i>
                            </span>
                            <span class="hidden-xs-down">Rekap Polis Bulanan</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lapol3" id="tabpolis3" role="tab">
                            <span class="hidden-sm-up"> 3
                                <i class="ti-arrow-right"> </i>
                            </span>
                            <span class="hidden-xs-down">Rekap Polis Keseluruhan</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lapol4" id="tabpolis4" role="tab">
                            <span class="hidden-sm-up"> 4
                                <i class="ti-arrow-right"> </i>
                            </span>
                            <span class="hidden-xs-down"> Rekap Data Penerbitan Polis Tahunan</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lapol5" id="tabpolis5" role="tab">
                            <span class="hidden-sm-up"> 5
                                <i class="ti-arrow-right"> </i>
                            </span>
                            <span class="hidden-xs-down">Penerbitan Polis per Bank</span>
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <input type="hidden" name="tipe" id="tipepolis" value="">
                <div class="tab-content">
                    <div class="tab-pane active" id="lapol1" role="tabpanel">
                        <div>
                            <h6>Laporan rincian polis per Bank sesuai periode yang ditentukan</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Dari</small>
                            <input id="tgl_mulai" type="text" class="custom-select f-s-12 datepicker" name="tgl_awal" placeholder="dd-mm-yyyy"></div>
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Sampai dengan</small>
                            <input id="tgl_akhir" type="text" class="custom-select f-s-12 datepicker" name="tgl_akhir" placeholder="dd-mm-yyyy"></div>
                        </div>
                    </div>
                    <div class="tab-pane " id="lapol2" role="tabpanel">
                        <div>
                            <h6>Rekapitulasi penerbitan polis per Asuradur dalam sebulan</h6>
                            <p>Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Pilih Bulan</small>
                             <input id="tgl_mulai" type="text" class="custom-select f-s-12 datepicker2" name="bulan" placeholder="dd-mm-yyyy"></div>
                    </div></div>
                    <div class="tab-pane " id="lapol3" role="tabpanel">
                        <div>
                            <h6>Rekapitulasi penerbitan polis per Asuradur dari awal sampai periode yang ditentukan</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Tanggal Akhir</small>
                            <input id="tgl_akhir" type="text" class="custom-select f-s-12 datepicker" name="tgl_akhir2" placeholder="dd-mm-yyyy"></div>
                        </div>
                    </div>
                    <div class="tab-pane " id="lapol4" role="tabpanel">
                        <div>
                            <h6>Rekapitulasi penerbitan polis per bulan dalam setahun</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Pilih Tahun</small>
                            <input type="text" class="custom-select f-s-12 datepicker3" name="tahun" placeholder="dd-mm-yyyy"></div>
                        </div>
                    </div>
                    <div class="tab-pane " id="lapol5" role="tabpanel">
                        <div>
                            <h6>Laporan penerbitan polis per Bank (Kantor Pusat, Wilayah, Cabang dan Cabang Pembantu)</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Dari</small>                            
                            <input id="tgl_mulai" type="text" class="custom-select f-s-12 datepicker" name="tgl_awal3" placeholder="dd-mm-yyyy"></div>
                            <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Sampai dengan</small>                            
                            <input id="tgl_akhir" type="text" class="custom-select f-s-12 datepicker" name="tgl_akhir3" placeholder="dd-mm-yyyy"></div>
                        </div>
                    </div>
                    <div class="row m-b-20">
                    <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Status   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                        <select class="js-example-basic-cabang form-control-feedback custom-select f-s-12" id="status" name="status">
                        <option value="">Semua</option>
                        <option value="0">Belum Terbit</option>
                        <option value="1">Sudah Terbit</option>
                    </select></div>
                    </div>
                    <div class="row m-b-20">
                    <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Produk   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                        <select class="js-example-basic-cabang form-control-feedback custom-select f-s-12" id="produk" name="produk">
                            <option value="">Semua</option>
                            <?php foreach ($produk as $key1): ?>
                            <option value="<?php echo $key1->idProduk?>"><?php echo $key1->NamaProduk?></option>
                            <?php endforeach ?>
                        </select></div>
                        <?php if ($this->session->userdata("roles")!=4): ?>
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Asuradur   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                            <select class="js-example-basic-cabang form-control-feedback custom-select f-s-12" id="asuransi" name="asuransi">
                            <option value="">Semua</option>
                            <?php foreach ($asuradur as $key): ?>
                            <option value="<?php echo $key->kodeAngka?>"><?php echo $key->asuransi?></option>
                            <?php endforeach ?>
                        </select></div>
                        <?php endif ?>
                    </div>
                    <div class="row m-b-20">
                        <?php if (cekOpBank($this->session->userdata("kodeBank"))!="CAPEM"): ?>
                          <?php if (cekOpBank($this->session->userdata("kodeBank"))=="CABANG"): ?>
                                <input type="hidden" placeholder="Nama" id="cabang2" name="cabang" class="col-xs-3 custom-select f-s-12 " value="<?php echo $this->session->userdata('kodeBank');?>" readonly>
                                
                            <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Kantor Cabang</small>                        
                            <input type="text" placeholder="Nama" id="cabang2" name="cabang2" class=" custom-select f-s-12" value="<?php echo cekNamaBank($this->session->userdata('kodeBank'));?>" readonly></div>                       
                            <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Kantor Operasional</small>
                            <select class="js-example-basic-cabang custom-select f-s-12" id="capem2" name="capem">
                            <option value="">Semua</option>
                            </select></div>
                            <?php else: ?>
                            <div class="text-left text-megna col-sm-5">
                                <small class="form-control-feedback f-s-10">Kantor Cabang</small>                        
                                <select class="js-example-basic-cabang custom-select f-s-12" id="cabang2" name="cabang">
                                <option value="">Semua</option>
                                <?php foreach ($cabang as $key): ?>
                                <option value="<?php echo $key->kodeBank?>"><?php echo $key->kodeBank.'-'.$key->cabang;?></option>
                                <?php endforeach ?>
                                </select>
                            </div>                    
                            <div class="text-left text-megna col-sm-5">
                                <small class="form-control-feedback f-s-10">Kantor Operasional</small>
                                <select class="js-example-basic-cabang custom-select f-s-12" id="capem2" name="capem">
                                <option value="">Semua</option>
                                </select></div>
                        <?php endif ?>
                        <?php endif ?>
                        
                    </div>
                    <div class="row m-b-20">
                        
                            <div class="col-sm-5 f-s-10 text-megna">
                                <input class="btn btn-info f-s-12" type="submit" name="tampilkan" target="_blank" value="Tampilkan">
                            </div>

                    </div>

                </div>
            </div>
            </form>
        </div>
    </div>
</div>