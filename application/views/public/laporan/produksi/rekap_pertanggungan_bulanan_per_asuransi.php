<div class="row">
	<div class="col-md-12">
	    <div class="card">
	        <div class="card-body">
	        	<li class="pull-right">
				<img class="dark-logo" src="<?php echo base_url()?>assets/ela/images/logorep.png">
						</li>
						<!-- Lebel Laporan  -->
						<h4 class="card-title">
							<strong><?php echo strtoupper("rekap pertanggungan bulanan perasuradur");?></strong>
						</h4>
						<div class="row lable-laporan m-b-20">
							<div class="col-lg-4">
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Periode</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $periode?></div>
								</div>
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Asuradur</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $asuransi?></div>
								</div>
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Produk</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $produk?></div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<strong>Bank</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $bank?></div>
								</div>
							</div>
							<div class="col-lg-4"></div>
						</div>
	          	<div class="table-responsive" id="abc">                
	            	<table id='Perasuransi' class='display nowrap table-hover table-bordered f-s-12' cellspacing='0' width='100%'>
	            		<thead>
	            			<tr>
	    	        			<th rowspan="2"></th>
			            		<th rowspan="2">ASURADUR/PENJAMIN</th>
			            		<th style="text-align:center;" colspan="4">PERTANGGUNGAN PER BULAN</th>
			            		<th style="text-align:center;" colspan="4">TOTAL PERTANGGUNGAN MASIH AKTIF SAMPAI BULAN SEBELUMNYA</th>
			            		<th style="text-align:center;" colspan="4">TOTAL SELURUH PERTANGGUNGAN</th>
			            	</tr>
			            	<tr>
			            		<th style="color: #0B6FA4; text-align: center;">DEBITUR</th>
			            		<th style="color: #0B6FA4; text-align: center;">UANG PERTANGGUNGAN </th>
			            		<th style="color: #0B6FA4; text-align: center;">PREMI YANG DITERIMA</th>
			            		<th style="color: #0B6FA4; text-align: center;">% </th>
			            		<th style="color: #0B6FA4; text-align: center;">DEBITUR</th>
			            		<th style="color: #0B6FA4; text-align: center;">UANG PERTANGGUNGAN </th>
			            		<th style="color: #0B6FA4;">PREMI YANG DITERIMA</th>
			            		<th style="color: #0B6FA4;">% </td>
			            		<th style="color: #0B6FA4;">DEBITUR</th>
			            		<th style="color: #0B6FA4;">TOTAL UANG PERTANGGUNGAN </th>
			            		<th style="color: #0B6FA4;">PREMI YANG DITERIMA</th>
			            		<th style="color: #0B6FA4;">% </th>
			            	</tr>
			            </thead>
			            <tbody>
		            	<?php  
			            	$ndebitur_bulan=0;
						    $nilai_pertangguan_bulan=0;
						    $premi_bulan=0;
						    $total_premi_bulan=0;
						    $ndebitur_aktif=0;
						    $nilai_pertangguan_aktif=0;
						    $premi_aktif=0;
						    $persen_aktif=0;
						    $ndebitur_total=0;
						    $nilai_pertangguan_total=0;
						    $premi_total=0;
						    $persen_total=0;
						    $persen_semua = 0;
						    $i = 1;
						    foreach($debitur->result() as $r) {
						    	if(empty($r->premiterimaaktif)){
						    		$persenaktif = 0;
						    	}else{
				    				$persenaktif = ($r->premiterimaaktif / $premiterimaaktif) *100;
						    	}
				    			$persenbulan = ($r->premiterima / $premiterima) *100;
				    			$peresentotal = (($r->premiterima+$r->premiterimaaktif)/$totalpremi)*100;
						    	
		            		?>	
	     					<tr>
				        		<td><?php echo $i;?></td>
				        		<td><?php echo cekNamaAsdur($r->kodeAsuransi);?></td>
					      		<td style="text-align: right;"><?php echo $r->jmldeb;?></td>
					          	<td style="text-align: right;"><?php echo price($r->jmlpremi);?></td>
					            <td style="text-align: right;"><?php echo price($r->premiterima);?></td>
					            <td style="text-align: right;"><?php echo round($persenbulan,2);?></td>
					            <td style="text-align: right;"><?php if(empty($r->jmldebaktif)) { echo '0'; } else {
					            	echo $r->jmldebaktif;}?></td>
					            <td style="text-align: right;"><?php if(empty($r->jmlpremiaktif)) { echo '0'; } else {
					            	echo price($r->jmlpremiaktif);}?></td>
					            <td style="text-align: right;"><?php if(empty($r->premiterimaaktif)) { echo '0'; } else {
					            	echo price($r->premiterimaaktif);}?></td>
					            <td style="text-align: right;"><?php echo round($persenaktif,2);?></td>
					            <td style="text-align: right;"><?php echo $r->jmldeb+$r->jmldebaktif;?></td>
					            <td style="text-align: right;"><?php echo price($r->jmlpremi+$r->jmlpremiaktif);?></td>
					            <td style="text-align: right;"><?php echo price($r->premiterima+$r->premiterimaaktif);?></td>
					            <td style="text-align: right;"><?php echo round($peresentotal,2);?></td>
	     					</tr>
	     					<?php 
	     						$ndebitur_bulan = $ndebitur_bulan+$r->jmldeb;
	     						$nilai_pertangguan_bulan = $nilai_pertangguan_bulan+$r->jmlpremi;
	     						$total_premi_bulan = $total_premi_bulan+$r->premiterima;
		     					$premi_bulan = $premi_bulan+round($persenbulan,2);

		     					$ndebitur_aktif=$ndebitur_aktif+$r->jmldebaktif;
							    $nilai_pertangguan_aktif=$nilai_pertangguan_aktif+$r->jmlpremiaktif;
							    $premi_aktif=$premi_aktif+$r->premiterimaaktif;
							    $persen_aktif=$persen_aktif+round($persenaktif,2);

							    $persen_semua = $persen_semua+round($peresentotal,2);


				           		$i++;
			           			} 

							    $premi_total=$total_premi_bulan+$premi_aktif;

			           		?>
			            </tbody>
			            <tfoot>
			            	<tr>
			            		<td colspan="2"  style="text-align: right;"><b>TOTAL</b></td>
			            		<td style="text-align: right;"><?php echo $ndebitur_bulan;?></td>
			            		<td style="text-align: right;"><?php echo price($nilai_pertangguan_bulan);?></td>
			            		<td style="text-align: right;"><?php echo price($total_premi_bulan);?></td>
			            		<td style="text-align: right;"><?php echo round($premi_bulan,0);?></td>
			            		<td style="text-align: right;"><?php echo $ndebitur_aktif;?></td>
			            		<td style="text-align: right;"><?php echo price($nilai_pertangguan_aktif);?></td>
			            		<td style="text-align: right;"><?php echo price($premi_aktif);?></td>
			            		<td style="text-align: right;"><?php echo round($persen_aktif,0);?></td>
			            		<td style="text-align: right;"><?php echo $ndebitur_bulan+$ndebitur_aktif;?></td>
			            		<td style="text-align: right;"><?php echo price($nilai_pertangguan_bulan+$nilai_pertangguan_aktif);?></td>
			            		<td style="text-align: right;"><?php echo price($premi_total);?></td>
			            		<td style="text-align: right;"><?php echo round($persen_semua,0);?></td>
			            	</tr>
			            </tfoot>
	            	</table>
	            </div>
            </div>
       	</div>
	</div>
</div>