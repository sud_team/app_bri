<div class="row">
	<div class="col-md-12">
	    <div class="card">
	        <div class="card-body">
	        	<li class="pull-right">
						<img class="dark-logo" src="<?php echo base_url()?>assets/ela/images/logorep.png">
						</li>
						<!-- Lebel Laporan  -->
						<h4 class="card-title">
							<strong><?php echo strtoupper("rekap produksi asuransi tahunan");?></strong>
						</h4>
						<div class="row lable-laporan m-b-20">
							<div class="col-lg-4">
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Periode</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $periode?></div>
								</div>
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Asuradur</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $asuransi2?></div>
								</div>
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Produk</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $produk?></div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<strong>Bank</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $bank?></div>
								</div>
							</div>
							<div class="col-lg-4"></div>
						</div>
	          	<div class="table-responsive" id="abc">                
	            	<table id='rekap_produksi_tahunan' class='display nowrap table-hover table-bordered f-s-12' cellspacing='0' width='100%'>
	            		<thead>
	            			<tr>
	    	        			<th rowspan="2">NO</th>
	    	        			<th rowspan="2">BULAN</th>
	    	        			<th rowspan="2">JUMLAH DEBITUR</th>
	    	        			<th rowspan="2">PLAFON</th>
	    	        			<th rowspan="2">PREMI GROSS</th>

	    	        			<?php if (($this->session->userdata("roles")==3 && ($this->session->userdata("hak_akses")==3 || $this->session->userdata("hak_akses")==10))  || $this->session->userdata("roles")==1): ?>
	    	        			<th rowspan="2">BROKERAGE</th>
	            				<?php //endif ?>
	            				<?php //if ($this->session->userdata("roles")==4 || $this->session->userdata("roles")==3 || $this->session->userdata("roles")==1): ?>
	    	        			<th rowspan="2">PREMI NET</th>
		    	        		<?php endif ?>
		    	        		<?php if ($this->session->userdata("roles")!=4): ?>
		    	        		<?php $i =1; foreach ($asuransi as $key): ?>
		    	        		<th colspan="2"><?php echo $key->kodeAsuransi?></th>	
		    	        		<?php $i++; endforeach ?>
		    	        		<?php endif ?>
			            	</tr>
			            	<?php // echo $i; ?>
			            	<?php if ($this->session->userdata("roles")!=4): ?>
			            	<tr>
			            		<?php for($a=1; $a<$i; $a++){ ?>
			            		<th>DEBITUR</th>
			            		<th>PREMI</th>
			            		<?php  } ?>
			            	</tr>
			            	<?php endif ?>
			            </thead>
			            <tbody>
		            		<?php 
		            			$totalden = 0; 
		            			$totalplafon = 0; 
		            			$totalpremi = 0; 
		            			$totalbroker = 0; 
		            			$totalnet = 0; 
		            			$no=1; 
		            			foreach ($debitur as $key): ?>
			            	<tr>
			            		<th><?php echo $no;?></th>
			            		<th><?php echo my_date_indo_bulan($key[0]->bulan)?></th>
		            			<td style="text-align: center;"><?php echo $key[0]->jmldeb?></td>
			            		<td style="text-align: right;"><?php echo price($key[0]->plafon)?></td>
			            		<td style="text-align: right;"><?php echo price($key[0]->premi)?></td>
			            		<?php if (($this->session->userdata("roles")==3 && ($this->session->userdata("hak_akses")==3 || $this->session->userdata("hak_akses")==10)) || $this->session->userdata("roles")==1): ?>
			            		<td style="text-align: right;"><?php echo price($key[0]->brokerage)?></td>
			            		<?php //endif ?>
	            				<?php //if ($this->session->userdata("roles")==4 || $this->session->userdata("roles")==3 || $this->session->userdata("roles")==1): ?>
			            		<td style="text-align: right;"><?php echo price($key[0]->net)?></td>
			            		<?php endif ?>
			            		<?php if ($this->session->userdata("roles")!=4): ?>
			            		<?php foreach ($asuransi as $v): ?>
			            		<td style="text-align: right;"><?php echo price(cekDebiturAsdur($key[0]->tahun,$v->kodeAngka));?></td>
			            		<td style="text-align: right;"><?php echo price(cekPremiAsdur($key[0]->tahun,$v->kodeAngka));?></td>
			            		<?php endforeach ?>
			            		<?php endif ?>

			               	</tr>
			            	<?php 
			            		$no++; 
			            		$totalden = $totalden+$key[0]->jmldeb; 
		            			$totalplafon = $totalplafon+$key[0]->plafon; 
		            			$totalpremi = $totalpremi+$key[0]->premi; 
		            			$totalbroker = $totalbroker+$key[0]->brokerage; 
		            			$totalnet = $totalnet+$key[0]->net;
			            	endforeach ?>
			            </tbody>
			            <tfoot>
			            	<tr>
			            		<th colspan="2">TOTAL</th>
			            		<th style="text-align: center;"><?php echo $totalden?></th>
			            		<th style="text-align: right;"><?php echo price($totalplafon)?></th>
			            		<th style="text-align: right;"><?php echo price($totalpremi)?></th>
			            		<?php if (($this->session->userdata("roles")==3 && ($this->session->userdata("hak_akses")==3 || $this->session->userdata("hak_akses")==10))  || $this->session->userdata("roles")==1): ?>
			            		<th style="text-align: right;"><?php echo price($totalbroker)?></th>
			            		<?php //endif ?>
	            				<?php //if ($this->session->userdata("roles")==4 || $this->session->userdata("roles")==3 || $this->session->userdata("roles")==1): ?>
			            		<th style="text-align: right;"><?php echo price($totalnet)?></th>
			            		<?php endif ?>
			            		<?php if ($this->session->userdata("roles")!=4): ?>
			            		<?php foreach ($asuransi as $k): ?>
			            		<th style="text-align: right;"><?php echo price(cekDebiturAsdurTahun($tahun,$k->kodeAngka));?></th>
			            		<th style="text-align: right;"><?php echo price(cekPremiAsdurTahun($tahun,$k->kodeAngka));?></th>
			            		<?php endforeach ?>
			            		<?php endif ?>

			            	</tr>
			            </tfoot>
	            	</table>
	            </div>
            </div>
       	</div>
	</div>
</div>