<div class="row">
	<div class="col-md-12">
	    <div class="card">
	        <div class="card-body">
	        	<li class="pull-right">
				<img class="dark-logo" src="<?php echo base_url()?>assets/ela/images/logorep.png">
						</li>
						<!-- Lebel Laporan  -->
						<h4 class="card-title">
							<strong><?php echo strtoupper("produksi asuransi perbank");?></strong>
						</h4>
						<div class="row lable-laporan m-b-20">
							<div class="col-lg-4">
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Periode</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $periode?></div>
								</div>
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Asuradur</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $asuransi?></div>
								</div>
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Produk</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $produk?></div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<strong>Bank</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $bank?></div>
								</div>
							</div>
							<div class="col-lg-4"></div>
						</div>
	          	<div class="table-responsive" id="abc">                
	            	<table id='DataPeserta' class='display nowrap table-hover table-bordered f-s-12' cellspacing='0' width='100%'>
	            		<thead>
	            			<tr>
	    	        			<th>No</th>
	    	        			<th>KANTOR CABANG</th>
	    	        			<th>KANTOR OPERASIONAL</th>
	    	        			<th>JUMLAH DEBITUR</th>
	    	        			<th>UANG PERTANGGUNGAN</th>
	    	        			<th>PREMI</th>
	    	        			<?php if (($this->session->userdata("roles")==3 && ($this->session->userdata("hak_akses")==3 || $this->session->userdata("hak_akses")==10)) || $this->session->userdata("roles")==1): ?>
	    	        			<th>BROKERAGE</th>
		    	        		<?php //endif ?>
	    	        			<?php //if ($this->session->userdata("roles")==3 || $this->session->userdata("roles")==1 || $this->session->userdata("roles")==4): ?>
	    	        			<th>NET PREMI</th>
		    	        		<?php endif ?>
			            	</tr>
			            </thead>
			            <tbody>
			            	<?php 
			            	$n_debitur=0;
						    $plafon=0;
						    $premi_gross=0;
						    $brokerage=0;
						    $netpr=0;
			            	$no=1; 
			            	foreach ($debitur->result() as $key): ?>
			            	<?php 
			            		$bangpem = bangpem($key->kodeBank);
						    	if($bangpem->capem==NULL){
						    		$bangpem->capem = $bangpem->cabang;
						    	}
			            	?>
			            	<tr>
			            		<td style="text-align: center;"><?php echo $no;?></td>
			            		<td><?php echo $bangpem->cabang?></td>
						    	<td><?php echo $bangpem->capem?></td>
						    	<td style="text-align: center;"><?php echo $key->jmldeb?></td>
						    	<td style="text-align: right;"><?php echo price($key->pertanggungan)?></td>
						    	<td style="text-align: right;"><?php echo price($key->premi)?></td>
						    	<?php if (($this->session->userdata("roles")==3 && ($this->session->userdata("hak_akses")==3 || $this->session->userdata("hak_akses")==10)) || $this->session->userdata("roles")==1): ?>
	    	        			<th style="text-align: right;"><?php echo price($key->brokerage)?></th>
		    	        		<?php // endif ?>
	    	        			<?php //if ($this->session->userdata("roles")==3 || $this->session->userdata("roles")==1 || $this->session->userdata("roles")==4): ?>
	    	        			<th style="text-align: right;"><?php echo price($key->net)?></th>
		    	        		<?php endif ?>
			            	</tr>
			            		
			            	<?php 
			            	$n_debitur=$n_debitur+$key->jmldeb;
					        $plafon=$plafon+$key->pertanggungan;
					        $premi_gross=$premi_gross+$key->premi;
					        
					        
				            $brokerage=$brokerage+$key->brokerage;
				            $netpr=$netpr+$key->net;
				            $no++;
			            	endforeach ?>
			            </tbody>
			            <tfoot>
			            	<tr>
				            	<th style="text-align: right;" colspan="3"><b>TOTAL</b></th>
						    	<th style="text-align: center;"><b><?php echo $n_debitur?></b></th>
						    	<th style="text-align: right;"><b><?php echo price($plafon)?></b></th>
						    	<th style="text-align: right;"><b><?php echo price($premi_gross)?></b></th>
						    	<?php if (($this->session->userdata("roles")==3 && ($this->session->userdata("hak_akses")==3 || $this->session->userdata("hak_akses")==10)) || $this->session->userdata("roles")==1): ?>
	    	        			<th style="text-align: right;"><b><?php echo price($brokerage)?></b></th>
		    	        		<?php //endif ?>
	    	        			<?php //if ($this->session->userdata("roles")==3 || $this->session->userdata("roles")==1 || $this->session->userdata("roles")==4): ?>
	    	        			<th style="text-align: right;"><b><?php echo price($netpr)?></b></th>
	    	        			<?php endif ?>
				            </tr>
			            </tfoot>
	            	</table>
	            </div>
            </div>
       	</div>
	</div>
</div>