<div class="row">
	<div class="col-md-12">
	    <div class="card">
	        <div class="card-body">
	        	<li class="pull-right">
							<img class="dark-logo" src="<?php echo base_url()?>assets/ela/images/logorep.png">
						</li>
						<!-- Lebel Laporan  -->
						<h4 class="card-title">
							<strong><?php echo strtoupper("Rincian produksi");?></strong>
						</h4>
						<div class="row lable-laporan m-b-20">
							<div class="col-lg-4">
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Periode</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $periode?></div>
								</div>
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Asuradur</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $asuransi?></div>
								</div>
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Produk</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $produk?></div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<strong>Bank</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $bank?></div>
								</div>
							</div>
							<div class="col-lg-4"></div>
						</div>
	          	<div class="table-responsive" id="abc">                
	            	<table id='DataPeserta' class='display nowrap table-hover table-bordered f-s-12' cellspacing='0' width='100%'>
	            		<thead>
	            			<tr >
	            				<th></th>
	            				<th>KANTOR CABANG</th>
	            				<th>KANTOR OPERASIONAL</th>
	            				<th>ASURADUR / PENJAMIN</th>
	            				<th>NO. AKAD KREDIT</th>
	            				<th>PEKERJAAN</th>
	            				<th>NO. PINJAMAN</th>
	            				<th>DEBITUR</th>
	            				<th>TGL. LAHIR</th>
	            				<th>MULAI ASURANSI</th>
	            				<th>TENOR</th>
	            				<th>AKHIR ASURANSI</th>
	            				<th>UANG PERTANGGUNGAN</th>
	            				<th>RATE PREMI &permil;</th>
	            				<th>PREMI</th>
	            				<th>OS PREMI</th>
	            				<th>STATUS</th>
	            				<?php if (($this->session->userdata("roles")==2 && $this->session->userdata("hak_akses")==3) || $this->session->userdata("roles")==3 || $this->session->userdata("roles")==1): ?>
	            				<th>FEE BANK</th>
	            				<?php endif ?>
	            				<?php if (($this->session->userdata("roles")==3 && ($this->session->userdata("hak_akses")==3 || $this->session->userdata("hak_akses")==10)) || $this->session->userdata("roles")==1 ): ?>
	            				<th>BROKERAGE</th>
	            				<?php // endif ?>
	            				<?php // if ($this->session->userdata("roles")==4 || $this->session->userdata("roles")==3 || $this->session->userdata("roles")==1): ?>
	            				<th>NET PREMI</th>
	            				<?php endif ?>
	            			</tr>
	            		</thead>

	            			<tbody>
	            				<?php foreach ($debitur as $r): ?>
	            					<?php 
	            						$bangpem = bangpem($r->kodeBank);
								    	if($bangpem->capem==NULL){
								    		$bangpem->capem = $bangpem->cabang;
								    	}
								    ?>
							    	<tr>
							    	<td></td>
							    	<td><?php echo $bangpem->cabang?></td>
							    	<td><?php echo $bangpem->capem?></td>
							    	<td><?php echo cekNamaAsdur($r->kodeAsuransi)?></td>
							    	<td><?php echo $r->NomorPK?></td>
							    	<td><?php echo cekPekerjaan($r->KodePekerjaan)?></td>
							    	<td><?php echo $r->NomorPinjaman?></td>
							    	<td><?php echo $r->NamaDebitur?></td>
							    	<td><?php echo date_format(date_create($r->TglLahir),"Y-m-d")?></td>
							    	<td><?php echo date_format(date_create($r->TglAkadKredit),"Y-m-d")?></td>
							    	<td><?php echo ($r->TenorBulan)." Bulan";?></td>
							    	<td><?php echo date_format(date_create($r->TglAkhirKredit),"Y-m-d")?></td>
							    	<td><?php echo price($r->plafon)?></td>
							    	<td><?php echo str_replace('.',',',$r->TarifPremi*1000)?></td>
							    	<td><?php echo price($r->JumlahPremiTenor)?></td>
							    	<td><?php echo price($r->TotalOsPremi)?></td>
							    	<td><?php echo $r->TcKet?></td>
							    	<?php if (($this->session->userdata("roles")==2 && $this->session->userdata("hak_akses")==3) || $this->session->userdata("roles")==3 || $this->session->userdata("roles")==1): ?>
							    	<td><?php echo price($r->FeePremiBank)?></td>
							    	<?php endif ?>
							    	<?php if (($this->session->userdata("roles")==3 && ($this->session->userdata("hak_akses")==3 || $this->session->userdata("hak_akses")==10))  || $this->session->userdata("roles")==1): ?>
							    	<td><?php echo price($r->FeePremiBroker)?></td>
							    	<?php // endif ?>
							    	<?php // if ($this->session->userdata("roles")==4 || $this->session->userdata("roles")==3 || $this->session->userdata("roles")==1): ?>
							    	<td><?php echo net_premi($r->JumlahPremiTenor,$r->FeePremiBank,$r->FeePremiBroker)?></td>
							    	<?php endif ?>
							    	</tr>
	            				<?php endforeach ?>
	            			</tbody>
	            	</table>
	            </div>
            </div>
       	</div>
	</div>
</div>