<div class="row">
  <div class="col-12">
      <div class="card">
          <div class="card-body">
              <h4 class="card-title">Kwitansi Pembayaran Premi Asuransi</h4>
              <div class="table-responsive">
                  <table id="invoice" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                      <thead >
                          <tr >
                              <th>#</th>
                              <th>Tanggal Cetak</th>
                              <th>Total Debitur</th>
                              <th>Total Premi</th>
                              <th>Print</th>
                          </tr>
                      </thead>
                      <tbody>
                        
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>
