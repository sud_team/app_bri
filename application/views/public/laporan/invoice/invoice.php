<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Favicon icon -->
  <title>KWITANSI PEMBAYARAN PREMI ASURANSI KREDIT</title>
  <link href="<?php echo base_url()?>assets/ela/css/lib/invoice/invoice.css" rel="stylesheet">
  <style type="text/css">
    tr { 
      page-break-inside:avoid !important;
      margin: 4px 0 4px 0;
    }
  </style>

</head>

<body class="body-invoice">
  <header class="clearfix" style="width: 700px;">
      <div id="logo" class="" >
        <img src="<?php echo base_url()?>assets/ela/images/Logo-Agra.png">
      </div>
      <div id="alamat">Gedung Menara Palma, Lt.9-01 <br> Jl. HR Rasuna Said, Blok X2 Kav.6 
        <br> Jakarta 12950, INDONESIA
        <br> Telp. : +62 (21) 5793 0433
        <br> Fax : +62 (21) 5793 0432</div>
        <h1>KWITANSI PEMBAYARAN PREMI ASURANSI KREDIT</h1>
  
    <div id="project" style="width: 200px;">
      <div><span>NOMOR INVOICE</span>19 Mei 2018</div>
      <div><span> TANGGAL </span>19 Mei 2018</div>
  </header>
<div>
  <main>
      <table style="width: 700px;" border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th>#</th>
            <th>ASURANSI</th>
            <th>DEBITUR</th>
            <th>PREMI</th>
          </tr>
        </thead>
        <tbody>
          <tr>
              <td class="no">2</td>
              <td class="pinjaman">PR092KAR</td>
              <td class="cif" style="text-align: center;">10146605</td>
              <td class="nama"  style="text-align: right;">TURNO</td>
            </tr>
        </tbody>
      </table>
      <div id="total">
        <h3>Rincian Pembayaran</h3>
        <div>Tanggal Pembayaran<span style="text-align: right;margin-right: 120px;">20 Mei 2018</span></div>
        <div>Jumlah Debitur<span style="text-align: right;margin-right: 87px;">20</span></div>
        <div>Total Premi<span style="text-align: right;margin-right: 65px;">6.960.000</span></div>
      </div>
      
      <div id="notices">
        <div><strong>NOTICE</strong>:</div>
        <div class="notice">Bukti pembayaran sah pendaftaran peserta Asuransi Kredit Bank Jatim</div>
      </div>

      <div id="paraf" class="" >
        <img src="images/mtstagi.png">
        <div><strong>Sumitro Sitorus</strong></div>
        <div>Direktur Utama</div>
      </div>

  </main>
  </div>

  <?php $jml = count($debitur); 
    $b = $jml/15;
    $d = 15;
    $c = round($b,0);

    for ($i=1; $i <= $c ; $i++) { 
    
  ?>
  
  <div class="page_break">
    <header class="clearfix" style="width: 700px;">
      <div id="logo" class="" >
        <img src="<?php echo base_url()?>assets/ela/images/Logo-Agra.png">
      </div>
      <div id="alamat" >Wisma Bakrie 2, Lt.10 <br>Jl. HR Rasuna Said, Kav B-2 Karet 
        <br> Setiabudi Jakarta Selatan 12920
        <br> Telp. : +62 (21) 5793 0433
        <br> Fax : +62 (21) 5793 0432
      </div>
      <h1>INVOICE PEMBAYARAN PREMI ASURANSI KREDIT</h1>
    </header>
    <table style="width: 700px;" border="0" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <th>#</th>
          <th>NO. PINJAMAN</th>
          <th>DEBITUR</th>
          <th>PEKERJAAN</th>
          <th>TGL AKAD</th>
          <th>TENOR</th>
          <th>TOTAL PREMI</th>
          <th>ASURANSI</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=0; foreach ($debitur as $key): ?>
          <tr class="page-break">
            <td class="no"><?php echo $key->rank;?></td>
            <td class="pinjaman"><?php echo $key->NomorPinjaman;?></td>
            <td class="cif"><?php echo $key->NamaDebitur;?></td>
            <td class="nama"><?php echo cekPekerjaan($key->KodePekerjaan);?></td>
            <td class="tglk"><?php echo my_date_indo($key->TglAkadKredit);?></td>
            <td class="tenor"><?php echo $key->TenorTahun;?></td>
            <td class="plafon"><?php echo price($key->JumlahPremiTenor);?></td>
            <td class="premi"><?php echo cekNamaAsdur($key->kodeAsuransi);?></td>
          </tr>            
        <?php $no++; endforeach ?>
      </tbody>
    </table>
  </div>

    <?php } ?>
</body>

</html>