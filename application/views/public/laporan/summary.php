<div class="col-md-12">
    <div class="card">
        <div class="card-body p-b-0">
            <form id="FormSubmit" method="post" target="_blank" action="<?php echo base_url()?>laporan/cariSummary">
            <h4 class="card-title font-weight-bold text-warning m-b-20">SUMMARY REPORT</h4>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs customtab2" role="tablist">
                <li class="nav-item w-20 text-center">
                    <a class="nav-link active" data-toggle="tab" href="#summ1" id="tabsumm1" role="tab">
                        <span class="hidden-sm-up">P
                        <i class="ti-arrow-down"> </i>
                        </span>
                        <span class="hidden-xs-down font-weight-bold">PRODUKSI</span>
                    </a>
                </li>
                <li class="nav-item w-20 text-center">
                    <a class="nav-link" data-toggle="tab" href="#summ2"  id="tabsumm2" role="tab">
                        <span class="hidden-sm-up">R
                        <i class="ti-arrow-down"> </i>
                        </span>
                        <span class="hidden-xs-down font-weight-bold">RESTITUSI</span>
                    </a>
                </li>
                <li class="nav-item w-20 text-center">
                    <a class="nav-link" data-toggle="tab" href="#summ3"  id="tabsumm3" role="tab">
                        <span class="hidden-sm-up">K
                        <i class="ti-arrow-down"> </i>
                        </span>
                        <span class="hidden-xs-down font-weight-bold">KLAIM</span>
                    </a>
                </li>

            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="summ1" role="tabpanel">
                    <div class="p-20">
                        <h6>Menampilkan ringkasan laporan Produksi</h6>
                        <p> Tentukan filter pencarian pada kolom dibawah</p>
                    </div>

                </div>
                <div class="tab-pane " id="summ2" role="tabpanel">
                    <div class="p-20">
                        <h6>Menampilkan ringkasan laporan Restitusi</h6>
                        <p> Tentukan filter pencarian pada kolom dibawah</p>
                    </div>

                </div>
                <div class="tab-pane " id="summ3" role="tabpanel">
                    <div class="p-20">
                        <h6>Menampilkan ringkasan laporan Klaim</h6>
                        <p> Tentukan filter pencarian pada kolom dibawah</p>
                    </div>
                </div>
                <input type="hidden" name="tipe" id="tipe" value="">
                <div class="row m-b-20 col-md-8">
                <div class="text-left text-megna col-sm-5">
                    <small class="form-control-feedback f-s-10">Periode Upload</small>
                    <input id="tgl_mulai" type="text" class="custom-select f-s-12 datepicker" name="tgl_awal" placeholder="dd-mm-yyyy"></div>
                    <div class="text-left text-megna col-sm-5">
                    <small class="form-control-feedback f-s-10">Sampai dengan</small>
                    <input id="tgl_akhir" type="text" class="custom-select f-s-12 datepicker" name="tgl_akhir" placeholder="dd-mm-yyyy"></div>
                </div>
                <div class="row m-b-20 col-md-8">
                <div class="text-left text-megna col-sm-5">
                    <small class="form-control-feedback f-s-10">Periode rekonsel</small>
                    <input id="tgl_awalrekonsel1" type="text" class="custom-select f-s-12 datepicker" name="tgl_awalrekonsel1" placeholder="dd-mm-yyyy"></div>
                    <div class="text-left text-megna col-sm-5">
                    <small class="form-control-feedback f-s-10">Sampai dengan</small>
                    <input id="tgl_akhirrekonsel1" type="text" class="custom-select f-s-12 datepicker" name="tgl_akhirrekonsel1" placeholder="dd-mm-yyyy"></div>
                </div>
                <div class="row m-b-20 col-md-8">
                <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Produk</small>
                    <select class="js-example-basic-cabang form-control-feedback custom-select f-s-12" id="produk" name="produk">
                        <option value="">Semua</option>
                        <?php foreach ($produk as $key1): ?>
                        <option value="<?php echo $key1->idProduk?>"><?php echo $key1->NamaProduk?></option>
                        <?php endforeach ?>
                    </select></div>
                    <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Asuradur</small>
                    <select class="js-example-basic-cabang form-control-feedback custom-select f-s-12" id="asuransi" name="asuransi">
                        <option value="">Semua</option>
                        <?php foreach ($asuradur as $key): ?>
                        <option value="<?php echo $key->kodeAngka?>"><?php echo $key->asuransi?></option>
                        <?php endforeach ?>
                    </select></div>
                </div>
                <div class="row m-b-20 col-md-8">
                    <?php if (cekOpBank($this->session->userdata("kodeBank"))!="CAPEM"): ?>
                          <?php if (cekOpBank($this->session->userdata("kodeBank"))=="CABANG"): ?>
                                <input type="hidden" placeholder="Nama" id="cabang" name="cabang" class="col-xs-3 custom-select f-s-12 " value="<?php echo $this->session->userdata('kodeBank');?>" readonly>
                                
                                <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Kantor Cabang</small>
                        <input type="text" placeholder="Nama" id="cabang1" name="cabang1" class="custom-select f-s-12" value="<?php echo cekNamaBank($this->session->userdata('kodeBank'));?>" readonly></div>                       
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Kantor Operasional</small>
                        <select class="js-example-basic-cabang custom-select f-s-12" id="capem" name="capem">
                            <option value="">Semua</option>
                        </select> </div>
                          <?php else: ?>
                          <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Kantor Cabang</small>
                        <select class="js-example-basic-cabang custom-select f-s-12" id="cabang" name="cabang">
                            <option value="">Semua</option>
                            <?php foreach ($cabang as $key): ?>
                            <option value="<?php echo $key->kodeBank?>"><?php echo $key->kodeBank.'-'.$key->cabang;?></option>
                            <?php endforeach ?>
                        </select> </div>                       
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Kantor Operasional</small>
                        <select class="js-example-basic-cabang custom-select f-s-12" id="capem" name="capem">
                            <option value="">Semua</option>
                        </select></div>
                          <?php endif ?>
                        <?php endif ?>

                    <?php /*<div class=" f-s-12 col-sm">Kantor Cabang</div>
                    <select class="js-example-basic-cabang custom-select f-s-12" id="cabang" name="cabang">
                        <option value="">Semua</option>
                        <?php foreach ($cabang as $key): ?>
                        <option value="<?php echo $key->kodeBank?>"><?php echo $key->kodeBank.'-'.$key->cabang;?></option>
                        <?php endforeach ?>
                    </select> 
                    <div class=" f-s-12 col-sm">Kantor Operasional</div>
                    <select class="js-example-basic-cabang custom-select f-s-12" id="capem" name="capem">
                        <option value="">Semua</option>
                    </select>*/?>
                </div>
                <div class="m-b-20 col-sm-5 ">
                        <button type="submit" class="btn btn-info f-s-12 ">
                            <i class="fa fa-search"></i> Tampilkan </button>
                    </div>
                   
            </div>
            </form>
        </div>
    </div>
</div>
