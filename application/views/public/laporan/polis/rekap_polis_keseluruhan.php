<div class="row">
	<div class="col-md-12">
	    <div class="card">
	        <div class="card-body">
	        	<li class="pull-right">
				<img class="dark-logo" src="<?php echo base_url()?>assets/ela/images/logorep.png">
						</li>
						<!-- Lebel Laporan  -->
						<h4 class="card-title">
							<strong><?php echo strtoupper("rekap polis keseluruhan");?></strong>
						</h4>
						<div class="row lable-laporan m-b-20">
							<div class="col-lg-4">
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Periode</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $periode?></div>
								</div>
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Asuradur</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $asuransi?></div>
								</div>
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Produk</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $produk?></div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<strong>Bank</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $bank?></div>
								</div>
							</div>
							<div class="col-lg-4"></div>
						</div>
	          	<div class="table-responsive" id="abc">                
	            	<table id='DataPeserta' class='display nowrap table-hover table-bordered f-s-12' cellspacing='0' width='100%'>
	            		<thead>
	            			<tr >			
		                        <th style="text-align:center" rowspan="3">ASURADUR</th>
								<th style="text-align:center;"  colspan="3">PENGAJUAN PESERTA ASURANSI</th>
								<th style="text-align:center;"  colspan="3">PENERBITAN POLIS</th>
								<th style="text-align:center;"  colspan="9">POLIS BELUM TERBIT</th>
							</tr>

							<tr >
								<th style="" rowspan="2">DEBITUR</th>
								<th style=" text-align:center" rowspan="2">UANG PERTANGGUNGAN</th>
								<th style=" text-align:center" rowspan="2">%</th>
								<th style="" rowspan="2">DEBITUR</th>
								<th style=" text-align:center" rowspan="2">UANG PERTANGGUNGAN</th>
								<th style=" text-align:center" rowspan="2">%</th>
								<th style=" text-align:center" colspan="2">DEBITUR</th>
								<th style=" text-align:center" colspan="2">UANG PERTANGGUNGAN</th>
		                        <th style=" text-align:center" colspan="2">%</th>
								<th style="text-align:center" colspan="3">TOTAL</th>
					        </tr>
							<tr>
							  	<th style=" text-align:center">SLA</th>
							  	<th style="text-align:center">NON SLA</th>
							  	<th style="text-align:center">SLA</th>
							  	<th style=" text-align:center">NON SLA</th>
							  	<th style=" text-align:center">SLA</th>
							  	<th style=" text-align:center">NON SLA</th>
							  	<th style=" text-align:center">DEBITUR</th>
							  	<th style=" text-align:center">UP</th>
							  	<th style="text-align:center">%</th>
						  	</tr>    
			            </thead>
			            <tbody>
			            	<?php 
			            	$totaljmldebajukan = 0;
			            	$totaljmlpengajuanplafon = 0;
			            	$totalpersenajukan = 0;
			            	$totaljmldebjalan = 0;
			            	$totaljmlpengajuanjalan = 0;
			            	$totalpersenterbit = 0;
			            	$totaluangnonterbitsla = 0;
			            	$totaluangnonterbitnonsla = 0;
			            	$totalpersensla = 0;
			            	$totalpersennonsla = 0;
			            	$totaldebnoterbit = 0;
			            	$totalplafonnoterbit = 0;
			            	$totalpersennoterbit = 0;

			            	foreach ($debitur->result() as $key): 
			            		if($key->jmlpengajuanplafon==0){
			            			$persenajukan = 0;
			            		}else{
			            			$persenajukan = ($key->jmlpengajuanplafon/$pengajuan)*100;
			            		}

			            		if($key->jmlpengajuanjalan==0){
			            			$persenterbit = 0;
			            		}else{
			            			$persenterbit = ($key->jmlpengajuanjalan/$terbit)*100;
			            		}

			            		if(cekSlaDebitur2($tahun,$key->kodeAsuransi)==0){
			            			$persendebitur = 0;
			            		}else{
			            			$persendebitur = (cekSlaDebitur2($tahun,$key->kodeAsuransi)/$debitursla)*100;
			            		}

			            		if(cekNonSlaPlafon2($tahun,$key->kodeAsuransi)==0){
			            			$persendebiturnon = 0;
			            		}else{
			            			$persendebiturnon = (cekNonSlaDebitur2($tahun,$key->kodeAsuransi)/$debiturnonsla)*100;
			            		}

			            	?>
		            		<tr>
			            		<td><?php echo cekNamaAsdur($key->kodeAsuransi)?></td>
			            		<td style="text-align:right;"><?php if(empty($key->jmldebajukan)){ echo "0";}else{ echo $key->jmldebajukan; }?></td>
			            		<td style="text-align:right;"><?php if(empty($key->jmlpengajuanplafon)){ echo "0";}else{ echo price($key->jmlpengajuanplafon); }?></td>
			            		<td style="text-align:right;"><?php echo round($persenajukan,2);?></td>
			            		<td style="text-align:right;"><?php if(empty($key->jmldebjalan)){ echo "0";}else{ echo $key->jmldebjalan; }?></td>
			            		<td style="text-align:right;"><?php if(empty($key->jmlpengajuanjalan)){ echo "0";}else{ echo price($key->jmlpengajuanjalan); }?></td>
			            		<td style="text-align:right;"><?php echo round($persenterbit,2);?></td>
			            		<td style="text-align:right;"><?php echo cekSlaDebitur2($tahun,$key->kodeAsuransi)?></td>
			            		<td style="text-align:right;"><?php echo cekNonSlaDebitur2($tahun,$key->kodeAsuransi)?></td>
			            		<td style="text-align:right;"><?php echo price(cekSlaPlafon2($tahun,$key->kodeAsuransi))?></td>
			            		<td style="text-align:right;"><?php echo price(cekNonSlaPlafon2($tahun,$key->kodeAsuransi))?></td>
			            		<td style="text-align:right;"><?php echo round($persendebitur,2)?></td>
			            		<td style="text-align:right;"><?php echo round($persendebiturnon,2)?></td>
			            		<td style="text-align:right;"><?php echo cekSlaDebitur2($tahun,$key->kodeAsuransi)+cekNonSlaDebitur2($tahun,$key->kodeAsuransi)?></td>
			            		<td style="text-align:right;"><?php echo price(cekSlaPlafon2($tahun,$key->kodeAsuransi)+cekNonSlaPlafon2($tahun,$key->kodeAsuransi))?></td>
			            		<td style="text-align:right;"><?php echo round($persendebitur+$persendebiturnon,2)?></td>
			            	</tr>
			            	<?php 
			            	$totaljmldebajukan = $totaljmldebajukan+$key->jmldebajukan;
			            	$totaljmlpengajuanplafon = $totaljmlpengajuanplafon+$key->jmlpengajuanplafon;
			            	$totalpersenajukan = $totalpersenajukan+$persenajukan;
			            	$totaljmldebjalan = $totaljmldebjalan+$key->jmldebjalan;
			            	$totaljmlpengajuanjalan = $totaljmlpengajuanjalan+$key->jmlpengajuanjalan;
			            	$totalpersenterbit = $totalpersenterbit+$persenterbit;

			            	$totaluangnonterbitsla = $totaluangnonterbitsla+cekSlaPlafon2($tahun,$key->kodeAsuransi);
			            	$totaluangnonterbitnonsla = $totaluangnonterbitnonsla+cekNonSlaPlafon2($tahun,$key->kodeAsuransi);

			            	$totalpersensla = $totalpersensla+$persendebitur;
			            	$totalpersennonsla = $totalpersennonsla+$persendebiturnon;

			            	$totaldebnoterbit = $totaldebnoterbit+(cekSlaDebitur2($tahun,$key->kodeAsuransi)+cekNonSlaDebitur2($tahun,$key->kodeAsuransi));
			            	$totalplafonnoterbit = $totalplafonnoterbit+(cekSlaPlafon2($tahun,$key->kodeAsuransi)+cekNonSlaPlafon2($tahun,$key->kodeAsuransi));
			            	$totalpersennoterbit = $totalpersennoterbit+($persendebitur+$persendebiturnon);

				            endforeach ?>
			            </tbody>
			            <tfoot>
			            	<tr>
			            		<th>TOTAL</th>
			            		<th style="text-align:right;"><?php echo $totaljmldebajukan;?></th>
			            		<th style="text-align:right;"><?php echo price($totaljmlpengajuanplafon);?></th>
			            		<th style="text-align:right;"><?php echo round($totalpersenajukan,0);?></th>
			            		<th style="text-align:right;"><?php echo $totaljmldebjalan;?></th>
			            		<th style="text-align:right;"><?php echo price($totaljmlpengajuanjalan);?></th>
			            		<th style="text-align:right;"><?php echo round($totalpersenterbit,0);?></th>
			            		<th style="text-align:right;"><?php echo $debitursla;?></th>
			            		<th style="text-align:right;"><?php echo $debiturnonsla;?></th>
			            		<th style="text-align:right;"><?php echo price($totaluangnonterbitsla);?></th>
			            		<th style="text-align:right;"><?php echo price($totaluangnonterbitnonsla);?></th>
			            		<th style="text-align:right;"><?php echo round($totalpersensla,0);?></th>
			            		<th style="text-align:right;"><?php echo round($totalpersennonsla,0);?></th>
			            		<th style="text-align:right;"><?php echo $totaldebnoterbit;?></th>
			            		<th style="text-align:right;"><?php echo price($totalplafonnoterbit);?></th>
			            		<th style="text-align:right;"><?php echo round($totalpersennoterbit,0);?></th>
			            	</tr>
			            </tfoot>
	            	</table>
	            </div>
            </div>
       	</div>
	</div>
</div>