<div class="row">
	<div class="col-md-12">
	    <div class="card">
	        <div class="card-body">
	        	<li class="pull-right">
				<img class="dark-logo" src="<?php echo base_url()?>assets/ela/images/logorep.png">
						</li>
						<!-- Lebel Laporan  -->
						<h4 class="card-title">
							<strong><?php echo strtoupper("Rincian polis");?></strong>
						</h4>
						<div class="row lable-laporan m-b-20">
							<div class="col-lg-4">
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Periode</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $periode?></div>
								</div>
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Asuradur</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $asuransi?></div>
								</div>
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Produk</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $produk?></div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<strong>Bank</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $bank?></div>
								</div>
							</div>
							<div class="col-lg-4"></div>
						</div>
	          	<div class="table-responsive" id="abc">                
	            	<table id='DataPeserta' class='display nowrap table-hover table-bordered f-s-12' cellspacing='0' width='100%'>
	            		<thead>
	            			<tr>
	    	        			<th>No</th>
	    	        			<th>KANTOR CABANG</th>
	    	        			<th>KANTOR OPERASIONAL</th>
	    	        			<th>NOMOR AKAD KREDIT</th>
	    	        			<th>NAMA DEBITUR</th>
	    	        			<th>TGL LAHIR</th>
	    	        			<th>TGL PENGAJUAN</th>
	    	        			<th>ASURANSI</th>
	    	        			<th>NOMOR POLIS</th>
	    	        			<th>UANG PERTANGGUNGAN</th>
	    	        			<th>LAMA PROSES</th>
	    	        			<th>STATUS PENERBITAN POLIS</th>
			            	</tr>
			            </thead>
			            <tbody>
			            	<?php $no=1; foreach ($debitur->result() as $key): ?>
			            	<?php 
			            		$bangpem = bangpem($key->kodeBank);
						    	if($bangpem->capem==NULL){
						    		$bangpem->capem = $bangpem->cabang;
						    	}
			            	?>
			            	<tr>
			            		<td style="text-align: center;"><?php echo $no;?></td>
			            		<td><?php echo $bangpem->cabang?></td>
						    	<td><?php echo $bangpem->capem?></td>
						    	<td><?php echo $key->NomorPK?></td>
						    	<td><?php echo $key->NamaDebitur?></td>
						    	<td><?php echo my_date_indo($key->TglLahir)?></td>
						    	<td><?php echo my_date_indo($key->create_date)?></td>
						    	<td><?php echo cekNamaAsdur($key->kodeAsuransi)?></td>
						    	<td><?php echo cekPolis($key->NomorRegistrasi)?></td>
						    	<td style="text-align: right;"><?php echo price($key->plafon)?></td>
						    	<td><?php
						    			$NomorRegistrasi = $key->NomorRegistrasi;
						    			if($key->StatusPolis==0){ 
						    				echo lead_time(hariini(),$key->TglRekonsel);
						    			}else{
						    				$tgl_polis = tgl_polis($NomorRegistrasi);
						    				echo lead_time_stop($tgl_polis,$key->TglRekonsel);
						    			}
						    		?>	
						    	</td>
						    		<?php 
						    		if($key->StatusPolis==0){ 
						    			
						    			echo "<td style='color:red;'>Belum Terbit</td>"; 
						    		}else{
						    			echo "<td>Sudah Terbit</td>";
						    		}

						    		?>
						    		
			            	</tr>
			            	<?php $no++; endforeach ?>
			            </tbody>
	            	</table>
	            </div>
            </div>
       	</div>
	</div>
</div>