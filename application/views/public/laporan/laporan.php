 <div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <form id="FormSubmit" method="post" target="_blank" action="<?php echo base_url()?>laporan/cariProduksi">
            <h4 class="card-title font-weight-bold text-info">PRODUKSI</h4>
            <!-- Nav tabs -->
            <div class="vtabs">
                <ul class="nav nav-tabs tabs-vertical" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#lapro1" id="tablapro1" role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-check"> 1</i>
                            </span>
                            <span class="hidden-xs-down"> Rincian Data Produksi Asuransi
                            </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lapro2" id="tablapro2" role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-check"> 2</i>
                            </span>
                            <span class="hidden-xs-down"> Rekap Pertanggungan Bulanan Per Asuransi
                            </span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lapro3" id="tablapro3" role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-check"> 3</i>
                            </span>
                            <span class="hidden-xs-down"> Produksi Asuransi per Bank
                            </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lapro4" id="tablapro4" role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-check"> 4</i>
                            </span>
                            <span class="hidden-xs-down"> Rekap Produksi Asuransi Tahunan
                            </span>
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content w-75">
                    <div class="tab-pane" id="lapro0" role="tabpanel">
                        <div class="p-20">
                            <h4 class="text-primary">Menampilkan seluruh laporan Produksi</h4>
                            <h6>Penarikan data terhitung dari seluruh Produksi yang telah diterima</h6>
                        </div>
                    </div>
                    <input type="hidden" name="tipe" id="tipe" value="">
                    <div class="tab-pane active" id="lapro1" role="tabpanel">
                        <div>
                            <h6>Menampilkan laporan data produksi per debitur secara rinci</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-sm-2">Dari</div>
                            <input id="tgl_mulai" type="text" class="col-sm custom-select f-s-12 datepicker" name="tgl_awal" placeholder="dd-mm-yyyy">
                            <div class="col-sm-2">s/d</div>
                            <input id="tgl_akhir" type="text" class="col-sm custom-select f-s-12 datepicker" name="tgl_akhir" placeholder="dd-mm-yyyy">
                        </div>
                    </div>
                    <div class="tab-pane " id="lapro2" role="tabpanel">
                        <div>
                            <h6>Menampilkan laporan rekap pertanggungan per asuransi dalam sebulan</h6>
                            <p>Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-sm-2">Pilih Bulan</div>
                            <input type="text" class="col-sm-4 custom-select f-s-12 datepicker2" name="bulan" placeholder="mm-yyyy">
                        </div>
                    </div>
                    <div class="tab-pane " id="lapro3" role="tabpanel">
                        <div>
                            <h6>Laporan produksi asuransi per bank (Kantor Pusat, Wilayah Cabang dan Cabang Pembantu)
                            </h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-sm-2">Dari</div>
                            <input id="tgl_mulai" type="text" class="col-sm custom-select f-s-12 datepicker" name="tgl_awal2" placeholder="dd-mm-yyyy">
                            <div class="col-sm-2">s/d</div>
                            <input id="tgl_akhir" type="text" class="col-sm custom-select f-s-12 datepicker" name="tgl_akhir2" placeholder="dd-mm-yyyy">
                        </div>
                    </div>
                    <div class="tab-pane " id="lapro4" role="tabpanel">
                        <div>
                            <h6>Laporan Produksi Asuransi dalam setahun</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-sm-2">Pilih Tahun</div>
                            <input type="text" class="col-sm-4 custom-select f-s-12 datepicker3" name="tahun" placeholder="yyyy">
                        </div>
                    </div>
                    <div class="row m-b-20">
                        <div class=" f-s-12 col-sm">Produk</div>
                        <select class="js-example-basic-cabang form-control-feedback custom-select f-s-12 col-sm-4" id="produk" name="produk">
                            <option value="">Pilih</option>
                            <?php foreach ($produk as $key1): ?>
                            <option value="<?php echo $key1->idProduk?>"><?php echo $key1->NamaProduk?></option>
                            <?php endforeach ?>
                        </select>
                        <div class=" f-s-12 col-sm">Asuradur</div>
                        <select class="js-example-basic-cabang form-control-feedback custom-select f-s-12 col-sm-4" id="asuransi" name="asuransi">
                            <option value="">Pilih</option>
                            <?php foreach ($asuradur as $key): ?>
                            <option value="<?php echo $key->idAsuransi?>"><?php echo $key->asuransi?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="row m-b-20">
                        

                       <?php if (cekOpBank($this->session->userdata("kodeBank"))!="CAPEM"): ?>
                          <?php if (cekOpBank($this->session->userdata("kodeBank"))=="CABANG"): ?>
                                <input type="hidden" placeholder="Nama" id="cabang" name="cabang" class="col-xs-3 custom-select f-s-12 " value="<?php echo $this->session->userdata('kodeBank');?>" readonly>
                                
                            <div class=" f-s-12 col-sm">Kantor Cabang</div>
                        <input type="text" placeholder="Nama" id="cabang1" name="cabang1" class="col-sm-4 custom-select f-s-12" value="<?php echo cekNamaBank($this->session->userdata('kodeBank'));?>" readonly>                       
                    <div class=" f-s-12 col-sm">Kantor Operasional</div>
                        <select class="js-example-basic-cabang col-sm-4 custom-select f-s-12" id="capem" name="capem">
                            <option value="">Semua</option>
                        </select> 
                          <?php else: ?>
                            <div class=" f-s-12 col-sm">Kantor Cabang</div>
                        <select class="js-example-basic-cabang col-sm-4 custom-select f-s-12" id="cabang" name="cabang">
                            <option value="">Semua</option>
                            <?php foreach ($cabang as $key): ?>
                            <option value="<?php echo $key->kodeBank?>"><?php echo $key->kodeBank.'-'.$key->cabang;?></option>
                            <?php endforeach ?>
                        </select>                        
                        <div class=" f-s-12 col-sm">Kantor Operasional</div>
                        <select class="js-example-basic-cabang col-sm-4 custom-select f-s-12" id="capem" name="capem">
                            <option value="">Semua</option>
                        </select>
                          <?php endif ?>
                        <?php endif ?>
                        
                    </div>
                    <div class="row m-b-20">
                        <div class="col-sm">
                            <input class="btn btn-info f-s-12" type="submit" name="tampilkan" target="_blank" value="Tampilkan">
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- POLIS -->
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <form id="FormSubmit" method="post" target="_blank" action="<?php echo base_url()?>laporan/cariPolis">
            <h4 class="card-title font-weight-bold text-info">POLIS</h4>
            <!-- Nav tabs -->
            <div class="vtabs">
                <ul class="nav nav-tabs tabs-vertical" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" id="tabpolis1" href="#lapol1" role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-check"> 1</i>
                            </span>
                            <span class="hidden-xs-down">Rincian Polis</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lapol2" id="tabpolis2" role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-check"> 2</i>
                            </span>
                            <span class="hidden-xs-down">Rekap Polis Bulanan</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lapol3" id="tabpolis3" role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-check"> 3</i>
                            </span>
                            <span class="hidden-xs-down">Rekap Polis Keseluruhan</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lapol4" id="tabpolis4" role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-check"> 4</i>
                            </span>
                            <span class="hidden-xs-down"> Rekap Data Penerbitan Polis Tahunan</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lapol5" id="tabpolis5" role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-check"> 5</i>
                            </span>
                            <span class="hidden-xs-down">Penerbitan Polis per Bank</span>
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <input type="hidden" name="tipe" id="tipepolis" value="">
                <div class="tab-content w-75">
                    <div class="tab-pane active" id="lapol1" role="tabpanel">
                        <div>
                            <h6>Laporan rincian polis per Bank sesuai periode yang ditentukan</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-sm-2">Dari</div>
                            <input id="tgl_mulai" type="text" class="col-sm custom-select f-s-12 datepicker" name="tgl_awal" placeholder="dd-mm-yyyy">
                            <div class="col-sm-2">s/d</div>
                            <input id="tgl_akhir" type="text" class="col-sm custom-select f-s-12 datepicker" name="tgl_akhir" placeholder="dd-mm-yyyy">
                        </div>
                    </div>
                    <div class="tab-pane " id="lapol2" role="tabpanel">
                        <div>
                            <h6>Rekapitulasi penerbitan polis per Asuradur dalam sebulan</h6>
                            <p>Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-sm-2">Pilih Bulan</div>
                             <input id="tgl_mulai" type="text" class="col-sm-4 custom-select f-s-12 datepicker2" name="bulan" placeholder="dd-mm-yyyy">
                        </div>
                    </div>
                    <div class="tab-pane " id="lapol3" role="tabpanel">
                        <div>
                            <h6>Rekapitulasi penerbitan polis per Asuradur dari awal sampai periode yang ditentukan</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">

                            <div class="col-sm-2">Tanggal Akhir</div>
                            <input id="tgl_akhir" type="text" class="col-sm-4 custom-select f-s-12 datepicker" name="tgl_akhir2" placeholder="dd-mm-yyyy">
                        </div>
                    </div>
                    <div class="tab-pane " id="lapol4" role="tabpanel">
                        <div>
                            <h6>Rekapitulasi penerbitan polis per bulan dalam setahun</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-sm-2">Pilih Tahun</div>
                            <input type="text" class="col-sm-4 custom-select f-s-12 datepicker3" name="tahun" placeholder="dd-mm-yyyy">
                        </div>
                    </div>
                    <div class="tab-pane " id="lapol5" role="tabpanel">
                        <div>
                            <h6>Laporan penerbitan polis per Bank (Kantor Pusat, Wilayah, Cabang dan Cabang Pembantu)</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-sm-2">Dari</div>
                            <input id="tgl_mulai" type="text" class="col-sm custom-select f-s-12 datepicker" name="tgl_awal3" placeholder="dd-mm-yyyy">
                            <div class="col-sm-2">s/d</div>
                            <input id="tgl_akhir" type="text" class="col-sm custom-select f-s-12 datepicker" name="tgl_akhir3" placeholder="dd-mm-yyyy">
                        </div>
                    </div>
                    <div class="row m-b-20">
                        <div class=" f-s-12 col-sm">Produk</div>
                        <select class="js-example-basic-cabang form-control-feedback custom-select f-s-12 col-sm-4" id="produk" name="produk">
                            <option value="">Pilih</option>
                            <?php foreach ($produk as $key1): ?>
                            <option value="<?php echo $key1->idProduk?>"><?php echo $key1->NamaProduk?></option>
                            <?php endforeach ?>
                        </select>
                        <div class=" f-s-12 col-sm">Asuradur</div>
                        <select class="js-example-basic-cabang form-control-feedback custom-select f-s-12 col-sm-4" id="asuransi" name="asuransi">
                            <option value="">Pilih</option>
                            <?php foreach ($asuradur as $key): ?>
                            <option value="<?php echo $key->idAsuransi?>"><?php echo $key->asuransi?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="row m-b-20">
                        <?php if (cekOpBank($this->session->userdata("kodeBank"))!="CAPEM"): ?>
                          <?php if (cekOpBank($this->session->userdata("kodeBank"))=="CABANG"): ?>
                                <input type="hidden" placeholder="Nama" id="cabang2" name="cabang" class="col-xs-3 custom-select f-s-12 " value="<?php echo $this->session->userdata('kodeBank');?>" readonly>
                                
                            <div class=" f-s-12 col-sm">Kantor Cabang</div>
                        <input type="text" placeholder="Nama" id="cabang2" name="cabang2" class="col-sm-4 custom-select f-s-12" value="<?php echo cekNamaBank($this->session->userdata('kodeBank'));?>" readonly>                       
                    <div class=" f-s-12 col-sm">Kantor Operasional</div>
                        <select class="js-example-basic-cabang col-sm-4 custom-select f-s-12" id="capem2" name="capem">
                            <option value="">Semua</option>
                        </select> 
                          <?php else: ?>
                            <div class=" f-s-12 col-sm">Kantor Cabang</div>
                        <select class="js-example-basic-cabang col-sm-4 custom-select f-s-12" id="cabang2" name="cabang">
                            <option value="">Semua</option>
                            <?php foreach ($cabang as $key): ?>
                            <option value="<?php echo $key->kodeBank?>"><?php echo $key->kodeBank.'-'.$key->cabang;?></option>
                            <?php endforeach ?>
                        </select>                        
                        <div class=" f-s-12 col-sm">Kantor Operasional</div>
                        <select class="js-example-basic-cabang col-sm-4 custom-select f-s-12" id="capem2" name="capem">
                            <option value="">Semua</option>
                        </select>
                          <?php endif ?>
                        <?php endif ?>
                        
                    </div>
                    <div class="row m-b-20">
                        
                        <div class="row m-b-20">
                            <div class="col-sm">
                                <input class="btn btn-info f-s-12" type="submit" name="tampilkan" target="_blank" value="Tampilkan">
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- RESTITUSI -->
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title font-weight-bold text-info">RESTITUSI</h4>
            <!-- Nav tabs -->
            <form id="FormSubmit" method="post" target="_blank" action="<?php echo base_url()?>laporan/cariRestitusi">
            <div class="vtabs">
                <ul class="nav nav-tabs tabs-vertical" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#lares1" id="tablares1" role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-check"> 1</i>
                            </span>
                            <span class="hidden-xs-down">Rincian Pengajuan Restitusi</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lares2" id="tablares2"  role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-check"> 2</i>
                            </span>
                            <span class="hidden-xs-down">Rekap Restitusi Bulanan</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lares3" id="tablares3" role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-check"> 3</i>
                            </span>
                            <span class="hidden-xs-down">Rekap Restitusi Keseluruhan</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lares4" id="tablares4" role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-check"> 4</i>
                            </span>
                            <span class="hidden-xs-down">Rekap Data Pengajuan Restitusi Tahunan</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lares5" id="tablares5"  role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-check"> 5</i>
                            </span>
                            <span class="hidden-xs-down">Penyelesaian Restitusi per Bank</span>
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <input type="hidden" name="tipe" id="tiperes" value="">
                <div class="tab-content w-75">
                    <div class="tab-pane active" id="lares1" role="tabpanel">
                        <div>
                            <h6>Laporan rincian pengajuan Restitusi per SLA sesuai periode yang ditentukan</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-sm-2">Dari</div>
                            <input id="tgl_mulai" type="text" class="col-sm custom-select f-s-12 datepicker" name="tgl_awal" placeholder="dd-mm-yyyy">
                            <div class="col-sm-2">s/d</div>
                            <input id="tgl_akhir" type="text" class="col-sm custom-select f-s-12 datepicker" name="tgl_akhir" placeholder="dd-mm-yyyy">
                        </div>
                    </div>
                    <div class="tab-pane " id="lares2" role="tabpanel">
                        <div>
                            <h6>Rekapitulasi Restitusi per Asuradur dalam sebulan</h6>
                            <p>Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-sm-2">Pilih Bulan</div>
                            <input type="text" class="col-sm-4 custom-select f-s-12 datepicker2" name="bulan" placeholder="mm-yyyy">
                        </div>
                    </div>
                    <div class="tab-pane " id="lares3" role="tabpanel">
                        <div>
                            <h6>Rekapitulasi Restitusi per Asuradur dari awal sampai periode yang ditentukan</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-sm-2">Tanggal Akhir</div>
                            <input type="text" class="col-sm-4 custom-select f-s-12 datepicker" name="tgl_akhir3" placeholder="dd-mm-yyyy">
                        </div>
                    </div>
                    <div class="tab-pane " id="lares4" role="tabpanel">
                        <div>
                            <h6>Rekapitulasi data pengajuan Restitusi dalam setahun</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-sm-2">Pilih Tahun</div>
                            <input type="text" class="col-sm-4 custom-select f-s-12 datepicker3" name="tahun2" placeholder="yyyy">
                        </div>
                    </div>
                    <div class="tab-pane " id="lares5" role="tabpanel">
                        <div>
                            <h6>Penyelesaian Restitusi per Bank (Kantor Pusat, Wilayah, Cabang dan Cabang pembantu)</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-sm-2">Dari</div>
                            <input id="tgl_mulai" type="text" class="col-sm custom-select f-s-12 datepicker" name="tgl_awal2" placeholder="dd-mm-yyyy">
                            <div class="col-sm-2">s/d</div>
                            <input id="tgl_akhir" type="text" class="col-sm custom-select f-s-12 datepicker" name="tgl_akhir2" placeholder="dd-mm-yyyy">
                        </div>
                    </div>
                    <div class="row m-b-20">
                        <div class=" f-s-12 col-sm">Produk</div>
                        <select class="js-example-basic-cabang form-control-feedback custom-select f-s-12 col-sm-4" id="produk" name="produk">
                            <option value="">Pilih</option>
                            <?php foreach ($produk as $key1): ?>
                            <option value="<?php echo $key1->idProduk?>"><?php echo $key1->NamaProduk?></option>
                            <?php endforeach ?>
                        </select>
                        <div class=" f-s-12 col-sm">Asuradur</div>
                        <select class="js-example-basic-cabang form-control-feedback custom-select f-s-12 col-sm-4" id="asuransi" name="asuransi">
                            <option value="">Pilih</option>
                            <?php foreach ($asuradur as $key): ?>
                            <option value="<?php echo $key->idAsuransi?>"><?php echo $key->asuransi?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="row m-b-20">
                        <?php if (cekOpBank($this->session->userdata("kodeBank"))!="CAPEM"): ?>
                          <?php if (cekOpBank($this->session->userdata("kodeBank"))=="CABANG"): ?>
                                <input type="hidden" placeholder="Nama" id="cabang3" name="cabang" class="col-xs-3 custom-select f-s-12 " value="<?php echo $this->session->userdata('kodeBank');?>" readonly>
                                
                            <div class=" f-s-12 col-sm">Kantor Cabang</div>
                        <input type="text" placeholder="Nama" id="cabang3" name="cabang3" class="col-sm-4 custom-select f-s-12" value="<?php echo cekNamaBank($this->session->userdata('kodeBank'));?>" readonly>                       
                    <div class=" f-s-12 col-sm">Kantor Operasional</div>
                        <select class="js-example-basic-cabang col-sm-4 custom-select f-s-12" id="capem3" name="capem">
                            <option value="">Semua</option>
                        </select> 
                          <?php else: ?>
                            <div class=" f-s-12 col-sm">Kantor Cabang</div>
                        <select class="js-example-basic-cabang col-sm-4 custom-select f-s-12" id="cabang3" name="cabang">
                            <option value="">Semua</option>
                            <?php foreach ($cabang as $key): ?>
                            <option value="<?php echo $key->kodeBank?>"><?php echo $key->kodeBank.'-'.$key->cabang;?></option>
                            <?php endforeach ?>
                        </select>                        
                        <div class=" f-s-12 col-sm">Kantor Operasional</div>
                        <select class="js-example-basic-cabang col-sm-4 custom-select f-s-12" id="capem3" name="capem">
                            <option value="">Semua</option>
                        </select>
                          <?php endif ?>
                        <?php endif ?>
                        
                    </div>
                    <div class="row m-b-20">
                        
                        <div class="row m-b-20">
                            <div class="col-sm">
                                <input class="btn btn-info f-s-12" type="submit" name="tampilkan" target="_blank" value="Tampilkan">
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- KLAIM -->
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title font-weight-bold text-info">KLAIM</h4>
            <!-- Nav tabs -->
            <form id="FormSubmit" method="post" target="_blank" action="<?php echo base_url()?>laporan/cariKlaim">
            <div class="vtabs">
                <ul class="nav nav-tabs tabs-vertical" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#lakla1" id="tablakla1" role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-check"> 1</i>
                            </span>
                            <span class="hidden-xs-down">Rincian Pengajuan Klaim</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lakla3" id="tablakla2"  role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-check"> 2</i>
                            </span>
                            <span class="hidden-xs-down">Rekap Pengajuan Klaim Bulanan</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lakla4" id="tablakla3"  role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-check"> 3</i>
                            </span>
                            <span class="hidden-xs-down">Rekap Pengajuan Klaim Keseluruhan</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lakla2" id="tablakla4"  role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-check"> 4</i>
                            </span>
                            <span class="hidden-xs-down">Rekap Pengajuan Klaim Tahunan</span>
                        </a>
                    </li>

                    
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lakla5" id="tablakla5"  role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-check"> 5</i>
                            </span>
                            <span class="hidden-xs-down">Penyelesaian Klaim per Bank</span>
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <input type="hidden" name="tipe" id="tipeklaim" value="">
                <div class="tab-content w-75">
                    <div class="tab-pane active" id="lakla1" role="tabpanel">
                        <div>
                            <h6>Laporan rincian pengajuan klaim per SLA sesuai periode yang ditentukan</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-sm-2">Dari</div>
                            <input id="tgl_mulai" type="text" class="col-sm custom-select f-s-12 datepicker" name="tgl_awal" placeholder="dd-mm-yyyy">
                            <div class="col-sm-2">s/d</div>
                            <input id="tgl_akhir" type="text" class="col-sm custom-select f-s-12 datepicker" name="tgl_akhir" placeholder="dd-mm-yyyy">
                        </div>
                    </div>
                    <div class="tab-pane " id="lakla2" role="tabpanel">
                        <div>
                            <h6>Rekapitulasi pengajuan klaim per bulan dalam setahun</h6>
                            <p>Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-sm-2">Pilih Tahun</div>
                            <input type="text" class="col-sm-4 custom-select f-s-12 datepicker3" name="tahun" placeholder="dd-mm-yyyy">
                        </div>
                    </div>
                    <div class="tab-pane " id="lakla3" role="tabpanel">
                        <div>
                            <h6>Rekapitulasi pengajuan klaim per Asuradur dalam sebulan</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-sm-2">Pilih Bulan</div>
                            <input type="text" class="col-sm-4 custom-select f-s-12 datepicker2" name="bulan" placeholder="dd-mm-yyyy">
                        </div>
                    </div>
                    <div class="tab-pane " id="lakla4" role="tabpanel">
                        <div>
                            <h6>Rekapitulasi pengajuan klaim per Asuradur dari awal hingga periode yang ditentukan</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-sm-2">Tanggal Akhir</div>
                            <input id="tgl_akhir" type="text" class="col-sm-4 custom-select f-s-12 datepicker" name="tgl_akhir2" placeholder="dd-mm-yyyy">
                        </div>
                    </div>
                    <div class="tab-pane " id="lakla5" role="tabpanel">
                        <div>
                            <h6>Laporan penyelesaian klaim per Bank (Kantor Pusat, Wilayah, Cabang dan Cabang
                                Pembantu)
                            </h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-sm-2">Dari</div>
                            <input id="tgl_mulai" type="text" class="col-sm custom-select f-s-12 datepicker" name="tgl_awal3" placeholder="dd-mm-yyyy">
                            <div class="col-sm-2">s/d</div>
                            <input id="tgl_akhir" type="text" class="col-sm custom-select f-s-12 datepicker" name="tgl_akhir3" placeholder="dd-mm-yyyy">
                        </div>
                    </div>
                    <div class="row m-b-20">
                        <div class=" f-s-12 col-sm">Produk</div>
                        <select class="js-example-basic-cabang form-control-feedback custom-select f-s-12 col-sm-4" id="produk" name="produk">
                            <option value="">Pilih</option>
                            <?php foreach ($produk as $key1): ?>
                            <option value="<?php echo $key1->idProduk?>"><?php echo $key1->NamaProduk?></option>
                            <?php endforeach ?>
                        </select>
                        <div class=" f-s-12 col-sm">Asuradur</div>
                        <select class="js-example-basic-cabang form-control-feedback custom-select f-s-12 col-sm-4" id="asuransi" name="asuransi">
                            <option value="">Pilih</option>
                            <?php foreach ($asuradur as $key): ?>
                            <option value="<?php echo $key->idAsuransi?>"><?php echo $key->asuransi?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="row m-b-20">
                        <?php if (cekOpBank($this->session->userdata("kodeBank"))!="CAPEM"): ?>
                          <?php if (cekOpBank($this->session->userdata("kodeBank"))=="CABANG"): ?>
                                <input type="hidden" placeholder="Nama" id="cabang" name="cabang" class="col-xs-3 custom-select f-s-12 " value="<?php echo $this->session->userdata('kodeBank');?>" readonly>
                                
                            <div class=" f-s-12 col-sm">Kantor Cabang</div>
                        <input type="text" placeholder="Nama" id="cabang4" name="cabang4" class="col-sm-4 custom-select f-s-12" value="<?php echo cekNamaBank($this->session->userdata('kodeBank'));?>" readonly>                       
                    <div class=" f-s-12 col-sm">Kantor Operasional</div>
                        <select class="js-example-basic-cabang col-sm-4 custom-select f-s-12" id="capem4" name="capem">
                            <option value="">Semua</option>
                        </select> 
                          <?php else: ?>
                            <div class=" f-s-12 col-sm">Kantor Cabang</div>
                        <select class="js-example-basic-cabang col-sm-4 custom-select f-s-12" id="cabang4" name="cabang">
                            <option value="">Semua</option>
                            <?php foreach ($cabang as $key): ?>
                            <option value="<?php echo $key->kodeBank?>"><?php echo $key->kodeBank.'-'.$key->cabang;?></option>
                            <?php endforeach ?>
                        </select>                        
                        <div class=" f-s-12 col-sm">Kantor Operasional</div>
                        <select class="js-example-basic-cabang col-sm-4 custom-select f-s-12" id="capem4" name="capem">
                            <option value="">Semua</option>
                        </select>
                          <?php endif ?>
                        <?php endif ?>
                        
                    </div>
                    <div class="row m-b-20">
                        
                        <div class="row m-b-20">
                            <div class="col-sm">
                                <input class="btn btn-info f-s-12" type="submit" name="tampilkan" target="_blank" value="Tampilkan">
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            </form>
        </div>
    </div>
</div>