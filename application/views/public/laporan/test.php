<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>KWITANSI PEMBAYARAN PREMI ASURANSI KREDIT</title>
    <link href="'.base_url().'assets/ela/css/lib/invoice/invoice.css" rel="stylesheet">
    <link href="'.base_url().'application/third_party/mpdf/mpdf.css" rel="stylesheet">
    <link href="'.base_url().'application/third_party/mpdf/lang2fonts.css" rel="stylesheet">

</head>

<body class="body-invoice">

    <header class="clearfix">
        <img style="width:10%;" src="'.base_url().'assets/ela/images/logoinv.jpg">
        <div id="alamat">Graha Tirtadi Lantai. 4 Suite 401
            <br> JL. Raden Saleh, No. 20, Kenari,
            <br> Kota Jakarta Pusat, DKI Jakarta 10330
            <br> Telp. : +62 (21) 39837478</div>
    </header>
    <h1>KWITANSI PEMBAYARAN PREMI ASURANSI KREDIT</h1>
    <table style="width: 100%;" border="0.1" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th>#</th>
                <th>ASURANSI</th>
                <th>DEBITUR</th>
                <th>PREMI</th>
            </tr>
        </thead>
        </tbody>
    </table>
    <div id="notices">
        <div><strong>NOTICE</strong>:</div>
        <div class="notice">Bukti pembayaran sah pendaftaran peserta Asuransi Kredit Bank</div>
    </div>
    <div id="total">
        <h1>Rincian Pembayaran</h1>
        <table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>Tanggal Pembayaran</td>
                <td style="text-align:right;">'.my_date_indo($tgl).'</td>
            </tr>
            <tr>
                <td>Jumlah Debitur</td>
                <td style="text-align:right;">'.$totaldebitur.'</td>
            </tr>
            <tr>
                <td>Total Premi</td>
                <td style="text-align:right;">'.price($totalpremi).'</td>
            </tr>
        </table>
    </div>
    <br>
    <div id="paraf" class="">
        <img style="width:150px;" src="'.base_url().'assets/ela/images/mtstagi.png">
        <div><strong>Kristinan Benny Hapsoro</strong></div>
        <div>Direktur Utama</div>
    </div>
    </tbody>
    </table>
    </div>
</body>

</html>