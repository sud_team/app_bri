<div class="row">
  <div class="col-12">
      <div class="card">
          <div class="card-body">
          <h4 class="card-title">Laporan Premi Per Asuransi</h4>
              <h6 class="card-subtitle">Menampilkan laporan produksi per Asuransi sesuai periode yang ditentukan</h6><!-- Menu Pencairan -->
            <form id="FormSubmit" method="post">
              <div class="col-md-7">
                <div class="row m-b-20">
                  <div class="text-left text-megna col-sm-4">
                      <small class="form-control-feedback f-s-10 text-megna">Periode upload dari</small>
                      <input type="text" class="custom-select f-s-12 datepicker" placeholder="dd-mm-yyyy" name="tgl_mulai" id="tgl_mulai" required>
                  </div>
                  <div class="text-left text-megna col-sm-4">
                      <small class="form-control-feedback f-s-10 text-megna">Sampai dengan</small>
                      <input type="text" class="custom-select f-s-12 datepicker" placeholder="dd-mm-yyyy" name="tgl_akhir" id="tgl_akhir" required>
                  </div>
                  <div class="col-sm-2 m-t-20 text-right">
                      <a id="cari" class="btn btn-info f-s-12 text-light">
                      <i class="fa fa-search"></i> Tampilkan
                      </a>
                  </div>
                </div>
              </div>
            </form>
<!-- Menu Pencairan Akhir -->

              <div class="table-responsive">
                  <table id="DataAsuransi" class="display nowrap table-hover table-bordered f-s-12 h-100" cellspacing="0" width="100%">
                      <thead >
                          <tr >
                              <th class="w-6">Asuransi</th>
                              <th class="w-18">Plafon</th>
                              <th>Premi</th>
                              <th>OS Premi</th>
                              <th>Fee Bank</th>
                              <th>Brokerage</th>
                              <th>Net Premi</th>
                          </tr>
                      </thead>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>