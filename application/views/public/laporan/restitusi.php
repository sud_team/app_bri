<!-- RESTITUSI -->
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title font-weight-bold text-megna">RESTITUSI</h4>
            <!-- Nav tabs -->
            <form id="FormSubmit" method="post" target="_blank" action="<?php echo base_url()?>laporan/cariRestitusi">
            <div class="vtabs">
                <ul class="nav nav-tabs tabs-vertical" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#lares1" id="tablares1" role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-arrow-right"> 1</i>
                            </span>
                            <span class="hidden-xs-down">Rincian Pengajuan Restitusi</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lares2" id="tablares2"  role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-arrow-right"> 2</i>
                            </span>
                            <span class="hidden-xs-down">Rekap Restitusi Bulanan</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lares3" id="tablares3" role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-arrow-right"> 3</i>
                            </span>
                            <span class="hidden-xs-down">Rekap Restitusi Keseluruhan</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lares4" id="tablares4" role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-arrow-right"> 4</i>
                            </span>
                            <span class="hidden-xs-down">Rekap Data Pengajuan Restitusi Tahunan</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#lares5" id="tablares5"  role="tab">
                            <span class="hidden-sm-up">
                                <i class="ti-arrow-right"> 5</i>
                            </span>
                            <span class="hidden-xs-down">Penyelesaian Restitusi per Bank</span>
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <input type="hidden" name="tipe" id="tiperes" value="">
                <div class="tab-content">
                    <div class="tab-pane active" id="lares1" role="tabpanel">
                        <div>
                            <h6>Laporan rincian pengajuan Restitusi per SLA sesuai periode yang ditentukan</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Dari</small>
                            <input id="tgl_mulai" type="text" class="col-sm custom-select f-s-12 datepicker" name="tgl_awal" placeholder="dd-mm-yyyy">
                        </div>
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Sampai dengan</small>
                            <input id="tgl_akhir" type="text" class="col-sm custom-select f-s-12 datepicker" name="tgl_akhir" placeholder="dd-mm-yyyy">
                        </div>
                        </div>
                    </div>
                    <div class="tab-pane " id="lares2" role="tabpanel">
                        <div>
                            <h6>Rekapitulasi Restitusi per Asuradur dalam sebulan</h6>
                            <p>Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Pilih Bulan</small>
                            <input type="text" class="col-sm custom-select f-s-12 datepicker2" name="bulan" placeholder="mm-yyyy">
                        </div></div>
                    </div>
                    <div class="tab-pane " id="lares3" role="tabpanel">
                        <div>
                            <h6>Rekapitulasi Restitusi per Asuradur dari awal sampai periode yang ditentukan</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Tanggal Akhir</small>
                            <input type="text" class="col-sm custom-select f-s-12 datepicker" name="tgl_akhir3" placeholder="dd-mm-yyyy"></div>
                        </div>
                    </div>
                    <div class="tab-pane " id="lares4" role="tabpanel">
                        <div>
                            <h6>Rekapitulasi data pengajuan Restitusi dalam setahun</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Pilih Tahun</small>
                            <input type="text" class="col-sm custom-select f-s-12 datepicker3" name="tahun2" placeholder="yyyy"></div>
                        </div>
                    </div>
                    <div class="tab-pane " id="lares5" role="tabpanel">
                        <div>
                            <h6>Penyelesaian Restitusi per Bank (Kantor Pusat, Wilayah, Cabang dan Cabang pembantu)</h6>
                            <p> Tentukan filter pencarian pada kolom dibawah</p>
                        </div>
                        <div class="row m-b-20">
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Dari</small>
                            <input id="tgl_mulai" type="text" class="col-sm custom-select f-s-12 datepicker" name="tgl_awal2" placeholder="dd-mm-yyyy"></div>
                            <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Sampai dengan</small>
                            <input id="tgl_akhir" type="text" class="col-sm custom-select f-s-12 datepicker" name="tgl_akhir2" placeholder="dd-mm-yyyy"></div>
                        </div>
                    </div>
                    <div class="row m-b-20">
                    <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Produk   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>                        <select class="js-example-basic-cabang form-control-feedback custom-select f-s-12 col-sm" id="produk" name="produk">
                            <option value="">Pilih</option>
                            <?php foreach ($produk as $key1): ?>
                            <option value="<?php echo $key1->idProduk?>"><?php echo $key1->NamaProduk?></option>
                            <?php endforeach ?>
                        </select></div>
                        <?php if ($this->session->userdata("roles")!=4): ?>
                        <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Asuradur   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                        <select class="js-example-basic-cabang form-control-feedback custom-select f-s-12 col-sm" id="asuransi" name="asuransi">
                            <option value="">Pilih</option>
                            <?php foreach ($asuradur as $key): ?>
                            <option value="<?php echo $key->kodeAngka?>"><?php echo $key->asuransi?></option>
                            <?php endforeach ?>
                        </select></div>
                        <?php endif ?>
                    </div>
                    <div class="row m-b-20">
                        <?php if (cekOpBank($this->session->userdata("kodeBank"))!="CAPEM"): ?>
                          <?php if (cekOpBank($this->session->userdata("kodeBank"))=="CABANG"): ?>
                                <input type="hidden" placeholder="Nama" id="cabang3" name="cabang" class="col-xs-3 custom-select f-s-12 " value="<?php echo $this->session->userdata('kodeBank');?>" readonly>
                                
                        <div class="text-left text-megna col-sm-5">
                        <small class="form-control-feedback f-s-10">Kantor Cabang</small> 
                        <input type="text" placeholder="Nama" id="cabang3" name="cabang3" class="col-sm custom-select f-s-12" value="<?php echo cekNamaBank($this->session->userdata('kodeBank'));?>" readonly>    
                        </div>

                    <div class="text-left text-megna col-sm-5">
                            <small class="form-control-feedback f-s-10">Kantor Operasional</small>
                        <select class="js-example-basic-cabang col-sm custom-select f-s-12" id="capem3" name="capem">
                            <option value="">Semua</option>
                        </select> </div>
                          <?php else: ?>
                          <div class="text-left text-megna col-sm-5">
                                <small class="form-control-feedback f-s-10">Kantor Cabang</small>
                        <select class="js-example-basic-cabang col-sm custom-select f-s-12" id="cabang3" name="cabang">
                            <option value="">Semua</option>
                            <?php foreach ($cabang as $key): ?>
                            <option value="<?php echo $key->kodeBank?>"><?php echo $key->kodeBank.'-'.$key->cabang;?></option>
                            <?php endforeach ?>
                        </select>       </div>                 
                        <div class="text-left text-megna col-sm-5">
                                <small class="form-control-feedback f-s-10">Kantor Operasional</small>
                        <select class="js-example-basic-cabang col-sm custom-select f-s-12" id="capem3" name="capem">
                            <option value="">Semua</option>
                        </select></div>
                          <?php endif ?>
                        <?php endif ?>
                        
                    </div>
                        
                        <div class="row m-b-20">
                            <div class="col-sm">
                                <input class="btn btn-info f-s-12" type="submit" name="tampilkan" target="_blank" value="Tampilkan">
                            </div>
                        </div>


                </div>
            </div>
            </form>
        </div>
    </div>
</div>