<div class="row">
	<div class="col-md-12">
	    <div class="card">
	        <div class="card-body">
	        	<li class="pull-right">
				<img class="dark-logo" src="<?php echo base_url()?>assets/ela/images/logorep.png">
		</li>
						<!-- Lebel Laporan  -->
						<h4 class="card-title">
							<strong><?php echo strtoupper("Rincian Pengajuan Klaim");?></strong>
						</h4>
						<div class="row lable-laporan m-b-20">
							<div class="col-lg-4">
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Periode</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $periode?></div>
								</div>
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Asuradur</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $asuransi?></div>
								</div>
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Produk</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $produk?></div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<strong>Bank</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $bank?></div>
								</div>
							</div>
							<div class="col-lg-4"></div>
						</div>
	          	<div class="table-responsive" id="abc">                
	            	<!-- <h4>PROSES</h4> -->
	            	<table id='DataPeserta' class='display nowrap table-hover table-bordered f-s-12' cellspacing='0' width='100%'>
	            		<thead>
	            			<tr>
	    	        			<th>No</th>
	    	        			<th>KANTOR CABANG</th>
	    	        			<th>KANTOR OPERASIONAL</th>
	    	        			<th>NOMOR PINJAMAN</th>
	    	        			<th>NAMA DEBITUR</th>
	    	        			<th>TGL KLAIM</th>
	    	        			<th>JENIS KLAIM</th>
	    	        			<th>ASURANSI</th>
	    	        			<th>NOMINAL DIAJUKAN</th>
	    	        			<th>NOMINAL DISETUJUI</th>
	    	        			<th>LAMA PROSES</th>
	    	        			<th>POSISI KLAIM</th>
			            	</tr>
			            </thead>
			            <tbody>
			            	<?php $no=1; foreach ($debitur->result() as $key): ?>
			            	<?php 
        						$bangpem = bangpem($key->kodeBank);
						    	if($bangpem->capem==NULL){
						    		$bangpem->capem = $bangpem->cabang;
						    	}
						    ?>
			            	<tr>
			            		<td><?php echo $no;?></td>
						    	<td><?php echo $bangpem->cabang?></td>
						    	<td><?php echo $bangpem->capem?></td>
						    	<td><?php echo $key->NomorPinjaman?></td>
						    	<td><?php echo $key->NamaDebitur?></td>
						    	<td><?php echo my_date_indo($key->TglKlaim)?></td>
						    	<td><?php echo cekResiko($key->idResiko)?></td>
						    	<td><?php echo cekNamaAsdur($key->kodeAsuransi)?></td>
						    	<td><?php echo price($key->NominalDiajukan)?></td>
						    	<td><?php echo price($key->NominalDisetujui)?></td>
						    	<td>
						    		<?php if (lead_time($key->TglKlaim,$key->TanggalBayar)>1000) {
							    		$lama = lead_time($key->create_spv,hariini());
							    	} else {
							    		$lama = lead_time($key->TglKlaim,$key->TanggalBayar);
							    	}?>
						    		<?php if($lama<=14){ echo $lama." (SLA)";} else { echo $lama." (NON SLA)";}?>
						    		
						    	</td>
						    	<td>
						    		<?php 
						    			if($key->pialang=="0"){
						    				$posisi = "Menunggu Pialang";
						    			}else if($key->asuransi=="0"){
						    				$posisi = "Menunggu Asuransi";
						    			}else{
						    				$posisi = "";
						    			}

						    			if($key->keputusan=="0"){
						    				echo "Proses ".$posisi;
						    			}else if($key->keputusan=="1"){
						    				if($key->StatusPembayaran=="1"){
						    					echo "Selesai - Terbayar";
						    				}else{
						    					echo "Disetujui";
						    				}
						    			}else if($key->keputusan=="2"){
						    				echo "Ditangguhkan";
						    			}else{
						    				echo "Ditolak";
						    			}
						    		?>
						    	</td>
			            	</tr>
			            	<?php $no++; 
			            	endforeach  ?>
			            </tbody>
	            	</table>

	            	<?php if ($this->session->userdata("roles")==1 || $this->session->userdata("roles")==3): ?>
	            		
	            	<br>
	            	<br>
	            	<br>
	            	<table id='uat05' class='display nowrap table-hover table-bordered f-s-12' cellspacing='0' width='100%'>
	            		<thead>
	            			<tr>
	    	        			<th>Kode</th>
	    	        			<th></th>
	    	        			<th></th>
			            	</tr>
			            </thead>
			            <tbody>
			            	<?php foreach ($debitur->result() as $abc): ?>
			            	<tr>
			            		<td>05 DATA KLAIM <?php echo $abc->NomorPinjaman?></td>
						    	<td>
						    		<?php if($abc->keputusan==0){
						    			echo "3";
						    		}else if($abc->keputusan==-2){
						    			echo "2";
						    		}else{
						    			echo $abc->keputusan;
						    		}

						    		?>
						    	</td>
						    	<td>
						    		<?php if($abc->keputusan==0){
						    			if($abc->pialang=="0"){
						    				echo $posisi = "Menunggu Pialang";
						    			}else if($abc->asuransi=="0"){
						    				echo $posisi = "Menunggu Asuransi";
						    			}else{
						    				echo $posisi = "";
						    			}
						    		}else {
						    			echo $abc->keterangan;
						    		}
						    		?>
						    	</td>
			            	</tr>
			            	<?php 
			            	endforeach ?>
			            </tbody>
	            	</table>
	            	
	            	<?php endif ?>
	            </div>
            </div>
       	</div>
	</div>
</div>