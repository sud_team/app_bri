<div class="row">
	<div class="col-md-12">
	    <div class="card">
	        <div class="card-body">
	        	<li class="pull-right">
				<img class="dark-logo" src="<?php echo base_url()?>assets/ela/images/logorep.png">
				</li>
						<!-- Lebel Laporan  -->
						<h4 class="card-title">
							<strong><?php echo strtoupper("penyelesaian klaim perbank");?></strong>
						</h4>
						<div class="row lable-laporan m-b-20">
							<div class="col-lg-4">
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Periode</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $periode?></div>
								</div>
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Asuradur</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $asuransi?></div>
								</div>
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Produk</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $produk?></div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<strong>Bank</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $bank?></div>
								</div>
							</div>
							<div class="col-lg-4"></div>
						</div>
	        	<div class="table-responsive" id="abc">                
	            	<table id='DataPeserta' class='display nowrap table-hover table-bordered f-s-12' cellspacing='0' width='100%'>
	            		<thead>
	            			<tr >			
		                        <th style="text-align:center;" rowspan="2">NO</th>
		                        <th style="text-align:center;" rowspan="2">KANTOR CABANG</th>
								<th style="text-align:center;" rowspan="2">KANTOR OPERASIONAL</th>
								<th style="text-align:center;" colspan="2">PENGAJUAN</th>
								<th style="text-align:center;" colspan="2">DIBAYAR</th>
							</tr>

							<tr >
								<th>DEBITUR</th>
								<th>NOMINAL</th>
								<th>DEBITUR</th>
								<th>NOMINAL</th>
					        </tr>
			            </thead>
			            <tbody>
			            	<?php 
			            		$no=1; 
			            		$totalpengajuandebitur = 0;
			            		$totalpengajuanup = 0;
			            		$totalpolisdebitur = 0;
			            		$totalpolisup = 0;
			            		foreach ($debitur->result() as $key): ?>
			            	<?php 
			            		$bangpem = bangpem($key->kodeBank);
						    	if($bangpem->capem==NULL){
						    		$bangpem->capem = $bangpem->cabang;
						    	}
			            	?>
			            	<tr>
			            		<td style="text-align: center;"><?php echo $no;?></td>
			            		<td><?php echo $bangpem->cabang?></td>
						    	<td><?php echo $bangpem->capem?></td>
						    	<td style="text-align: center;"><?php echo $key->jmldeb?></td>
						    	<td style="text-align: right;"><?php echo price($key->plafon)?></td>
						    	<td style="text-align: center;"><?php if(empty($key->jmldebx)){ echo 0;}else{ echo $key->jmldebx;}?></td>
						    	<td style="text-align: right;"><?php echo price($key->plafonx)?></td>
			            	</tr>
			            	<?php
			            		$totalpengajuandebitur = $totalpengajuandebitur+$key->jmldeb;
			            		$totalpengajuanup = $totalpengajuanup+$key->plafon;
			            		$totalpolisdebitur = $totalpolisdebitur+$key->jmldebx;
			            		$totalpolisup = $totalpolisup+$key->plafonx;
			            		$no++; 
			            	endforeach ?>
			            </tbody>
			            <tfoot>
			            	<tr>
			            		<th style="text-align: right;" colspan="3">TOTAL</th>
			            		<th style="text-align: center;"><?php echo $totalpengajuandebitur?></th>
			            		<th style="text-align: right;"><?php echo price($totalpengajuanup)?></th>
			            		<th style="text-align: center;"><?php echo $totalpolisdebitur?></th>
			            		<th style="text-align: right;"><?php echo price($totalpolisup)?></th>
			            	</tr>
			            </tfoot>
	            	</table>
	            </div>
            </div>
       	</div>
	</div>
</div>