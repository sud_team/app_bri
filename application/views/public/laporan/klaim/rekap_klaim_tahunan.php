<div class="row">
	<div class="col-md-12">
	    <div class="card">
	        <div class="card-body">
	        	<li class="pull-right">
				<img class="dark-logo" src="<?php echo base_url()?>assets/ela/images/logorep.png">
						</li>
						<!-- Lebel Laporan  -->
						<h4 class="card-title">
							<strong><?php echo strtoupper("rekap klaim tahunan");?></strong>
						</h4>
						<div class="row lable-laporan m-b-20">
							<div class="col-lg-4">
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Periode</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $periode?></div>
								</div>
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Asuradur</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $asuransi2?></div>
								</div>
								<div class="row">
									<div class="col-sm-4" id="judul">
										<strong>Produk</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $produk?></div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<strong>Bank</strong>
									</div>
									<div class="col-sm-7">
										<span>: </span><?php echo $bank?></div>
								</div>
							</div>
							<div class="col-lg-4"></div>
						</div>
	          	<div class="table-responsive" id="abc">                
	            	<table id='rekap_produksi_tahunan' class='display nowrap table-hover table-bordered f-s-12' cellspacing='0' width='100%'>
	            		<thead>
	            			<tr >			
		                        <th style="text-align:center" rowspan="3">NO</th>
		                        <th style="text-align:center" rowspan="3">BULAN</th>
								<th style="text-align:center;" colspan="2">PENGAJUAN</th>
								<th style="text-align:center;" colspan="2">DIBAYAR</th>
								<?php if ($this->session->userdata("roles")!=4): ?>
								<?php $i =1; foreach ($asuransi as $key): ?>
		    	        		<th colspan="4"><?php echo $key->kodeAsuransi?></th>	
		    	        		<?php $i++; endforeach ?>
							</tr>
							
							<tr >
								<th rowspan="2">DEBITUR</th>
								<th  rowspan="2">NOM</th>
								<th rowspan="2">DEBITUR</th>
								<th  rowspan="2">NOM</th>
								<?php foreach ($asuransi as $as): ?>
							  	<th colspan="2">PENGAJUAN</th>
								<th colspan="2">DIBAYAR</th>
								<?php  endforeach ?>
					        </tr>
					        <tr>
					        	<?php foreach ($asuransi as $as): ?>
							  	<th>DEB</th>
								<th>NOM</th>
								<th>DEB</th>
								<th>NOM</th>
								<?php  endforeach ?>
	    	        		<?php endif?>
					        </tr>
			            </thead>
			            <tbody>
			            	<?php 
			            		$no=1; 
			            		$totaldeb = 0; 
		            			$totalplafon = 0;
		            			$totaldebx = 0; 
		            			$totalplafonx = 0; 
			            		foreach ($debitur as $v): ?>
			            		<tr>
			            			<td><?php echo $no?></td>
			            			<td><?php echo my_date_indo_bulan($v[0]->bulan)?></td>
			            			<td style="text-align: right;"><?php echo $v[0]->jmldeb?></td>
			            			<td style="text-align: right;"><?php echo price($v[0]->plafon)?></td>
			            			<td style="text-align: right;"><?php echo $v[0]->jmldebx?></td>
			            			<td style="text-align: right;"><?php echo price($v[0]->plafonx)?></td>
			            			<?php if ($this->session->userdata("roles")!=4): ?>
			            			<?php $i =1; foreach ($asuransi as $t): ?>
			    	        		<td style="text-align: right;"><?php echo cekDebiturKlaimAsdur($v[0]->tahun,$t->kodeAngka);?></td>	
			    	        		<td style="text-align: right;"><?php echo price(cekNomKlaimAsdur($v[0]->tahun,$t->kodeAngka));?></td>
			    	        		<td style="text-align: right;"><?php echo cekDebiturKlaimAsdurBayar($v[0]->tahun,$t->kodeAngka);?></td>	
			    	        		<td style="text-align: right;"><?php echo price(cekNomKlaimAsdurBayar($v[0]->tahun,$t->kodeAngka));?></td>	
			    	        		<?php $i++; endforeach ?>
			    	        		<?php endif?>
			            		</tr>
			            	<?php 
			            		$no++;
			            		$totaldeb = $totaldeb+$v[0]->jmldeb; 
		            			$totalplafon = $totalplafon+$v[0]->plafon;
		            			$totaldebx = $totaldebx+$v[0]->jmldebx; 
		            			$totalplafonx = $totalplafonx+$v[0]->plafonx; 
			            	endforeach ?>
			            </tbody>
			            <tfoot>
			            	<tr>
			            		<th colspan="2">TOTAL</th>
			            		<th style="text-align: right;"><?php echo $totaldeb;?></th>
			            		<th style="text-align: right;"><?php echo price($totalplafon);?></th>
			            		<th style="text-align: right;"><?php echo $totaldebx;?></th>
			            		<th style="text-align: right;"><?php echo price($totalplafonx);?></th>
			            		<?php if ($this->session->userdata("roles")!=4): ?>
			            		<?php $i =1; foreach ($asuransi as $d): ?>
		    	        		<td style="text-align: right;"><?php echo cekDebiturKlaimAsdurTahun($tahun,$d->kodeAngka);?></td>	
		    	        		<td style="text-align: right;"><?php echo price(cekKlaimAsdurTahun($tahun,$d->kodeAngka));?></td>
		    	        		<td style="text-align: right;"><?php echo cekDebiturKlaimAsdurBayarTahun($tahun,$d->kodeAngka);?></td>	
		    	        		<td style="text-align: right;"><?php echo price(cekKlaimAsdurBayarTahun($tahun,$d->kodeAngka));?></td>	
		    	        		<?php $i++; endforeach ?>
		    	        		<?php endif?>
			            	</tr>
			            </tfoot>
	            	</table>
	            </div>
            </div>
       	</div>
	</div>
</div>