<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Internal Memo</h4>
                <h6 class="card-subtitle">Untuk pengajuan proses pembayaran Klaim dan Restitusi</h6>
                <!-- Panel Utama -->
                <div class="row">
                    <!-- Menu Pencairan -->
                    <div class="col-10 m-b-30">
                        <form id="FormSubmit" method="post">
                        <div class="col-md-8">
                        <div class="row m-b-10">
                        <div class="text-left text-danger col-sm-5">
                                    <small class="form-control-feedback f-s-10">Jenis Internal Memo</small>
                                    <select class="js-example-basic-cabang custom-select f-s-12" name="jenis_im" id="jenis_im">
                                        <option value="">Semua</option>
                                        <option value="Klaim">Klaim</option>
                                        <option value="Restitusi">Restitusi</option>
                                    </select>
                                </div>
                            <div class="text-left text-megna col-sm-5">
                                    <small class="form-control-feedback f-s-10">Produk  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                                    <select class="js-example-basic-cabang custom-select f-s-12" name="produk" id="produk">
                                        <option value="">Semua</option>
                                        <?php foreach ($produk as $pd): ?>
                                        <option value="<?php echo $pd->idProduk?>"><?php echo $pd->NamaProduk?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                               
                            </div>
                            <div class="row m-b-10">
                                <input type="hidden" id="link" value="filter_internalmemo">
                                <div class="text-left text-megna col-sm-5">
                                    <small class="form-control-feedback f-s-10">Cabang  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>

                                    <select class="js-example-basic-cabang custom-select f-s-12" id="cabang" name="cabang">
                                        <option value="">Semua</option>
                                        <?php foreach ($cabang as $key): ?>
                                        <option value="<?php echo $key->kodeBank?>"><?php echo $key->kodeBank.'-'.$key->cabang;?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="text-left text-megna col-sm-5">
                                    <small class="form-control-feedback f-s-10">Kantor Operasional</small>
                                    <select class="js-example-basic-cabang custom-select f-s-12" id="capem" name="capem">
                                        <option value="">Semua</option>
                                    </select>
                                </div>
                            </div>
                         
                            <div class="row m-b-20">
                            <div class="text-left text-megna col-sm-5">
                                    <small class="form-control-feedback f-s-10">Nama</small>

                                    <input type="text" placeholder="Nama" name="nama" id="nama" class="col-xs-3 custom-select f-s-12 ">
                                </div>
                                <div class="text-left text-megna col-sm-5">
                                    <small class="form-control-feedback f-s-10">No. Pinjaman</small>
                                    <input type="text" placeholder="No. Pinjaman" name="nopin" id="nopin" class="col-xs-3 custom-select f-s-12 ">
                                </div>
                            </div>

                            <div class="row m-b-20">
                            <div class="text-left text-megna col-sm-4">
                                    <small class="form-control-feedback f-s-10 text-megna">Periode Persetujuan </small>
                                    <input type="text" class="custom-select f-s-12 datepicker" placeholder="dd/mm/yyyy" name="tgl_mulai" id="tgl_mulai">
                                </div>
                                <div class="text-left text-megna col-sm-4">
                                    <small class="form-control-feedback f-s-10 text-megna">Sampai dengan</small>
                                    <input type="text" class="custom-select f-s-12 datepicker" placeholder="dd/mm/yyyy" name="tgl_akhir" id="tgl_akhir">
                                </div>
                          
                                <div class="col-sm-2 m-t-20 text-right">
                                    <a id="cari" class="btn btn-info f-s-12" style="color: #ffffff;">
                                        <i class="fa fa-search"></i> Tampilkan
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- Menu Pencairan Akhir -->
                    <!-- Display Konfirmasi "INVISIBLE SEBELUM PENCARIAN" -->
                    <?php /*<div class="col-4 m-b-20 bg-light-danger radius p-10 ">
                        <div class="box-title text-center">
                            <h6>
                                Konfirmasi Penerbitan Internal Memo
                            </h6>
                        </div>
                        <hr>
                        <div class="row m-l-10 f-s-12 color-danger">
                            <div class="w-35 ">Nomor IM </div>
                            <div class="w-40">
                                <b>IMKLM93293090518</b>
                            </div>
                        </div>
                        <div class="row m-l-10 text-dark">
                            <div class="w-35">Tanggal cetak</div>
                            <div class="w-40">
                                <b>currentdate</b>
                            </div>
                        </div>
                        <div class="row m-l-10 text-dark">
                            <div class="w-35">Jenis Pengajuan</div>
                            <div class="w-40">
                                <b>Klaim/Restitusi</b>
                            </div>
                        </div>
                        <div class="row m-l-10 text-dark">
                            <div class="w-35">Asuradur</div>
                            <div class="w-40">
                                <b>Asuransi A</b>
                            </div>
                        </div>
                        <div class="row m-l-10 text-dark">
                            <div class="w-35">Jumlah Peserta</div>
                            <div class="w-40">
                                <b>2</b> Debitur</div>
                        </div>
                        <div class="row m-l-10 m-b-10 text-dark">
                            <div class="w-35">Total Pengajuan</div>
                            <div class="w-40">
                                <b>Rp.21.200.000</b>
                            </div>
                        </div>
                        <div class="pull-right text-light">
                            <a id="cari" class="btn btn-sm btn-danger f-s-12">
                                <i class="fa fa-print"></i> Cetak
                            </a>
                        </div>
                    </div>*/?>
                    <!-- Display Konfirmasi Akhir "VISIBLE SETELAH PENCARIAN"-->

                </div>
                <!-- Panel Utama -->

                <!-- TABEL 
                    (VISIBLE SETELAH PENCARIAN (tampilkan) -->
                <!-- <label class="text-muted f-s-12 pull-right">Pilih data pengajuan Klaim/Restitusi dibawah yang akan diajukan</label> -->
                <div class="table-responsive">
                    <table id="debitur" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>No.Pinjaman</th>
                                <th>Nama</th>
                                <th>Tgl Persetujuan</th>
                                <th>Bank</th>
                                <th>Asdur</th>
                                <th>Jenis</th>
                                <th>Nominal</th>
                                <th><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- TABEL  (ditampilkan setelah di klik pencarian (tampilkan) -->
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form action="<?php echo base_url()?>pembayaran/savePembayaran" method="post" enctype="multipart/form-data">
          <div class="modal-header">
              <h6 class="modal-title font-weight-bold" id="exampleModalLabel">Pembayaran</h6>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body f-s-12 p-l-30 p-r-30">
            <div id="nomor">
                
            </div>
            <div class="row m-b-20">
                <div class="text-left text-megna w-50 m-l-20">
                    <small class="form-control-feedback f-s-10">Tgl Pembayaran</small>
                    <input type="text" placeholder="dd-mm-yyyy" name="tgl" id="tgl" class="col-xs-3 custom-select f-s-12 datepicker" required>
                </div>
            </div>
            <div class="row m-b-20">
                <div class="text-left text-megna w-50 m-l-20">
                    <small class="form-control-feedback f-s-10">Nominal</small>
                    <input type="text" placeholder="Nominal" name="nominal" id="nominal" class="col-xs-3 custom-select f-s-12 price" required>
                </div>
            </div>
          </div>
          <div class="modal-footer ">
            <button type="button" class="btn btn-secondary btn-sm " data-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-info btn-sm " onclick="return confirm('Apakah Anda Yakin?')" value="Kirim" style="color: #ffffff">
          </div>
        </form>
      </div>
  </div>
</div>