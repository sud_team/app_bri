<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Cetak Data Pembayaran</h4>
                <h6 class="card-subtitle">Untuk mencetak Klaim dan Restitusi</h6>
                <!-- Panel Utama -->
           
                    <!-- Menu Pencairan -->
                    <div class="col-md-8">
                        <form id="FormSubmit" method="post">
                        <div class="row m-b-10">
                        <div class="text-left text-danger col-sm-5">
                                    <small class="form-control-feedback f-s-10">Jenis Internal Memo</small>
                                    <select class="js-example-basic-cabang custom-select f-s-12" name="jenis_im" id="jenis_im">
                                        <option value="">Semua</option>
                                        <option value="Klaim">Klaim</option>
                                        <option value="Restitusi">Restitusi</option>
                                    </select>
                                </div>
                            <div class="text-left text-megna col-sm-5">
                                    <small class="form-control-feedback f-s-10">Produk  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>
                                    <select class="js-example-basic-cabang custom-select f-s-12" name="produk" id="produk">
                                        <option value="">Semua</option>
                                        <?php foreach ($produk as $pd): ?>
                                        <option value="<?php echo $pd->idProduk?>"><?php echo $pd->NamaProduk?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                               
                            </div>
                            <div class="row m-b-10">
                                <input type="hidden" id="link" value="filter_pembayaran">
                                <div class="text-left text-megna col-sm-5">
                                    <small class="form-control-feedback f-s-10">Cabang  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small>

                                    <select class="js-example-basic-cabang custom-select f-s-12" id="cabang" name="cabang">
                                        <option value="">Semua</option>
                                        <?php foreach ($cabang as $key): ?>
                                        <option value="<?php echo $key->kodeBank?>"><?php echo $key->kodeBank.'-'.$key->cabang;?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="text-left text-megna col-sm-5">
                                    <small class="form-control-feedback f-s-10">Kantor Operasional</small>
                                    <select class="js-example-basic-cabang custom-select f-s-12" id="capem" name="capem">
                                        <option value="">Semua</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row m-b-20">
                            <div class="text-left text-megna col-sm-5">
                                    <small class="form-control-feedback f-s-10">Nama</small>

                                    <input type="text" placeholder="Nama" name="nama" id="nama" class=" custom-select f-s-12 ">
                                </div>
                                <div class="text-left text-megna col-sm-5">
                                    <small class="form-control-feedback f-s-10">No. Pinjaman</small>
                                    <input type="text" placeholder="No. Pinjaman" name="nopin" id="nopin" class=" custom-select f-s-12 ">
                                </div>
                            </div>

                            <div class="row m-b-20">
                            <div class="text-left text-megna col-sm-4">
                                    <small class="form-control-feedback f-s-10 text-megna">Periode Persetujuan </small>
                                    <input type="text" class=" custom-select f-s-12 datepicker" placeholder="dd/mm/yyyy" name="tgl_mulai" id="tgl_mulai">
                                </div>
                                <div class="text-left text-megna col-sm-4">
                                    <small class="form-control-feedback f-s-10 text-megna">Sampai dengan</small>
                                    <input type="text" class=" custom-select f-s-12 datepicker" placeholder="dd/mm/yyyy" name="tgl_akhir" id="tgl_akhir">
                                </div>
         
                            <div class="col-sm-2 m-t-20 text-right">
                                    <a id="cari" class="btn btn-info f-s-12 text-light">
                                        <i class="fa fa-search"></i> Tampilkan
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>

                    <!-- Display Konfirmasi "INVISIBLE SEBELUM PENCARIAN" -->
                    <?php /* <div class="col-4 m-b-20 bg-light-danger radius p-10 " style="height: 150px;">
                        <div class="box-title text-center">
                            <h6>
                                Cetak Pembayaran
                            </h6>
                        </div>
                        <hr>
                        <div class="row m-l-10 text-dark">
                            <div class="w-35">Tanggal cetak</div>
                            <div class="w-40">
                                <b><?php echo my_date_indo(date("Y-m-d"))?></b>
                            </div>
                        </div>
                        <div class="row m-l-10 text-dark">
                            <div class="w-35">Jumlah Peserta</div>
                            <div class="w-40">
                                <b id="jmldebitur">0 Debitur</b></div>
                        </div>
                        <div class="pull-right text-light">
                            <a id="cari" class="btn btn-sm btn-danger f-s-12">
                                <i class="fa fa-print"></i> Cetak
                            </a>
                        </div>
                    </div> */?>
                    <!-- Display Konfirmasi Akhir "VISIBLE SETELAH PENCARIAN"-->

                <!-- Panel Utama -->

                <!-- TABEL 
                    (VISIBLE SETELAH PENCARIAN (tampilkan) -->
                <div class="table-responsive">
                    <table id="debitur" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Kode Pembayaran</th>
                                <th>No.Pinjaman</th>
                                <th>Nama</th>
                                <th>Tgl Pembayaran</th>
                                <th>Bank</th>
                                <th>Asdur</th>
                                <th>Jenis</th>
                                <th>Nominal</th>
                                <th><input type="checkbox" id="chkSelectAll"></th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- TABEL  (ditampilkan setelah di klik pencarian (tampilkan) -->
            </div>
        </div>
    </div>
</div>
