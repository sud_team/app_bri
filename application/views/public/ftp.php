<div class="row">
  <div class="col-md-6">
      <div class="card">
          <div class="card-body">
              <h4 class="card-title">FTP Penutupan Premi GET-01</h4>
              <h6 class="card-subtitle">GET Data Engine harian dari Bank</h6>

              <div class="table-responsive">
                  <table id="link" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                      <thead >
                          <tr >
                              <th>Tanggal</th>
                              <th>Nama File</th>
                              <th><i class="fa fa-download" aria-hidden="true"></i></th>
                          </tr>
                      </thead>
                  </table>
              </div>
          </div>
      </div>
  </div>
  <div class="col-md-6">
      <div class="card">
          <div class="card-body">
              <h4 class="card-title">FTP Data Tambahan</h4>
              <h6 class="card-subtitle">Export Data Produksi Data Tambahan dengan metode PUT</h6>

              <form action="<?php echo base_url()?>ftp/ftp_upload_history" method="post" enctype="multipart/form-data">
                  <div class="form-group row">
                      <div class="col-xs-6">
                          <input type="file" required class="btn btn-xs" name="upload_file">
                      </div>
                      <div class="col-xs-6">
                          <input type="submit" class="btn btn-xs btn-primary" name="Upload" value="Upload">
                      </div>
                  </div>
              </form>

              <div class="table-responsive">
                  <table id="history" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                      <thead >
                          <tr >
                              <th>Nama File</th>
                              <th><i class="fa fa-download" aria-hidden="true"></i></th>
                          </tr>
                      </thead>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
      <div class="card">
          <div class="card-body">
              <h4 class="card-title">FTP Data Gagal PUT-02</h4>
              <h6 class="card-subtitle">Export Data Produksi Gagal ke Bank dengan metode PUT</h6>

              <form action="<?php echo base_url()?>ftp/ftp_upload02" method="post" enctype="multipart/form-data">
                  <div class="form-group row">
                      <div class="col-xs-6">
                          <input type="file" required class="btn btn-xs" name="upload_file">
                      </div>
                      <div class="col-xs-6">
                          <input type="submit" class="btn btn-xs btn-primary" name="Upload" value="Upload">
                      </div>
                  </div>
              </form>
              <div class="table-responsive">
                  <table id="put02" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                      <thead >
                          <tr >
                          <th>Tanggal</th>
                              <th>Nama File</th>
                              <th><i class="fa fa-download" aria-hidden="true"></i></th>
                          </tr>
                      </thead>
                  </table>
              </div>
          </div>
      </div>
  </div>

  <div class="col-md-6">
      <div class="card">
          <div class="card-body">
              <h4 class="card-title">FTP Data Berhasil PUT-03</h4>
              <h6 class="card-subtitle">Export Data Produksi Berhasil ke Bank dengan metode PUT</h6>

              <form action="<?php echo base_url()?>ftp/ftp_upload03" method="post" enctype="multipart/form-data">
                  <div class="form-group row">
                      <div class="col-xs-6">
                          <input type="file" required class="btn btn-xs" name="upload_file">
                      </div>
                      <div class="col-xs-6">
                          <input type="submit" class="btn btn-xs btn-primary" name="Upload" value="Upload">
                      </div>
                  </div>
              </form>
              <div class="table-responsive">
                  <table id="put03" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                      <thead >
                          <tr >
                          <th>Tanggal</th>
                              <th>Nama File</th>
                              <th><i class="fa fa-download" aria-hidden="true"></i></th>
                          </tr>
                      </thead>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
      <div class="card">
          <div class="card-body">
              <h4 class="card-title">FTP Data Restitusi PUT-04</h4>
              <h6 class="card-subtitle">Export data Restitusi ke Bank dengan metode PUT</h6>

              <form action="<?php echo base_url()?>ftp/ftp_upload04" method="post" enctype="multipart/form-data">
                  <div class="form-group row">
                      <div class="col-xs-6">
                          <input type="file" required class="btn btn-xs" name="upload_file">
                      </div>
                      <div class="col-xs-6">
                          <input type="submit" class="btn btn-xs btn-primary" name="Upload" value="Upload">
                      </div>
                  </div>
              </form>
              <div class="table-responsive">
                  <table id="put04" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                      <thead >
                          <tr >
                          <th>Tanggal</th>
                              <th>Nama File</th>
                              <th><i class="fa fa-download" aria-hidden="true"></i></th>
                          </tr>
                      </thead>
                  </table>
              </div>
          </div>
      </div>
  </div>
  <div class="col-md-6">
      <div class="card">
          <div class="card-body">
              <h4 class="card-title">FTP Data Klaim PUT-05</h4>
              <h6 class="card-subtitle">Export data Klaim ke Bank dengan metode PUT</h6>

              <form action="<?php echo base_url()?>ftp/ftp_upload05" method="post" enctype="multipart/form-data">
                  <div class="form-group row">
                      <div class="col-xs-6">
                          <input type="file" required class="btn btn-xs" name="upload_file">
                      </div>
                      <div class="col-xs-6">
                          <input type="submit" class="btn btn-xs btn-primary" name="Upload" value="Upload">
                      </div>
                  </div>
              </form>
              <div class="table-responsive">
                  <table id="put05" class="display nowrap table-hover table-bordered f-s-12" cellspacing="0" width="100%">
                      <thead >
                          <tr >
                          <th>Tanggal</th>
                              <th>Nama File</th>
                              <th><i class="fa fa-download" aria-hidden="true"></i></th>
                          </tr>
                      </thead>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>
