<div class="row">
    <div class="col-lg-12 m-t-0">

        <div class="card ">
            <!-- Data Debitur -->
            <div class="card-outline-fade2 p-t-30 p-b-20">
                <div class="card-header">
                    <h5 class="m-b-0 text-white">Data Debitur
                        <span class="m-l-40 m-t-10 m-b-10 text-right">No. Pinjaman : <?php echo $NomorPinjaman?></span>
                    </h5>
                </div>
            </div>
            <div class="card-body">
                    <div class="form-body">
                        <div class="table-responsive data-toggle">
                            <table id="my-lg-0 " class="display narrow table-hover table-sm f-s-14" cellspacing="0" width="100%">
                                <tbody>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Tgl Kredit :</td>
                                        <td class="text-dark w-25"><?php echo my_date_indo($debitur->TglAkadKredit);?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Tgl. Akhir Kredit :</td>
                                        <td class="text-dark w-25"><?php echo my_date_indo($debitur->TglAkhirKredit);?></td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">CIF :</td>
                                        <td class="text-dark w-25"><?php echo $debitur->cif;?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Nama Debitur :</td>
                                        <td class="text-dark w-25"><?php echo $debitur->NamaDebitur;?></td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Tgl. Lahir :</td>
                                        <td class="text-dark w-25"><?php echo my_date_indo($debitur->TglLahir);?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Usia :</td>
                                        <td class="text-dark w-25"><?php echo str_replace('.',' Tahun ',$debitur->UsiaDebitur)." Bulan";?></td>
                                        <td class="w-3"></td>
                                    </tr>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Kategori :</td>
                                        <td class="text-dark w-25"><?php echo cekPekerjaan($debitur->KodePekerjaan);?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Status :</td>
                                        <td class="text-dark w-25"><?php echo $debitur->TcKet;?></td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Tenor :</td>
                                        <td class="text-dark w-25"><?php echo $debitur->TenorTahun." Tahun (".$debitur->TenorBulan." Bulan)";?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Plafon :</td>
                                        <td class="text-dark w-25"><?php echo price($debitur->plafon);?></td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Premi :</td>
                                        <td class="text-dark w-25"><?php echo price($debitur->premi);?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Total Premi :</td>
                                        <td class="text-dark w-25"><?php echo price($debitur->JumlahPremiTenor);?></td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td class="w-12 text-right">Asuradur :</td>
                                        <td class="text-dark w-25"><?php echo cekNamaAsdur($debitur->kodeAsuransi)?></td>
                                        <td>&nbsp;</td>
                                        <td class="w-12 text-right">Polis :</td>
                                        <td class="text-dark w-25"><?php echo $debitur->NomorPolis?></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td class="w-12 text-right">Bank :</td>
                                        <td class="text-dark w-25"><?php echo cekNamaBank($debitur->kodeBank)?></td>
                                        <td>&nbsp;</td>
                                        <td class="w-12 text-right">Tgl. Polis :</td>
                                        <td class="text-dark w-25"><?php echo my_date_indo($debitur->TglPolis)?></td>
                                        <td>&nbsp;</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>

                
            </div>
            <!-- Data Debitur -->
            <!-- Form Klaim (bikin errro=align-self-center) -->
            <form class="form-horizontal " role="form" action="<?php echo base_url()?>klaim/saveKlaim" method="post" enctype="multipart/form-data">    
            <div class="card-outline-fade3 p-t-30 p-b-20">
                <div class="card-header">
                    <h5 class="m-b-0 text-white">Form Klaim
                        <span class="m-l-40 m-t-10 m-b-10 text-right">No. Klaim : KLM<?php echo $NomorRegistrasi?></span>
                    </h5>
                </div>
            </div>
            <div class="card-body" role="form">
                <div class="form-body">
                    <div class="table-responsive">
                        <input type="hidden" name="KodeKlaim" value="KLM<?php echo $NomorRegistrasi?>">
                        <input type="hidden" name="NomorRegistrasi" value="<?php echo $NomorRegistrasi?>">
                        <table id="my-lg-0" class="display narrow table-sm f-s-14" cellspacing="0" width="100%">
                            <tbody>

                                <tr>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-15"> Tanggal Klaim </td>
                                    <td class="w-20 ">
                                            <?php if(!empty($pinjaman->TglKlaim)){ echo date_format(date_create($pinjaman->TglKlaim),'d-m-Y'); } else { echo date("d-m-Y"); } ?>
                                        <input type="hidden" class="form-control f-s-14" name="tglklaim" id="tglklaim" value="<?php if(!empty($pinjaman->TglKlaim)){ echo date_format(date_create($pinjaman->TglKlaim),'d-m-Y'); } else { echo date('d-m-Y'); } ?>">
                                    </td>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-15"> Tanggal Risiko </td>
                                    <td class="w-20 ">
                                        <div class="col-xs-4 has-warning">
                                            <input type="text" class="form-control f-s-14 datepicker" name="tglresiko" id="tglresiko" value="<?php if(!empty($pinjaman->TglResiko)){ echo date_format(date_create($pinjaman->TglResiko),'d-m-Y'); }?>">
                                        </div>
                                    </td>
                                    <td class="w-3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-15">Nominal Diajukan </td>
                                    <td class="w-20 ">
                                        <div class="col-xs-4 has-warning">
                                            <input type="text" class="form-control f-s-14 price" placeholder="Rp." name="nominaldiajukan" value="<?php if(!empty($pinjaman->NominalDiajukan)){ echo price($pinjaman->NominalDiajukan);}?>">
                                        </div>
                                    </td>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-15"> Jenis Risiko </td>
                                    <td class="w-20">
                                        <div class="col-xs-4 has-warning">
                                            <select class="form-control custom-select f-s-14" name="idResiko"  id="idResiko">
                                                <option value=""></option>
                                                <?php foreach ($resiko as $key): ?>
                                                <option value="<?php echo $key->idResiko?>" <?php if (!empty($pinjaman->idResiko)){ if($key->idResiko==$pinjaman->idResiko){ echo "selected";} }?>><?php echo $key->Resiko?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </td>
                                    <td class="w-3">&nbsp;</td>
                                </tr>
                                <tr>
                                <td class="w-3">&nbsp;</td>

                                <td colspan="6">Kronologi</td>

                            </tr>
                                <tr>
                                    <td colspan="7">
                                        <div class="card-body">
                                            <form method="post">
                                                <div class="form-group">
                                                    <textarea class="textarea_editor form-control f-s-14" rows="15" placeholder="uraikan..." style="height:200px; width: 100%" name="kronologi"  id="kronologi"><?php if(!empty($pinjaman->kronologi)){ echo $pinjaman->kronologi;}?>
                                                    </textarea>
                                                </div>
                                            </form>
                                        </div>
                                    </td>
                                </tr>

                                
                            </tbody>
                        </table>
                        <!--/span-->
                    </div>
                </div>

            </div>
            <!-- Form Klaim -->
            <!-- Dokumen Klaim -->
            <div class="card-outline-fade3 p-t-30 p-b-20">
                <div class="card-header">
                    <h5 class="m-b-0 text-white">Dokumen Klaim
                        <span class="m-l-10 m-t-10 m-b-10 text-right">No. Klaim KLM<?php echo $NomorRegistrasi?></span>
                    </h5>
                </div>
            </div>
            <style type="text/css">
               /*  
                input[type="file"] {
                    display: none;
                } */
            </style>
            <div class="card-body">
                <!-- Dokumen Umum -->
                <h5 class="box-title text-center "> Dokumen Umum </h5>
                <div class="table-responsive">
                    <table id="" class="display narrow table-sm table-striped f-s-14 " cellspacing="0" width="100%">
                        <tbody>
                            <?php $no=1; foreach ($umum as $value): ?>
                            <tr>
                                <td class="w-3">&nbsp;</td>
                                <td class="w-50"><?php echo $value->dokumen;?></td>
                                <td align="right">
                                    <div class="form-actions p-2">
                                        <label  class="btn btn-outline-warning btn-sm">
                                            <input type="hidden" name="no[]" value="<?php echo $no;?>">
                                            <input type="hidden" name="dokumen<?php echo $no;?>" value="<?php echo $value->dokumen;?>">
                                            <input type="file" name="fileumum<?php echo $no;?>" value="<?php if(!empty($pinjaman->KodeKlaim)) { echo cekDokumenUmum($pinjaman->KodeKlaim,$value->dokumen); }?>">
                                            <!-- <i class="fa fa-upload"></i>  -->
                                            <!-- Upload -->
                                        </label>
                                        <?php if(!empty($pinjaman->KodeKlaim)) { if (!empty(cekDokumenUmum($pinjaman->KodeKlaim,$value->dokumen))){ ?>
                                        <label class="btn btn-outline-primary btn-sm">
                                            <a target="_blank" href="<?php echo base_url()?>upload/klaim/<?php echo cekDokumenUmum($pinjaman->KodeKlaim,$value->dokumen)?>">
                                            <i class="fa fa-eye"></i> Lihat <?php echo cekDokumenUmum($pinjaman->KodeKlaim,$value->dokumen)?></a>
                                        </label>
                                        <?php } }?>
                                    </div>
                                </td>
                            </tr> 
                            <?php $no++; endforeach ?>
                        </tbody>
                    </table>

                    <!-- Dokumen Khusus -->
                    <h5 class="box-title text-center p-t-20  "> Dokumen Khusus </h5>
                    <?php if (!empty($pinjaman)): ?>
                    <table class="display narrow table-sm table-striped f-s-14" cellspacing="0" width="100%" id="khusus">
                        <tbody>
                            <?php $ab =1; foreach ($khusus as $key): ?>                                
                            <tr>
                                <td class="w-3">&nbsp;</td>
                                <td class="w-50"><?php echo $key->dokumen?></td>
                                <td align="right">
                                    <div class="form-actions p-2">
                                        <label  class="btn btn-outline-warning btn-sm">
                                            <input type="hidden" name="ab[]" value="<?php echo $ab?>">
                                            <input type="hidden" name="khusus<?php echo $ab?>" value="<?php echo $key->dokumen?>">
                                            <input type="file" name="filekhusus<?php echo $ab?>" value="<?php echo cekDokumenKhusus($pinjaman->KodeKlaim,$key->dokumen)?>">
                                        </label>
                                        <?php if (!empty(cekDokumenKhusus($pinjaman->KodeKlaim,$key->dokumen))): ?>
                                        <label class="btn btn-outline-primary btn-sm">
                                            <a target="_blank" href="<?php echo base_url()?>upload/klaim/<?php echo cekDokumenKhusus($pinjaman->KodeKlaim,$key->dokumen)?>">
                                            <i class="fa fa-eye"></i> Lihat <?php echo cekDokumenKhusus($pinjaman->KodeKlaim,$key->dokumen)?></a>
                                        </label>
                                        <?php endif ?>
                                    </div>
                                </td>
                            </tr>
                            <?php $ab++; endforeach ?>
                        </tbody>
                    </table>    
                    <?php else: ?>
                    <table class="display narrow table-sm table-striped f-s-14" cellspacing="0" width="100%" id="khusus">
                       
                    </table>
                    <?php endif ?>
                </div>
                <div class="col-lg-3">&nbsp;</div>
                <div class="form-actions p-2 pull-left">
                    <a href="<?php echo base_url()?>debitur/cari" class="btn btn-dark btn-sm"><i class="fa fa-undo"></i> Kembali</a>
                </div>
                <div class="form-actions p-2 pull-right">
<!--                     <button type="submit" class="btn  btn-danger btn-sm">
                        <i class="fa fa-close"></i> Hapus </button> -->
                    
                    <button type="submit" class="btn  btn-warning btn-sm">
                        <i class="fa fa-paper-plane "></i> Ajukan </button>
<!--                     <button type="submit" class="btn  btn-warning btn-sm">
                        <i class="fa fa-check"></i> Setujui </button> -->
                </div>
            </div>
            </form>
        </div>
    </div>

</div>