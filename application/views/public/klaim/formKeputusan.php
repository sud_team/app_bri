<div class="row">
    <div class="col-lg-12 m-t-0">

        <div class="card ">
            <!-- Tracking Bar -->
            <div class="card-body text-center"> Tracking Klaim </div>

            <div class="row bs-wizard  " style="border-bottom:0;">
                <div class=" bs-wizard-step complete w-20">
                    <div class="text-center bs-wizard-stepnum">Diajukan</div>
                    <?php if ($pinjaman->diajukan==1): ?>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center col-m-2"><?php echo my_date_indo($pinjaman->create_date);?></div>
                    <?php endif ?>
                </div>

                <div class=" bs-wizard-step complete  w-20">
                    <!-- complete -->
                    <div class="text-center bs-wizard-stepnum">Disposisi SPV</div>
                    <?php if ($pinjaman->spv==1): ?>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center col-xs-2"><?php echo my_date_indo($pinjaman->create_spv);?> </div>
                    <?php endif ?>
                </div>

                <div class=" bs-wizard-step complete w-20">
                    <!-- complete -->
                    <div class="text-center bs-wizard-stepnum">Verifikasi Pialang</div>
                    <?php if ($pinjaman->pialang==1): ?>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center col-xs-2"><?php echo my_date_indo($pinjaman->create_pialang);?> </div>
                    <?php endif ?>
                </div>

                <div class=" bs-wizard-step complete w-20">
                    <!-- active -->
                    <div class="text-center bs-wizard-stepnum">Penilaian Asuransi</div>
                    <?php if ($pinjaman->asuransi==1): ?>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center "><?php echo my_date_indo($pinjaman->create_asuransi);?> </div>
                    <?php endif ?>
                </div>

                <div class=" bs-wizard-step complete  w-20">
                    <!-- active -->
                    <div class="text-center bs-wizard-stepnum">Penyelesaian</div>
                    <?php if ($pinjaman->selesai==1): ?>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"><?php echo my_date_indo($pinjaman->create_selesai);?> </div>
                    <?php endif ?>
                </div>
            </div>
            <!-- Tracking Bar -->
            <!-- Data Debitur -->
            <div class="card-outline-fade2 p-t-30 p-b-20">
                <div class="card-header">
                    <h5 class="m-b-0 text-white">Data Debitur
                        <span class="m-l-40 m-t-10 m-b-10 text-right">No. Pinjaman : <?php echo $NomorPinjaman?></span>
                    </h5>
                </div>
            </div>
            <div class="card-body">
                    <div class="form-body">
                        <div class="table-responsive data-toggle">
                            <table id="my-lg-0 " class="display narrow table-hover table-sm f-s-14" cellspacing="0" width="100%">
                                <tbody>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Tgl Kredit :</td>
                                        <td class="text-dark w-25"><?php echo my_date_indo($debitur->TglAkadKredit);?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Tgl. Akhir Kredit :</td>
                                        <td class="text-dark w-25"><?php echo my_date_indo($debitur->TglAkhirKredit);?></td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">CIF :</td>
                                        <td class="text-dark w-25"><?php echo $debitur->cif;?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Nama Debitur :</td>
                                        <td class="text-dark w-25"><?php echo $debitur->NamaDebitur;?></td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Tgl. Lahir :</td>
                                        <td class=" text-dark w-25"><?php echo my_date_indo($debitur->TglLahir);?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Usia :</td>
                                        <td class=" text-dark w-25"><?php echo str_replace('.',' Tahun ',$debitur->UsiaDebitur)." Bulan";?></td>
                                        <td class="w-3"></td>
                                    </tr>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Kategori :</td>
                                        <td class=" text-dark w-25"><?php echo cekPekerjaan($debitur->KodePekerjaan);?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Status :</td>
                                        <td class=" text-dark w-25"><?php echo $debitur->TcKet;?></td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Tenor :</td>
                                        <td class=" text-dark w-25"><?php echo $debitur->TenorTahun." Tahun (".$debitur->TenorBulan." Bulan)";?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Plafon :</td>
                                        <td class=" text-dark w-25"><?php echo price($debitur->plafon);?></td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Premi :</td>
                                        <td class=" text-dark w-25"><?php echo price($debitur->premi);?></td>
                                        <td class="w-3">&nbsp;</td>
                                        <td class="w-12 text-right">Total Premi :</td>
                                        <td class=" text-dark w-25"><?php echo price($debitur->JumlahPremiTenor);?></td>
                                        <td class="w-3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td class="w-12 text-right">Asuradur :</td>
                                        <td class=" text-dark w-25"><?php echo cekNamaAsdur($debitur->kodeAsuransi)?></td>
                                        <td>&nbsp;</td>
                                        <td class="w-12 text-right">Polis :</td>
                                        <td class=" text-dark w-25"><?php echo $debitur->NomorPolis?></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td class="w-12 text-right">Bank :</td>
                                        <td class=" text-dark w-25"><?php echo cekNamaBank($debitur->kodeBank)?></td>
                                        <td>&nbsp;</td>
                                        <td class="w-12 text-right">Tgl. Polis :</td>
                                        <td class=" text-dark w-25"><?php echo my_date_indo($debitur->TglPolis)?></td>
                                        <td>&nbsp;</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>

                
            </div>
            <!-- Data Debitur -->
            <!-- Form Klaim -->
            <div class="card-outline-fade3 p-t-30 p-b-20">
                <div class="card-header">
                    <h5 class="m-b-0 text-white">Form Klaim
                        <span class="m-l-40 m-t-10 m-b-10 text-right">No. Klaim : KLM<?php echo $NomorRegistrasi?></span>
                    </h5>
                </div>
            </div>
            <div class="card-body" role="form">
                <div class="form-body">
                    <div class="table-responsive">
                        <input type="hidden" name="KodeKlaim" value="KLM<?php echo $NomorRegistrasi?>">
                        <input type="hidden" name="NomorRegistrasi" value="<?php echo $NomorRegistrasi?>">
                        <table id="my-lg-0" class="display narrow table-sm f-s-14" cellspacing="0" width="100%">
                            <tbody>

                                <tr>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-15"> Tanggal Klaim </td>
                                    <td class="w-20 ">: <?php echo date_format(date_create($pinjaman->TglKlaim),'d/m/Y')?>
                                    </td>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-15"> Tanggal Risiko </td>
                                    <td class="w-20 ">: <?php echo date_format(date_create($pinjaman->TglResiko),'d/m/Y')?>
                                    </td>
                                    <td class="w-3">&nbsp;</td>
                                </tr>

                                

                                <tr>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-15">Nominal Diajukan </td>
                                    <td class="w-20 ">: <?php echo price($pinjaman->NominalDiajukan);?>
                                    </td>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-10"> Jenis Risiko </td>
                                    <td class="w-50">:
                                                <?php foreach ($resiko as $key): ?>
                                                <?php if($key->idResiko==$pinjaman->idResiko){ echo $key->Resiko;}?>
                                                <?php endforeach ?>
                                    </td>
                                    <td class="w-3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-15">Kronologi</td>
                                    <td class="w-20">:</td>
                                    </tr>
                                    <tr>                                                            
                                    <td class="w-3">&nbsp;</td>
                                    <td colspan="5" style="line-height: normal "> <?php echo $pinjaman->kronologi?></td>
                                </tr>
                            </tbody>
                        </table>
                        <!--/span-->
                    </div>
                </div>

            </div>
            <!-- Form Klaim -->
            <!-- Dokumen Klaim -->
            <div class="card-outline-fade3 p-t-30 p-b-20">
                <div class="card-header">
                    <h5 class="m-b-0 text-white">Dokumen Klaim
                        <span class="m-l-10 m-t-10 m-b-10 text-right">No. Klaim KLM<?php echo $NomorRegistrasi?></span>
                    </h5>
                </div>
            </div>
            <div class="card-body">
                <!-- Dokumen Umum -->
                <h5 class="box-title text-center "> Dokumen Umum </h5>
                <div class="table-responsive">
                    <table id="" class="display narrow table-sm table-striped f-s-14" cellspacing="0" width="100%">
                        <tbody>
                            <?php $no=1; foreach ($umum as $value): ?>
                            <tr>
                                <td class="w-3">&nbsp;</td>
                                <td class="w-50"><?php echo $value->dokumen;?></td>
                                <td align="right">
                                    <div class="form-actions p-2">
                                        <?php if (!empty(cekDokumenUmum($pinjaman->KodeKlaim,$value->dokumen))): ?>
                                        <label class="btn btn-outline-primary btn-sm">
                                            <a target="_blank" href="<?php echo base_url()?>upload/klaim/<?php echo cekDokumenUmum($pinjaman->KodeKlaim,$value->dokumen)?>">
                                            <i class="fa fa-eye"></i> Lihat <?php echo cekDokumenUmum($pinjaman->KodeKlaim,$value->dokumen)?></a>
                                        </label>
                                        <?php endif ?>
                                    </div>
                                </td>
                            </tr> 
                            <?php $no++; endforeach ?>
                        </tbody>
                    </table>

                    <!-- Dokumen Khusus -->
                    <h5 class="box-title text-center p-t-20  "> Dokumen Khusus </h5>
                    <?php if (!empty($pinjaman)): ?>
                    <table class="display narrow table-sm table-striped f-s-14" cellspacing="0" width="100%" id="khusus">
                        <tbody>
                            <?php $ab =1; foreach ($khusus as $key): ?>                                
                            <tr>
                                <td class="w-3">&nbsp;</td>
                                <td class="w-50"><?php echo $key->dokumen?></td>
                                <td align="right">
                                    <div class="form-actions p-2">
                                        <?php if (!empty(cekDokumenKhusus($pinjaman->KodeKlaim,$key->dokumen))): ?>
                                        <label class="btn btn-outline-primary btn-sm">
                                            <a target="_blank" href="<?php echo base_url()?>upload/klaim/<?php echo cekDokumenKhusus($pinjaman->KodeKlaim,$key->dokumen)?>">
                                            <i class="fa fa-eye"></i> Lihat <?php echo cekDokumenKhusus($pinjaman->KodeKlaim,$key->dokumen)?></a>
                                        </label>
                                        <?php endif ?>
                                    </div>
                                </td>
                            </tr>
                            <?php $ab++; endforeach ?>
                        </tbody>
                    </table>    
                    <?php else: ?>
                    <table class="display narrow table-sm table-striped f-s-14" cellspacing="0" width="100%" id="khusus">
                       
                    </table>
                    <?php endif ?>
                </div>
                <div class="col-lg-3">&nbsp;</div>
                <div class="form-actions p-2 pull-left">
                    <a href="<?php echo base_url()?>klaim/<?php echo $status;?>" class="btn btn-dark btn-sm"><i class="fa fa-undo"></i> Kembali</a>
                </div>
                <?php if (cekDokKlaim($pinjaman->KodeKlaim)->jml==cekDokKlaim($pinjaman->KodeKlaim)->upload){ ?>

                <?php if ($this->session->userdata("hak_akses")==4 && $this->session->userdata("roles")==2 || $this->session->userdata("roles")==1): ?>                    
                <div class="form-actions p-2 pull-right">
                    <?php if ($pinjaman->diajukan==1 && $pinjaman->spv==0 && $pinjaman->pialang==0 && $pinjaman->asuransi==0 && $pinjaman->selesai==0): ?>
                    <a href="<?php echo base_url()?>klaim/tolakSpv/KLM<?php echo $pinjaman->NomorRegistrasi?>" class="btn  btn-warning btn-sm"><i class="fa fa-times"></i> Tolak</a>
                    <a href="<?php echo base_url()?>klaim/approveSpv/KLM<?php echo $pinjaman->NomorRegistrasi?>" class="btn  btn-info btn-sm">
                        <i class="fa fa-paper-plane "></i> Approve </a>
                    <?php endif ?>
                </div>
                <?php endif ?>

                <?php } else { ?>
                <div class="form-actions p-2 pull-right">
                    Dokumen Belum Lengkap
                </div>
                <?php } ?>


                <?php if ($this->session->userdata("roles")==3 || $this->session->userdata("roles")==1): ?>                    
                <div class="form-actions p-2 pull-right">
                    <?php if ($pinjaman->diajukan==1 && $pinjaman->spv==1 && $pinjaman->pialang==0 && $pinjaman->asuransi==0 && $pinjaman->selesai==0): ?>
                    <a href="<?php echo base_url()?>klaim/formKlaimUpdate/<?php echo $pinjaman->NomorRegistrasi?>" type="submit" class="btn  btn-warning btn-sm">
                    <i class="fa fa-refresh" aria-hidden="true"></i> Update </a>

                    <a href="<?php echo base_url()?>klaim/approvePialang/KLM<?php echo $pinjaman->NomorRegistrasi?>" type="submit" class="btn  btn-primary btn-sm">
                    <i class="fa fa-check" aria-hidden="true"></i> Setujui </a>
                    <?php endif ?>
                </div>
                <?php endif ?>

            </div>



            <?php if ($this->session->userdata("roles")==4 || $this->session->userdata("roles")==1): ?>                
            <?php if ($pinjaman->diajukan==1 && $pinjaman->spv==1 && $pinjaman->pialang==1): ?>
            <div class=" card-outline-fade5 p-t-30 p-b-20">
                <div class="card-header">
                    <h5 class="m-b-0 text-white"> Penilaian Klaim
                        <span class="m-l-40 m-t-10 m-b-10 text-right">No. Klaim : KLM<?php echo $debitur->NomorRegistrasi?></span>
                    </h5>
                </div>
            </div>
            <div class="card-body">
                <form class="form-horizontal align-self-center" role="form" action="<?php echo base_url()?>klaim/saveKeputusan" method="post" enctype="multipart/form-data">
                <input type="hidden" name="KodeKlaim" value="KLM<?php echo $pinjaman->NomorRegistrasi;?>">
                <div class="table-responsive">
                    <table class="display narrow table-sm f-s-14" cellspacing="0" width="100%">
                        <tbody>
                            <tr>
                                <td class="w-3">&nbsp;</td>
                                <td class="w-15"> Keputusan </td>
                                <td class="w-20 ">
                                    <?php if ($pinjaman->keputusan==1): ?>
                                        : Disetujui
                                    <?php else: ?>
                                    <div class="col-xs-4 has-error ">
                                        <select class="form-control custom-select f-s-14" name="keputusan">
                                            <option value="1">Disetujui</option>
                                            <option value="-2">Ditolak</option>
                                        </select>
                                    </div>
                                    <?php endif ?>
                                </td>
                                <td class="w-3">&nbsp;</td>
                                <td class="w-15"> Nominal Persetujuan </td>
                                <td class="w-20 ">
                                    <?php if ($pinjaman->NominalDisetujui!=0): ?>
                                        : <?php echo price($pinjaman->NominalDisetujui)?>
                                    <?php else: ?>
                                    <div class="col-xs-4 has-error">
                                        <input type="text" class="form-control f-s-14 price" name="nominaldisetujui" placeholder="Rp.">
                                    </div>
                                    <?php endif ?>
                                </td>
                                <td class="w-3">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="w-3" rowspan="2">&nbsp;</td>
                                <td class="w-10" rowspan="2"> Keterangan Tambahan </td>
                                <td class="w-20" colspan="4">
                                    <?php if ($pinjaman->keputusan==1): ?>
                                        : <?php echo $pinjaman->keterangan?>
                                    <?php else: ?>
                                    <div class="col-xs-4 has-error">
                                        <input type="text" class="form-control f-s-14" placeholder=" " name="keterangan">
                                    </div>
                                    <?php endif ?>
                                </td>
                                <td class="w-3">&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-3">&nbsp;</div>
                <?php if ($pinjaman->diajukan==1 && $pinjaman->spv==1 && $pinjaman->pialang==1 && $pinjaman->asuransi==0 && $pinjaman->selesai==0): ?>
                <div class="form-actions p-2 pull-right">
                    <!-- <button type="submit" class="btn  btn-info btn-sm">
                        <i class="fa fa-download"></i> Downloadn Semua Dokumen </button>
                    <button type="submit" class="btn  btn-info btn-sm">
                        <i class="fa fa-download"></i> Downloadn Form Underwriting </button> -->
                    <button type="submit" class="btn  btn-danger btn-sm">
                        <i class="fa fa-save"></i> Simpan Keputusan </button>
                </div>
            <?php endif ?>

            </form>
            </div>
            <?php endif ?>
            <?php endif ?>
            <?php if ($pinjaman->keputusan=="-2"): ?>
                <div class=" card-outline-fade5 p-t-30 p-b-20">
                    <div class="card-header">
                        <h5 class="m-b-0 text-white"> Penilaian Klaim
                            <span class="m-l-40 m-t-10 m-b-10 text-right">No. Klaim : KLM<?php echo $debitur->NomorRegistrasi?></span>
                        </h5>
                    </div>
                </div>
                <div class="card-body">
                    <form class="form-horizontal align-self-center" role="form" action="<?php echo base_url()?>klaim/saveKeputusan" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="KodeKlaim" value="KLM<?php echo $pinjaman->NomorRegistrasi;?>">
                    <div class="table-responsive">
                        <table class="display narrow table-sm f-s-14" cellspacing="0" width="100%">
                            <tbody>
                                <tr>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-15"> Keputusan </td>
                                    <td class="w-20 ">
                                        <?php if ($pinjaman->keputusan==1): ?>
                                            <b>: Disetujui</b>
                                        <?php else: ?>
                                            <b>: Ditolak</b>
                                        <?php endif ?>
                                    </td>
                                    <td class="w-3">&nbsp;</td>
                                    <td class="w-15"> Nominal Persetujuan </td>
                                    <td class="w-20 ">
                                        <?php if ($pinjaman->NominalDisetujui!=0): ?>
                                            <b>: <?php echo price($pinjaman->NominalDisetujui)?></b>
                                        <?php endif ?>
                                    </td>
                                    <td class="w-3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="w-3" rowspan="2">&nbsp;</td>
                                    <td class="w-10" rowspan="2"> Keterangan Tambahan </td>
                                    <td class="w-20" colspan="4">
                                        <?php if ($pinjaman->keputusan==1): ?>
                                            <b>: <?php echo $pinjaman->keterangan?></b>
                                        <?php else: ?>
                                            <b>: <?php echo $pinjaman->keterangan?></b>
                                        <?php endif ?>
                                    </td>
                                    <td class="w-3">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </form>
                </div>
            <?php endif ?>
        </div>
    </div>

</div>