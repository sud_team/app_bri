<div class="header">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <!-- Logo -->
        <div class="navbar-header">
            <a class="navbar-brand" href="javascript:void(0)">
                <img src="<?php echo base_url() ?>assets/ela/images/logo.png" alt=" " class="dark-logo nav-toggler" />
            </a>
            <a class="navbar-brand hvr-bounce-out hidden-sm-down" href="http://agraindonesia.com/id/beranda/" target="_blank">
                <span><img src="<?php echo base_url() ?>assets/ela/images/logo-text.png" alt=" " class="dark-logo hidden-sm-down" /></span>
            </a>
        </div>


        <!-- End Logo -->
        <div class="navbar-collapse">
            <!-- toggle and nav items -->
            <ul class="navbar-nav mt-md-0">
                <!-- This is  -->
                 <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down  text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
            </ul>

            <ul class="navbar-nav m-auto hidden-sm-down">
                <a href="#" class="hvr-bounce-out"><img src="<?php echo base_url() ?>assets/ela/images/logorep.png" alt="Logo BRI" class="dark-logo" /> </a>
            </ul>

            <!-- User profile and search -->
            <ul class="navbar-nav my-lg-0 float-right">
                <!-- VIA Chat -->
                <li class="nav-item dropdown col-sm-4">
                    <a class="nav-link dropdown-toggle  text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-comments" aria-hidden="true" placeholder="Rp.">
                            <span class="label label-rounded label-danger hidden-xs-down"><?php echo count_chat_all(); ?></span>
                        </i>
                        <!-- <div class="notify"> -->
                        <!-- <span class="heartbit"></span>  -->
                        <!-- <span class="point"> </span>  -->
                        <!-- </div> -->
                    </a>
                    <div class="dropdown-menu dropdown-menu-right mailbox animated slideInDown bg-inverse"  style="z-index: -20;">
                        <ul>

                            <li>
                                <div class="drop-title f-s-16 text-left"> <img src="<?php echo base_url() ?>assets/ela/images/logoviakecil.png" alt="homepage" class="w-70" /></div>
                            </li>
                            <li>
                                <div class="message-center">
                                    <!-- Message -->
                                    <?php /*foreach (not_online()->result() as $item): ?>
                                                <a href="javascript:;" data-friend="<?php echo $item->send_to ?>">
                                                    <img src="<?php echo base_url()?>assets/ela/offline.png" style="width: 10px;"> <?php echo $item->username ?> <span class="label label-rounded label-danger"><?php echo count_chat($item->send_to); ?></span></a>
                                                <?php endforeach */ ?>
                                    <?php foreach (is_online()->result() as $item) : ?>
                                        <a href="javascript:;" data-friend="<?php echo $item->idUser ?>">
                                            <img src="<?php echo base_url() ?>assets/ela/online.png" style="width: 10px;"> <?php echo $item->username ?>
                                            <?php if (count_chat($item->idUser) > 0) : ?>
                                                <span class="label label-rounded label-danger"><?php echo count_chat($item->idUser); ?></span>
                                            <?php endif ?>
                                        </a>
                                    <?php endforeach ?>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <!-- Calculator  -->
                <li class="nav-item dropdown mega-dropdown">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-calculator text-muted"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right mailbox animated slideInDown mailbox p-5 bg-inverse" style="z-index: -20;">
 
                        <ul class="mega-dropdown-menu">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kalkupremi" role="tab">
                                        <span class="hidden-sm-up"><b>PREMI</b>
                                        </span>
                                        <span class="hidden-xs-down"><b>PREMI</b></span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " data-toggle="tab" href="#kalkuprapen" role="tab">
                                        <span class="hidden-sm-up"><b>PRA PEN</b>
                                        </span>
                                        <span class="hidden-xs-down"><b>PRA PEN</b></span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " data-toggle="tab" href="#kalkuresti" role="tab">
                                        <span class="hidden-sm-up"><b>RESTITUSI</b>
                                        </span>
                                        <span class="hidden-xs-down"><b>RESTITUSI</b></span>
                                    </a>
                                </li>

                            </ul>
                            <div class="tab-content tabcontent p-5">
                                <div class="tab-pane active p-10" id="home" role="tabpanel">
                                    <div class="card-title">
                                        <h3 class="text-light">KALKULATOR SIMULASI</h3>
                                    </div>
                                    <div class="card-subtitle">
                                        <p class="text-light">Untuk melakukan simulasi perhitungan Premi Asuransi dan simulasi Perhitungan Restitusi</p>
                                    </div>
                                </div>
                                <!-- KALKULATOR PREMI -->
                                <div class="tab-pane p-10" id="kalkupremi" role="tabpanel">
                                    <div>
                                        <h6 class="card-subtitle text-light">Simulasi perhitungan premi Asuransi Kredit</h6>
                                        <form>
                                            <div>
                                                <small class="text-light f-s-10"> Plafon pinjaman </small>

                                                <input type="text" class="form-control input-sm has-success price" id="plafon" placeholder="Rp.">
                                                <small class="text-light f-s-10"> Suku bunga pinjaman </small>

                                                <input type="text" class="form-control input-sm has-success price" id="bunga" placeholder="%">
                                            </div>
                                            <div>
                                                <small class="text-light f-s-10"> Kategori debitur </small>
                                                <br>
                                                <select id="rPremi" class="form-control f-s-12 bg-white" name="rPremi">
                                                    <option value="">Pilih</option>
                                                    <option value="0.004"> PNS / Swasta / Wirausaha (0.004)</option>
                                                    <option value="0.005"> Pensiunan (0.005)</option>
                                                    <option value="0.00375"> Karyawan Bank (0.00375)</option>
                                                </select>
                                            </div>
                                            <div>
                                                <small class="text-light f-s-10"> Tenor pinjaman kredit </small>
                                                <br>
                                                <select id="period" class="form-control f-s-12 bg-white" name="period">
                                                    <option value="">Pilih</option>
                                                    <option value="1"> 1 tahun</option>
                                                    <option value="2"> 2 tahun</option>
                                                    <option value="3"> 3 tahun</option>
                                                    <option value="4"> 4 tahun</option>
                                                    <option value="5"> 5 tahun</option>
                                                    <option value="6"> 6 tahun</option>
                                                    <option value="7"> 7 tahun</option>
                                                    <option value="8"> 8 tahun</option>
                                                    <option value="9"> 9 tahun</option>
                                                    <option value="10"> 10 tahun</option>
                                                    <option value="11"> 11 tahun</option>
                                                    <option value="12"> 12 tahun</option>
                                                    <option value="13"> 13 tahun</option>
                                                    <option value="14"> 14 tahun</option>
                                                    <option value="15"> 15 tahun</option>
                                                    <option value="16"> 16 tahun</option>
                                                    <option value="17"> 17 tahun</option>
                                                    <option value="18"> 18 tahun</option>
                                                    <option value="19"> 19 tahun</option>
                                                    <option value="20"> 20 tahun</option>

                                                </select>
                                            </div>
                                            <div>
                                                <button type="button" id="calc" class="btn btn-info btn-sm text-light m-t-10" value="Calculate">Hitung</button>
                                            </div>
                                            <div class="p-b-20">
                                                <small class="text-light f-s-10"> Asumsi Premi </small>
                                                <div type="text" class="form-control input-sm text-danger font-weight-bold f-s-16 " id="premitotal" placeholder="0"></div>
                                                </span>
                                            </div>
                                            <?php /*<div class="p-b-20">
                                                        <small class="text-light f-s-10"> Total premi sesuai tenor</small>
                                                        <div type="text" class="form-control input-sm text-danger font-weight-bold f-s-16 " id="premitotal" placeholder="0"></div>
                                                        </span>
                                                    </div>*/ ?>
                                        </form>
                                    </div>
                                </div>
                                <!-- KALKULATOR PRAPEN -->
                                <div class="tab-pane p-10" id="kalkuprapen" role="tabpanel">
                                    <div>
                                        <h6 class="card-subtitle text-light">Simulasi perhitungan premi Pra Pensiun</h6>
                                        <form>
                                            <div>
                                                <small class="text-light f-s-10"> Plafon pinjaman </small>

                                                <input type="text" class="form-control input-sm has-success price" id="plafonprapen" placeholder="Rp.">
                                                <small class="text-light f-s-10"> Usia Debitur </small>
                                                <input type="text" class="form-control input-sm has-success price" id="usiadebitur" placeholder="Usia berjalan">
                                                <small class="text-light f-s-10"> Batas Usia Pensiun </small>
                                                <input type="text" class="form-control input-sm has-success price" id="usiabatas" placeholder="Sesuai UU ASN">
                                            </div>
                                            <div>
                                                <small class="text-light f-s-10"> Tenor pinjaman kredit </small>
                                                <br>
                                                <select id="tenor" class="form-control f-s-12 bg-white" name="period">
                                                    <option value="0">Pilih</option>
                                                    <option value="1"> 1 tahun</option>
                                                    <option value="2"> 2 tahun</option>
                                                    <option value="3"> 3 tahun</option>
                                                    <option value="4"> 4 tahun</option>
                                                    <option value="5"> 5 tahun</option>
                                                    <option value="6"> 6 tahun</option>
                                                    <option value="7"> 7 tahun</option>
                                                    <option value="8"> 8 tahun</option>
                                                    <option value="9"> 9 tahun</option>
                                                    <option value="10"> 10 tahun</option>
                                                    <option value="11"> 11 tahun</option>
                                                    <option value="12"> 12 tahun</option>
                                                    <option value="13"> 13 tahun</option>
                                                    <option value="14"> 14 tahun</option>
                                                    <option value="15"> 15 tahun</option>
                                                    <option value="16"> 16 tahun</option>
                                                    <option value="17"> 17 tahun</option>
                                                    <option value="18"> 18 tahun</option>
                                                    <option value="19"> 19 tahun</option>
                                                    <option value="20"> 20 tahun</option>

                                                </select>
                                            </div>
                                            <div>
                                                <button type="button" id="hitungprapen" class="btn btn-info btn-sm text-light m-t-10" value="Calculate">Hitung</button>
                                            </div>
                                            <div class="p-b-20">
                                                <small class="text-light f-s-10"> Asumsi Premi </small>
                                                <div type="text" class="form-control input-sm text-danger font-weight-bold f-s-16 " id="premiprapen" placeholder="0"></div>
                                                </span>
                                            </div>
                                            <?php /*<div class="p-b-20">
                                                        <small class="text-light f-s-10"> Total premi sesuai tenor</small>
                                                        <div type="text" class="form-control input-sm text-danger font-weight-bold f-s-16 " id="premitotal" placeholder="0"></div>
                                                        </span>
                                                    </div>*/ ?>
                                        </form>
                                    </div>
                                </div>
                                <!-- KALKULATOR RESTITUSI -->
                                <div class="tab-pane p-10" id="kalkuresti" role="tabpanel">
                                    <div>
                                        <h6 class="card-subtitle text-light">Simulasi perhitungan nominal Restitusi</h6>
                                        <form>
                                            <div>
                                                <small class="text-light f-s-10"> Plafon pinjaman </small>

                                                <input type="text" class="form-control input-sm f-s-12 has-success price" id="plafonR" placeholder="Rp.">
                                            </div>
                                            <div>
                                                <small class="text-light f-s-10"> Kategori debitur </small>

                                                <select id="interest" class="form-control f-s-12 bg-white" name="rPremiR">
                                                    <option value="">Pilih</option>
                                                    <option value="0.004"> PNS / Swasta / Wirausaha (0.004)</option>
                                                    <option value="0.005"> Pensiunan (0.005)</option>
                                                    <option value="0.00375"> Karyawan Bank (0.00375)</option>
                                                </select>
                                            </div>
                                            <div>
                                                <small class="text-light f-s-10"> Tenor pinjaman kredit </small>

                                                <select id="periodR" class="form-control f-s-12 bg-white" name="periodR">
                                                    <option value="">Pilih</option>
                                                    <option value="1"> 1 tahun</option>
                                                    <option value="2"> 2 tahun</option>
                                                    <option value="3"> 3 tahun</option>
                                                    <option value="4"> 4 tahun</option>
                                                    <option value="5"> 5 tahun</option>
                                                    <option value="6"> 6 tahun</option>
                                                    <option value="7"> 7 tahun</option>
                                                    <option value="8"> 8 tahun</option>
                                                    <option value="9"> 9 tahun</option>
                                                    <option value="10"> 10 tahun</option>
                                                    <option value="11"> 11 tahun</option>
                                                    <option value="12"> 12 tahun</option>
                                                    <option value="13"> 13 tahun</option>
                                                    <option value="14"> 14 tahun</option>
                                                    <option value="15"> 15 tahun</option>
                                                    <option value="16"> 16 tahun</option>
                                                    <option value="17"> 17 tahun</option>
                                                    <option value="18"> 18 tahun</option>
                                                    <option value="19"> 19 tahun</option>
                                                    <option value="20"> 20 tahun</option>

                                                </select>
                                            </div>
                                            <div>
                                                <small class="text-light f-s-10"> Pembayaran berjalan </small>

                                                <input type="text" class="form-control input-sm f-s-12 has-success price" id="bulanBayar" placeholder="bulan">
                                            </div>
                                            <div>
                                                <button type="button" id="rest" class="btn btn-info btn-sm text-light m-t-10" value="Calculate">Hitung</button>
                                            </div>
                                            <div class="p-b-20">
                                                <small class="text-light f-s-10"> Nominal Restitusi</small>
                                                <div type="text" class="form-control input-sm text-danger font-weight-bold f-s-16 " id="sudahbayar" placeholder="0"></div>
                                                </span>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </ul>
                    </div>
                </li>
                <!-- FAQ -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle  text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="hidden-sm-up">
                            <i class="fa fa-question-circle"></i>
                            <div class="notify">
                                <!-- <span class="heartbit"></span> 
                                    <span class="point"></span>  -->
                            </div>
                        </span>
                        <span class="hidden-xs-down">
                            <i class="fa fa-question-circle"></i>
                            <div class="notify">
                                <!-- <span class="heartbit"></span> 
                                    <span class="point"></span>  -->
                            </div>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right mailbox animated slideInDown"  style="z-index: -20;">
                        <ul>
                            <li>
                                <div class="drop-title drop-title f-s-16 text-inverse"><b>FAQ</b></div>
                            </li>
                            <li>
                                <div class="message-center">
                                    <!-- <a>
                                        <button class="btn btn-success btn-sm fa fa-table m-r-5"></button>
                                        <div class="mail-contnet f-s-12 text-muted"> Untuk mencetak laporan fixed di menu Laporan Produksi, tentukan periode tanggal Rekonsel</div>
                                    </a>
                                    <a>
                                        <button class="btn btn-success btn-sm fa fa-bell m-r-5"></button>
                                        <div class="mail-contnet f-s-12 text-muted"> Kalkulator Pra Pensiun, untuk perhitungan premi pada produk KMGPA & KMGPP
                                        </div>
                                    </a> -->
                                    <!-- <a href="#" target="_blank">
                                               <button type="button" class="btn btn-success btn-sm fa fa-download m-r-5"></button>
                                                  <div class="mail-contnet f-s-12"> Download Panduan Pengguna Sistem 
                                                  </div>
                                              </a> -->
                                    <!-- Message -->
                                </div>

                            </li>
                        </ul>
                    </div>
                    <!-- End Comment -->
                <li class="nav-item ">
                    <a class="nav-link text-center  text-muted f-s-12" href="javascript:void(0)">
                        <?php
                        if ($this->session->userdata("hak_akses") == 1) {
                            echo "Administrator";
                        } else if ($this->session->userdata("hak_akses") == 2) {
                            echo "Admin";
                        } else if ($this->session->userdata("hak_akses") == 3) {
                            echo "Management";
                        } else if ($this->session->userdata("hak_akses") == 4) {
                            echo "SPV";
                        } else if ($this->session->userdata("hak_akses") == 7) {
                            echo "Produksi";
                        } else if ($this->session->userdata("hak_akses") == 8) {
                            echo "Keuangan";
                        } else if ($this->session->userdata("hak_akses") == 8) {
                            echo "Klaim";
                        } else {
                            echo "Staff";
                        }
                        ?>
                    </a>
                </li>
                <!-- Profile -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="i-circle">1</span></a>
                    <div  class="dropdown-menu dropdown-menu-right animated slideInDown bg-inverse mailbox" style="z-index: -20;">
                        <ul class="dropdown-user ">
                            <li>

                                <!-- Avatar Upload -->

                                <!-- <div class="avatar-upload">
                                    <div class="avatar-edit">
                                        <input type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                        <label for="imageUpload"></label>
                                    </div>
                                    <div class="avatar-preview ">
                                       
                                    </div>
                                </div> -->
                            </li>
                            <li>&nbsp;</li>

                            <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i> &nbsp;
                                    <?php if ($this->session->userdata("roles") == 2) : ?>
                                        <?php echo cekNamaBank($this->session->userdata("kodeBank")) ?>
                                    <?php else : ?>
                                        <?php echo $this->session->userdata("nama"); ?>
                                    <?php endif ?><br></a></li>
                            <li><a href="#" id="changePassword"><i class="fa fa-key" aria-hidden="true"></i>
                                    </i> &nbsp;Ganti Password <br></a></li>
                            <!--                                     <li><a href="#"><i class="ti-wallet"></i> Balance</a></li>
                                    <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                                    <li><a href="#"><i class="ti-settings"></i> Setting</a></li> -->
                            <li><a href="<?php echo base_url() ?>login/logout"><i class="fa fa-power-off"></i> &nbsp;Logout</a></li>
                            <li>&nbsp</li>

                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</div>
<!-- End header header -->

<script language="javascript">
    jam();

    function jam() {
        var now = new Date();
        var hours = now.getHours();
        var minutes = now.getMinutes();
        var seconds = now.getSeconds()
        var timeValue = "" + ((hours > 23) ? hours - 24 : hours)
        timeValue = ((hours < 10) ? "0" : "") + hours
        timeValue += ((minutes < 10) ? ":0" : ":") + minutes
        timeValue += ((seconds < 10) ? ":0" : ":") + seconds
        document.getElementById("jam").innerHTML = " " + timeValue;
        setTimeout("jam()", 1000);
    }
</script>