<script type="text/javascript">
	var base_url = '<?php echo base_url();?>';
	var current_active = '<?php echo $this->app->getActive();?>';
</script>

<?php if ($this->router->fetch_class()=="produksi"): ?>
	<script type="text/javascript">
		var file02 = '<?php echo $file02?>';
		var file03 = '<?php echo $file03?>';
	</script>
<?php endif ?>
 <!-- All Jquery -->
<script src="<?php echo base_url()?>assets/ela/js/lib/jquery/jquery.min.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="<?php echo base_url()?>assets/ela/js/lib/bootstrap/js/popper.min.js"></script>
<script src="<?php echo base_url()?>assets/ela/js/lib/bootstrap/js/bootstrap.min.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="<?php echo base_url()?>assets/ela/js/jquery.slimscroll.js"></script>
<!--Menu sidebar -->
<script src="<?php echo base_url()?>assets/ela/js/sidebarmenu.js"></script>
<!--stickey kit -->
<script src="<?php echo base_url()?>assets/ela/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>

<script src="<?php echo base_url()?>assets/ela/js/lib/owl-carousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url()?>assets/ela/js/lib/owl-carousel/owl.carousel-init.js"></script>

<script src="<?php echo base_url()?>assets/ela/js/custom.min.js"></script>

<script src="<?php echo base_url()?>assets/ela/js/lib/fastselect/fastselect.standalone.js"></script>
<script src="<?php echo base_url()?>assets/ela/js/lib/fastselect/fastselect.standalone.min.js"></script>


<!--Custom JavaScript -->
<script src="<?php echo base_url()?>assets/ela/js/lib/datatables/datatables.min.js"></script>
<script src="<?php echo base_url()?>assets/ela/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url()?>assets/ela/js/lib/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url()?>assets/ela/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url()?>assets/ela/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="<?php echo base_url()?>assets/ela/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="<?php echo base_url()?>assets/ela/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="<?php echo base_url()?>assets/ela/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url()?>assets/ela/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

<script src="<?php echo base_url(); ?>assets/ela/js/lib/select2/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/ela/js/lib/custom-file-input.js"></script>

<!-- InputMask -->
<script src="<?php echo base_url(); ?>assets/ela/js/lib/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url(); ?>assets/ela/js/lib/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url(); ?>assets/ela/js/lib/input-mask/jquery.inputmask.extensions.js"></script>

<!-- Mask Money -->
<script src="<?php echo base_url(); ?>assets/ela/js/lib/MaskMoney/jquery.maskMoney.min.js"></script>
<script src="<?php echo base_url(); ?>assets/ela/js/lib/MaskMoney/numeral.min.js"></script>

<script src="<?php echo base_url(); ?>assets/ela/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
<script src="<?php echo base_url(); ?>assets/ela/js/lib/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?php echo base_url(); ?>assets/ela/js/lib/html5-editor/bootstrap-wysihtml5.js"></script>
<script src="<?php echo base_url(); ?>assets/ela/js/lib/html5-editor/wysihtml5-init.js"></script>
    <!--Tracking Bar -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/ela/js/lib/filestyle/bootstrap-filestyle.min.js"> </script>

<script src="<?php echo base_url(); ?>assets/ela/js/lib/datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/ela/js/sweetalert2.js"></script>

<script src="<?php echo base_url(); ?>assets/ela/js/lib/calc/calc.js"></script>

<script src="<?php echo base_url(); ?>assets/ela/js/lib/highchart/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/ela/js/lib/highchart/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/ela/js/lib/highchart/export-data.js"></script>

<script src="<?php echo base_url(); ?>assets/ela/js/lib/toastr/toastr.min.js"></script>
<script src="<?php echo base_url(); ?>assets/ela/js/lib/toastr/toastr.init.js"></script>

<script src="<?php echo base_url(); ?>assets/avatar/js/index.js"></script>

<script type="text/x-template" id="msg-template" style="display: none">
    <tbody>
        <tr class="msg-wgt-message-list-header">
            <td rowspan="2"><img src="<?php echo  base_url('assets/chat/avatar.png') ?>"></td>
            <td class="name"></td>
            <td class="time"></td>
        </tr>
        <tr class="msg-wgt-message-list-body">
            <td colspan="2"></td>
        </tr>
        <tr class="msg-wgt-message-list-separator"><td colspan="3"></td></tr>
    </tbody>
</script>


<script type="text/javascript">
$(document).ready(function($) {
    var chatPosition = [
        false, // 1
        false, // 2
        false, // 3
        false, // 4
        false, // 5
        false, // 6
        false, // 7
        false, // 8
        false, // 9
        false // 10
    ];

    $('<audio id="chatAudio"><source src='+base_url+'"/assets/ela/notifikasi.ogg" type="audio/ogg"><source src="'+base_url+'"/assets/ela/notifikasi.mp3" type="audio/mpeg"><source src="'+base_url+'"/assets/ela/notifikasi.wav" type="audio/wav"></audio>').appendTo('body');

    // New chat
    $(document).on('click', 'a[data-friend]', function(e) {
        var $data = $(this).data();
        if ($data.friend !== undefined && chatPosition.indexOf($data.friend) < 0) {
            var posRight = 0;
            var position;
            for(var i in chatPosition) {
                if (chatPosition[i] == false) {
                    posRight = (i * 270) + 20;
                    chatPosition[i] = $data.friend;
                    position = i;
                    break;
                }
            }
            var tpl = $('#wgt-container-template').html();
            var tplBody = $('<div/>').append(tpl);
            tplBody.find('.msg-wgt-container').addClass('msg-wgt-active');
            tplBody.find('.msg-wgt-container').css('right', posRight + 'px');
            tplBody.find('.msg-wgt-container').attr('data-chat-position', position);
            tplBody.find('.msg-wgt-container').attr('data-chat-with', $data.friend);
            $('body').append(tplBody.html());
            initializeChat();
        }
    });

    // Minimize Maximize
    $(document).on('click', '.msg-wgt-header > a.name', function() {
        var parent = $(this).parent().parent();
        if (parent.hasClass('minimize')) {
            parent.removeClass('minimize')
        } else {
            parent.addClass('minimize');
        }
    });

    // Close
    $(document).on('click', '.msg-wgt-header > a.close', function() {
        var parent = $(this).parent().parent();
        var $data = parent.data();
        parent.remove();
        chatPosition[$data.chatPosition] = false;
        setTimeout(function() {
            initializeChat();
        }, 1000)
    });

    var chatInterval = [];

    var initializeChat = function() {
        $.each(chatInterval, function(index, val) {
            clearInterval(chatInterval[index]);   
        });

        $('.msg-wgt-active').each(function(index, el) {
            var $data = $(this).data();
            var $that = $(this);
            var $container = $that.find('.msg-wgt-message-container');

            chatInterval.push(setInterval(function() {

                var oldscrollHeight = $container[0].scrollHeight;
                var oldLength = 0;
                $.post('<?php echo site_url('chat/getChats') ?>', {chatWith: $data.chatWith}, function(data, textStatus, xhr) {
                    $that.find('a.name').text(data.name);
                    // from last
                    var chatLength = data.chats.length;
                    var newIndex = data.chats.length;
                    $.each(data.chats, function(index, el) {
                        newIndex--;
                        var val = data.chats[newIndex];

                        var tpl = $('#msg-template').html();
                        var tplBody = $('<div/>').append(tpl);
                        var id = (val.chat_id +'_'+ val.send_by +'_'+ val.send_to).toString();
                        

                        if ($that.find('#'+ id).length == 0) {
                            tplBody.find('tbody').attr('id', id); // set class
                            tplBody.find('td.name').text(val.nama); // set name
                            tplBody.find('td.time').text(val.time); // set time
                            tplBody.find('.msg-wgt-message-list-body > td').html(nl2br(val.message)); // set message
                            $that.find('.msg-wgt-message-list').append(tplBody.html()); // append message

                            //Auto-scroll
                            var newscrollHeight = $container[0].scrollHeight - 20; //Scroll height after the request
                            if (newIndex === 0) {
                                $container.animate({ scrollTop: newscrollHeight }, 'normal'); //Autoscroll to bottom of div
                            }
                        }
                    });
                });
            }, 1000));

            $that.find('textarea').on('keydown', function(e) {
                var $textArea = $(this);
                if (e.keyCode === 13 && e.shiftKey === false) {
                    $.post('<?php echo site_url('chat/sendMessage') ?>', {message: $textArea.val(), chatWith: $data.chatWith}, function(data, textStatus, xhr) {
                    	$('#chatAudio')[0].play();
                    });
                    $textArea.val(''); // clear input

                    e.preventDefault(); // stop 
                    return false;
                }
            });
        });
    }
    var nl2br = function(str, is_xhtml) {
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>'; // Adjust comment to avoid issue on phpjs.org display
        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    }


    // on load
    initializeChat();
});
</script>



<script type="text/javascript">
	$(document).ready(function() {
		var cabang = $("#cabang").val();
	   	$.ajax({
	      	url: base_url+"produksi/capem/"+cabang,
	      	type: 'POST',
	      	success: function (data, textStatus, xhr) {
	      		$("#capem").html(data);
	      	}
	    });

		var cabang2 = $("#cabang2").val();
	    $.ajax({
	      	url: base_url+"produksi/capem/"+cabang2,
	      	type: 'POST',
	      	success: function (data, textStatus, xhr) {
	      		$("#capem2").html(data);
	      	}
	    });

	    var cabang3 = $("#cabang3").val();
	    $.ajax({
	      	url: base_url+"produksi/capem/"+cabang3,
	      	type: 'POST',
	      	success: function (data, textStatus, xhr) {
	      		$("#capem3").html(data);
	      	}
	    });

	    var cabang4 = $("#cabang4").val();
	    $.ajax({
	      	url: base_url+"produksi/capem/"+cabang4,
	      	type: 'POST',
	      	success: function (data, textStatus, xhr) {
	      		$("#capem4").html(data);
	      	}
	    });
			

		
		
		$('.js-example-basic-cabang').select2();
	});

	$("[data-mask]").inputmask();
	if(document.getElementsByClassName('price')!=null){
	    $('.price').maskMoney({prefix:'', thousands:'.', decimal:',', precision:0});
	}

	// $(":file").filestyle({input: false});
	
	$(".datepicker").datepicker({
	    format: 'dd-mm-yyyy',
	    autoclose: true,
	    todayHighlight: true,
	});



	$("#tgl_mulai").on('changeDate', function (selected) {
	    var startDate = new Date(selected.date.valueOf());
	    $("#tgl_akhir").datepicker('setStartDate', startDate);
	    if ($("#tgl_mulai").val() > $("#tgl_akhir").val()) {
	        $("#tgl_akhir").val($("#tgl_mulai").val());
	    }
	});

	

	// $("#cabang").addClass("col-xs-3 custom-select f-s-12");
	// 
	$("#cabang").change(function(){
		var cabang = $("#cabang").val();
		if(cabang==""){
			$("#capem").html("<option value=''>Semua</option>");
		}else{
		    $.ajax({
		      	url: base_url+"produksi/capem/"+cabang,
		      	type: 'POST',
		      	success: function (data, textStatus, xhr) {
		      		$("#capem").html(data);
		      	}
		    });
		}
	});

	$("#cabang2").change(function(){
		var cabang = $("#cabang2").val();
		if(cabang==""){
			$("#capem2").html("<option value=''>Semua</option>");
		}else{
		    $.ajax({
		      	url: base_url+"produksi/capem/"+cabang,
		      	type: 'POST',
		      	success: function (data, textStatus, xhr) {
		      		$("#capem2").html(data);
		      	}
		    });
		}
	});

	$("#cabang3").change(function(){
		var cabang = $("#cabang3").val();
		if(cabang==""){
			$("#capem3").html("<option value=''>Semua</option>");
		}else{
		    $.ajax({
		      	url: base_url+"produksi/capem/"+cabang,
		      	type: 'POST',
		      	success: function (data, textStatus, xhr) {
		      		$("#capem3").html(data);
		      	}
		    });
		}
	});

	$("#cabang4").change(function(){
		var cabang = $("#cabang4").val();
		if(cabang==""){
			$("#capem4").html("<option value=''>Semua</option>");
		}else{
		    $.ajax({
		      	url: base_url+"produksi/capem/"+cabang,
		      	type: 'POST',
		      	success: function (data, textStatus, xhr) {
		      		$("#capem4").html(data);
		      	}
		    });
		}
	});

	$("#changePassword").click(function(){
	    $.ajax({
	      	url: base_url+"dashboard/changepassword",
	      	type: 'POST',
	      	success: function (data, textStatus, xhr) {
	      		$("#changepass").modal();
	      		$("#dok").html(data);
	      	}
	    });
	});

	$("#saveChangePassword").click(function(){
	    $.ajax({
	      	url: base_url+"dashboard/updatePass",
	      	type: 'POST',
	      	success: function (data, textStatus, xhr) {
	      		alert(data);
	      	}
	    });
	});

	// $("#repass").keypress(function(){
	// 	var pass = $("#pass").val();
	// 	var repass = $("#repass").val();
	// 	alert(repass);
	// });
</script>

<?php echo $this->app->script(); ?>