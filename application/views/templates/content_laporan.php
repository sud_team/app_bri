<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="Laporan Virtual Insurance Application System">
    <meta content="" name="Agra Indonesia">
    <!-- Favicon icon -->
  
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url()?>assets/ela/icons/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url()?>assets/ela/icons/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url()?>assets/ela/icons/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url()?>assets/ela/icons/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url()?>assets/ela/icons/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url()?>assets/ela/icons/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url()?>assets/ela/icons/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url()?>assets/ela/icons/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url()?>assets/ela/icons/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url()?>assets/ela/icons/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url()?>assets/ela/icons/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url()?>assets/ela/icons/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>assets/ela/icons/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php echo base_url()?>assets/ela/icons/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#00897b">
<meta name="msapplication-TileImage" content="<?php echo base_url()?>assets/ela/icons/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#00897b">
  

    <title><?php echo $this->app->getTitle();?></title>
    <!-- Bootstrap Core CSS -->

    <?php $this->load->view('templates/_css'); ?>

    
</head>

<body class="fix-header fix-sidebar">
    <div class="preloader">
        <svg class="circular" viewbox="25 25 50 50">
            <circle class="path" cx="50" cy="50" fill="none" r="20" stroke-miterlimit="10" stroke-width="2"></circle>
        </svg>
    </div>
    <div id="main-wrapper">
      
                <?php echo $this->app->GetLayout(); ?>
            
        <?php $this->load->view('templates/footer'); ?>
    </div>

    <?php $this->load->view('templates/_js'); ?>
</body>

</html>