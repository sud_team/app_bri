<?php 
     // header("Cache-Control: private, max-age=10800, pre-check=10800");
     // header("Pragma: private");
     // header("Expires: " . date(DATE_RFC822,strtotime("+1 day")));
     // 
    header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="Virtual Insurance Application System">
    <meta content="" name="Agra Indonesia">
    <!-- Favicon icon -->

<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url()?>assets/ela/icons/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url()?>assets/ela/icons/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url()?>assets/ela/icons/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url()?>assets/ela/icons/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url()?>assets/ela/icons/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url()?>assets/ela/icons/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url()?>assets/ela/icons/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url()?>assets/ela/icons/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url()?>assets/ela/icons/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url()?>assets/ela/icons/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url()?>assets/ela/icons/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url()?>assets/ela/icons/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>assets/ela/icons/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php echo base_url()?>assets/ela/icons/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#00897b">
<meta name="msapplication-TileImage" content="<?php echo base_url()?>assets/ela/icons/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#00897b">
<link rel="icon" type="image/x-icon" href="<?php echo base_url()?>assets/ela/icons/favicon/favicon.ico">

    <!-- <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>assets/ela/icons/favicon/favicon.ico"> -->
    <title> <?php echo $this->app->getTitle();?></title>
    <!-- Bootstrap Core CSS -->

    <?php $this->load->view('templates/_css'); ?>

</head>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <!-- <div class="preloader">
        <svg class="circular" viewbox="25 25 50 50">
            <circle class="path" cx="50" cy="50" fill="none" r="20" stroke-miterlimit="10" stroke-width="2"></circle>
        </svg>
    </div> -->
    <div id="main-wrapper">
        <?php $this->load->view('templates/header'); ?>
        <!-- Left Sidebar  -->
        <?php $this->load->view('templates/sidebar'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles bg-megna">
                <!-- <div class="col-md-5 align-self-center">
                    <li class=" "><?php echo strtoupper($this->router->fetch_class());?></li>
                </div> -->
                <div class="col-md-12 float-right">
                    <?php echo $this->app->generateBreadcrumb(); ?>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
                <?php $success = $this->session->flashdata('success');?>
                <?php if ($success):?>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="alert alert-success  alert-dismissable">
                                <h6><i class="fa fa-check"></i> <?php echo $this->session->flashdata('success'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif?>
                <?php $warning = $this->session->flashdata('warning');?>
                <?php if ($warning):?>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="alert alert-warning  alert-dismissable">
                                    <h5><i class="fa fa-ban"></i> <?php echo $this->session->flashdata('warning'); ?></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php endif?>
                <?php $danger = $this->session->flashdata('danger');?>
                <?php if ($danger):?>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="alert alert-danger  alert-dismissable">
                                    <i class="fa fa-ban"></i>
                                    <small><?php echo $this->session->flashdata('danger'); ?></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif?>
                <?php echo $this->app->GetLayout(); ?>
            </div>
            <!-- End PAge Content -->
        </div>
        <!-- End Container fluid  -->
        <!-- footer -->

<div class="modal fade" id="changepass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="<?php echo base_url()?>dashboard/updatePass" method="post" enctype="multipart/form-data">
      <div class="modal-content">
          <div class="modal-header">
              <h6 class="modal-title font-weight-bold" id="exampleModalLabel">Ganti Password</h6>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body f-s-12">
              <div class="col-xs-4" id="dok">
                          
              </div>
          </div>
          <div class="modal-footer ">
              <button type="button" class="btn btn-secondary btn-sm " data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-info btn-sm " value="Simpan" style="color: #ffffff">
          </div>
      </div>
      </form>
  </div>
</div>

        <!-- TEMPLATE -->
<div id="wgt-container-template" style="display: none">
    <div class="msg-wgt-container">
        <div class="msg-wgt-header">
            <a href="javascript:;" class="online"></a>
            <a href="javascript:;" class="name"></a>
            <a href="javascript:;" class="close">x</a>
        </div>
        <div class="msg-wgt-message-container">
            <table width="100%" class="msg-wgt-message-list">
            </table>
        </div>
        <div class="msg-wgt-message-form">
            <textarea name="message" placeholder="Ketik pesan anda. Tekan Shift + Enter untuk alinea baru"></textarea>
        </div>
    </div>
</div>
        <?php $this->load->view('templates/footer'); ?>
        <!-- End footer -->
    </div>
    <!-- End Page wrapper  -->
    <!-- End Wrapper -->
    <?php $this->load->view('templates/_js'); ?>
    <?php // $this->load->view('messages'); ?>
</body>

</html>