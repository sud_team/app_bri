<!DOCTYPE html>
<html lang="en">

<head>
      <title>VIA Demo</title>
      <meta charset="UTF-8">
      <meta content="width=device-width, initial-scale=1" name="viewport">
      <meta content="IE=edge" http-equiv="X-UA-Compatible">
      <link href="images/icons/favicon.ico" rel="icon" type="image/png">
      <link href="icons/material-design-iconic-font/css/material-design-iconic-font.min.css" rel="stylesheet" type="text/css">
   

      <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>assets/ela/favicon.ico">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/ela/css/lib/login/main.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/ela/css/lib/login/util.css">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/ela/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/ela/css/lib/datepicker/datarangepicker.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/ela/css/lib/datepicker/bootstrap-datepicker3.min.css">

    <link href="<?php echo base_url()?>assets/ela/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
    
</head>

<body>
      <div class="limiter ">
            <div class="container-login100">
                  <div class="wrap-login100 animated fadeIn">
                            <form id="login_form" class="login100-form validate-form" action="<?php echo base_url();?>login/updatePassword" method="post" name="loginform">

                              <div class="text-center "><img alt="homepage" class=" logo-login p-b-10" src="<?php echo base_url()?>assets/ela/images/logorep.png"></div><span
                                    class="login100-form-title p-b-21">
                                    <font color="red">V</font>irtual <font color="red">I</font>nsurance <font color="red">A</font>pplication<br>
                                    <font color="grey">Demo System</font>
                              </span>
                              <fieldset>
                                <?php $success_message = $this->session->flashdata('warning');?>
                                     <?php if ($success_message):?>
                                        <div class="alert alert-danger  alert-dismissable">
                                            <i class="fa fa-ban"></i>
                                            <small style="color: #ffffff;"><?php echo $this->session->flashdata('warning'); ?></small>
                                        </div>
                                <?php endif?>
                                <?php $error_message = $this->session->flashdata('login-salah');?>
                                     <?php if ($error_message):?>
                                        <div class="login-salah">
                                            <i class="fa fa-ban"></i>
                                            <small><?php echo $this->session->flashdata('login-salah'); ?></small>
                                        </div>
                                <?php endif?>
                            
                                    <div class="wrap-input100 validate-input" data-validate="">
                                    <input class="input100" name="idUser" type="hidden"  value="<?php echo $this->session->userdata('idUserPass')?>"  >
                                    <input class="input100" id="user_username" name="password" type="password" autocomplete="off" >
                                    <span class="focus-input100" data-placeholder="Password Baru"></span>
                                    </div>
                                    <div class="wrap-input100 validate-input" data-validate="">
                                    <input class="input100" type="password" id="retry" class="form-control" name="password" autocomplete="off"  >
                                    <span class="focus-input100" data-placeholder="Ulangi Password"></span>
                                    <span id="gksama"></span>
                                    </div>
                                <div class="container-login100-form-btn">
                                    <div class="wrap-login100-form-btn">
                                          <div class="login100-form-bgbtn"></div><button class="login100-form-btn" name="button" type="submit" id="konfirmasi" value="Konfirmasi">Login</button>
                                    </div>
                              </div>                     
                        
                              <div class="text-center p-t-95">
                                    <span class="txt2">Lupa password?</span> <a class="txt2" href="#">Reset</a>
                              </div>
                              <div class="text-center">
                                    <span class="txt1 text-center">Copyright © 2018 Sakti Unggul Data PT,<br>
                                          All rights reserved.</span>
                              </div>
                              </fieldset>

                        </form>
                  </div>
            </div>
      </div>
      <div id="dropDownSelect1"></div>
      <!--===============================================================================================-->
      <script src="<?php echo base_url()?>assets/ela/js/jquery-1.11.1.min.js"></script>
      <!--===============================================================================================-->

      <script src="<?php echo base_url()?>assets/ela/js/custom.min.js"></script>
      <!--===============================================================================================-->

      <script src="<?php echo base_url()?>assets/ela/js/lib/bootstrap/js/popper.min.js"></script>
      <script src="<?php echo base_url()?>assets/ela/js/lib/bootstrap/js/bootstrap.min.js"></script>
      <!--===============================================================================================-->


      <script src="<?php echo base_url()?>assets/ela/js/lib/login/main.js"></script>


         <script src="<?php echo base_url()?>assets/ela/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url()?>assets/ela/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url()?>assets/ela/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url()?>assets/ela/js/lib/login/main.js"></script>
    <!--stickey kit -->

    <script src="<?php echo base_url()?>assets/ela/js/custom.min.js"></script>
</body>

</html>