<!DOCTYPE html>
<html lang="en">

<head>
      <title>App BRI</title>
      <meta charset="UTF-8">
      <meta content="width=device-width, initial-scale=1" name="viewport">
      <meta content="IE=edge" http-equiv="X-UA-Compatible">
      <link href="images/icons/favicon.ico" rel="icon" type="image/png">
      <!-- <link href="<?php echo base_url() ?>assets/ela/icons/material-design-iconic-font/css/materialdesignicons.min.css" rel="stylesheet" type="text/css"> -->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">



      <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url() ?>assets/ela/icons/favicon/apple-icon-57x57.png">
      <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url() ?>assets/ela/icons/favicon/apple-icon-60x60.png">
      <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url() ?>assets/ela/icons/favicon/apple-icon-72x72.png">
      <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>assets/ela/icons/favicon/apple-icon-76x76.png">
      <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url() ?>assets/ela/icons/favicon/apple-icon-114x114.png">
      <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url() ?>assets/ela/icons/favicon/apple-icon-120x120.png">
      <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url() ?>assets/ela/icons/favicon/apple-icon-144x144.png">
      <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url() ?>assets/ela/icons/favicon/apple-icon-152x152.png">
      <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url() ?>assets/ela/icons/favicon/apple-icon-180x180.png">
      <link rel="icon" type="image/png" sizes="192x192" href="<?php echo base_url() ?>assets/ela/icons/favicon/android-icon-192x192.png">
      <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url() ?>assets/ela/icons/favicon/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url() ?>assets/ela/icons/favicon/favicon-96x96.png">
      <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>assets/ela/icons/favicon/favicon-16x16.png">
      <link rel="icon" type="image/x-icon" href="<?php echo base_url() ?>assets/ela/icons/favicon/favicon.ico">

      <link rel="manifest" href="<?php echo base_url() ?>assets/ela/icons/favicon/manifest.json">
      <meta name="msapplication-TileColor" content="#0A539C">
      <meta name="msapplication-TileImage" content="<?php echo base_url() ?>assets/ela/icons/favicon/ms-icon-144x144.png">
      <meta name="theme-color" content="#0A539C">

      <link href="<?php echo base_url(); ?>assets/ela/css/style.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/ela/css/animate.css" rel="stylesheet">

      <link rel="stylesheet" href="<?php echo base_url() ?>assets/ela/css/lib/login/main.css">
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/ela/css/lib/login/util.css">

      <link rel="stylesheet" href="<?php echo base_url() ?>assets/ela/css/animate.css">
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/ela/css/lib/datepicker/datarangepicker.css">
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/ela/css/lib/datepicker/bootstrap-datepicker3.min.css">

      <link href="<?php echo base_url() ?>assets/ela/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">

</head>

<body>
      <div class="limiter ">
            <div class="container-login100">
                  <div class="wrap-login100 animated fadeIn">
                        <form id="login_form" class="login100-form validate-form" action="<?php echo base_url(); ?>login/auth" method="post" name="loginform">

                              <div class="text-center"><a href="https://bri.co.id/" target="_blank"><img alt="homepage" class="logo-login p-t-10" src="<?php echo base_url() ?>assets/ela/images/logorep.png"></a></div><span class="login100-form-title">
                                    <span class="fs-12 text-grey">System Asuransi Kredit</span>

                              </span>
                              <fieldset>
                                    <?php $success_message = $this->session->flashdata('success'); ?>
                                    <?php if ($success_message) : ?>
                                         <div class="text-center text-danger animated flash">
                                                <span><?php echo $this->session->flashdata('success'); ?></span></div>
                                     <?php endif ?>
                                    <?php $error_message = $this->session->flashdata('warning'); ?>
                                    <?php if ($error_message) : ?>
                                    <div class="text-center text-danger animated flash">

                                                <span><?php echo $this->session->flashdata('warning'); ?></span></div>
                                     <?php endif ?>

                                    <div class="wrap-input100 validate-input" data-validate="Valid email is: a@b.c">
                                          <input class="input100" name="username" type="text" id="user_username">
                                          <span class="focus-input100" data-placeholder="Username"></span>
                                    </div>
                                    <div class="wrap-input100 validate-input" data-validate="Enter password">
                                          <input class="input100" name="password" type="password" id="user_password">
                                          <span class="focus-input100" data-placeholder="Password"></span>
                                    </div>
                                    <div class="container-login100-form-btn">
                                          <div class="wrap-login100-form-btn">
                                                <div class="login100-form-bgbtn"></div><button class="login100-form-btn" name="button" type="submit" value="Login">Login</button>
                                          </div>
                                    </div>

                                    <div class="text-center p-t-95">
                                          <span class="txt2">Lupa password?</span> <a class="txt2" href="#">Reset</a>
                                    </div>
                                    <div class="text-center"><a href="http://agraindonesia.com/" target="_blank"><img alt="homepage" style="width:120px" class="logo-login p-t-10" src="<?php echo base_url() ?>assets/ela/images/Logo-Agra.png"></a>

                                    </div>
                              </fieldset>

                        </form>
                  </div>
            </div>
      </div>
      <div id="dropDownSelect1"></div>
      <!--===============================================================================================-->
      <script src="js/jquery-1.11.1.min.js"></script>
      <!--===============================================================================================-->

      <script src="js/custom.min.js"></script>
      <!--===============================================================================================-->

      <script src="js/lib/bootstrap/js/popper.min.js"></script>
      <script src="js/lib/bootstrap/js/bootstrap.min.js"></script>
      <!--===============================================================================================-->

      <script src="vendor/select2/select2.min.js"></script>
      <!--===============================================================================================-->
      <script src="js/lib/datepicker/daterangepicker.min.js"></script>
      <!--===============================================================================================-->

      <script src="vendor/countdowntime/countdowntime.js"></script>
      <!--===============================================================================================-->

      <script src="js/lib/login/main.js"></script>


      <script src="<?php echo base_url() ?>assets/ela/js/lib/jquery/jquery.min.js"></script>
      <!-- Bootstrap tether Core JavaScript -->
      <script src="<?php echo base_url() ?>assets/ela/js/lib/bootstrap/js/popper.min.js"></script>
      <script src="<?php echo base_url() ?>assets/ela/js/lib/bootstrap/js/bootstrap.min.js"></script>
      <!-- slimscrollbar scrollbar JavaScript -->
      <script src="<?php echo base_url() ?>assets/ela/js/lib/login/main.js"></script>
      <!--stickey kit -->

      <script src="<?php echo base_url() ?>assets/ela/js/custom.min.js"></script>
</body>

</html>