$(document).ready(function() {
	$("#calc").click(function() {
		var credit = $("#credit").val();
		var period = $("#period").val(); // количество платежных периодов, месяцев
		var interestRate = $("#interest").val() / 100; //процент по займу

		if (credit > 0 && period > 0 && interestRate > 0) {
			var payment; //ежемесячный платеж
			var paymentDecimal;
			payment = (interestRate / 12 * credit) / (1 - Math.pow((1 + interestRate / 12),(-period)));
		//	payment = Math.floor(payment);
			$("#summ").text(credit);
			$("#payment").text(payment.toFixed(2));
			$("#totalSumm").text((payment * period).toFixed(2));
		} else {
			alert("Введите числа больше ноля");
		}
	});

});