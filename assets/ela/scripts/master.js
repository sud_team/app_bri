$(document).ready(function() {
    $('#myTable').DataTable();
    $('#DataPeserta').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        responsive: true
    });
    $(document).ready(function() {
        var table = $('#example').DataTable({
            "columnDefs": [{
                "visible": false,
                "targets": 2
            }],
            "order": [
                [2, 'asc']
            ],
            "displayLength": 25,
            "drawCallback": function(settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;
                api.column(2, {
                    page: 'current'
                }).data().each(function(group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                        last = group;
                    }
                });
            }
        });
        // Order by the grouping
        $('#example tbody').on('click', 'tr.group', function() {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                table.order([2, 'desc']).draw();
            } else {
                table.order([2, 'asc']).draw();
            }
        });
    });
    $('.js-example-basic-cabang').select2();
    $('.select2').select2();
    $('#user').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "scrollX": true,
        "pageLength": 10,
        "destroy": true,
        "responsive": true,
        "ajax": {
            "url": base_url + "master/getUser",
            "type": "POST",
            "dataType": "json"
        }
    });
});

$("#cari").click(function() {
    var abc = $('#cabang').val();
    // alert(abc);
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "scrollX": true,
        "pageLength": 10,
        "destroy": true,
        "responsive": true,
        "ajax": {
            "url": base_url + "master/getBank/" + abc,
            "type": "POST",
            "dataType": "json"
        }
    });
});

function updatePass(argument) {
    $.ajax({
        url: base_url + "master/updatePass",
        type: 'POST',
        data: 'idUser=' + argument,
        success: function(data, textStatus, xhr) {
            $('#reset').modal();
            $('#dok').html(data);
        }
    });

}

function unlockPass(argument) {
    $.ajax({
        url: base_url + "master/unlockPass",
        type: 'POST',
        data: 'idUser=' + argument,
        success: function(data, textStatus, xhr) {
            alert(data);
        }
    });

}

function editAsuransi(argument) {
    $.ajax({
        url: base_url + "master/editAsuransi",
        type: 'POST',
        data: 'kodeAngka=' + argument,
        success: function(data, textStatus, xhr) {
            $("#asuransi").modal();
            $("#dok").html(data);
        }
    });

}


function editRate(argument) {
    $.ajax({
        url: base_url + "master/editRate",
        type: 'POST',
        data: 'kodePremi=' + argument,
        success: function(data, textStatus, xhr) {
            $("#ratepremi").modal();
            $("#dok").html(data);
        }
    });

}