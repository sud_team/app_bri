$(document).ready(function() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();

	if(dd<10) {
	    dd = '0'+dd
	} 

	if(mm<10) {
	    mm = '0'+mm
	} 

	today = yyyy+mm+dd;

	$('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "pagingType": "full_numbers"
    });

    $('#upload').DataTable({
        dom: 'Bfrtip',
        buttons: [
	        {
		    	extend: 'csv',
		    	text: 'Export Detail Upload',
		    	title: 'Detail Upload',
		    	fieldSeparator: '|',
		    	fieldBoundary: '',
		    	header: false
		    }
        ],
        "pagingType": "full_numbers"
    });

    $('#uat02').DataTable({
        dom: 'Bfrtip',
        buttons: [
	        {
		    	extend: 'csv',
		    	text: 'Export '+file02,
		    	title: file02,
		    	fieldSeparator: '|',
		    	fieldBoundary: '',
                header: false
		    }
        ],
        "pagingType": "full_numbers"
    });

    $('#uat03').DataTable({
        dom: 'Bfrtip',
        buttons: [
	        {
		    	extend: 'csv',
		    	text: 'Export '+file03,
		    	title: file03,
		    	fieldSeparator: '|',
		    	fieldBoundary: '',
		    	header: false
		    }
        ],
        "pagingType": "full_numbers"
    });

    $('#example233').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

    $('#rekonsel').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
		"pageLength": 50
    });

    $('#polis').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
		"pageLength": 50
    });

    $('#DataPrapen').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "responsive" : true,
        "scrollX": true,
        "pageLength" : 30,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"produksi/filter_prapen",
            "type"  : "POST",
            "dataType" : "json",
            "data": function(d) {
                var frm_data = $('#FormSubmit').serializeArray();
                    $.each(frm_data, function(key, val) {
                    d[val.name] = val.value;
                });
            }
        }
        
    });

    $('#DataBunga').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "responsive" : true,
        "scrollX": true,
        "pageLength" : 30,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"produksi/filter_bunga",
            "type"  : "POST",
            "dataType" : "json",
            "data": function(d) {
                var frm_data = $('#FormSubmit').serializeArray();
                    $.each(frm_data, function(key, val) {
                    d[val.name] = val.value;
                });
            }
        }
        
    });

    var NomorRegistrasi = $("#NomorRegistrasi").val();
    $('#PerhitunganBunga').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "responsive" : true,
        "scrollX": true,
        "pageLength" : 30,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"produksi/perhitungan_bunga/"+NomorRegistrasi,
            "type"  : "POST",
            "dataType" : "json"
        }
        
    });

    $('.js-example-basic-cabang').select2();
});

function cekModalDokKurang(argument) {
	$.ajax({
      	url: base_url+"produksi/melengkapiDokumen",
      	type: 'POST',
      	data: 'noreg='+argument,
      	success: function (data, textStatus, xhr) {
      		$('#exampleModal').modal();
      		$('#dok').html(data);
      	}
    });
}

$("#import").click(function(){
	var oke = confirm("Apakah anda sudah yakin?");
    if(oke){
        $.ajax({
          url: base_url+"produksi/file_upload",
          type: 'POST',
          success: function (data, textStatus, xhr) {
          	alert(data);
          }
        });
    }
});



$("#cari").click(function(){
	var cabang = $("#cabang").val();
	var capem = $("#capem").val();
	var produk = $("#produk").val();
	var nopin = $("#nopin").val();
	var nama = $("#nama").val();
	var tglawal = $("#tgl_mulai").val();
	var tglakhir = $("#tgl_akhir").val();
	var link = $("#link").val();
	$('#DataPeserta').DataTable({
    	dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "responsive" : true,
        "scrollX": true,
        "pageLength" : 30,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"produksi/"+link,
            "type"  : "POST",
            "dataType" : "json",
            "data": function(d) {
                var frm_data = $('#FormSubmit').serializeArray();
                    $.each(frm_data, function(key, val) {
                    d[val.name] = val.value;
                });
            }
        }
        
    });
});

$("#hitung").click(function(){
    if($("#bunga").val()==" "){
        alert("Interest Harus Tidak Boleh Kosong");
    }else{        
        $('#PerhitunganBunga').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "responsive" : true,
            "scrollX": true,
            "pageLength" : 30,
            "destroy" : true,
            "ajax": {
                "url"   : base_url+"produksi/hitungBunga",
                "type"  : "POST",
                "dataType" : "json",
                "data": function(d) {
                    var frm_data = $('#FormSubmit').serializeArray();
                        $.each(frm_data, function(key, val) {
                        d[val.name] = val.value;
                    });
                }
            }  
        });
    }
});

$("#simpan").click(function(){
    if($("#bunga").val()==" "){
        alert("Interest Harus Tidak Boleh Kosong");
    }else{
        var oke = confirm("Apakah Anda Yakin?");
        if(oke){
            $('#PerhitunganBunga').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "responsive" : true,
                "scrollX": true,
                "pageLength" : 30,
                "destroy" : true,
                "ajax": {
                    "url"   : base_url+"produksi/simpanHitungBunga",
                    "type"  : "POST",
                    "dataType" : "json",
                    "data": function(d) {
                        var frm_data = $('#FormSubmit').serializeArray();
                            $.each(frm_data, function(key, val) {
                            d[val.name] = val.value;
                        });
                    }
                }
            });
            $("#bunga").setAttribute("readonly", true);
        }
    }
});

$("#cariBunga").click(function(){
    var cabang = $("#cabang").val();
    var capem = $("#capem").val();
    var produk = $("#produk").val();
    var nopin = $("#nopin").val();
    var nama = $("#nama").val();
    var tglawal = $("#tgl_mulai").val();
    var tglakhir = $("#tgl_akhir").val();
    var link = $("#link").val();
    $('#DataBunga').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "responsive" : true,
        "scrollX": true,
        "pageLength" : 30,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"produksi/filter_bunga",
            "type"  : "POST",
            "dataType" : "json",
            "data": function(d) {
                var frm_data = $('#FormSubmit').serializeArray();
                    $.each(frm_data, function(key, val) {
                    d[val.name] = val.value;
                });
            }
        }
        
    });
});

$("#cariPrapen").click(function(){
    var cabang = $("#cabang").val();
    var capem = $("#capem").val();
    var produk = $("#produk").val();
    var nopin = $("#nopin").val();
    var nama = $("#nama").val();
    var tglawal = $("#tgl_mulai").val();
    var tglakhir = $("#tgl_akhir").val();
    var link = $("#link").val();
    $('#DataPrapen').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "responsive" : true,
        "scrollX": true,
        "pageLength" : 30,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"produksi/"+link,
            "type"  : "POST",
            "dataType" : "json",
            "data": function(d) {
                var frm_data = $('#FormSubmit').serializeArray();
                    $.each(frm_data, function(key, val) {
                    d[val.name] = val.value;
                });
            }
        }
        
    });
});


$(function() {
  $('#uploadedImagePopup').modal({
    show: true,
    keyboard: false,
    backdrop: 'static'
  });
});

$("#chkSelectAll").on('click', function(){
    this.checked ? $(".chkDel").prop("checked",true) : $(".chkDel").prop("checked",false);  
});

var countChecked = function() {
var n = $( "input:checked" ).length;
  $( "#jmldebitur" ).text( n + " Debitur" );
};
countChecked();

var countChecked2 = function() {
var n = $( "input:checked" ).length-1;
  $( "#jmldebitur" ).text( n + " Debitur" );
};
countChecked2();
 
$( ".chkDel" ).on( "click", countChecked );
$("#chkSelectAll").on( "click", countChecked2 );

$("#validasi").click(function(){
    var link = $("#link").val();
    if(link=="filter_rekonsel"){
        link = "validasiRekonsel";
    }else{
        link = "validasiRekonselBj";
    }
	if($(".chkDel").is(":checked")){
		var oke = confirm("Apakah Anda Yakin?");
		if(oke){
			i = 0;
		   	var arr = [];
		   	$('.chkDel:checked').each(function () {
		        arr[i++] = $(this).val();
		    });
		    $.ajax({
		      	url: base_url+"produksi/"+link,
		      	type: 'POST',
		      	data: 'NomorPinjaman='+arr,
		      	success: function (data, textStatus, xhr) {
		      		alert(data);
		      		window.location.reload();
		      	}
		    });
	    }
	}else{
		alert("tidak di cek");
	}
});



function terbitkan(argument) {
	var tglpolis = $("#tglpolis").val();
	var nopolis = $("#nopolis").val();
	if(tglpolis==" " && nopolis==" "){
		alert("Nomor Polis dan Tgl Polis Harus Terisi");
	}else{
		var oke = confirm("Apakah Anda Yakin?");
		if(oke){
			$.ajax({
		      	url: base_url+"produksi/terbitkanPolis",
		      	type: 'POST',
		      	data: 'nopolis='+nopolis+"&tglpolis="+tglpolis+"&NomorRegistrasi="+argument,
		      	success: function (data, textStatus, xhr) {
		      		alert(data);
		      		window.location.reload();
		      	}
		    });
		}
	}
}

function Input(argument) {
    var i = argument.split("-"); 
    var batas = $("#batas"+i[0]).val();
    var submit = $("#submit"+i[0]).val();
    if(batas==" " && submit==" "){
        alert("Nomor Polis dan Tgl Polis Harus Terisi");
    }else{
        var oke = confirm("Apakah Anda Yakin?");
        if(oke){
            $.ajax({
                url: base_url+"produksi/saveBatasPensiun",
                type: 'POST',
                data: 'batas='+batas+"&submit="+submit+"&noreg="+i[1],
                success: function (data, textStatus, xhr) {
                    alert(data);
                    window.location.reload();
                }
            });
        }
    }
}

function simpanAsdur(argument) {
    var asdur = $("#asdur").val();
    if(asdur==""){
        alert("Asuransi haris terisi !");
    }else{
        var oke = confirm("Apakah Anda Yakin?");
        if(oke){
            $.ajax({
                url: base_url+"produksi/simpanAsdur",
                type: 'POST',
                data: 'asdur='+asdur+'&noreg='+argument,
                success: function (data, textStatus, xhr) {
                    alert(data);
                    window.location.reload();
                }
            });
        }
    }
}



