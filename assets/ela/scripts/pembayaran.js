$(document).ready(function() {
	$('.js-example-basic-cabang').select2();
});

$("#cari").click(function(){
	var cabang = $("#cabang").val();
	var capem = $("#capem").val();
	var produk = $("#produk").val();
	var nopin = $("#nopin").val();
	var nama = $("#nama").val();
	var tglawal = $("#tgl_mulai").val();
	var tglakhir = $("#tgl_akhir").val();
	var link = $("#link").val();
    $('#debitur').DataTable({
    	dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "responsive" : true,
        "scrollX": true,
        "pageLength" : 10,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"pembayaran/"+link,
            "type"  : "POST",
            "dataType" : "json",
            "data": function(d) {
                var frm_data = $('#FormSubmit').serializeArray();
                    $.each(frm_data, function(key, val) {
                    d[val.name] = val.value;
                });
            }
        }
    });
});

function cekPembayaran(argument) {
	$.ajax({
      	url: base_url+"pembayaran/cekPembayaran/"+argument,
      	success: function (data, textStatus, xhr) {
  			$("#nomor").html(data);
  			$('#exampleModal').modal();
			$(".datepicker").datepicker({
			    format: 'dd-mm-yyyy',
			    autoclose: true,
			    todayHighlight: true,
			    maxDate:0
			});
      	}
    });
}

$("#chkSelectAll").on('click', function(){
    this.checked ? $(".chkDel").prop("checked",true) : $(".chkDel").prop("checked",false);  
});

var countChecked = function() {
var n = $(".chkDel").is(":checked").length;
  $( "#jmldebitur" ).text( n + " Debitur" );
};
countChecked();

// var countChecked2 = function() {
// var n = $( "input:checked" ).length-1;
//   $( "#jmldebitur" ).text( n + " Debitur" );
// };
// countChecked2();
 
$( ".chkDel" ).on( "click", countChecked );
// $("#chkSelectAll").on( "click", countChecked2 );

$("#validasi").click(function(){
	if($(".chkDel").is(":checked")){
		var oke = confirm("Apakah Anda Yakin?");
		if(oke){
			i = 0;
		   	var arr = [];
		   	$('.chkDel:checked').each(function () {
		        arr[i++] = $(this).val();
		    });
		    $.ajax({
		      	url: base_url+"produksi/validasiRekonsel",
		      	type: 'POST',
		      	data: 'NomorPinjaman='+arr,
		      	success: function (data, textStatus, xhr) {
		      		alert(data);
		      		window.location.reload();
		      	}
		    });
	    }
	}else{
		alert("tidak di cek");
	}
});

