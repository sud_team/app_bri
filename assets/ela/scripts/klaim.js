$(document).ready(function() {
  var link = $("#link").val();
	$('#debitur').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        responsive: true,
        "scrollX": true,
        "pageLength" : 10,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"klaim/"+link,
            "type"  : "POST",
            "dataType" : "json"
        }
    });
    $('.js-example-basic-cabang').select2();

    
});

$("#cari").click(function(){
  var cabang = $("#cabang").val();
  var capem = $("#capem").val();
  var produk = $("#produk").val();
  var nopin = $("#nopin").val();
  var nama = $("#nama").val();
  var tglawal = $("#tgl_mulai").val();
  var tglakhir = $("#tgl_akhir").val();
  var link = $("#link").val();
  // alert("oke");
  $('#debitur').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        responsive: true,
        "scrollX": true,
        "pageLength" : 10,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"klaim/"+link,
            "type"  : "POST",
            "dataType" : "json",
            "data": function(d) {
                var frm_data = $('#FormSubmit').serializeArray();
                    $.each(frm_data, function(key, val) {
                    d[val.name] = val.value;
                });
            }
        }
    });
});


$("#idResiko").change(function(){
	var id = $("#idResiko").val();
	$.ajax({
      url: base_url+"klaim/DokKhusus/"+id,
      type: 'POST',
      success: function (data, textStatus, xhr) {
      	$("#khusus").html(data);
      }
    });
});

