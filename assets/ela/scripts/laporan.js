$(document).ready(function() {	
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();

	if(dd<10) {
	    dd = '0'+dd
	} 

	if(mm<10) {
	    mm = '0'+mm
	} 

	today = yyyy+mm+dd;

	$('#debitur').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        responsive: true
    });

    $('#DataPeserta').DataTable({
        dom: 'Bfrtip',
        "buttons": [{
            extend: 'excel',
            exportOptions: {
            	format: {
	                body: function ( data, row, column, node ) {
	                    return column >= 12 && column <= 19 ?
	                        data.replace( /[.]/g, '' ) :
	                        data;
	                }
	            }
	        }
            // customizeData: function ( data ) {
                // for (var i=0; i<data.body.length; i++){
                //     for (var j=0; j<data.body[i].length; j++ ){
                //         data.replace( /[.,]/g, '' );                    
                //         data.body[i][j] = '\u200C' + data.body[i][j];
                //     }
                // }
            // }               
        }],
        responsive: true
    });

    $('#Perasuransi').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

    $('#DataPeserta2').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

    $('#rekap_produksi_tahunan').DataTable({
        "paging": false,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        columnDefs: [
		    { orderable: false, targets: 0 }
		]
    });

    $('#rekap_polis_bulanan').DataTable({
        "paging": false,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

    $('#uat04').DataTable({
        dom: 'Bfrtip',
        buttons: [
          {
          extend: 'csv',
          text: 'Export AGRA'+today+'-04',
          title: 'AGRA'+today+'-04',
          fieldSeparator: '|',
          fieldBoundary: '',
                header: false
        }
        ],
        "pagingType": "full_numbers"
    });

    $('#uat05').DataTable({
        dom: 'Bfrtip',
        buttons: [
          {
          extend: 'csv',
          text: 'Export AGRA'+today+'-05',
          title: 'AGRA'+today+'-05',
          fieldSeparator: '|',
          fieldBoundary: '',
                header: false
        }
        ],
        "pagingType": "full_numbers"
    });

    $('#DataPeserta3').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
 	
 	$('.js-example-basic-cabang').select2();

 	$(".filter").hide();

 	$('#tipe').val('1');
 	$('#tipepolis').val('1');
 	$('#tiperes').val('1');
 	$('#tipeklaim').val('1');

 	$(".datepicker2").datepicker({
	    format: 'mm-yyyy',
	    autoclose: true,
	    todayHighlight: true,
	});

 	$(".datepicker3").datepicker({
	    format: 'yyyy',
	    autoclose: true,
	    todayHighlight: true,
	});

	$('#invoice').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        responsive: true,
        "scrollX": true,
        "pageLength" : 10,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"laporan/dataInvoice",
            "type"  : "POST",
            "dataType" : "json"
        },
        columnDefs: [
		   		{ className: "center", targets: [ 0, 1, 2 ] },
		   		{ className: "dt-body-right", targets: [ 3 ] }
		]
    });
});

$("#tab1").click(function(){
	$("#jelas").html("<h6>Menampilkan laporan data produksi per debitur secara rinci</h6><p> Tentukan filter pencarian pada kolom dibawah</p>");
	$(".filter").show();
	$('#tipe').val('1');
	$("#filter_tgl").html("<div class='f-s-12 col-sm-4'>Tanggal Produksi</div><input id='tgl_mulai' type='text' class='col-xs-3 custom-select f-s-12 datepicker col-sm-3' name='tgl_awal' placeholder='dd/mm/yyyy'><div class='f-s-12 col-xs-2'>&nbsp;</div><input id='tgl_akhir' type='text' class='col-xs-3 custom-select f-s-12 datepicker col-sm-3' name='tgl_akhir' placeholder='dd/mm/yyyy'>");
	$(".datepicker").datepicker({
	    format: 'dd-mm-yyyy',
	    autoclose: true,
	    todayHighlight: true,
	});

	$("#tgl_mulai").on('changeDate', function (selected) {
	    var startDate = new Date(selected.date.valueOf());
	    $("#tgl_akhir").datepicker('setStartDate', startDate);
	    if ($("#tgl_mulai").val() > $("#tgl_akhir").val()) {
	        $("#tgl_akhir").val($("#tgl_mulai").val());
	    }
	});
});

$("#tabsumm1").click(function(){
	$('#tipe').val('1');
});
$("#tabsumm2").click(function(){
	$('#tipe').val('2');
});
$("#tabsumm3").click(function(){
	$('#tipe').val('3');
});

$("#tabpolis1").click(function(){
	$('#tipepolis').val('1');
});
$("#tabpolis2").click(function(){
	$('#tipepolis').val('2');
});
$("#tabpolis3").click(function(){
	$('#tipepolis').val('3');
});
$("#tabpolis4").click(function(){
	$('#tipepolis').val('4');
});
$("#tabpolis5").click(function(){
	$('#tipepolis').val('5');
});

$("#tablares1").click(function(){
	$('#tiperes').val('1');
});
$("#tablares2").click(function(){
	$('#tiperes').val('2');
});
$("#tablares3").click(function(){
	$('#tiperes').val('3');
});
$("#tablares4").click(function(){
	$('#tiperes').val('4');
});
$("#tablares5").click(function(){
	$('#tiperes').val('5');
});

$("#tablakla1").click(function(){
	$('#tipeklaim').val('1');
});
$("#tablakla2").click(function(){
	$('#tipeklaim').val('2');
});
$("#tablakla3").click(function(){
	$('#tipeklaim').val('3');
});
$("#tablakla4").click(function(){
	$('#tipeklaim').val('4');
});
$("#tablakla5").click(function(){
	$('#tipeklaim').val('5');
});

$("#tablapro1").click(function(){
	$('#tipe').val('1');
});
$("#tablapro2").click(function(){
	$('#tipe').val('2');
	$(".datepicker2").datepicker({
	    format: 'mm-yyyy',
	    autoclose: true,
	    todayHighlight: true,
	});
});
$("#tablapro3").click(function(){
	$('#tipe').val('3');
});
$("#tablapro4").click(function(){
	$('#tipe').val('4');
	$(".datepicker3").datepicker({
	    format: 'yyyy',
	    autoclose: true,
	    todayHighlight: true,
	});

});

$("#tab2").click(function(){
	$("#jelas").html("<h6>Menampilkan laporan rekap pertanggungan per asuransi dalam sebulan</h6><p> Tentukan filter pencarian pada kolom dibawah</p>");
	$(".filter").show();
	$("#filter_tgl").html("<div class='f-s-12 col-sm-4'>Pilih Bulan dan Tahun</div><input type='text' class='form-control-feedback custom-select f-s-12 col-sm-4 datepicker' placeholder='mm/yyyy' name='bulan'>");
	$('#tipe').val('2');
	$(".datepicker").datepicker({
	    format: 'mm-yyyy',
	    autoclose: true,
	    todayHighlight: true,
	});


});

$("#tab3").click(function(){
	$("#jelas").html("<h6>Laporan produksi asuransi per bank (Kantor Pusat, Wilayah Cabang dan Cabang Pembantu)</h6><p> Tentukan filter pencarian pada kolom dibawah</p>");
	$(".filter").show();
	$('#tipe').val('3');
	$("#filter_tgl").html("<div class='f-s-12 col-sm-4'>Tanggal Produksi</div><input id='tgl_mulai' type='text' class='col-xs-3 custom-select f-s-12 datepicker col-sm-3' name='tgl_awal' placeholder='dd/mm/yyyy'><div class='f-s-12 col-xs-2'>&nbsp;</div><input id='tgl_akhir' type='text' class='col-xs-3 custom-select f-s-12 datepicker col-sm-3' name='tgl_akhir' placeholder='dd/mm/yyyy'>");
	$(".datepicker").datepicker({
	    format: 'dd-mm-yyyy',
	    autoclose: true,
	    todayHighlight: true,
	});

	$("#tgl_mulai").on('changeDate', function (selected) {
	    var startDate = new Date(selected.date.valueOf());
	    $("#tgl_akhir").datepicker('setStartDate', startDate);
	    if ($("#tgl_mulai").val() > $("#tgl_akhir").val()) {
	        $("#tgl_akhir").val($("#tgl_mulai").val());
	    }
	});
});

$("#tab4").click(function(){
	$("#jelas").html("<h6>Laporan Produksi Asuransi dalam setahun</h6><p> Tentukan filter pencarian pada kolom dibawah</p>");
	$(".filter").show();
	$('#tipe').val('4');
	$("#filter_tgl").html("<div class='f-s-12 col-sm-4'>Pilih Tahun</div><input type='text' class='form-control-feedback custom-select f-s-12 col-sm-4 datepicker' placeholder='yyyy' name='tahun'>");
	$(".datepicker").datepicker({
	    format: 'yyyy',
	    autoclose: true,
	    todayHighlight: true,
	});
});

$("#cabang").change(function(){
	var cabang = $("#cabang").val();
	if(cabang==""){
		$("#capem").html("<option value=''>Semua</option>");
	}else{
	    $.ajax({
	      	url: base_url+"produksi/capem/"+cabang,
	      	type: 'POST',
	      	success: function (data, textStatus, xhr) {
	      		$("#capem").html(data);
	      	}
	    });
	}
});

$("#btn-cari").click(function(){
	var asuransi = $("#asuransi").val();
	var produk = $("#produk").val();
	var cabang = $("#cabang").val();
	var capem = $("#capem").val();
	var tgl_mulai = $("#tgl_mulai").val();
	var tgl_akhir = $("#tgl_akhir").val();
	var tipe = $("#tipe").val();
	$.ajax({
      	url: base_url+"laporan/cariProduksi",
      	type: 'POST',
      	data: 'asuransi='+asuransi+'&produk='+produk+'&cabang='+cabang+'&capem='+capem+'&tgl_mulai='+tgl_mulai+'&tgl_akhir='+tgl_akhir+'&tipe='+tipe,
      	success: function (data, textStatus, xhr) {
      	}
    });
});

$("#cari").click(function(){
	var tgl_mulai = $("#tgl_mulai").val();
	var tgl_akhir = $("#tgl_akhir").val();
	$('#DataAsuransi').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        responsive: true,
        "scrollX": true,
        "pageLength" : 10,
        "destroy" : true,
        "ajax": {
            "url"   : base_url+"laporan/lap_asuransi",
            "type"  : "POST",
            "dataType" : "json",
            "data": function(d) {
                var frm_data = $('#FormSubmit').serializeArray();
                    $.each(frm_data, function(key, val) {
                    d[val.name] = val.value;
                });
            }
        }
    });
});
