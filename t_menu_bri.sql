-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 25, 2019 at 04:05 AM
-- Server version: 5.5.60-MariaDB
-- PHP Version: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_askred_bri`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_menu_bri`
--

DROP TABLE IF EXISTS `t_menu_bri`;
CREATE TABLE IF NOT EXISTS `t_menu_bri` (
  `idMenu` int(11) NOT NULL,
  `menu` varchar(30) NOT NULL,
  `controller` varchar(50) NOT NULL,
  `urut` decimal(9,2) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `link` varchar(40) DEFAULT NULL,
  `icon` varchar(25) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `kategori` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_menu_bri`
--

INSERT INTO `t_menu_bri` (`idMenu`, `menu`, `controller`, `urut`, `status`, `link`, `icon`, `parent_id`, `kategori`) VALUES
(1, 'Dashboard', 'dashboard', '1.00', 1, 'dashboard', 'fa fa-tachometer', NULL, 'H'),
(2, 'Bank', 'dashboard', '2.00', 0, 'dashboard/bank', '', 1, ''),
(3, 'Asuransi', 'dashboard', '3.00', 0, 'dashboard/asuransi', '', 1, ''),
(4, 'Broker', 'dashboard', '4.00', 0, 'dashboard/broker', '', 1, ''),
(5, 'Produksi', 'produksi', '5.00', 1, '#', 'fa-cart-plus', NULL, 'A'),
(6, 'Upload Data', 'produksi', '6.00', 1, 'produksi/upload', '', 5, ''),
(7, 'Non AC', 'produksi', '7.00', 1, 'produksi/nonac', '', 5, ''),
(8, 'Rekonsel', 'produksi', '8.00', 1, 'produksi/rekonsel', '', 5, ''),
(9, 'Polis', 'produksi', '9.00', 1, 'produksi/polis', '', 5, ''),
(10, 'Cari Data Debitur', 'debitur', '11.00', 1, 'debitur/cari', 'fa-search', NULL, ''),
(11, 'Klaim', 'klaim', '12.00', 1, '#', 'fa-clipboard', NULL, ''),
(12, 'Menunggu Keputusan', 'klaim', '13.00', 1, 'klaim/menunggu', '', 11, 'K'),
(13, 'Ditangguhkan', 'klaim', '14.00', 1, 'klaim/ditangguhkan', '', 11, 'K'),
(14, 'Ditolak', 'klaim', '15.00', 1, 'klaim/ditolak', '', 11, 'K'),
(15, 'Disetujui', 'klaim', '16.00', 1, 'klaim/disetujui', '', 11, 'K'),
(16, 'Restitusi', 'restitusi', '17.00', 1, '#', 'fa-undo', NULL, ''),
(17, 'Menunggu Keputusan', 'klaim', '18.00', 1, 'restitusi/menunggu', '', 16, 'R'),
(18, 'Ditangguhkan', 'klaim', '19.00', 0, 'restitusi/ditangguhkan', '', 16, 'R'),
(19, 'Disetujui', 'klaim', '20.00', 1, 'restitusi/disetujui', '', 16, 'R'),
(20, 'Pembayaran', 'pembayaran', '21.00', 1, '#', 'fa-credit-card', NULL, ''),
(21, 'Internal Memo', 'pembayaran', '22.00', 1, 'pembayaran/internalmemo', '', 20, ''),
(22, 'Data Pembayaran', 'pembayaran', '23.00', 1, 'pembayaran/datapembayaran', '', 20, ''),
(23, 'Laporan', 'laporan', '24.00', 1, '#', 'fa-table', NULL, 'L'),
(24, 'Detail', 'laporan', '25.00', 1, 'laporan/detail', '', 23, ''),
(25, 'Summary', 'laporan', '26.00', 1, 'laporan/summary', '', 23, ''),
(26, 'Klaim', 'laporan', '27.00', 0, 'laporan/klaim', '', 23, ''),
(27, 'Restitusi', 'laporan', '28.00', 0, 'laporan/restitusi', '', 23, ''),
(28, 'Status Debitur', 'laporan', '29.00', 0, 'laporan/debitur', '', 23, ''),
(29, 'Pengaturan', 'master', '30.00', 1, '#', 'fa-cogs', NULL, 'E'),
(30, 'Asuransi & Premi', 'master', '31.00', 1, 'master/asuransi', '', 29, ''),
(31, 'Wilayah', 'master', '35.00', 0, 'master/wilayah', '', 29, ''),
(32, 'Bank', 'master', '32.00', 1, 'master/kcp', '', 29, ''),
(33, 'User', 'master', '34.00', 1, 'master/user', '', 29, ''),
(34, 'TC', 'master', '33.00', 1, 'master/tc', '', 29, ''),
(35, 'Produk', 'master', '36.00', 1, 'master/produk', '', 29, ''),
(36, 'Reset User', 'master', '37.00', 1, 'master/reset', '', 29, ''),
(37, 'Alokasi Asuradur', 'produksi', '7.10', 1, 'produksi/alokasi', '', 5, ''),
(38, 'Rekonsel BJ', 'master', '8.10', 1, 'produksi/rekonsel_bj', '', 5, ''),
(39, 'Produksi', 'laporan', '25.10', 1, 'laporan/produksi', '', 24, ''),
(40, 'Polis', 'laporan ', '25.20', 1, 'laporan/polis', '', 24, ''),
(41, 'Klaim', 'laporan', '25.30', 1, 'laporan/klaim', '', 24, ''),
(42, 'Restitusi', 'laporan', '25.40', 1, 'laporan/restitusi', '', 24, ''),
(43, 'Invoice', 'laporan', '24.10', 1, 'laporan/invoice', 'fa-money', NULL, ''),
(44, 'FTP', 'ftp', '5.10', 1, 'ftp', '', 5, ''),
(45, 'Laporan Asuransi', 'laporan', '25.50', 1, 'laporan/asuransi', 'NULL', 24, 'NULL'),
(46, 'Pra Pen', 'produksi', '6.10', 1, 'produksi/prapen', '', 5, ''),
(47, 'Penerapan Bunga', 'produksi', '6.20', 1, 'produksi/bunga', '', 5, 'NULL'),
(49, 'Set Prapen', 'setting', '30.10', 1, 'setting/setPrapen', '', 29, ''),
(50, 'Premi Prorate', 'setting', '30.20', 1, 'setting/setProrate', '', 29, ''),
(51, 'Pindah Asuransi', 'setting', '30.30', 1, 'setting/setPindahan', '', 29, '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
